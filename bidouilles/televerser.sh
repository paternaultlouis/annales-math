#!/bin/bash

set -x

# Déplacement à la racine du projet
cd "$(dirname $0)/.."

# Création de la page web::
./aspirateur html

# Création d'une archive, et publication::
cd public
rm annales-math.zip
zip -r annales-math.zip concours/ examens/ index.html  olympiades/ recrutement/ static/ concoursgénéral/
cp annales-math.zip ~/Bureau/nuage/annales-math.zip
