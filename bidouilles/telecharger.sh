#!/bin/bash

set -x

# Déplacement à la racine du projet
cd "$(dirname $0)/.."

# Nettoyage des liens morts
./aspirateur download clean

# Recherche de nouveaux fichiers::
./aspirateur scan

# Recherche des liens morts pour les nouveaux fichiers
# (juste pour écrire dans le cache que ça a été fait)
./aspirateur download clean

# Téléchargement des nouveaux fichiers::
./aspirateur download all
