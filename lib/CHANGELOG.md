* aspirateur 3.0.0 (2021-01-21)

    * Troisième départ : on casse tout et on recommence.

    -- Louis Paternault <spalax@gresille.org>

* aspirateur 0.1.0 (2017-12-20)

    * Le classement fonctionne (mais c'est compliqué, et long).

    -- Louis Paternault <spalax@gresille.org>
