# Copyright 2017-2023 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Télécharge les sujets (et leur source) depuis le site web de l'APMEP."""

import collections
import contextlib
import dataclasses
import datetime
import hashlib
import json
import logging
import random
import time

import requests
from bs4 import BeautifulSoup

from .. import CACHEDIR, URL_PREFIXES, ErreurTéléchargement, url_absolue
from ..scan import scan_pages

CIBLES = ("head", "fichier", "page")
DELAI = 1
DATES = CACHEDIR / "dates.json"
TODAY = datetime.date.today()
TOUJOURS = collections.defaultdict(
    set,  # Liste des cibles à toujours télécharger
    {
        "page": {
            "https://www.apmep.fr/-Annales-Bac-Brevet-BTS-",
            "https://www.apmep.fr/-Annales-examens-concours-",
            "https://www.apmep.fr/-Concours-de-recrutement-",
            "https://www.apmep.fr/-Olympiades-A-partir-de-2010-",
            "https://www.apmep.fr/-Olympiades-De-2001-a-2009-",
        }
    },
)

# pylint: disable=invalid-name, global-statement
_last_request_time = 0


class ErreurTropTôt(ErreurTéléchargement):
    """Il faut encore attendre avant de télécharger ce fichier.

    Fait pour éviter de télécharger les fichiers toutes les semaines
    alors qu'ils changent rarement.
    """

    def __init__(self, prochain):
        super().__init__(f"Trop tôt (prochain téléchargement le {prochain}).")


def md5sum(data: bytes):
    """Renvoie le code md5 de l'argument."""
    return hashlib.md5(data).hexdigest()


def _date2json(truc):
    if isinstance(truc, datetime.date):
        return truc.isoformat()
    return truc


def entre(a, x, b):
    """Renvoit le nombre x, en s'assurant qu'il est entre a et b.

    Si x est trop petit : renvoit a ;
    si x est trop grand : renvoit b.
    """
    return min(max(a, x), b)


class Téléchargeur(contextlib.AbstractContextManager):
    """Télécharge, en évitant de télécharger trop souvent.

    - Interdit le téléchargement moins d'une semaine après un précédent changement des données téléchargées
    - Plus il y a de temps entre le dernier essai et le dernier changement des données, plus on attend avant le prochain.
    - Un an après le dernier essai, il est à nouveau autorisé de télécharger.
    """

    def __init__(self):
        super().__init__()

        try:
            with open(DATES, encoding="utf8") as fileobj:
                jsondata = json.load(fileobj)
        except OSError:
            jsondata = {}

        self.dates = {}
        for cible in CIBLES:
            self.dates[cible] = collections.defaultdict(
                lambda: {
                    "changement": datetime.date.fromtimestamp(0),
                    "essai": datetime.date.fromtimestamp(0),
                    "checksum": "",
                }
            )
            for url, subdict in jsondata.get(cible, {}).items():
                self.dates[cible][url] = {
                    "changement": datetime.date.fromisoformat(subdict["changement"]),
                    "essai": datetime.date.fromisoformat(subdict["essai"]),
                    "checksum": subdict["checksum"],
                }

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        super().__exit__(exc_type, exc_value, traceback)
        if exc_type is None:
            CACHEDIR.mkdir(exist_ok=True)
            with open(DATES, mode="w", newline="\n", encoding="utf8") as fileobj:
                json.dump(
                    self.dates, fileobj, sort_keys=True, indent=4, default=_date2json
                )

    def prochain(self, cible, url):
        """Renvoit la date du prochain téléchargement autorisé.

        De l'aléa est utilisé pour éviter d'avoir beaucoup de fichiers téléchargés
        au même moment.
        """
        if url in TOUJOURS[cible]:
            return TODAY
        changement = self.dates[cible][url]["changement"]
        essai = self.dates[cible][url]["essai"]
        random.seed(url)
        return essai + datetime.timedelta(
            days=entre(
                7,
                (essai - changement).days * random.uniform(2, 3),
                5 * 365,
            )
        )

    def miseàjour(self, cible, url, data):
        """Vérifie si les données ont changé, et met à jour les informations en cache."""
        self.dates[cible][url]["essai"] = TODAY

        if cible == "head":
            checksum = md5sum(data.content)
        elif cible == "fichier":
            checksum = md5sum(data.content)
        elif cible == "page":
            checksum = md5sum(
                str(list(sorted(scan_pages(BeautifulSoup(data.text, "lxml"))))).encode()
            )
        else:
            logging.error("Erreur: L'argument 'cible' n'est pas du bon type…")
            return

        if checksum != self.dates[cible][url]["checksum"]:
            self.dates[cible][url]["checksum"] = checksum
            self.dates[cible][url]["changement"] = TODAY

    def télécharge_page(self, url, *, force=False, cache=True):
        """Télécharge la page web, et la retourne.

        Le type de l'objet retourné est le résultat de `request.get()`.

        :param str url: URL de la page à télécharger
        :param bool force: Force le téléchargement, même si la page a déjà été téléchargée récemment.
        :param bool cache: Si ``False``, force le téléchargement,
            et n'enregistre pas les informations du téléchargement en cache
            (utile pour forcer aussi le prochain téléchargement).
        """
        with _Antispam(self, "page", url, force, cache) as antispam:
            with delai():
                try:
                    request = requests.get(
                        url,
                        verify=True,
                        allow_redirects=False,
                        timeout=10,
                    )  # Passer à False si la vérification du certificat pose problème.
                    request.raise_for_status()
                except requests.RequestException as error:
                    raise ErreurTéléchargement(error) from error
                return antispam.vérifie(request)

    def télécharge_fichier(self, url, *, force=False):
        """Télécharge l'URL, et retourne son contenu."""
        with _Antispam(self, "fichier", url, force) as antispam:
            with delai():
                try:
                    request = requests.get(
                        url,
                        stream=True,
                        verify=True,
                        timeout=10,
                    )  # Passer à False si la vérification du certificat pose problème.
                    request.raise_for_status()
                except requests.RequestException as error:
                    raise ErreurTéléchargement(error) from error
                request.raw.decode_content = True
                return antispam.vérifie(request).content

    def vérifie_url(self, url, *, force=False):
        """Vérifie si l'URL du fichier existe."""
        with _Antispam(self, "head", url, force) as antispam:
            with delai():
                try:
                    # pylint: disable=no-member
                    return (
                        antispam.vérifie(requests.head(url, timeout=10)).status_code
                        == requests.codes.ok
                    )
                except requests.RequestException as error:
                    raise ErreurTéléchargement(error) from error


@dataclasses.dataclass
class _Antispam(contextlib.AbstractContextManager):
    téléchargeur: Téléchargeur
    cible: str
    url: str
    # Force le téléchargement
    force: bool = False
    # Enregistre les informations en cache
    cache: bool = True

    def __enter__(self):
        if self.force or not self.cache:
            return self
        if self.téléchargeur.prochain(self.cible, self.url) > TODAY:
            raise ErreurTropTôt(self.téléchargeur.prochain(self.cible, self.url))
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # pylint: disable=useless-super-delegation
        super().__exit__(exc_type, exc_value, traceback)

    def vérifie(self, data):
        """Vérifie le résultat du téléchargement. Met à jour les info en cache si nécessaire."""
        if self.cache:
            self.téléchargeur.miseàjour(self.cible, self.url, data)
        return data


@contextlib.contextmanager
def delai():
    """Contexte garantissant un temps de `DELAI` secondes entre deux exécutions de ce contexte."""
    global _last_request_time

    time.sleep(max(0, _last_request_time + DELAI - time.time()))
    yield
    _last_request_time = time.time()
