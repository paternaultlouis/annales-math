# Copyright 2022 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Mets des dates aléatoires dans le cache des dates de téléchargement."""

import datetime
import logging
import random
import shutil

from ..database import DataBase
from . import DATES, Téléchargeur

logging.basicConfig(level=logging.INFO, format="{asctime} {message}", style="{")

BACKUP = DATES.with_suffix(f".json.back.{datetime.datetime.now().isoformat()}")
TODAY = datetime.date.today()


################################################################################
# main


def main():
    """Fonction principale."""

    # Make a backup copy
    try:
        shutil.copyfile(DATES, BACKUP)
        logging.info(f"A backup of the cache has been kept in {BACKUP}.")
    except FileNotFoundError:
        pass

    # Create random cache
    with (
        DataBase() as database,
        Téléchargeur() as téléchargeur,
    ):
        for page in database.pages:
            téléchargeur.dates["page"][page]["changement"] = (
                datetime.date.fromtimestamp(0)
            )
            téléchargeur.dates["page"][page]["essai"] = TODAY - datetime.timedelta(
                days=random.randint(0, 365)
            )
        for fichier in database.fichiers:
            téléchargeur.dates["fichier"][fichier]["changement"] = (
                datetime.date.fromtimestamp(0)
            )
            téléchargeur.dates["fichier"][fichier]["essai"] = (
                TODAY - datetime.timedelta(days=random.randint(0, 365))
            )
            téléchargeur.dates["head"][fichier]["changement"] = (
                datetime.date.fromtimestamp(0)
            )
            téléchargeur.dates["head"][fichier]["essai"] = TODAY - datetime.timedelta(
                days=random.randint(0, 365)
            )

    # Done
    logging.info("Done")


if __name__ == "__main__":
    main()
