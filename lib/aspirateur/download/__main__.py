# Copyright 2017-2023 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Ligne de commande : Téléchargement des fichiers."""

import argparse
import datetime

from rich.progress import track

from .. import CONSOLE, ErreurTéléchargement, download, filtre, utils
from ..database import DataBase
from . import ErreurTropTôt

TODAY = datetime.date.today()


def all_(args):
    """Re-télécharge tous les fichiers."""
    parser = argparse.ArgumentParser(
        description="Télécharge les fichiers.",
        parents=[filtre.parser_fichiers()],
        epilog="Les fichiers qui ont déjà été téléchargés récemment ne sont pas re-téléchargés. Ils le seront plus tard, lors d'un nouvel appel à cette commande.",
    )
    parser.add_argument(
        "-f",
        "--fix",
        help="Ne télécharge que les fichiers vides ou manquants.",
        default=False,
        action=argparse.BooleanOptionalAction,
    )
    parser.add_argument(
        "-c",
        "--clean",
        help="Supprime de la base de donnée les fichiers dont le lien est mort.",
        default=False,
        action=argparse.BooleanOptionalAction,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="Augmente le nombre d'informations affichées.",
        default=1,
        action="count",
    )
    options = parser.parse_args(args)

    with (
        DataBase() as database,
        download.Téléchargeur() as téléchargeur,
    ):
        fichiers = list(sorted(database.fichiers.values(filtres=options.filtre)))

        for i, fichier in track(
            list(enumerate(fichiers)), description="Téléchargement…", console=CONSOLE
        ):
            if fichier.catégorie.startswith("_"):
                if options.verbose > 1:
                    CONSOLE.log_error(
                        f"[{i+1}/{len(fichiers)}] Fichier {fichier.nom} ignoré."
                    )
                continue
            if options.fix:
                if fichier.path.exists() and fichier.path.stat().st_size != 0:
                    if options.verbose > 1:
                        CONSOLE.log_info(
                            # pylint: disable=line-too-long
                            f"[{i+1}/{len(fichiers)}] Le fichier {fichier.nom} n'a pas besoin d'être téléchargé."
                        )
                    continue

            try:
                content = téléchargeur.télécharge_fichier(
                    fichier.url,
                    force=(  # Force le téléchargement si le fichier est absent du système de fichier.
                        options.fix
                        and (
                            (not fichier.path.exists())
                            or fichier.path.stat().st_size == 0
                        )
                    ),
                )
            except ErreurTropTôt as error:
                if options.verbose > 1:
                    CONSOLE.log_error(
                        f"[{i+1:>{len(str(len(fichiers)))}}/{len(fichiers)}] {fichier.nom}: {error}"
                    )
                continue
            except ErreurTéléchargement as error:
                if options.clean and error.status_code == 404:
                    téléchargeur.dates["page"][fichier.page]["changement"] = TODAY
                    database.supprime_fichier(fichier)
                    CONSOLE.log_info(f'Fichier "{fichier.url}" supprimé.')
                    continue
                CONSOLE.log_error(
                    f"[{i+1:>{len(str(len(fichiers)))}}/{len(fichiers)}] {fichier.nom}: {error}"
                )
                continue
            else:
                CONSOLE.log_info(
                    f"[{i+1:>{len(str(len(fichiers)))}}/{len(fichiers)}] {fichier.nom}: téléchargé."
                )
            fichier.path.parent.mkdir(parents=True, exist_ok=True)
            with open(fichier.path, mode="wb") as fileobj:
                fileobj.write(content)


def clean(args):
    """Vérifie les URLs, et supprime les fichiers disparus du site web."""
    # pylint: disable=line-too-long

    parser = argparse.ArgumentParser(
        description="Vérifie les URLs, et supprime les fichiers disparus du site web",
        parents=[filtre.parser_fichiers()],
        epilog="Les fichiers qui ont déjà été vérifiés récemment ne sont pas re-vérifiés. Ils le seront plus tard, lors d'un nouvel appel à cette commande.",
    )
    parser.add_argument(
        "-f",
        "--fix",
        help="Ne vérifie que les fichiers vides ou manquants.",
        default=False,
        action=argparse.BooleanOptionalAction,
    )
    parser.add_argument(
        "--force",
        help="Vérifie les fichiers, même s'ils ont été vérifiés récemment.",
        default=False,
        action=argparse.BooleanOptionalAction,
    )
    parser.add_argument(
        # pylint: disable=duplicate-code
        "-v",
        "--verbose",
        help="Augmente le nombre d'informations affichées.",
        default=1,
        action="count",
    )
    options = parser.parse_args(args)

    with (
        DataBase() as database,
        download.Téléchargeur() as téléchargeur,
    ):
        fichiers = list(sorted(database.fichiers.values(filtres=options.filtre)))

        for i, fichier in track(
            list(enumerate(fichiers)), description="Vérification…", console=CONSOLE
        ):
            if options.fix:
                if fichier.path.exists() and fichier.path.stat().st_size != 0:
                    CONSOLE.log_info(
                        # pylint: disable=line-too-long
                        f"[{i+1}/{len(fichiers)}] Le fichier {fichier.nom} n'a pas besoin d'être vérifié."
                    )
                    continue
            try:
                if not téléchargeur.vérifie_url(fichier.url, force=options.force):
                    téléchargeur.dates["page"][fichier.page]["changement"] = TODAY
                    database.supprime_fichier(fichier)
                    CONSOLE.log_info(f'Fichier "{fichier.url}" supprimé.')
            except ErreurTéléchargement as error:
                if options.verbose > 1:
                    CONSOLE.log_error(
                        f"[{i+1}/{len(fichiers)}] Fichier {fichier.nom} ignoré à cause d'une erreur : {error}"
                    )
                continue
            else:
                CONSOLE.log(f"[{i+1}/{len(fichiers)}] Fichier {fichier.nom} vérifié.")


def main():
    """Fonction principale."""

    with utils.parser(
        description="Télécharge les fichiers de l'APMEP.",
        formatter_class=argparse.RawTextHelpFormatter,
    ) as parser:
        subparser = parser.add_subparsers()

        subparser.add_function(clean)
        subparser.add_function(all_, command="all")

        subparser.required = True
        subparser.dest = "commande"


if __name__ == "__main__":
    main()
