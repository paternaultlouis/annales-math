# Copyright 2017-2021 Louis Paternault
#
# This file is part of Aspirateur.
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Quelques utilitaires pour Aspirateur."""

import collections
import contextlib
import filecmp
import re
import shutil
import sys

import argdispatch

from . import MINIMUM_PYTHON_VERSION, AspirateurException


def _check_python_version():
    """Vérifie que la version minimale de Python est utilisée."""
    if sys.version_info < MINIMUM_PYTHON_VERSION:
        raise AspirateurException(
            "ERREUR : Ce programme ne fonctionne qu'avec python version {} ou supérieur.".format(  # pylint: disable=line-too-long
                ".".join((str(num) for num in MINIMUM_PYTHON_VERSION))
            )
        )


@contextlib.contextmanager
def parser(*args, **kwargs):
    """Contexte qui définit par parser, et l'exécute prudemment.

    Les arguments sont passés directement à :class:`argdispatch.ArgumentParser`.
    """
    _check_python_version()

    myparser = argdispatch.ArgumentParser(*args, **kwargs)

    yield myparser

    try:
        myparser.parse_args()
    except AspirateurException as error:
        print()
        print(error)
        sys.exit(1)


def copie(source, dest):
    """Lie ou copie un fichier.

    Les deux arguments sont des noms de fichiers, pas de dossier.

    - Si les fichiers sont déjà le même fichier (hard link), rien n'est fait.
    - Sinon, si le fichier de destination est plus récent que la source, rien n'est fait.
    - Sinon, si les deux fichiers sont égaux, rien n'est fait.
    - Sinon, un lien (hard link) est créé.
    - En cas d'erreur, le fichier est copié.
    - Une exception lors de cette dernière tentative est transmise au contexte appelant.
    """
    # Si les deux fichiers sont déjà le même fichier, rien à faire.
    try:
        if source.samefile(dest):
            return
    except OSError:
        pass

    # Si le fichier de destination est plus récent que la source, rien n'est fait.
    try:
        if source.stat().st_mtime < dest.stat().st_mtime:
            return
    except FileNotFoundError:
        pass

    # Si les deux fichiers sont égaux, rien n'est fait
    try:
        if filecmp.cmp(source, dest):
            return
    except OSError:
        pass

    # Suppression de l'ancien fichier
    dest.unlink(missing_ok=True)

    # Création du répertoire
    dest.parent.mkdir(parents=True, exist_ok=True)

    # Tentative de création de lien
    try:
        dest.hardlink_to(source)
        return
    except OSError:
        pass

    # On copie bêtement le fichier
    shutil.copy2(source, dest)


def multisplit(texte, séparateurs):
    """Découpe le texte selon les caractères de la chaîne séparateurs.

    Les chaînes vides sont ignorées.


    >>> multisplit("+p1-m1-m2+p2+p3++-m3", séparateurs="+-/")
    {'+': ['p1', 'p2', 'p3'], '-': ['m1', 'm2', 'm3'], '/': []}
    """
    résultat = collections.defaultdict(list)
    for mot in re.split(f"([{séparateurs}][^{séparateurs}]*)", texte):
        if len(mot) <= 1:
            continue
        résultat[mot[0]].append(mot[1:])
    return {key: résultat[key] for key in séparateurs}
