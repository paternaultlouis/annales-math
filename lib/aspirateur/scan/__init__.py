# Copyright 2017-2021 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Parcours le site web de l'APMEP à la recherche de nouvelles pages et fichiers."""

from .. import RE_FICHIERS, database, url_absolue, valide_url


def scan_pages(document):
    """Parcours la page donnée en argument à la recherche de nouvelles pages."""
    for link in document.find_all("a"):
        try:
            if valide_url(link["href"]):
                yield url_absolue(link["href"]).split("?", maxsplit=1)[0].split(
                    "#", maxsplit=1
                )[0]
        except KeyError:
            pass


def scan_fichiers(document, *, page):
    """Parcours la page donnée en argument à la recherche de nouveaux fichiers."""
    #  pylint: disable=too-many-branches

    # Recherche dans les tables
    trouvés = set()
    for table in document.find_all("table"):
        for row in table.find_all("tr"):
            description = ""
            dernier = ""
            # On remet à zéro si on a du texte qui suit un lien
            for cell in row.find_all("td", recursive=False):
                if not cell.decode_contents():
                    continue
                for lien in cell.find_all("a"):
                    dernier = "lien"
                    if "href" not in lien.attrs:
                        continue
                    if lien["href"] in trouvés:
                        continue
                    if not RE_FICHIERS.match(lien["href"]):
                        continue
                    trouvés.add(lien["href"])
                    yield database.Fichier(
                        url=url_absolue(lien["href"]),
                        description=f"{document.title.decode_contents()} {description} {lien.text}".replace(
                            "\n", " "
                        ).strip(),
                        page=page,
                    )
                if cell.text.strip() and not cell.find_all("a"):
                    if dernier == "lien":
                        description = cell.text
                    else:
                        description += " " + cell.text
                    dernier = "texte"

    # Recherche hors des tables (Olympiades)
    # Par exemple https://www.apmep.fr/Sujet-national-Exercice-no1,2850
    for lien in document.find_all("a"):
        if "href" not in lien.attrs:
            continue
        if lien["href"] in trouvés:
            continue
        if RE_FICHIERS.match(lien["href"]):
            trouvés.add(lien["href"])
            yield database.Fichier(
                url=url_absolue(lien["href"]),
                description=f"{document.title.decode_contents()} {lien.text}".replace(
                    "\n", " "
                ).strip(),
                page=page,
            )
