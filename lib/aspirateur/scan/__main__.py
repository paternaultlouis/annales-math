# Copyright 2017-2023 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Ligne de commande : Cherche des nouvelles pages et fichiers."""

import argparse
import textwrap

from bs4 import BeautifulSoup
from rich.progress import Progress

from .. import CONSOLE, RE_FICHIERS, ErreurTéléchargement, download, filtre, scan
from ..database import DataBase, ErreurAutrePage, Page
from ..download import ErreurTropTôt


def main():  #  pylint: disable=too-many-locals, too-many-branches, too-many-statements
    """Fonction principale."""

    parser = argparse.ArgumentParser(
        description="Recherche de nouvelles pages et fichiers.",
        parents=[filtre.parser_pages()],
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=textwrap.dedent(
            """\
                Remarque : Les pages qui ont été téléchargées trop récemment sont ignorées, et seront téléchargées plus tard, lors d'un nouvel appel à cette commande.

                Attention : Les nouveaux fichiers sont recherchés, indexés, mais pas téléchargés. Il est nécessaire de lancer `aspirateur download fichiers --fix` pour télécharger ces fichiers.
                """
        ),
    )
    parser.add_argument(
        # pylint: disable=duplicate-code
        "-v",
        "--verbose",
        help="Augmente le nombre d'informations affichées.",
        default=1,
        action="count",
    )
    options = parser.parse_args()

    with (
        DataBase() as database,
        download.Téléchargeur() as téléchargeur,
    ):
        à_analyser = list(
            sorted(page.url for page in database.pages.values(filtres=options.filtre))
        )
        analysées = set()
        nouvelles_pages = {}
        nouveaux_fichiers = []
        compteur = 0

        with Progress(console=CONSOLE) as progress:
            task = progress.add_task("Analyse…", total=len(à_analyser))

            while à_analyser:
                compteur += 1
                url = à_analyser.pop(0)
                progrès = "[{compteur:>{taille}}/{total}]".format(
                    compteur=compteur,
                    taille=len(str(compteur + len(à_analyser))),
                    total=compteur + len(à_analyser),
                )

                try:
                    document = BeautifulSoup(
                        téléchargeur.télécharge_page(url).text,
                        "lxml",
                    )
                    CONSOLE.log_info(
                        "{progrès} Analyse de la page {url}…".format(
                            progrès=progrès,
                            url=url,
                        )
                    )
                except ErreurTropTôt as error:
                    if options.verbose > 1:
                        CONSOLE.log_error(f"{progrès} Page {url} ignorée: {error}")
                    progress.update(
                        task, completed=compteur, total=compteur + len(à_analyser)
                    )
                    continue
                except ErreurTéléchargement as error:
                    if error.status_code == 404:
                        database.supprime_page(url)
                        CONSOLE.log_info(
                            f'{progrès} Page "{url}" supprimée (erreur 404).'
                        )
                    elif options.verbose > 1:
                        CONSOLE.log_error(f"{progrès} Page {url} ignorée: {error}")
                    progress.update(
                        task, completed=compteur, total=compteur + len(à_analyser)
                    )
                    continue

                # Nouveaux fichiers
                for fichier in scan.scan_fichiers(document, page=url):
                    if fichier.url not in database.fichiers:
                        CONSOLE.log_info(
                            f"Nouveau fichier {fichier.url} ({fichier.description})."
                        )
                        nouveaux_fichiers.append(fichier)

                # Nouvelles pages
                for nouvelle_url in scan.scan_pages(document):
                    if (
                        nouvelle_url in database.pages
                        or nouvelle_url in à_analyser
                        or nouvelle_url in nouvelles_pages
                    ):
                        # Page déjà analysée, ou bientôt analysée
                        continue
                    if nouvelle_url in database.autres:
                        # Page déjà analysée : ce n'est pas une page d'annales
                        continue
                    if RE_FICHIERS.match(nouvelle_url):
                        # C'est un fichier, pas une page
                        continue
                    try:
                        page = Page.from_response(
                            téléchargeur.télécharge_page(nouvelle_url, cache=False)
                        )
                    except ErreurAutrePage:
                        database.autres.add(nouvelle_url)
                        continue
                    except ErreurTéléchargement as error:
                        CONSOLE.log_warning(f"Page {nouvelle_url} ignorée: {error}")
                        continue

                    CONSOLE.log_info(
                        f"Nouvelle page {nouvelle_url} ({page.description})."
                    )
                    nouvelles_pages[nouvelle_url] = page
                    if nouvelle_url not in analysées:
                        à_analyser.append(nouvelle_url)

                analysées.add(url)
                progress.update(
                    task, completed=compteur, total=compteur + len(à_analyser)
                )

        if nouvelles_pages:
            database.classe_pages(nouvelles_pages.values())
            database.ajoute_pages(nouvelles_pages.values())
            database.edit_pages(nouvelles_pages[url] for url in sorted(nouvelles_pages))

        if nouveaux_fichiers:
            database.classe_fichiers(nouveaux_fichiers)
            database.ajoute_fichiers(nouveaux_fichiers)
            database.edit_fichiers(nouveaux_fichiers)


if __name__ == "__main__":
    main()
