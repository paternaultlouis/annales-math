# Copyright 2017-2024 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Ligne de commande."""

import logging
import textwrap

import argdispatch

import aspirateur

from . import URL_PREFIXES, utils

logging.basicConfig(level=logging.INFO, format="{asctime} {message}", style="{")

################################################################################
# main


def main():
    """Fonction principale."""

    with utils.parser(
        description=textwrap.dedent(
            """\
            Télécharge les annales depuis le site de l'APMEP [1]. Voir [2] pour plus d'informations.

            [1] {}
            [2] https://paternaultlouis.forge.apps.education.fr/annales-math/
        """
        ).format(URL_PREFIXES[0]),
        formatter_class=argdispatch.RawTextHelpFormatter,
    ) as parser:
        subparser = parser.add_subparsers()

        subparser.add_submodules(aspirateur)

        subparser.required = True
        subparser.dest = "commande"


if __name__ == "__main__":
    main()
