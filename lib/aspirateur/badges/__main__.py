# Copyright 2017-2022 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Génère les badges comptabilisant le nombre de fichiers dans la base."""

import argparse
import logging
import pathlib

from .. import PUBLICDIR, download
from ..database import DataBase


def main():
    """Génère un badge présentant des statistiques sur les fichiers téléchargés."""

    parser = argparse.ArgumentParser(
        description="Génère des badges présentant des statistiques sur les fichiers téléchargés."
    )
    parser.add_argument(
        "dest",
        type=pathlib.Path,
        nargs="?",
        help="Répertoire de destination.",
        default=PUBLICDIR / "badges",
    )
    options = parser.parse_args()

    with DataBase() as database:
        logging.info("Décompte du nombre de fichiers.")
        total = len(list(database.fichiers))
        corrigés = len(list(database.fichiers.values(filtres=["+corrigé"])))
        sujets = total - corrigés

    # Création du dossier
    options.dest.mkdir(parents=True, exist_ok=True)

    logging.info("Téléchargement et écriture des badges…")
    with download.Téléchargeur() as téléchargeur:
        with open(options.dest / "sujets.svg", "wb") as file:
            file.write(
                téléchargeur.télécharge_fichier(
                    f"https://img.shields.io/badge/Sujets-{sujets}-brightgreen.svg",
                    force=True,
                )
            )

        with open(options.dest / "corriges.svg", "wb") as file:
            file.write(
                téléchargeur.télécharge_fichier(
                    f"https://img.shields.io/badge/Corrigés-{corrigés}-brightgreen.svg",
                    force=True,
                )
            )

        with open(options.dest / "fichiers.svg", "wb") as file:
            file.write(
                téléchargeur.télécharge_fichier(
                    f"https://img.shields.io/badge/Fichiers-{total}-brightgreen.svg",
                    force=True,
                )
            )


if __name__ == "__main__":
    main()
