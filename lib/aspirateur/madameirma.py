# Copyright 2017-2021 Louis Paternault
#
# This file is part of Aspirateur.
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""🔮 Devine comment classer les pages et fichiers téléchargés."""

import dataclasses
import logging
import pathlib
import re

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.multioutput import MultiOutputClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MultiLabelBinarizer


@dataclasses.dataclass
class BouleDeCristalCatégorie:
    """Devine à quelle catégorie appartient une page ou un fichier."""

    descriptions: list[str]
    catégories: list[str]
    _pipeline: None = dataclasses.field(init=False, default=None)

    @classmethod
    def from_pages(cls, pages):
        """Instancie un objet à partir d'un itérable de pages."""
        return cls(*zip(*((page.description, page.catégorie) for page in pages)))

    @classmethod
    def from_fichiers(cls, fichiers, *, pages):
        """Instancie un objet à partir d'un itérable de fichiers."""
        return cls(
            *zip(
                *(
                    (
                        f"{pages[fichier.page].description} {fichier.description}",
                        fichier.catégorie,
                    )
                    for fichier in fichiers
                )
            )
        )

    def _entraîne(self):
        """Si nécessaire, entraîne le classifieur.

        Les données utilisées sont celles fournies dans le constructeur.
        """
        if self._pipeline is not None:
            return

        logging.info("Entraînement du classifieur de catégories.")
        self._pipeline = Pipeline(
            [("vectorizer", CountVectorizer()), ("classifier", MultinomialNB())]
        )
        self._pipeline.fit(self.descriptions, self.catégories)

    def devine(self, description):
        """Devine la catégorie."""
        self._entraîne()
        return self._pipeline.predict([description])[0]


@dataclasses.dataclass
class BouleDeCristalTags:
    """Devine quels tags correspondent à une page ou un fichier."""

    descriptions: list[str]
    tags: list[set[str]]
    _pipeline: None = dataclasses.field(init=False, default=None)
    _binary: None = dataclasses.field(init=False, default=None)

    @classmethod
    def from_pages(cls, pages):
        """Instancie un objet à partir d'un itérable de pages."""
        pages = list(pages)
        if not pages:
            return cls([], [])
        return cls(*zip(*((page.description, page.tags) for page in pages)))

    @classmethod
    def from_fichiers(cls, fichiers, *, pages):
        """Instancie un objet à partir d'un itérable de fichiers."""
        return cls(
            *zip(
                *(
                    (
                        " ".join(
                            [
                                pages[fichier.page].description,
                                fichier.description,
                                *re.split("[-_]", pathlib.Path(fichier.nom).stem),
                            ]
                        ),
                        fichier.tags,
                    )
                    for fichier in fichiers
                )
            )
        )

    def _entraîne(self):
        """Si nécessaire, entraîne le classifieur.

        Les données utilisées sont celles fournies dans le constructeur.
        """
        if self._pipeline is not None:
            return

        logging.info("Entraînement du classifieur de tags.")
        self._pipeline = Pipeline(
            [
                ("vectorizer", CountVectorizer()),
                ("classifier", MultiOutputClassifier(MultinomialNB(), n_jobs=-1)),
            ]
        )
        self._binary = MultiLabelBinarizer()
        self._pipeline.fit(self.descriptions, self._binary.fit_transform(self.tags))

    def devine(self, description):
        """Devine les tags."""
        self._entraîne()
        prediction = self._pipeline.predict([description])[0]
        for i, booléen in enumerate(prediction):
            if booléen:
                yield self._binary.classes_[i]
