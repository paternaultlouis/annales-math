# Copyright 2017-2024 Louis Paternault
#
# This file is part of Aspirateur.
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Gestion des interactions avec l'utilisateur."""

import logging
import os
import re
import subprocess
import tempfile
import textwrap

import jinja2

from . import RESOURCESDIR, AspirateurException

EXECUTABLES = {
    "posix": {
        "browser": ["sensible-browser"],
        "editor": [os.environ.get("EDITOR", "vim")],
    },
    "nt": {
        "browser": ["cmd", "/c", "start"],
        "editor": [os.environ.get("EDITOR", "vim")],
    },
}


def executable(programme, *arguments):
    """Construit la ligne de commande en fonction du type de programme demandé et des arguments.

    Le résultat dépend du système d'exploitation.
    """
    systeme = os.name

    if os.name not in EXECUTABLES:
        systeme = "posix"
        logging.warning(
            #  pylint: disable=line-too-long
            "Système d'exploitation inconnu. Les appels à des programmes externes risquent d'échouer…"
        )

    return EXECUTABLES[systeme][programme] + list(arguments)


def qcm(texte, réponses, /, *, default=None, exception=None):
    """Pose une question à l'utilisatrice, et renvoit la réponse.

    :param str texte: Texte (question) à afficher.
    :param str réponses: Liste des réponses possibles (une réponse = un caractère).
    :param str default: Réponse par défaut si l'utilisatrice n'en fournit pas.
        Si None, la question est reposée.
    :param str exception: Réponse par défaut si une exception
        KeyboardInterrupt ou EOFError est levée.  Si None,
        l'exception est propagée.
    """
    while True:
        try:
            réponse = input(texte).lower()
        except (KeyboardInterrupt, EOFError):
            if exception is None:
                raise
            réponse = exception.lower()

        if not réponse:
            réponse = default.lower()
        if réponse in réponses.lower():
            return réponse


def yesno(texte, /, *, default=None, exception=None):
    """Pose une question à l'utilisatrice, et renvoit True si elle répond oui.

    :param str texte: Texte (question) à afficher.
    :param str default: Réponse par défaut si l'utilisatrice n'en fournit pas ("o" ou "n").
        Si None, la question est reposée.
    :param str exception: Réponse par défaut si une exception
        KeyboardInterrupt ou EOFError est levée.  Si None,
        l'exception est propagée.
    """
    if default == "o":
        choix = "[On]"
    elif default == "n":
        choix = "[oN]"
    else:
        default = None
        choix = "[on]"

    return qcm(f"{texte} {choix} ", "on", default=default, exception=exception) == "o"


def trie_tags(catégorie, tags, *, nouveaux=None):
    """Trie les tags, en y insérant éventuellement les nouveaux tags."""
    if nouveaux is None:
        nouveaux = set()

    while True:
        # Écriture du texte
        texte = textwrap.dedent(
            f"""\
                # Vous pouvez ici modifier l'ordre des tags de la catégorie : {catégorie}
                """
        )
        if nouveaux:
            texte += "# Voici les nouveaux tags à classer :\n"
            texte += "\n".join(sorted(nouveaux)) + "\n"
            texte += "# Voici les anciens tags :\n"
        texte += "\n".join(tags) + "\n"

        # Édition et lecture du fichier temporaire
        with tempfile.TemporaryDirectory() as tempdir:
            filename = os.path.join(tempdir, "tags")
            with open(filename, "w", encoding="utf8") as fileobj:
                fileobj.write(texte)
            subprocess.run(executable("editor", filename), check=True)
            with open(filename, "r", encoding="utf8") as fileobj:
                classement = [
                    line.strip()
                    for line in fileobj
                    if line.strip() and not line.startswith("#")
                ]

        if set(classement) == set(tags) | set(nouveaux):
            return classement

        print("Erreur !")
        supprimés = set(classement) - (set(tags) | set(nouveaux))
        if supprimés:
            print(
                "Les tags suivants ont été supprimés :",
                " ".join(supprimés),
                "Utilisez 'aspirateur tag edit' pour supprimer des tags.",
            )
        oubliés = (set(tags) | set(nouveaux)) - set(classement)
        if oubliés:
            print("Les tags suivants n'ont pas été classés :", " ".join(oubliés))

        if (
            qcm(
                "[R]ecommencer, ou [a]bandonner ? [Ra]",
                "ra",
                default="r",
                exception="a",
            )
            == "a"
        ):
            raise AspirateurException("Abandon…")


def edit_tags(catégorie, tags):
    """Ouvre un éditeur de texte pour modifier les tags (nom et description)."""
    #  pylint: disable=too-many-branches, too-many-locals, too-many-statements
    nom_description_re = re.compile(r"^([^:]+)\s*:\s*(.*)$")
    texte = None
    with tempfile.TemporaryDirectory() as tempdir:
        while True:
            # Écriture du texte
            if texte is None:
                #  pylint: disable=line-too-long
                texte = textwrap.dedent(
                    f"""\
                        # Vous pouvez modifier ici le nom et la description des tags de l acatégorie {catégorie}.
                        # Attention ! Il ne faut surtout pas modifier l'ordre des tags (utiliser "aspirateur tag sort" pour cela).
                        # Pour supprimer des tags, remplacez la ligne par une ligne vide.
                        # Il est possible de remplacer un tag par plusieurs en les séparant par des espaces (la description est alors ignorée).
                        """
                )
                for tag, desc in tags:
                    texte += f"{tag}: {desc}\n"

            # Édition et lecture du fichier temporaire
            filename = os.path.join(tempdir, "tags")
            with open(filename, "w", encoding="utf8") as fileobj:
                fileobj.write(texte)
            subprocess.run(executable("editor", filename), check=True)
            with open(filename, "r", encoding="utf8") as fileobj:
                erreurs = set()
                réponse = []
                for i, ligne in enumerate(fileobj):
                    if ligne.startswith("#"):
                        continue
                    if ligne.strip() == "":
                        réponse.append([])
                    elif (match := nom_description_re.match(ligne.strip())) is None:
                        erreurs.add(i + 1)
                    else:
                        nouveaux, description = match.groups()
                        if " " in nouveaux.strip():
                            réponse.append(
                                [(tag, "") for tag in nouveaux.strip().split(" ")]
                            )
                        else:
                            réponse.append([(nouveaux, description)])

            # Analyse de la réponse
            print("Voici la liste des modifications.")
            modifications = []
            for i, nouveau in enumerate(réponse):
                if len(nouveau) == 0:
                    print(f"Suppression du tag {tags[i][0]}.")
                    modifications.append((tags[i][0], "", ""))
                elif len(nouveau) == 1:
                    if nouveau[0] == tags[i]:
                        continue
                    if nouveau[0][0] != tags[i][0]:
                        print(f"""{tags[i][0]} => {nouveau[0][0]}""")
                        modifications.append((tags[i][0], (nouveau[0][0],), tags[i][1]))
                    elif nouveau[0][0] == tags[i][0] and nouveau[0][1] != tags[i][1]:
                        print(
                            #  pylint: disable=line-too-long
                            f"""{nouveau[0][0]} : Description modifiée de "{tags[i][1]}" en "{nouveau[0][1]}"."""
                        )
                        modifications.append(
                            (tags[i][0], (nouveau[0][0],), nouveau[0][1])
                        )
                else:
                    print(
                        "{} => {}".format(
                            tags[i][0], ", ".join(tag for tag, _desc in nouveau)
                        )
                    )
                    modifications.append(
                        (tags[i][0], list(tag for tag, _desc in nouveau), tags[i][1])
                    )

            # Analyse des erreurs
            if erreurs or len(tags) != len(réponse):
                question = []
                if erreurs:
                    question.append(
                        "Les lignes suivantes n'ont pas le bon format : {}.".format(
                            ", ".join(str(i) for i in sorted(erreurs))
                        )
                    )
                if len(tags) > len(réponse) + len(erreurs):
                    question.append(
                        f"Il manque {len(tags) - len(réponse) - len(erreurs)} lignes."
                    )
                elif len(tags) < len(réponse) + len(erreurs):
                    question.append(
                        f"Il y a {len(réponse) + len(erreurs) - len(tags)} lignes en trop."
                    )

                rep = qcm(
                    " ".join(question)
                    + " [C]orriger, [r]ecommencer, ou [a]bandonner ? [Cra] ",
                    "cra",
                    default="c",
                    exception="a",
                )
                if rep == "c":
                    with open(filename, "r", encoding="utf8") as fileobj:
                        texte = fileobj.read().strip()
                    continue
                if rep == "r":
                    texte = None
                    continue
                if rep == "a":
                    raise AspirateurException("Abandon…")
            else:
                rep = qcm(
                    "[V]alider, [c]orriger, ou [a]bandonner ? [Vca]",
                    "vca",
                    default="v",
                    exception="a",
                )
                if rep == "v":
                    return modifications
                if rep == "a":
                    raise AspirateurException("Abandon…")
                if rep == "c":
                    with open(filename, "r", encoding="utf8") as fileobj:
                        texte = fileobj.read().strip()
                    continue


def edit_fichiers(page, fichiers, catégories):
    """Modifie les tags d'un fichier."""
    # pylint: disable=too-many-locals, too-many-branches
    ligne_re = re.compile(r"^([_a-z]+) *: *([^#]*) *#.*$")
    texte = None
    with tempfile.TemporaryDirectory() as tempdir:
        affiche_navigateur(fichiers, tempdir=tempdir, source=page)
        while True:
            # Écriture du texte
            if texte is None:
                texte = textwrap.dedent(
                    #  pylint: disable=line-too-long
                    """\
                        # Modifiez ici les catégories et étiquettes des fichiers de la page :
                        # {page.description}
                        # {page.url}
                        ################################################################################
                        # Ne modifiez surtout pas l'ordre des fichiers, ne supprimez aucune ligne.
                        # La liste des tags disponibles pour chaque catégorie est affichée en fin de fichier.
                        """.format(
                        page=page
                    )
                )
                texte += 80 * "#" + "\n"
                for fichier in fichiers:
                    #  pylint: disable=line-too-long
                    texte += "{fichier.catégorie}: {tags} # {fichier.description} {fichier.url}\n".format(
                        fichier=fichier,
                        tags=" ".join(catégories.trie(fichier.catégorie, fichier.tags)),
                    )
                texte += 80 * "#" + "\n"
                for catégorie, tags in sorted(catégories.items()):
                    if catégorie or tags:
                        texte += "# {}: {}\n".format(catégorie, " ".join(tags))

            # Édition et lecture du fichier temporaire
            filename = os.path.join(tempdir, "tags")
            with open(filename, "w", encoding="utf8") as fileobj:
                fileobj.write(texte)
            subprocess.run(executable("editor", filename), check=True)
            with open(filename, "r", encoding="utf8") as fileobj:
                erreurs = set()
                réponse = []
                for i, ligne in enumerate(fileobj):
                    if ligne.startswith("#"):
                        continue
                    if (match := ligne_re.match(ligne.strip())) is None:
                        erreurs.add(i + 1)
                    else:
                        groups = match.groups()
                        réponse.append((groups[0], groups[1].strip().split()))

            if erreurs or len(fichiers) != len(réponse):
                question = []
                if erreurs:
                    question.append(
                        "Les lignes suivantes n'ont pas le bon format : {}.".format(
                            ", ".join(str(i) for i in sorted(erreurs))
                        )
                    )
                if len(fichiers) > len(réponse) + len(erreurs):
                    question.append(
                        f"Il manque {len(fichiers) - len(réponse) - len(erreurs)} lignes."
                    )
                elif len(fichiers) < len(réponse) + len(erreurs):
                    question.append(
                        f"Il y a {len(réponse) + len(erreurs) - len(fichiers)} lignes en trop."
                    )
                rep = qcm(
                    " ".join(question)
                    + " [C]orriger, [r]ecommencer, ou [a]bandonner ? [Cra] ",
                    "cra",
                    default="c",
                    exception="a",
                )
                if rep == "c":
                    with open(filename, "r", encoding="utf8") as fileobj:
                        texte = fileobj.read().strip()
                    continue
                if rep == "r":
                    texte = None
                    continue
                if rep == "a":
                    raise AspirateurException("Abandon…")

            for i, ligne in enumerate(réponse):
                fichiers[i].catégorie = ligne[0]
                fichiers[i].tags = ligne[1]

            return fichiers


def affiche_navigateur(éléments, *, tempdir, source=None):
    """Affiche le classement des éléments dans le navigateur.

    Ceci permet de cliquer sur les liens pour se faire une idée de ce qui doit être classé.
    """
    filename = os.path.join(tempdir, "classement.html")
    environment = jinja2.Environment(
        loader=jinja2.FileSystemLoader([RESOURCESDIR / "templates"])
    )
    with open(filename, mode="w", encoding="utf8") as fileobj:
        fileobj.write(
            environment.get_template("verification.html").render(
                éléments=éléments, source=source
            )
        )

    # Ouverture du navigateur web en arrière-plan
    # pylint: disable=consider-using-with
    subprocess.Popen(executable("browser", filename))


def edit_pages(pages, catégories):
    """Modifie les tags de pages."""
    # Il doit être possible de factoriser plein de choses avec edit_fichiers().
    # pylint: disable=too-many-locals, too-many-branches
    ligne_re = re.compile(r"^([_a-z]+) *: *([^#]*) *#.*$")
    texte = None
    pages = list(pages)
    with tempfile.TemporaryDirectory() as tempdir:
        affiche_navigateur(pages, tempdir=tempdir)
        while True:
            # Écriture du texte
            if texte is None:
                #  pylint: disable=line-too-long
                texte = textwrap.dedent(
                    """\
                        # Modifiez ici les catégories et étiquettes des pages.
                        ################################################################################
                        # Ne modifiez surtout pas l'ordre des pages, ne supprimez aucune ligne.
                        # La liste des tags disponibles pour chaque catégorie est affichée en fin de fichier.
                        """
                )
                texte += 80 * "#" + "\n"
                for page in pages:
                    #  pylint: disable=line-too-long
                    texte += "{page.catégorie}: {tags} # {page.description} {page.url}\n".format(
                        page=page,
                        tags=" ".join(catégories.trie(page.catégorie, page.tags)),
                    )
                texte += 80 * "#" + "\n"
                for catégorie, tags in sorted(catégories.items()):
                    if catégorie or tags:
                        texte += "# {}: {}\n".format(catégorie, " ".join(tags))

            # Édition et lecture du fichier temporaire
            filename = os.path.join(tempdir, "tags")
            with open(filename, "w", encoding="utf8") as fileobj:
                fileobj.write(texte)
            subprocess.run(executable("editor", filename), check=True)
            with open(filename, "r", encoding="utf8") as fileobj:
                erreurs = set()
                réponse = []
                for i, ligne in enumerate(fileobj):
                    if ligne.startswith("#"):
                        continue
                    if (match := ligne_re.match(ligne.strip())) is None:
                        erreurs.add(i + 1)
                    else:
                        groups = match.groups()
                        réponse.append((groups[0], groups[1].strip().split()))

            if erreurs or len(pages) != len(réponse):
                question = []
                if erreurs:
                    question.append(
                        "Les lignes suivantes n'ont pas le bon format : {}.".format(
                            ", ".join(str(i) for i in sorted(erreurs))
                        )
                    )
                if len(pages) > len(réponse) + len(erreurs):
                    question.append(
                        f"Il manque {len(pages) - len(réponse) - len(erreurs)} lignes."
                    )
                elif len(pages) < len(réponse) + len(erreurs):
                    question.append(
                        f"Il y a {len(réponse) + len(erreurs) - len(pages)} lignes en trop."
                    )
                rep = qcm(
                    " ".join(question)
                    + " [C]orriger, [r]ecommencer, ou [a]bandonner ? [Cra] ",
                    "cra",
                    default="c",
                    exception="a",
                )
                if rep == "c":
                    with open(filename, "r", encoding="utf8") as fileobj:
                        texte = fileobj.read().strip()
                    continue
                if rep == "r":
                    texte = None
                    continue
                if rep == "a":
                    raise AspirateurException("Abandon…")

            for i, ligne in enumerate(réponse):
                pages[i].catégorie = ligne[0]
                pages[i].tags = ligne[1]

            return pages
