# Copyright 2017-2021 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Ligne de commande : Téléchargement des fichiers."""

import argdispatch

try:
    import ipdb as pdb
except ImportError:
    import pdb

from .. import utils
from ..database import DataBase


def rewrite(args):  # pylint: disable=unused-argument
    """Lit et ré-écrit la base de données (pour la normaliser)."""

    with DataBase():
        pass  # pylint: disable=unnecessary-pass


def shell(args):  # pylint: disable=unused-argument
    """Charge la base de données, et ouvre un shell."""
    with DataBase() as database:  # pylint: disable=unused-variable
        print("La base de données est disponible dans la variable : `database`.")
        pdb.set_trace()  # pylint: disable=forgotten-debug-statement
        pass  # pylint: disable=unnecessary-pass


def main():
    """Fonction principale."""

    with utils.parser(
        description="Manipule la base de données",
        formatter_class=argdispatch.RawTextHelpFormatter,
    ) as parser:
        subparser = parser.add_subparsers()

        subparser.add_function(rewrite)
        subparser.add_function(shell)

        subparser.required = True
        subparser.dest = "commande"


if __name__ == "__main__":
    main()
