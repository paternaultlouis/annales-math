# Copyright 2017-2022 Louis Paternault
#
# This file is part of Aspirateur.
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Manipule la base de données"""

import collections
import contextlib
import dataclasses
import functools
import hashlib
import itertools
import json
import logging
import os
import pathlib
import re
import urllib.parse

import requests
from bs4 import BeautifulSoup

from .. import (
    DATADIR,
    DOWNLOADDIR,
    ErreurTéléchargement,
    filtre,
    html,
    madameirma,
    url_absolue,
    userinput,
)

FICHIERS = DATADIR / "fichiers.json"
PAGES = DATADIR / "pages.json"
AUTRES = DATADIR / "autres.json"
TAGS = DATADIR / "tags.json"
CATÉGORIES = DATADIR / "categories.json"


class ErreurAutrePage(Exception):
    """Erreur levée si la page n'est pas une page d'annales."""


def _tags2set(dct):
    """Convertit une liste de tags d'un dictionnaire en ensemble de tags."""
    if "tags" in dct:
        dct["tags"] = set(dct["tags"])
    return dct


@dataclasses.dataclass
class Page:
    """Page web sur laquelle sont téléchargés des sujets d'examen."""

    url: str
    description: str
    catégorie: str = None
    redir: str = ""
    tags: set = dataclasses.field(default_factory=set)

    def __hash__(self):
        return hash(self.url)

    def json(self):
        """Convertit l'objet en un dictionnaire convertible en JSON."""
        return {
            "catégorie": self.catégorie,
            "description": self.description,
            "redir": self.redir,
            "tags": list(sorted(self.tags)),
        }

    @classmethod
    def from_response(cls, response):
        """Analyse la page téléchargée, et renvoit un objet `Page`.

        Cet objet n'a ni tags ni catégorie.
        """
        if response.status_code == 200:
            redir = ""
        elif response.status_code == 301:
            redir = response.headers["Location"]
        else:
            raise ErreurTéléchargement(
                f"Erreur lors du téléchargement de la page : code HTTP {response.status_code}."
            )

        document = BeautifulSoup(response.text, "lxml")

        # Est-ce une page d'annales ?
        annales = False
        for breadcrumb in document.find_all("ul", class_="breadcrumb"):
            for item in breadcrumb.find_all("li"):
                if any(
                    item.text.lower().startswith(keyword)
                    for keyword in ("annales", "olympiades", "les olympiades")
                ):
                    annales = True
        if not annales:
            raise ErreurAutrePage("Cette page n'est pas une page d'annales.")

        # Obtention de la description
        if redir:
            description = f"Redirection vers {url_absolue(redir)}."
        elif document.title is None:
            description = ""
        else:
            description = document.title.text
        if description.startswith("APMEP : "):
            description = description[len("APMEP : ") :]

        return cls(
            url=response.url,
            description=description.strip().replace("\n", " "),
            redir=redir,
        )


class Pages(collections.UserDict):  # pylint: disable=too-many-ancestors
    """Dictionnaire de toutes les pages de la base de données."""

    def values(self, *, catégorie=None, filtres=None):
        # pylint: disable=arguments-differ
        if filtres is None:
            filtres = [lambda x: True]

        for page in filtre.filtre_pages(
            super().values(),
            filtres=filtres,
        ):
            if catégorie is None or page.catégorie == catégorie:
                yield page

    def json(self):
        """Renvoit un dictionnaire, convertible en JSON."""
        return {url: page.json() for url, page in self.items()}

    def retag(self, catégorie, tag, nouveaux):
        """Affecte les tags `nouveaux` à la place de `tag`."""
        for page in self.values(catégorie=catégorie, filtres=[f"+{tag}"]):
            page.tags.remove(tag)
            page.tags |= set(nouveaux)

    def __getitem__(self, key):
        if key in self:
            return self.data[key]

        # Page absente : renvoit une coquille vide
        return Page(url=key, description="")


class Catégories(collections.UserDict):  # pylint: disable=too-many-ancestors
    """Dictionnaire des catégories de la base de données."""

    def ajoute(self, catégorie, tags):
        """Ajoute de nouveaux tags à une catégorie."""
        nouveaux = set(tags) - set(self[catégorie])
        if nouveaux:
            logging.info("Nouveaux tags: %s.", ", ".join(nouveaux))
            self[catégorie] = userinput.trie_tags(
                catégorie, self[catégorie], nouveaux=nouveaux
            )
        else:
            logging.info("Aucun nouveau tag.")

    def json(self):
        """Renvoit un dictionnaire, convertible en JSON."""
        return dict(self)

    def trie(self, catégorie, tags):
        """Renvoit les tags, triés.

        Si un des tags n'est pas connu de la base de données,
        le comportement est indéfini.
        """
        for tag in self[catégorie]:
            if tag in tags:
                yield tag


class DataBase(contextlib.AbstractContextManager):
    """Base des données nécessaires à la manipulation des annales."""

    def __init__(self):
        logging.info("Lecture de la base de données…")
        with open(FICHIERS, encoding="utf8") as fileobj:
            self.fichiers = Fichiers(
                {
                    url: Fichier(url, **value)
                    for url, value in json.load(fileobj, object_hook=_tags2set).items()
                }
            )
        with open(AUTRES, encoding="utf8") as fileobj:
            self.autres = set(json.load(fileobj))
        with open(PAGES, encoding="utf8") as fileobj:
            self.pages = Pages(
                {
                    url: Page(url, **value)
                    for url, value in json.load(fileobj, object_hook=_tags2set).items()
                }
            )
        with open(TAGS, encoding="utf8") as fileobj:
            self.tags = json.load(fileobj)
        with open(CATÉGORIES, encoding="utf8") as fileobj:
            self.catégories = Catégories(json.load(fileobj))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        super().__exit__(exc_type, exc_value, traceback)
        if exc_type is None:
            logging.info("Écriture de la base de données…")
            with open(FICHIERS, mode="w", newline="\n", encoding="utf8") as fileobj:
                json.dump(
                    self.fichiers.json(),
                    fileobj,
                    sort_keys=True,
                    indent=4,
                    ensure_ascii=False,
                )
            with open(AUTRES, mode="w", newline="\n", encoding="utf8") as fileobj:
                json.dump(
                    list(sorted(self.autres)),
                    fileobj,
                    sort_keys=True,
                    indent=4,
                    ensure_ascii=False,
                )
            with open(PAGES, mode="w", newline="\n", encoding="utf8") as fileobj:
                json.dump(
                    self.pages.json(),
                    fileobj,
                    sort_keys=True,
                    indent=4,
                    ensure_ascii=False,
                )
            with open(TAGS, mode="w", newline="\n", encoding="utf8") as fileobj:
                json.dump(
                    self.tags,
                    fileobj,
                    sort_keys=True,
                    indent=4,
                    ensure_ascii=False,
                )
            with open(CATÉGORIES, mode="w", newline="\n", encoding="utf8") as fileobj:
                json.dump(
                    self.catégories.json(),
                    fileobj,
                    sort_keys=True,
                    indent=4,
                    ensure_ascii=False,
                )
        else:
            logging.info("Les modifications de la base de données sont ignorées.")

    def supprime_fichier(self, fichier):
        """Supprime le fichier de la base de données et du système de fichiers."""
        if os.path.exists(fichier.path):
            os.remove(fichier.path)
        del self.fichiers[fichier.url]

    def supprime_page(self, url):
        """Supprime la page de la base de données."""
        del self.pages[url]

    def edit_tags(self, catégorie):
        """Ouvre un éditeur de texte pour modifier les tags (nom et description)."""
        tags = [(tag, self.tags[catégorie][tag]) for tag in self.catégories[catégorie]]

        modifications = userinput.edit_tags(catégorie, tags)

        for ancien, nouveaux, description in modifications:
            if not nouveaux:
                # Suppression
                self.supprime_tag(catégorie, ancien)
                continue
            if set(nouveaux) - set(self.catégories[catégorie]):
                self.catégories.ajoute(
                    catégorie, set(nouveaux) - set(self.catégories[catégorie])
                )
            self.fichiers.retag(catégorie, ancien, nouveaux)
            self.pages.retag(catégorie, ancien, nouveaux)
            for nouveau in nouveaux:
                self.tags[catégorie][nouveau] = description

        self.nettoie_tags([catégorie])

    def tags_vides(self, catégorie):
        """Recherche les tags vides de la catégorie."""
        compteur = collections.Counter()
        for tag in self.catégories[catégorie]:
            compteur[tag] = 0

        for élément in itertools.chain(self.fichiers.values(), self.pages.values()):
            if élément.catégorie == catégorie:
                compteur.update(élément.tags)

        for tag, compte in compteur.items():
            if compte == 0:
                yield tag

    def supprime_tag(self, catégorie, tag):
        """Supprime le tag de la base de données."""
        self.catégories[catégorie].remove(tag)
        del self.tags[catégorie][tag]

    def nettoie_tags(self, catégories=None):
        """Supprime les tags vides des catégories données."""
        if catégories is None:
            catégories = self.catégories
        for catégorie in catégories:
            for tag in self.tags_vides(catégorie):
                logging.info("Suppression du tag %s (catégorie %s).", tag, catégorie)
                self.supprime_tag(catégorie, tag)

    def ajoute_fichiers(self, fichiers):
        """Ajoute un ensemble de fichiers à la base de données."""
        for fichier in fichiers:
            self.fichiers[fichier.url] = fichier

    def classe_fichiers(self, fichiers):
        """Assigne une catégorie et des tags à chacun des fichiers donnés en argument."""
        bouledecristal_catégorie = madameirma.BouleDeCristalCatégorie.from_fichiers(
            self.fichiers.values(),
            pages=self.pages,
        )
        bouledecristal_tags = {
            catégorie: madameirma.BouleDeCristalTags.from_fichiers(
                self.fichiers.values(catégorie=catégorie),
                pages=self.pages,
            )
            for catégorie in self.catégories
            if list(self.fichiers.values(catégorie=catégorie))
        }

        # Classification
        for fichier in fichiers:
            description = " ".join(
                [
                    self.pages[fichier.page].description,
                    fichier.description,
                    *re.split("[-_]", pathlib.Path(fichier.nom).stem),
                ]
            )
            fichier.catégorie = bouledecristal_catégorie.devine(description)
            fichier.tags = set(
                bouledecristal_tags[fichier.catégorie].devine(description)
            )

    def edit_fichiers(self, fichiers):
        """Modifie les tags de fichiers."""
        classés = collections.defaultdict(list)
        for fichier in fichiers:
            classés[fichier.page].append(fichier)

        for page in sorted(classés):
            for fichier in userinput.edit_fichiers(
                self.pages[page], classés[page], self.catégories
            ):
                self.fichiers[fichier.url] = fichier
                for tag in fichier.tags:
                    if tag not in self.catégories[fichier.catégorie]:
                        self.tags[fichier.catégorie][tag] = ""
                        self.catégories.ajoute(fichier.catégorie, [tag])

    def classe_pages(self, pages):
        """Assigne une catégorie et des tags à chacune des pages données en argument."""
        # Création des classifieurs
        bouledecristal_catégorie = madameirma.BouleDeCristalCatégorie.from_pages(
            self.pages.values()
        )
        bouledecristal_tags = {
            catégorie: madameirma.BouleDeCristalTags.from_pages(
                self.pages.values(catégorie=catégorie)
            )
            for catégorie in self.catégories
        }

        # Classification
        for page in pages:
            page.catégorie = bouledecristal_catégorie.devine(page.description)
            page.tags = set(
                bouledecristal_tags[page.catégorie].devine(page.description)
            )

    def ajoute_pages(self, pages):
        """Ajoute un ensemble de pages à la base de données."""
        for page in pages:
            self.pages[page.url] = page

    def edit_pages(self, pages):
        """Modifie les tags de pages."""
        for page in userinput.edit_pages(pages, self.catégories):
            self.pages[page.url] = page
            for tag in page.tags:
                if tag not in self.catégories[page.catégorie]:
                    self.tags[page.catégorie][tag] = ""
                    self.catégories.ajoute(page.catégorie, [tag])


@functools.total_ordering
@dataclasses.dataclass
class Fichier:
    """Sujet d'examen, ou sa source."""

    url: str
    description: str
    page: str
    catégorie: str = None
    tags: set = dataclasses.field(default_factory=set)

    @property
    def nom(self):
        """Renvoit le nom du fichier (dans le sens d'un système de fichier)."""
        return pathlib.Path(urllib.parse.urlparse(self.url).path).name

    @property
    def path(self):
        """Renvoit le nom du fichier (dans le système de fichier)."""
        md5 = hashlib.new(
            "md5",
            bytes(
                pathlib.Path(urllib.parse.urlparse(self.url).path).stem, encoding="utf8"
            ),
            usedforsecurity=False,
        ).hexdigest()
        return DOWNLOADDIR / md5[0] / md5[1] / self.nom

    def __hash__(self):
        return hash(self.url)

    def json(self):
        """Convertit l'objet en un dictionnaire convertible en JSON."""
        return {
            "catégorie": self.catégorie,
            "description": self.description,
            "page": self.page,
            "tags": list(sorted(self.tags)),
        }

    def __lt__(self, other):
        return self.nom < other.nom

    def __eq__(self, other):
        return self.url == other.url


class Fichiers(collections.UserDict):  # pylint: disable=too-many-ancestors
    """Dictionnaire de tous les fichiers de la base de données."""

    def values(self, *, catégorie=None, filtres=None):
        # pylint: disable=arguments-differ
        if filtres is None:
            filtres = [lambda x: True]

        for fichier in filtre.filtre_fichiers(
            super().values(),
            filtres=filtres,
        ):
            if catégorie is None or fichier.catégorie == catégorie:
                yield fichier

    def json(self):
        """Renvoit un dictionnaire, convertible en JSON."""
        return {url: fichier.json() for url, fichier in self.items()}

    def retag(self, catégorie, tag, nouveaux):
        """Affecte les tags `nouveaux` à la place de `tag`."""
        for fichier in self.values(catégorie=catégorie, filtres=[f"+{tag}"]):
            fichier.tags.remove(tag)
            fichier.tags |= set(nouveaux)
