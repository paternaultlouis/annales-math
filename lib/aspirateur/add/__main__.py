# Copyright 2017-2022 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Ligne de commande : Ajoute des pages et fichiers."""

import argparse
import logging

from .. import URL_PREFIXES, ErreurTéléchargement, download, utils, valide_url
from ..database import DataBase, Page


def pages(args):
    """Ajoute des pages en donnant leur URL."""
    parser = argparse.ArgumentParser(
        description="Ajoute des pages.",
    )
    parser.add_argument(
        "URL",
        help="URL des pages à ajouter.",
        nargs="+",
    )
    options = parser.parse_args(args)

    with (
        DataBase() as database,
        download.Téléchargeur() as téléchargeur,
    ):
        nouvelles = []

        for url in set(options.URL):
            logging.info("Traitement de %s…", url)

            if not valide_url(url):
                logging.warning("L'URL doit commencer par %s.", URL_PREFIXES[0])

            if url in database.pages:
                logging.warning("Page déjà connue. Ignorée.")
                continue

            try:
                page = Page.from_response(
                    téléchargeur.télécharge_page(url, cache=False)
                )
            except ErreurTéléchargement as error:
                logging.warning("Page %s ignorée: %s", url, error)
                continue

            nouvelles.append(page)

        if nouvelles:
            database.classe_pages(nouvelles)
            database.ajoute_pages(nouvelles)
            database.edit_pages(nouvelles)


def main():
    """Fonction principale."""

    with utils.parser(
        description="Ajoute des pages en donnant leur URL.",
        formatter_class=argparse.RawTextHelpFormatter,
    ) as parser:
        subparser = parser.add_subparsers()

        subparser.add_function(pages)

        subparser.required = True
        subparser.dest = "commande"


if __name__ == "__main__":
    main()
