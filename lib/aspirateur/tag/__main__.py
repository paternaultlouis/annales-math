# Copyright 2017-2021 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Manipulation des tags"""

import argparse
import logging
import operator
import sys

from .. import filtre, userinput, utils
from ..database import DataBase


def clean(args):
    """Supprime les tags sans page ou fichier."""
    parser = argparse.ArgumentParser(
        description="Supprime les tags sans page ou fichier.",
    )
    #  pylint: disable=unused-variable
    options = parser.parse_args(args)

    with DataBase() as database:
        database.nettoie_tags()


def rename(args):
    """Renomme un tag."""
    parser = argparse.ArgumentParser(
        description="Renomme un tag.",
    )
    parser.add_argument(
        "-f",
        "--force",
        help="Ne demande pas confirmation avant des opérations suspectes (fusion ou suppression de tags par exemple).",  # pylint: disable=line-too-long
        default=False,
        action="store_true",
    )
    parser.add_argument(
        "catégorie",
        help="Catégorie des tags à manipuler.",
    )
    parser.add_argument(
        "tag",
        help="Tag à renommer",
    )
    parser.add_argument(
        "nouveau",
        help="Nouveaux noms pour le tag. Si cet argument est absent, le tag est supprimé.",
        nargs="*",
    )
    options = parser.parse_args(args)

    with DataBase() as database:
        if options.catégorie not in database.catégories:
            logging.error(
                "Catégorie %s inconnue. Choisissez parmi %s.",
                options.catégorie,
                ", ".join(database.catégories),
            )
            sys.exit(1)

        if options.tag not in database.catégories[options.catégorie]:
            logging.error(
                "Pas de tag %s dans la catégorie %s.",
                options.tag,
                options.catégorie,
            )
            sys.exit(1)

        if not options.nouveau:
            if not userinput.yesno(
                f"Supprimer le tag {options.tag} ?", default="n", exception="n"
            ):
                logging.error("Abandon")
                sys.exit(0)

        intersection = set(options.nouveau) & set(
            database.catégories[options.catégorie]
        )
        if intersection:
            if not userinput.yesno(
                "Les tags {} existent déjà. Continuer ?".format(
                    ", ".join(intersection)
                ),
                default="n",
                exception="n",
            ):
                logging.error("Abandon")
                sys.exit(0)

        # Fichiers
        database.fichiers.retag(options.catégorie, options.tag, options.nouveau)

        # Pages
        database.pages.retag(options.catégorie, options.tag, options.nouveau)

        # Tags
        del database.tags[options.catégorie][options.tag]
        for nouveau in options.nouveau:
            if nouveau not in database.tags[options.catégorie]:
                database.tags[options.catégorie][nouveau] = ""

        # Catégories
        if options.tag in database.catégories[options.catégorie]:
            database.catégories[options.catégorie].remove(options.tag)
        database.catégories.ajoute(options.catégorie, options.nouveau)


def liste(args):
    """Liste les tags"""
    champs = {"tag", "fichiers", "pages", "description"}

    parser = argparse.ArgumentParser(
        description="Affiche les tags existants.",
    )
    parser.add_argument(
        "-f",
        "--format",
        help="""Format de la sortie. Par exemple, pour afficher le tag, le nombre de fichiers et de pages, et sa description, on utilisera : "{tag} ({pages} pages, {fichiers} fichiers) : {description}".""",  # pylint: disable=line-too-long
        default="{tag}",
    )
    parser.add_argument(
        "-s",
        "--sort",
        help="""Clef de tri des tags.""",
        choices=sorted(champs),
        default="tag",
    )
    parser.add_argument(
        "-r",
        "--reverse",
        help="""Inverse le tri.""",
        action="store_true",
    )
    parser.add_argument(
        "catégorie",
        help="Catégorie des tags à afficher.",
    )
    options = parser.parse_args(args)

    with DataBase() as database:
        tags = (
            {
                "tag": tag,
                "fichiers": len(
                    list(
                        database.fichiers.values(
                            catégorie=options.catégorie, filtres=[f"+{tag}"]
                        )
                    )
                ),
                "pages": len(
                    list(
                        database.pages.values(
                            catégorie=options.catégorie, filtres=[f"+{tag}"]
                        )
                    )
                ),
                "description": database.tags[options.catégorie][tag],
            }
            for tag in database.tags[options.catégorie]
        )
        for tag in sorted(
            tags, key=operator.itemgetter(options.sort), reverse=options.reverse
        ):
            print(options.format.format(**tag))


def edit(args):
    """Ouvre un éditeur pour modifier les tags et leur description."""
    parser = argparse.ArgumentParser(
        description="Ouvre un éditeur pour modifier les tags, et leur description.",
    )
    parser.add_argument(
        "catégorie",
        help="Catégorie des tags à modifier.",
    )
    options = parser.parse_args(args)

    with DataBase() as database:
        if options.catégorie not in database.catégories:
            logging.error(
                "Catégorie %s inconnue. Choisissez parmi %s.",
                options.catégorie,
                ", ".join(database.catégories),
            )
            sys.exit(1)

        database.edit_tags(options.catégorie)


def sort(args):
    """Ouvre un éditeur pour trier les tags."""
    parser = argparse.ArgumentParser(
        description="Ouvre un éditeur pour modifier l'ordre des tags."
    )
    parser.add_argument(
        "catégorie",
        help="Catégorie des tags à trier.",
    )
    options = parser.parse_args(args)

    with DataBase() as database:
        if options.catégorie not in database.catégories:
            logging.error(
                "Catégorie %s inconnue. Choisissez parmi %s.",
                options.catégorie,
                ", ".join(database.catégories),
            )
            sys.exit(1)

        database.catégories[options.catégorie] = userinput.trie_tags(
            options.catégorie, database.catégories[options.catégorie]
        )


def pages(args):
    """Modifie les tags de pages."""
    parser = argparse.ArgumentParser(
        description="Modifie les tags de pages",
        parents=[filtre.parser_pages()],
    )
    options = parser.parse_args(args)

    with DataBase() as database:
        database.edit_pages(database.pages.values(filtres=options.filtre))


def files(args):
    """Modifie les tags de fichiers."""
    parser = argparse.ArgumentParser(
        description="Modifie les tags de fichiers",
        parents=[filtre.parser_fichiers()],
    )
    options = parser.parse_args(args)

    with DataBase() as database:
        database.edit_fichiers(database.fichiers.values(filtres=options.filtre))


def main():
    """Fonction principale."""

    with utils.parser(
        description="Manipule les tags des pages et fichiers.",
        formatter_class=argparse.RawTextHelpFormatter,
    ) as parser:
        subparser = parser.add_subparsers()

        subparser.add_function(clean)
        subparser.add_function(edit)
        subparser.add_function(files)
        subparser.add_function(liste, command="list")
        subparser.add_function(pages)
        subparser.add_function(rename)
        subparser.add_function(sort)

        subparser.required = True
        subparser.dest = "commande"


if __name__ == "__main__":
    main()
