# Copyright 2017-2024 Louis Paternault
#
# This file is part of Aspirateur.
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Filtre des itérateurs"""

import argparse
import os
import pathlib

from .. import DOWNLOADDIR, PUBLICDIR, utils
from ..html import public2download


def _type_filtre_commun(texte):
    """Retourne une fonction qui filtre une page ou un fichier selon un critère."""

    if texte.startswith("+"):
        tags = utils.multisplit(texte, "+-")
        exclus = set(tags["-"])
        inclus = set(tags["+"])

        def _filtre_tags(fichier):
            return (
                # Les tags inclus sont tous présents
                inclus <= fichier.tags
                and
                # Il n'y a pas de tags à exclure, OU aucun n'est présent
                not exclus & fichier.tags
            )

        return _filtre_tags

    if texte.startswith("="):
        tags = set(texte[1:].split("+"))

        def _filtre_tags(fichier):
            return tags == fichier.tags

        return _filtre_tags

    if texte.startswith("https://"):

        def _filtre_url(fichier):
            return fichier.url == texte

        return _filtre_url

    return None


def type_filtre_fichier(texte):
    """Retourne une fonction qui filtre un fichier selon un critère donné en argument."""

    fonction = _type_filtre_commun(texte)
    if fonction is not None:
        return fonction

    if DOWNLOADDIR in pathlib.Path(texte).resolve().parents:

        def _filtre_download(fichier):
            return fichier.path.resolve() == pathlib.Path(texte).resolve()

        return _filtre_download

    if PUBLICDIR in pathlib.Path(texte).resolve().parents:
        download = (
            DOWNLOADDIR
            / public2download.read().get(
                str(pathlib.Path(texte).resolve().relative_to(PUBLICDIR)), "/"
            )
        ).resolve()

        def _filtre_public(fichier):
            try:
                return fichier.path.resolve() == download
            except KeyError:
                return False

        return _filtre_public

    raise ValueError(f"L'argument '{texte}' n'est pas reconnu comme un filtre.")


def type_filtre_page(texte):
    """Retourne une fonction qui filtre une page selon un critère donné en argument."""

    fonction = _type_filtre_commun(texte)
    if fonction is not None:
        return fonction

    raise ValueError(f"Filtre {texte} non reconnu comme filtre de page.")


def filtre_fichiers(itérateur, *, filtres):
    """Applique la fonction :func:`filtre` à un itérateur de fichiers."""
    for i, item in enumerate(filtres):
        if isinstance(item, str):
            filtres[i] = type_filtre_fichier(item)

    yield from filtre(itérateur, filtres=filtres)


def filtre_pages(itérateur, *, filtres):
    """Applique la fonction :func:`filtre` à un itérateur de pages."""
    for i, item in enumerate(filtres):
        if isinstance(item, str):
            filtres[i] = type_filtre_page(item)

    yield from filtre(itérateur, filtres=filtres)


def filtre(itérateur, *, filtres):
    """Applique les filtres à l'itérateur.

    Vous devriez probablement utiliser :func:`filtre_fichiers` ou :func:`filtre_pages`
    suivant le type d'objets itérés.

    Itère selon l'argument,
    mais uniquement les éléments validant l'un des filtres du second argument.
    """
    for elem in itérateur:
        for fonction in filtres:
            if fonction(elem):
                yield elem
                break


def parser_pages():
    """Analyseur de ligne de commande qui accepte des filtres de pages comme arguments."""
    parent = argparse.ArgumentParser(add_help=False)
    parent.add_argument(
        "filtre",
        help=(
            "Filtre les pages à traiter. "
            "Voir la commande `filtre --help` pour afficher de l'aide sur la syntaxe des filtres."
        ),
        type=type_filtre_page,
        nargs="*",
        default=[lambda x: True],
    )
    return parent


def parser_fichiers():
    """Analyseur de ligne de commande qui accepte des filtres de fichiers comme arguments."""
    parent = argparse.ArgumentParser(add_help=False)
    parent.add_argument(
        "filtre",
        help=(
            "Filtre les fichiers à traiter. "
            "Voir la commande `filtre --help` pour afficher de l'aide sur la syntaxe des filtres."
        ),
        type=type_filtre_fichier,
        nargs="*",
        default=[lambda x: True],
    )
    return parent
