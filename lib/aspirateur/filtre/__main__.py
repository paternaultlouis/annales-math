# Copyright 2017-2021 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Ligne de commande : Téléchargement des fichiers."""

import argparse
import sys
import textwrap

from .. import filtre, utils
from ..database import DataBase

# pylint: disable=line-too-long
_EPILOG = textwrap.dedent(
    """\
        Un filtre de fichier peut prendre différentes formes :
        - +tag (mot commençant par +): tous les fichiers étiquetés avec ce tag ;
        - +tag1+tag2 (mots commençant et séparés par +) : tous les fichiers étiquetés avec ces deux tags (le nombre de tags est illimité) ;
        - +tag1-tag2-tag3+tag4 (mots commençant par + et séparés par + ou -) : tous les fichiers étiquetés avec les tags tag1 et tag4, sauf ceux étiquetés avec tag2 ou tag3 (le nombre de tags est illimité) ;
        - =tag1+tag2 (mots commençant par =, et séparés par +) : tous les fichiers étiquetés uniquement avec ces deux tags ;
        - https://www.apmep.fr/IMG/pdf/Metropole_S_juin_2003.pdf (mot commençant par https://) : le fichier ayant cette URL ;
        - n'importe quel autre format est interprété comme un chemin sur le système te fichier.
        Si plusieurs filtres sont fournis, les fichiers filtrés sont ceux vérifiant l'un au moins de ces critères.
        """
)


def fichiers(args):
    """Affiche les fichiers (éventuellement filtrées)."""
    # pylint: disable=line-too-long

    parser = argparse.ArgumentParser(description="Liste des fichiers.")
    parser.add_argument(
        "filtre",
        help="Filtre les fichiers à traiter. Voir la commande `filtre --help` pour afficher de l'aide sur la syntaxe des filtres.",
        type=filtre.type_filtre_fichier,
        nargs="*",
    )
    options = parser.parse_args(args)

    with DataBase() as database:
        for fichier in database.fichiers.values(filtres=options.filtre):
            print(fichier)


def pages(args):
    """Affiche les pages (éventuellement filtrées)."""

    # pylint: disable=line-too-long
    parser = argparse.ArgumentParser(description="Liste des pages.")
    parser.add_argument(
        "filtre",
        help="Filtre les pages à traiter. Voir la commande `filtre --help` pour afficher de l'aide sur la syntaxe des filtres.",
        type=filtre.type_filtre_page,
        nargs="*",
    )
    options = parser.parse_args(args)

    with DataBase() as database:
        for page in database.pages.values(filtres=options.filtre):
            print(page)


def main():
    """Fonction principale."""

    with utils.parser(
        description=textwrap.dedent(
            """\
                Affiche des informations sur des pages ou des fichiers.

                Les filtres définis ici sont aussi utilisés par d'autres commandes.
                """
        ),
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=_EPILOG,
    ) as parser:
        subparser = parser.add_subparsers()

        subparser.add_function(pages)
        subparser.add_function(fichiers)

        subparser.required = True
        subparser.dest = "commande"

    sys.exit(0)


if __name__ == "__main__":
    main()
