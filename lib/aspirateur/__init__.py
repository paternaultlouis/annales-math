# Copyright 2017-2024 Louis Paternault
#
# This file is part of Aspirateur.
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Aspirateur des annales du site web de l'APMEP."""

import os
import pathlib
import re

import pkg_resources
import xdg_base_dirs
from rich import console, theme

EMAIL = "spalax@gresille.org"
FORGEEDUC = "https://forge.apps.education.fr"
URL_PREFIXES = ("https://www.apmep.fr", "http://www.apmep.fr")
ROOTDIR = pathlib.Path(__file__).parent.parent.parent.resolve()
DATADIR = ROOTDIR / "data"
DOWNLOADDIR = ROOTDIR / "download"
PUBLICDIR = ROOTDIR / "public"
RESOURCESDIR = pathlib.Path(pkg_resources.resource_filename("aspirateur", "data"))
CACHEDIR = xdg_base_dirs.xdg_cache_home() / "aspirateur"

EXTENSIONS = ("zip", "tex", "pdf")
RE_FICHIERS = re.compile(r".*\.({})".format("|".join(EXTENSIONS)))

MINIMUM_PYTHON_VERSION = (3, 9)


class Console(console.Console):
    """Console, avec des fonctions pour logguer une info, un avertissement, une erreur."""

    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            **kwargs,
            theme=theme.Theme(
                {
                    "info": "dim cyan",
                    "warning": "magenta",
                    "error": "red",
                }
            ),
        )

    def log_error(self, *args, **kwargs):
        """Affiche une erreur."""
        super().log(*args, **kwargs, style="error")

    def log_warning(self, *args, **kwargs):
        """Affiche un avertissement."""
        super().log(*args, **kwargs, style="warning")

    def log_info(self, *args, **kwargs):
        """Affiche une information."""
        super().log(*args, **kwargs, style="info")


CONSOLE = Console(log_path=False, log_time=False)


class AspirateurException(Exception):
    """Erreur générique pour les exceptions prévues."""


def url_absolue(url):
    """Transforme une URL relative en URL absolue."""
    if url.startswith("http://") or url.startswith("https://"):
        return url
    return f"{URL_PREFIXES[0]}/{url}"


def valide_url(url):
    """Renvoie `True` si et seulement si l'URL est valide."""
    return any(url_absolue(url).startswith(préfixe) for préfixe in URL_PREFIXES)


class ErreurTéléchargement(Exception):
    """Le téléchargement n'est pas arrivé à son terme (ou n'a même pas commencé).

    Si l'argument du constructeur est une erreur du module :mod:`requests`,
    l'attribut :attr:`status_code` de cet objet renvoit le code HTML 200 (succès), 404 (non trouvé), etc.
    Sinon, il vaut :class:`None`.
    """

    def __init__(self, message):
        super().__init__(message)
        try:
            self.status_code = message.response.status_code
        except AttributeError:
            self.status_code = None
