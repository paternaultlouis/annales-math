# Copyright 2017-2021 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Recherche des incohérences dans la base de données."""

import argparse

from .. import filtre
from ..database import DataBase
from . import incohérences


class _ValidateurDeClef:
    #  pylint: disable=too-few-public-methods

    def __init__(self, option=None):
        if option is None:
            self.option = None
        else:
            self.option = option.split(":")

    def match(self, clef):
        """Vérifie si la clef correspond aux clefs valides ou non."""
        if self.option is None:
            return True
        return clef in self.option


def main():
    """Fonction principale"""
    parser = argparse.ArgumentParser(
        description="Recherche des incohérences dans la base de données.",
        parents=[filtre.parser_fichiers()],
    )
    parser.add_argument(
        #  pylint: disable=line-too-long
        "-f",
        "--format",
        help="""Format d'affichage des résultats : "humain" (phrase compréhensible par un humain) ou "brut" (liste des fichiers).""",
        default="humain",
        choices={"humain", "brut"},
    )
    parser.add_argument(
        #  pylint: disable=line-too-long
        "-c",
        "--clefs",
        help="""Liste des mots-clefs d'incohérence affichés, séparés par des ":". Par exemple, pour n'avoir que les fichiers sans année et les fichiers vides (et ignorer les autres incohérences), on utilisera "--clefs="sans-année:fichier-vide".""",
        default=_ValidateurDeClef(),
        type=_ValidateurDeClef,
    )
    #  pylint: disable=unused-variable
    options = parser.parse_args()

    if options.format == "humain":
        options.format = "{inco.clef}: {inco}"
    else:  # options.format == "brut"
        options.format = "{inco.brut}"

    with DataBase() as database:
        for inco in incohérences(database, options.filtre):
            if options.clefs.match(inco.clef):
                print(options.format.format(inco=inco))


if __name__ == "__main__":
    main()
