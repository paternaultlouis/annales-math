# Copyright 2017-2021 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Recherche des incohérences dans la base de données."""

import dataclasses
import re
import subprocess

from .. import DOWNLOADDIR
from ..database import Fichier


@dataclasses.dataclass
class Incohérence:
    """Incohérence dans la base de données."""

    clef: str
    explication: str
    brut: str
    fichiers: list[Fichier]

    def __str__(self):
        return self.explication


def _iter_versionned():
    for name in subprocess.check_output(
        ["git", "ls-files"], cwd=DOWNLOADDIR
    ).splitlines():
        if name.decode() not in {".gitignore", "README.txt"}:
            yield name.decode()


ANNÉE_RE = re.compile(r"^\d{4}$")


def _iter_fichiers_sans_années(database, filtres):
    for fichier in database.fichiers.values(filtres=filtres):
        if "compilation" in fichier.tags:
            continue
        for tag in fichier.tags:
            if ANNÉE_RE.match(tag):
                break
        else:
            yield Incohérence(
                "sans-année",
                f"Le fichier '{fichier.path}' n'a pas d'année.",
                fichier.path,
                {fichier},
            )


def _iter_fichiers_manquants(database, filtres):
    for fichier in database.fichiers.values(filtres=filtres):
        if not fichier.path.exists():
            yield Incohérence(
                "fichier-manquant",
                f"Le fichier {fichier.path} n'existe pas.",
                fichier.path,
                {fichier},
            )


def _iter_fichiers_vides(database, filtres):
    for fichier in database.fichiers.values(filtres=filtres):
        if fichier.path.exists() and fichier.path.stat().st_size == 0:
            yield Incohérence(
                "fichier-vide",
                f"Le fichier {fichier.path} est vide.",
                fichier.path,
                {fichier},
            )


def _iter_fichiers_obsolètes(database, filtres):
    #  pylint: disable=unused-argument
    fichiers_git = set(_iter_versionned())
    fichiers_db = {
        str(fichier.path.relative_to(DOWNLOADDIR))
        for fichier in database.fichiers.values()
    }

    for fichier in fichiers_git - fichiers_db:
        yield Incohérence(
            "fichier-obsolète",
            f"Le fichier {fichier} n'est pas indexé dans la base de données.",
            fichier,
            {},
        )


def _iter_tags_vides(database, filtres):
    """Recherche les tags sans pages."""
    #  pylint: disable=unused-argument
    for catégorie in database.catégories:
        for tag in database.tags_vides(catégorie):
            yield Incohérence(
                "tag-vide",
                f"Aucun fichier n'est taggué avec {catégorie}:{tag}.",
                tag,
                {},
            )


def incohérences(database, filtres):
    """Recherche, et itère, les incohérences de la base de données."""
    fonctions = [
        _iter_fichiers_sans_années,
        # _iter_corrigé_sans_sujet,
        _iter_fichiers_obsolètes,
        _iter_fichiers_vides,
        _iter_fichiers_manquants,
        _iter_tags_vides,
    ]
    for fonction in fonctions:
        yield from fonction(database, filtres)
