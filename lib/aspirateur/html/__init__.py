# Copyright 2017-2024 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Génère la page HTML listant tous les sujets."""

import collections
import logging
import os
import pathlib

from .. import DOWNLOADDIR


class Arbre:
    """Représentation des pages et fichiers de la base de données, classés par tag."""

    database = None

    def __init__(self, dest, database=None):
        self.branches = {}
        self.pages = set()
        self.fichiers = set()
        self.dest = dest

        if database is not None:
            self.__class__.database = database

            # Ajout des fichiers de la base de données
            for fichier in database.fichiers.values():
                if fichier.catégorie.startswith("_"):
                    # Fichier exclu
                    continue
                self.ajoute_fichier(fichier)
            for page in database.pages.values():
                if page.catégorie.startswith("_"):
                    # Page exclu
                    continue
                self.ajoute_page(page)

            self.élague()

    @property
    def label(self):
        """Renvoit une étiquette unique pour cet arbre."""
        return hash(self.dest.absolute())

    @property
    def tag(self):
        """Renvoit le tag de l'arbre, ou sa description."""
        tag = self.dest.name
        for élément in self.pages | self.fichiers:
            description = self.database.tags[élément.catégorie].get(tag, None)
            if description:
                return description
        return tag

    def _crée_branche(self, tag):
        """Crée une branche nommée `tag`.

        Si cette branche existe déjà, ne fait rien.
        """
        if tag in self.branches:
            return
        self.branches[tag] = self.__class__(dest=self.dest / tag)

    def ajoute_fichier(self, fichier, *, tags=None):
        """Ajoute un fichier à l'arbre, qui sera placé dans le sous-arbre correct.

        Si `tags` n'est pas nul,
        il correspond à la liste des sous-tags dans lequel doit être classé le fichier.
        """
        if tags is None:
            tags = list(self.database.catégories.trie(fichier.catégorie, fichier.tags))
        if tags:
            self._crée_branche(tags[0])
            self.branches[tags[0]].ajoute_fichier(fichier, tags=tags[1:])
        else:
            self.fichiers.add(fichier)

    def ajoute_page(self, page, *, tags=None):
        """Ajoute une page à l'arbre, qui sera placée dans le sous-arbre correct.

        Si `tags` n'est pas nul,
        il correspond à la liste des sous-tags dans lequel doit être classé la page.
        """
        if page.redir:
            return
        if tags is None:
            tags = list(self.database.catégories.trie(page.catégorie, page.tags))
        if tags:
            self._crée_branche(tags[0])
            self.branches[tags[0]].ajoute_page(page, tags=tags[1:])
        else:
            self.pages.add(page)

    @property
    def poids(self):
        """Retourne le nombre de fichiers contenus dans cet arbre et ses branches."""
        return len(self.fichiers) + sum(
            branche.poids for branche in self.branches.values()
        )

    def pprint(self, *, prefix=""):
        """Affiche tous les fichiers de l'arbre, en préservant la structure."""
        for tag in sorted(self.branches):
            print(f"{prefix} {tag}:")
            self.branches[tag].pprint(prefix=prefix + " ")
        for fichier in sorted(self.fichiers):
            print(f"{prefix} {fichier.path}")

    def élague(self):
        """Supprime les branches sans fichiers, et rassemble les branches avec peu de fichiers."""
        # Suppression des branches sans fichiers
        for tag, branche in list(self.branches.items()):
            branche.élague()
            if not (branche.branches or branche.fichiers):
                del self.branches[tag]

        # Rassemblement des branches avec peu de fichiers
        if (
            sum(branche.poids for branche in self.branches.values()) <= 50
            or max(branche.poids for branche in self.branches.values()) <= 15
        ):
            for branche in self.branches.values():
                self.fichiers |= branche.fichiers
            self.branches = {}

    def iter_par_tags_et_suffixes(self):
        """Itère sur les fichiers regroupés par tags et suffixes.

        Un groupe regroupe tous les fichiers partageant les mêmes tags et les mêmes suffixes.

        Chaque groupe est une liste de `(nom, fichiers, base)`, où :
        - `nom` est le nom du fichier (au sens du système de fichier) ;
        - `fichier` est le fichier de type :class:`aspirateur.database.Fichier` ;
        - `base` est le nom du fichier, sans extension ni numéro.
        """
        groupes = collections.defaultdict(set)
        for fichier in self.fichiers:
            groupes[
                "-".join(self.database.catégories.trie(fichier.catégorie, fichier.tags))
                + fichier.path.suffix
            ].add(fichier)

        for nom, synonymes in sorted(groupes.items()):
            # On supprime l'extension
            base = nom[0 : -len(pathlib.Path(nom).suffix)]

            if len(synonymes) == 1:
                yield [(nom, synonymes.pop(), base)]
            else:
                # Puis on rajoute les numéros
                groupe = []
                for i, fichier in enumerate(sorted(synonymes), start=1):
                    groupe.append((f"{base}-{i}{fichier.path.suffix}", fichier, base))
                yield groupe

    def iter_par_tags(self):
        """Itère sur des groupes de fichiers, regroupés par tags.

        Deux fichiers dont seule l'extension différe sont dans le même groupe.

        Chaque groupe est une liste de tuples `(nom, fichier)`, où :
        - `nom` est le nom du fichier (au sens du système de fichier),
        - et `fichier` est l'objet `class:aspirateur.database.Fichier`.
        """
        groupes = collections.defaultdict(list)
        for groupe in self.iter_par_tags_et_suffixes():
            for nom, fichier, base in groupe:
                groupes[base].append((nom, fichier))
        yield from groupes.values()

    def fichiers_à_copier(self, dest):
        """Itère des tuples (source, dest) de fichiers à copier.

        - Le fichier source est relatif à DOWNLOADDIR.
        - Le fichier de destination est relatif à `dest` (racine du dossier de destination).

        L'argument `dest` doit être un chemin absolu.
        """
        for branche in self.branches.values():
            yield from branche.fichiers_à_copier(dest)

        for groupe in self.iter_par_tags_et_suffixes():
            for nom, fichier, _ in groupe:
                yield (
                    fichier.path.relative_to(DOWNLOADDIR),
                    self.dest.relative_to(dest) / nom,
                )
