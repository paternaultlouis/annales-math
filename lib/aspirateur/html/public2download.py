# Copyright 2021-2024 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Outils qui permettent de retrouver un fichier à partir de son chemin public."""

import contextlib
import dbm
import functools
import logging
import shelve

from .. import CACHEDIR

CACHENAME = CACHEDIR / "public2download.db"


@contextlib.contextmanager
def write():
    """Renvoit le cache sous la forme d'un pseudo-dictionnaire (avec droits d'écriture)."""
    CACHENAME.parent.mkdir(parents=True, exist_ok=True)
    # Suppression du cache obsolète
    CACHENAME.unlink(missing_ok=True)
    # Création d'un nouveau cache
    with shelve.open(str(CACHENAME)) as shelf:
        yield shelf


@functools.cache
def read(*, warning: bool = True):
    """Renvoit le cache sous la forme d'un pseudo-dictionnaire (en lecture seulement).

    En cas d'erreur de lecture, un dictionnaire vide est renvoyé.

    :param bool warning: Si ``True``, affiche l'avertissement selon lequel les données sont peut-être obsolètes.
    """
    try:
        with shelve.open(str(CACHENAME), flag="r") as shelf:
            if warning:
                logging.info(
                    # pylint: disable=line-too-long
                    """Attention : Les données des filtres du dossier public sont peut-être obsolètes. Exécutez `aspirateur html` pour les mettre à jour."""
                )
            return dict(shelf)
    except dbm.error as error:
        logging.error(
            # pylint: disable=line-too-long
            f"""Impossible de filtrer les fichiers du dossier "public" tant que `aspirateur html` n'a pas été exécuté: {error}."""
        )
        return {}
