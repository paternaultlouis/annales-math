# Copyright 2017-2024 Louis Paternault
#
# Aspirateur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Aspirateur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Aspirateur.  If not, see <http://www.gnu.org/licenses/>.

"""Génère la page HTML listant tous les sujets."""

import argparse
import datetime
import logging
import pathlib
import zipfile

import jinja2
from rich.progress import track

from .. import DOWNLOADDIR, PUBLICDIR, RESOURCESDIR, AspirateurException, utils
from ..database import DataBase
from . import Arbre, public2download

ARCHIVE = "annales-math.zip"


def main():
    """Génère la page HTML listant tous les sujets."""

    parser = argparse.ArgumentParser(
        description="Génère la page HTML listant tous les sujets."
    )
    parser.add_argument(
        "dest",
        type=pathlib.Path,
        nargs="?",
        default=PUBLICDIR,
        help="Répertoire de destination.",
    )
    parser.add_argument(
        #  pylint: disable=line-too-long
        "-c",
        "--copy",
        help="Ne copie pas les fichiers dans le répertoire de destination, mais fais des liens vers le site web de l'APMEP.",
        default=True,
        action=argparse.BooleanOptionalAction,
    )
    parser.add_argument(
        #  pylint: disable=line-too-long
        "-z",
        "--archive",
        help="Crée une archive qui contient tous les fichiers (sujets et corrections, sources et PDF).",
        default=False,
        action=argparse.BooleanOptionalAction,
    )
    parser.add_argument(
        #  pylint: disable=line-too-long
        "-u",
        "--archive-url",
        help="URL de l'archive ZIP (par défault, l'URL de l'archive générée est utilisée, seulement si celle-ci a été générée (voire --archive).",
        default=None,
    )
    options = parser.parse_args()

    if options.dest == PUBLICDIR:
        options.dest.mkdir(parents=True, exist_ok=True)
    if not (options.dest.exists() and options.dest.is_dir()):
        raise AspirateurException(
            f"Le chemin {options.dest} n'existe pas ou n'est pas un répertoire."
        )

    # Préparation de l'environnement Jinja2
    environment = jinja2.Environment(
        loader=jinja2.FileSystemLoader([RESOURCESDIR / "templates"]),
        extensions=["jinja2.ext.loopcontrols"],
    )

    with DataBase() as database:
        # Création de l'arbre
        logging.info("Construction de l'arborescence de pages et fichiers…")
        arbre = Arbre(options.dest.resolve(), database)

        logging.info("Création du fichier `%s`…", options.dest / "index.html")
        with open(options.dest / "index.html", mode="w", encoding="utf8") as fileobj:
            fileobj.write(
                environment.get_template("index.html").render(
                    arbre=arbre,
                    date=datetime.datetime.now().strftime("%d/%m/%Y à %H:%M:%S"),
                    copie=options.copy,
                    archive=ARCHIVE if options.archive else None,
                    archive_url=options.archive_url,
                ),
            )

        if options.copy or options.archive:
            logging.info("Calcul des chemins de destination des fichiers…")
            with public2download.write() as cache:
                for source, dest in track(
                    list(arbre.fichiers_à_copier(options.dest.resolve())),
                    description="Calcul…",
                ):
                    cache[str(dest)] = source

            if options.copy:
                logging.info(f"Copie des {arbre.poids} fichiers…")
                for dest, source in track(
                    sorted(public2download.read(warning=False).items()),
                    description="Copie…",
                ):
                    try:
                        utils.copie(DOWNLOADDIR / source, options.dest / dest)
                    except OSError as error:
                        logging.error(
                            f"Échec lors de la copie de {source} vers {dest} : {error}."
                        )

            if options.archive:
                logging.info("Création de l'archive…")
                # Suppression de l'ancienne archive
                (options.dest / ARCHIVE).unlink(missing_ok=True)
                # Création de la nouvelle archive
                with zipfile.ZipFile(
                    options.dest / ARCHIVE,
                    mode="w",
                    compression=zipfile.ZIP_LZMA,
                ) as archive:
                    for dest, source in track(
                        public2download.read(warning=False).items(),
                        description="Compression…",
                    ):
                        try:
                            archive.write(DOWNLOADDIR / source, arcname=dest)
                        except OSError as error:
                            logging.error(
                                f"Échec lors de l'archivage de {source} : {error}."
                            )


if __name__ == "__main__":
    main()
