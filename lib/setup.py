#!/usr/bin/env python3

"""Installation script…"""

import setuptools

if __name__ == "__main__":
    setuptools.setup()
