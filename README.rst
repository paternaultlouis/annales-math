Annales de mathématiques ∞ Sujets d'examens et concours répertoriés par l'APMEP
===============================================================================

Ce projet est une tentative de téléchargement de l'ensemble des annales de mathématiques `répertoriées par l'APMEP <http://www.apmep.fr/-Annales-examens-concours->`__. Il ne vise pas à *remplacer* le travail fait par l'APMEP, mais à proposer :

- le `téléchargement <http://annales-math.readthedocs.io/fr/latest/telecharger/>`__ de toutes les annales en une seule archive : `annales-math.zip <https://nuage03.apps.education.fr/index.php/s/mmjaM5azMBHmRXD>`__ ;
- le `téléchargement <https://annales-math.readthedocs.io/fr/latest/telecharger/#le-depot-git>`__ de toutes les annales sous la forme de ce dépôt git, facilement mis à jour ;
- une `présentation concise <https://paternaultlouis.forge.apps.education.fr/annales-math/>`__ de l'ensemble des annales.
