Les fichiers sont regroupés dans des répertoires de manière arbitraire pour être certain de ne pas dépasser la limite du nombre de fichier par répertoire de certains systèmes de fichier.
Pour pouvoir accéder à ces fichiers de manière ergonomique, lancez la commande : `aspirateur html`, puis visitez le répertoire `public`.
