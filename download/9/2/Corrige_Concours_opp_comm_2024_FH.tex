\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{tabularx}
\usepackage{enumitem}
\usepackage{textcomp}
\usepackage{arydshln} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Corrigé : François Hache
\usepackage{pstricks,pst-plot,pst-text,pst-tree,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=1.9cm, bottom=2.4cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\newcommand{\vectt}[1]{\overrightarrow{\,\mathstrut\text{#1}\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Baccalauréat spécialité},
pdftitle = {Concours contrôleur des douanes session 2024\\},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
\newcommand{\ds}{\displaystyle}

\begin{document}

\setlength\parindent{0mm}
\setlength\parskip{4pt}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Concours contrôleur des douanes}
\lfoot{\small{Contrôle des opérations commerciales}}
\rfoot{\small{session 2024}}
\pagestyle{fancy}
\thispagestyle{empty}

\begin{center}
{\Large \textbf{\decofourleft~Corrigé du Concours contrôleur des douanes session 2024~\decofourright\\[7pt]Branche du contrôle des opérations commerciales et de l'administration générale}}
\end{center}

%\textbf{Remarques préliminaires :\\
%-- Sauf précision contraire figurant dans un énoncé, lorsque des calculs sont demandés, les résultats seront donnés sous forme décimale au centième près.\\
%-- Chaque réponse doit être précédée du numéro de la question à laquelle elle se rapporte, sur la copie et les intercalaires destinés à cet effet. \emph{Aucune réponse ne doit être inscrite sur le sujet}.}

\bigskip

\textbf{\large Exercice 1}

\medskip

Soit $f$ la fonction définie sur $]-\infty~;~-2[ \cup ]-2~;~+ \infty[$ par:
$f(x) = \dfrac{x^3 +x^2 + x + 1}{x + 2}.$

%\smallskip

\begin{enumerate}
\item On détermine les limites de $f$ aux bornes de son ensemble de définition.

\begin{list}{\textbullet}{}
\item La fonction $f$ est une fonction rationnelle donc sa limite en l'infini est la limite du quotient de ses termes de plus haut degré.

$\ds\lim_{x\to -\infty} f(x) = \ds\lim_{x\to -\infty} \dfrac{x^3 +x^2 + x + 1}{x + 2} 
= \ds\lim_{x\to -\infty} \dfrac{x^3}{x}
= \ds\lim_{x\to -\infty} x^2 = +\infty$

$\ds\lim_{x\to +\infty} f(x) = \ds\lim_{x\to +\infty} \dfrac{x^3 +x^2 + x + 1}{x + 2} 
= \ds\lim_{x\to +\infty} \dfrac{x^3}{x}
= \ds\lim_{x\to +\infty} x^2 = +\infty$

\item Limite à gauche de $-2$

$\left .
\begin{array}{r}
\ds\lim_{x\to -2 \atop x<-2} x^3+x^2+x+1 = -5<0\\
\ds\lim_{x\to -2 \atop x<-2} x+2 = 0\\
x+2<0
\end{array}
\right \}
\implies \ds\lim_{x\to -2 \atop x<-2} f(x)=+\infty$

\item Limite à droite de $-2$

$\left .
\begin{array}{r}
\ds\lim_{x\to -2 \atop x>-2} x^3+x^2+x+1 = -5<0\\
\ds\lim_{x\to -2 \atop x>-2} x+2 = 0\\
x+2>0
\end{array}
\right \}
\implies \ds\lim_{x\to -2 \atop x>-2} f(x)=-\infty$
\end{list}

\item $f'(x)
= \dfrac{\left ( 3x^2+2x+1 \right )\times \left ( x+2\right ) - \left ( x^3+x^2+x+1\right ) \times 1}{\left (x+2\right )^2}\\
\phantom{f'(x)}
=\dfrac{3x^3+2x^2+x +6x^2+4x+2 -x^3-x^2-x-1}{\left (x+2\right )^2}
=\dfrac{2x^3+7x^2 + 4x +1}{\left (x+2\right )^2}$

\item On note $g$ la fonction définie sur $\R$ par:
$g(x) = 2x^3 + 7x^2 + 4x + 1$.

	\begin{enumerate}
		\item %Étudier les variations de la fonction $g$ et dresser son tableau de variation complet.
$g'(x)= 6x^2 + 14x +4$

On détermine le signe du trinôme 	$6x^2 + 14x +4$.

$\Delta= 14^2-4 \times 6 \times 4 = 100=10^2$

Le trinôme admet deux racines:	

$x'=\dfrac{-b-\sqrt{\Delta}}{2a} = \dfrac{-14-10}{2\times 6}=-2$ et
$x''=\dfrac{-b+\sqrt{\Delta}}{2a} = \dfrac{-14+10}{2\times 6}=\dfrac{-4}{12}=-\dfrac{1}{3}$.

D'où le signe de $g'(x)$ sur $\R$:

\begin{center}
{
\renewcommand{\arraystretch}{1.5}
\def\esp{\hspace*{1cm}}
$\begin{array}{|c | *{7}{c} |} 
\hline
x  & -\infty & \esp & -2 & \esp & -\frac{1}{3} & \esp & +\infty \\
\hline
g'(x) &  & \pmb{+} &  \vline\hspace{-2.7pt}{0} & \pmb{-} & \vline\hspace{-2.7pt}{0} & \pmb{+} & \\
\hline
\end{array}$
}
\end{center}		

$\ds\lim_{x\to -\infty} g(x) = \ds\lim_{x\to -\infty} 2x^3 + 7x^2 + 4x + 1 = \ds\lim_{x\to -\infty} 2x^3 = -\infty$

$\ds\lim_{x\to +\infty} g(x) = \ds\lim_{x\to +\infty} 2x^3 + 7x^2 + 4x + 1 = \ds\lim_{x\to +\infty} 2x^3 = +\infty$

$g(-2)= 2\times (-2)^3 + 7\times (-2)^2 + 4\times (-2) +1 = -16+28-8+1=5$

$g \left (-\dfrac{1}{3}\right )= 2\times \left (-\dfrac{1}{3}\right )^3 + 7\times \left (-\dfrac{1}{3}\right )^2 + 4\times \left (-\dfrac{1}{3}\right ) +1 =-\dfrac{2}{27}+\dfrac{7}{9}- \dfrac{4}{3}+1 = \dfrac{10}{27}$

On établit le tableau des variations de $g$ sur $\R$.

\begin{center}
{\renewcommand{\arraystretch}{1.3}
\psset{nodesep=3pt,arrowsize=2pt 3}  % paramètres
\def\esp{\hspace*{1cm}}% pour modifier la largeur du tableau
\def\hauteur{20pt}% mettre au moins 20pt pour augmenter la hauteur
$\begin{array}{|c| *6{c} c|}
\hline
 x & -\infty & \esp & -2 & \esp & -\frac{1}{3} & \esp & +\infty \\
 \hline
g'(x) &  &  \pmb{+} & \vline\hspace{-2.7pt}0 & \pmb{-} & \vline\hspace{-2.7pt}0 & \pmb{+} & \\  
\hline
  &  &  & \Rnode{max1}{5} & & & &  \Rnode{max2}{+\infty} \\
g(x) & &  & & & & & \rule{0pt}{\hauteur}\\
 & \Rnode{min1}{-\infty} & & & & \Rnode{min2}{\frac{10}{27}} & & \rule[-7pt]{0pt}{\hauteur} 
\ncline{->}{min1}{max1} 
\ncline{->}{max1}{min2}
\ncline{->}{min2}{max2} \\
\hline
\end{array}$
}
\end{center}


		\item %Montrer que l'équation $g(x)= 0$ admet une solution unique sur $]-\infty~;~-2[$, que l'on notera $\alpha$.
La fonction $g$ est une fonction polynôme donc elle est continue sur $\R$. De plus elle est strictement décroissante sur $]-\infty~;~-2[$. Enfin $\ds\lim_{x\to -\infty} g(x)<0$ et $g(-2)=5>0$.
\begin{center}
{\renewcommand{\arraystretch}{1.3}
\def\esp{\hspace*{3cm}}
\psset{nodesep=3pt,arrowsize=2pt 3}  % paramètres
$\begin{array}{|c| *3{c}|}
\hline
 x & -\infty   & \esp & -2 \\
 \hline
  &   &    & \Rnode{max}{5}   \\
g(x) & &  &  \\
 &     \Rnode{min}{-\infty} & & 
\ncline{->}{min}{max}
\rput*(-2,0.8){\Rnode{zero}{\red 0}}
\rput(-2,1.9){\Rnode{alpha}{\red \alpha}}
\ncline[linestyle=dotted, linecolor=red]{alpha}{zero}\\
\hline
\end{array}$
}
\end{center}

Donc, d'après le corollaire du théorème des valeurs intermédiaires, on peut dire que l'équation $g(x)=0$ admet une solution unique sur $]-\infty~;~-2[$. On l'appelle $\alpha$ et on admettra que $\alpha = - 2,86$.

		\item %L'équation $g(x) = 0$ admet-elle d'autres solutions dans $\R$ ?
D'après son tableau de variations, on peut dire que la fonction $g$ admet sur l'intervalle $[-2\;;\; +\infty[$ un minimum égal à $\frac{10}{27}$, donc strictement positif.

On en déduit que l'équation $g(x)=0$ n'a pas de solution sur $[-2\;;\; +\infty[$.	
	\end{enumerate}
	
\item %Dresser un tableau indiquant, en fonction de $x$, le signe de $f'(x)$ et les variations de $f$.
$f'(x) =\dfrac{2x^3+7x^2 + 4x +1}{\left (x+2\right )^2}=\dfrac{g(x)}{\left (x+2\right )^2}$

D'après les questions précédentes, on peut déterminer le signe de $g(x)$, puis le signe de $f'(x)$, puis les variations de la fonction $f$.

\begin{center}
{\renewcommand{\arraystretch}{1.3}
\psset{nodesep=3pt,arrowsize=2pt 3}  % paramètres
\def\esp{\hspace*{1cm}}% pour modifier la largeur du tableau
\def\hauteur{20pt}% mettre au moins 20pt pour augmenter la hauteur
$\begin{array}{|c| *{7}{c}  |}
\hline
  x & -\infty & \esp & \alpha  & \esp &  -2 &  \esp & +\infty  \\
 \hline
g(x) & &  \pmb{-} & \vline\hspace{-2.7pt}0 & \pmb{+} & \vline\hspace{-2.7pt}{\phantom 0} &  \pmb{+} & \\ 
 \hline
 (x+2)^2 & &  \pmb{+} & \vline\hspace{-2.7pt}{\phantom 0} & \pmb{+} & \vline\hspace{-2.7pt}0&  \pmb{+} &\\ 
 \hline
f'(x) &  &  \pmb{-} & \vline\hspace{-2.7pt}0 & \pmb{+} & \vline\;\vline  & \pmb{+} & \\  
\hline
  &   \Rnode{max1}{+\infty} & &  & &\Rnode{max2}{+\infty~}  \vline\;\vline\phantom{+\infty~} & &   \Rnode{max3}{+\infty} \\
f (x) & &  & & &  \vline\;\vline &    & \rule{0pt}{\hauteur}\\
 & & & \Rnode{min1}{f(\alpha)} & &   \phantom{~-\infty}\vline\;\vline  \Rnode{min2}{~-\infty} & &  \rule{0pt}{\hauteur} 
\ncline{->}{max1}{min1}  \ncline{->}{min1}{max2}
\ncline{->}{min2}{max3}\\
\hline
\end{array}$
}
\end{center}

$f(\alpha)\approx 19,85$
\end{enumerate}

%\bigskip
\newpage

\textbf{\large Exercice 2}

\medskip

Une entreprise de location de voitures sur circuit propose à ses clients deux types de voitures : voiture à moteur classique ou voiture électrique.
Par ailleurs, un client peut prendre l'option PILOTE. Dans ce cas, la voiture, qu'elle soit électrique ou à moteur classique, est louée avec un pilote.

\begin{list}{\textbullet}{On sait que:}
\item 60\,\% des clients choisissent une voiture électrique ; parmi eux, 20\,\% prennent l'option PILOTE ;
\item 42\,\% des clients prennent l'option PILOTE.
\end{list}

\medskip

\begin{list}{\textbullet}{On choisit au hasard un client et on considère les évènements:}
\item $E$ : le client choisit une voiture électrique;
\item $L$ : le client prend l'option PILOTE.
\end{list}

\medskip

\textbf{Partie A :}

\medskip

\begin{enumerate}
\item On traduit la situation par un arbre pondéré.

\begin{center}
%\bigskip
  \pstree[treemode=R,nodesepA=0pt,nodesepB=4pt,levelsep=3.5cm,nrot=:U]{\TR{}}
 {
 	\pstree[nodesepA=4pt]{\TR{$E$}\naput{$0,60$}}
 	  { 
 		  \TR{$L$}\naput{$0,20$}
 		  \TR{$\overline{L}$}\nbput{\blue $1-0,20=0,80$}	   
 	  }
 	\pstree[nodesepA=4pt]{\TR{$\overline{E}$}\nbput{\blue $1-0,60=0,40$}}
 	  {
 		  \TR{$L$}\naput{}
          \TR{$\overline{L}$}\nbput{} 
     }
}
%\bigskip
\end{center}

\item La probabilité que le client choisisse une voiture électrique et qu'il ne prenne pas l'option PILOTE est:
$P\left ( E\cap \overline{L} \right ) = P(E)\times P_{E}\left (\overline{L}\right ) 
= 0,60\times 0,80=0,48$.

\item La probabilité que le client choisisse une voiture à moteur classique et qu'il prenne l'option PILOTE est: $P\left ( \overline{E}\cap L\right )$.

On sait que 42\,\% des clients prennent l'option PILOTE, donc $P(L)=0,42$.

D'après la formule des probabilités totales:
$P(L)=P(E\cap L) + P \left ( \overline{E}\cap L\right )$.

$P(E\cap L)=0,6\times 0,2 = 0,12$ donc $0,42 = 0,12 + P \left ( \overline{E}\cap L\right )$ donc $P \left ( \overline{E}\cap L\right )=0,30$.

\item %En déduire $P_{\overline{E}}(L)$, probabilité de $L$ sachant que $E$ n'est pas réalisé.
$P_{\overline{E}}(L) = \dfrac{P\left ( \overline{E} \cap L\right )}{P\left ( \overline{E} \right )}
= \dfrac{0,30}{0,40}=0,75$

On peut compléter l'arbre pondéré.

\begin{center}
%\bigskip
  \pstree[treemode=R,nodesepA=0pt,nodesepB=4pt,levelsep=3.5cm,nrot=:U]{\TR{}}
 {
 	\pstree[nodesepA=4pt]{\TR{$E$}\naput{$0,60$}}
 	  { 
 		  \TR{$L$}\naput{$0,20$}
 		  \TR{$\overline{L}$}\nbput{$0,80$}	   
 	  }
 	\pstree[nodesepA=4pt]{\TR{$\overline{E}$}\nbput{$0,40$}}
 	  {
 		  \TR{$L$}\naput{\blue $0,75$}
          \TR{$\overline{L}$}\nbput{\blue $1-0,75=0,25$} 
     }
}
%\bigskip
\end{center}

\item Un client a pris l'option PILOTE.

La probabilité qu'il ait choisi une voiture électrique est:

$P_{L}(E)=\dfrac{P(E\cap L)}{P(L)} = \dfrac{0,12}{0,42}=\dfrac{2}{7}\approx 0,29$.

\end{enumerate}

\medskip

\textbf{Partie B :}

\medskip

Les arrondis se feront à $10^{-4}$ près.

Lorsqu'un client ne prend pas l'option PILOTE, la probabilité que sa voiture subisse un accident est égale à $0,12$. Cette probabilité est de $0,005$ si le client prend l'option PILOTE.

%\smallskip

On considère un client. On note $A$ l'évènement : sa voiture subit un accident.

%\medskip

\begin{enumerate}
\item On résume la situation par un arbre pondéré.

\begin{center}
\bigskip
  \pstree[treemode=R,nodesepA=0pt,nodesepB=4pt,levelsep=3.5cm,nrot=:U]{\TR{}}
 {
 	\pstree[nodesepA=4pt]{\TR{$L$}\naput{$0,42$}}
 	  { 
 		  \TR{$A$}\naput{$0,05$}
 		  \TR{$\overline{A}$}\nbput{\blue $1-0,05=0,95$}	   
 	  }
 	\pstree[nodesepA=4pt]{\TR{$\overline{L}$}\nbput{\blue $1-0,42=0,58$}}
 	  {
 		  \TR{$A$}\naput{$0,12$}
          \TR{$\overline{A}$}\nbput{\blue $1-0,12=0,88$} 
     }
}
\bigskip
\end{center}

$P(L \cap A)=0,42\times 0,05 = 0,021$ et 
$P\left(\overline{L} \cap A\right) = 0,58 \times 0,12 = \np{0,0696}$

\item L'entreprise loue \np{1000} voitures.

$P(A) = P(L \cap A) + P\left(\overline{L} \cap A\right) = 0,021 + \np{0,0696} = \np{0,0906}$

Il y a donc environ 9\;\% de risque d'accident, donc l'entreprise peut s'attendre à environ  90 accidents si elle loue $\np{1000}$ voitures.

%À combien d'accidents peut-elle s'attendre ?
\end{enumerate}

\bigskip

\textbf{\large Exercice 3}

\medskip

On considère la suite $\left(u_n\right)$ définie par $u_0 = 8$ et, pour tout entier naturel,
$u_{n+1} = \dfrac{6 u_n + 2}{u_n+ 5}.$

\begin{enumerate}
\item $u_1 = \dfrac{6 u_0 + 2}{u_0+ 5} = \dfrac{6\times 8 +2}{8+5} =\dfrac{50}{13}$

\item Soit $f$ la fonction définie sur l'intervalle $[0~;~ +\infty[$ par:
$f(x) = \dfrac{6x + 2}{x + 5}.$

%Ainsi, pour tout entier naturel $n$, on a: $u_{n+1}= f\left(u_n\right)$.

	\begin{enumerate}
		\item %Démontrer que la fonction $f$ est strictement croissante sur l'intervalle $[0~;~+\infty[$.
$f'(x)= \dfrac{6\times(x+5) - (6x+2)\times 1}{(x+2)^2}
= \dfrac{6x+30-6x-2}{(x+5)^2}
= \dfrac{28}{(x+5)^2}$

$f'(x)>0$ donc  la fonction $f$ est strictement croissante sur  $[0~;~+\infty[$.

%En déduire que pour tout réel $x > 2$,on a $f(x) > 2$.
On déduit que si $x>2$, on a $f(x)>f(2)$.
Or $f(2)=\dfrac{6\times 2+2}{2+5} = \dfrac{14}{7}=2$.

Donc pour tout réel $x > 2$,on a $f(x) > 2$.

		\item On va démontrer par récurrence que, pour tout entier naturel $n$, on a $u_n > 2$.
		
\begin{list}{\textbullet}{}
\item \textbf{Initialisation}

Pour $n=0$, $u_n=u_0=8>2$; donc la propriété est vraie au rang 0.
\item \textbf{Hérédité}

On suppose que la propriété est vraie au rang $n$, c'est-à-dire $u_n>2$.

La fonction $f$ est strictement croissante sur $[0\;;\;+\infty[$ donc $f(u_n)>f(2)$.

Or $f(u_n)=u_{n+1}$ et $f(2)=2$; on déduit que $u_{n+1}>2$ et donc que la propriété est vraie au rang $n+1$.
\item \textbf{Conclusion}

La propriété est vraie pour $n=0$ et elle est héréditaire. Donc, d'après le principe de récurrence, elle est vraie pour tout $n$.
\end{list}		
	\end{enumerate}
	
\item On admet que, pour tout entier naturel $n$, on a:
$u_{n+1} - u_n = \dfrac{\left(2 - u_n\right)\left(u_n + 1\right)}{u_n + 5}.$

	\begin{enumerate}
		\item %Démontrer que la suite $\left(u_n\right)$ est décroissante.
		
Pour tout $n$, on a $u_n>2$ donc $u_n+1>0$, $u_n+5>0$ et $2-u_n<0$		

Donc $\dfrac{\left(2 - u_n\right)\left(u_n + 1\right)}{u_n + 5}<0$ et donc $u_{n+1}-u_n <0$.

La suite $(u_n)$ est donc décroissante.
		
		\item% En déduire que la suite $\left(u_n\right)$ est convergente.
Pour tout $n$, $u_n>2$ donc  la suite $(u_n)$ est minorée par 2.

La suite $(u_n)$ est décroissante et minorée	 donc, d'après le théorème de la convergence monotone, on peut déduire que la suite $(u_n)$ est convergente.
		
		
	\end{enumerate}
\item On définit la suite $\left(v_n\right)$ pour tout entier naturel par : $v_n = \dfrac{u_n - 2}{u_n + 1}$.
	\begin{enumerate}
		\item $v_0  = \dfrac{u_0 - 2}{u_0 + 1} = \dfrac{8-2}{8+1}=\dfrac{6}{9}=\dfrac{2}{3}$
		
		\item %Démontrer que $\left(v_n\right)$  est une suite géométrique de raison $\dfrac47$.
$v_{n+1} = \dfrac{u_{n+1} - 2}{u_{n+1} + 1}
= \dfrac{\dfrac{6 u_n + 2}{u_n+ 5}-2}{\dfrac{6 u_n + 2}{u_n+ 5}+1}
= \dfrac{\dfrac{6 u_n + 2-2u_n-10}{u_n+ 5}}{\dfrac{6 u_n + 2 +u_n+5}{u_n+ 5}}
= \dfrac{4u_n - 8}{u_n+5}\times \dfrac{u_n+5}{7u_n+7}\\
\phantom{v_{n+1}}
= \dfrac{4\left (u_n-2\right )}{7\left ( u_n+1\right )}
=\dfrac{4}{7}\times \dfrac{u_n-2}{u_n+1}
=\dfrac{4}{7}v_n$

Donc $\left(v_n\right)$  est une suite géométrique de raison $\dfrac47$ et de premier terme $v_0=\dfrac{2}{3}$.
		
		\item %Déterminer, en justifiant, la limite de $\left(v_n\right)$. 
$\left(v_n\right)$  est une suite géométrique de raison $q=\dfrac47$ et de premier terme $v_0=\dfrac{2}{3}$ donc, pour tout $n$, on a $v_n=\dfrac{2}{3}\times \left (\dfrac{4}{7}\right )^n$.

Or $-1<\dfrac{4}{7}<1$, donc $\ds\lim_{n\to +\infty} v_n = 0$.
		
%En déduire la limite de $\left(u_n\right)$.
Pour tout $n$, $u_n-2 \neq u_n+1$ donc $\dfrac{u_n-2}{u_n+1}\neq 1$ donc $v_n \neq 1$.


$v_n = \dfrac{u_n - 2}{u_n + 1}
\iff v_n u_n + v_n = u_n - 2
\iff 2+v_n = u_n\left (1-v_n\right )
\iff \dfrac{2+v_n}{1-v_n} = u_n$

$\ds\lim_{n\to +\infty} v_n = 0$ donc
$\ds\lim_{n\to +\infty} 2+v_n = 2$ et
$\ds\lim_{n\to +\infty} 1-v_n = 0$;
donc $\ds\lim_{n\to +\infty} \dfrac{2+v_n}{1-v_n} = 2$
et donc $\ds\lim_{n\to +\infty} u_n=2$.
\end{enumerate}
\end{enumerate}

\newpage

\textbf{\large Exercice 4}

\medskip

L'espace est muni d'un repère orthonormal \Oijk.

\medskip

\textbf{Partie A}

\medskip

Soit $(\mathcal{P})$ le plan défini par le point M$(-1~;~1~;~0)$ et par le vecteur normal $\vect{n}$ de coordonnées $\begin{pmatrix}2\\3\\5\end{pmatrix}$. Soit A$(1~;~-4~;~5)$.

On veut déterminer la distance du point A au plan $(\mathcal{P})$, c'est-à-dire la distance
AH, où H est le projeté orthogonal de A sur $(\mathcal{P})$.

\begin{enumerate}
\item% Exprimer $\vectt{AM} \cdot \vect{n}$ en fonction de la distance AH.
$\vectt{AM} \cdot \vect{n}
= \left (\vectt{AH}+\vectt{HM}\right ) \cdot \vect{n}
=\vectt{AH}\cdot \vect{n}+\vectt{HM} \cdot \vect{n}$

Or H et M sont deux points du plan $(\mathcal{P})$ dont $\vect{n}$ est un vecteur normal, donc $\vectt{HM}\perp \vect{n}$ et donc $\vectt{HM} \cdot \vect{n}=0$.

On en déduit que $\vectt{AM} \cdot \vect{n} =\vectt{AH}\cdot \vect{n}$.

Le point H est le projeté orthogonal du point A sur le plan $(\mathcal{P})$, donc les vecteurs $\vectt{AH}$ et $\vect{n}$ sont colinéaires.

\begin{list}{\textbullet}{On en déduit que:}
\item $\vectt{AH}\cdot \vect{n}= \text{AH}\times \left |\left |\vect{n}\right |\right |$ si $\vectt{AH}$ et $\vect{n}$ sont de même sens;
\item $\vectt{AH}\cdot \vect{n}= -\text{AH}\times \left |\left |\vect{n}\right |\right |$ si $\vectt{AH}$ et $\vect{n}$ sont de sens contraire.
\end{list}

$\left |\left |\vect{n}\right |\right |= \ds\sqrt{2^2+3^2+5^2}= \sqrt{38}$

On peut donc en déduire que $\left|\vectt{AM} \cdot \vect{n}\right| = \text{AH}\sqrt{38}$.

\item %En déduire la distance A au plan $(\mathcal{P})$.
$\vectt{AM}$ a pour coordonnées
$\begin{pmatrix} -1-1\\ 1-(-4) \\ 0-5 \end{pmatrix}
=
\begin{pmatrix} -2 \\ 5 \\ -5 \end{pmatrix}$
et $\vect{n}$ a pour coordonnées $\begin{pmatrix}2\\3\\5\end{pmatrix}$.

Donc 
$\vectt{AM} \cdot \vect{n} = -2\times 2 + 5\times 3 +(-5)\times 5 = -14$.

$\left|\vectt{AM} \cdot \vect{n}\right| = \text{AH}\sqrt{38}$
équivaut à
$14=\text{AH}\sqrt{38}$
c'est-à-dire
$\text{AH} = \dfrac{14}{\sqrt{38}}$.
\end{enumerate}

\medskip

\textbf{Partie B}

\medskip

Soit $(\mathcal{P})$ le plan défini par le point $M(x~;~y~;~z)$ et par le vecteur normal $\vect{n}$ de coordonnées $\begin{pmatrix}a\\b\\c\end{pmatrix}$.
Soit $A\left(x_{A}~;~y_{A}~;~z_{A}\right)$ un point de l'espace et $H$ son projeté orthogonal sur le plan $(\mathcal{P})$.

%\medskip

\begin{enumerate}
\item %Exprimer $\vect{AM} \cdot \vect{n}$ en fonction de $AH,\: a,\: b$ et $c$.
$\vect{AM} \cdot \vect{n} = \left (\vect{AH}+\vect{HM}\right ) \cdot \vect{n} =\vect{AH}\cdot \vect{n}+\vect{HM} \cdot \vect{n}$

Or $H$ et $M$ sont deux points du plan $(\mathcal{P})$ dont $\vect{n}$ est un vecteur normal, donc $\vect{HM}\perp \vect{n}$ et donc $\vect{HM} \cdot \vect{n}=0$.

On en déduit que $\vect{AM} \cdot \vect{n} =\vect{AH}\cdot \vect{n}$.

Le point $H$ est le projeté orthogonal du point $A$ sur le plan $(\mathcal{P})$, donc les vecteurs $\vect{AH}$ et $\vect{n}$ sont colinéaires.

$\left |\left |\vect{n}\right |\right |= \ds\sqrt{a^2+b^2+c^2}$

\begin{list}{\textbullet}{On en déduit que:}
\item $\vect{AH}\cdot \vect{n}= AH\times \ds\sqrt{a^2+b^2+c^2}$ si $\vect{AH}$ et $\vect{n}$ sont de même sens;
\item $\vect{AH}\cdot \vect{n}= -AH\times \ds\sqrt{a^2+b^2+c^2}$ si $\vect{AH}$ et $\vect{n}$ sont de sens contraire.
\end{list}

On peut donc en déduire que $\left|\vect{AM} \cdot \vect{n}\right| = AH \times \ds\sqrt{a^2+b^2+c^2}$.

\item% Montrer que $\left | \vect{AM} \cdot \vect{n}\right | = \left|ax_{A}+ by_{A} + cz_{A} -d\right|$ où $d$ est une constante à préciser.
$\vect{AM}$ a pour coordonnées 
$\begin{pmatrix} x-x_A \\ y-y_A \\ z-z_A \end{pmatrix}$
et $\vect{n}$ a pour coordonnées
$\begin{pmatrix} a \\ b \\ c  \end{pmatrix}$.

$\text{Donc }\vect{AM} \cdot \vect{n} = a\left (x-x_A\right ) + b\left ( y-y_A\right ) + c\left ( z-z_A \right )\\
\phantom{\text{Donc }\vect{AM} \cdot \vect{n}}
= -ax_A -by_A -cz_A +  ax +by +cz \\
\phantom{\text{Donc }\vect{AM} \cdot \vect{n}}
= -\left (ax_A +by_A +cz_A -\left ( ax +by +cz \right )\right )$

$\text{et donc }\left | \vect{AM} \cdot \vect{n} \right | 
= \left | -\left (ax_A +by_A +cz_A - \left ( ax +by +cz \right )\right ) \right |\\
\phantom{\text{et donc }\left | \vect{AM} \cdot \vect{n} \right | }
= \left | ax_A +by_A +cz_A - \left ( ax +by +cz \right ) \right |\\
\phantom{\text{et donc }\left | \vect{AM} \cdot \vect{n} \right | }
= \left | ax_A +by_A +cz_A +d \right |$

avec $d=- \left ( ax +by +cz \right )$.

\item %Exprimer alors la distance de $A$ à $(\mathcal{P})$ en fonction de $x_{A},\: y_{A}, \:z_{A},\: a,\: b,\: c$ et $d$.
On a vu que: $\left|\vect{AM} \cdot \vect{n}\right| = AH \times \ds\sqrt{a^2+b^2+c^2}$ et que: $\left|\vect{AM} \cdot \vect{n}\right| = \left | ax_A +by_A +cz_A +d \right |$.

On en déduit que $AH \times \ds\sqrt{a^2+b^2+c^2}  = \left | ax_A +by_A +cz_A +d \right |$, et donc que: 

$AH = \dfrac{\left | ax_A +by_A +cz_A +d \right |}{\ds\sqrt{a^2+b^2+c^2} }$.
\end{enumerate}

\end{document}