\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Merci à Biram Ndiaye pour le sujet
\usepackage{pst-plot,pst-text,pst-eucl,pst-node,pst-circ,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS groupement B2},
pdftitle = {14 mai 2019},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Groupement B2}}
\rfoot{\small{13 mai 2019}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center}
{\Large\textbf{Brevet de technicien supérieur groupement B2 \\[5pt] 13 mai 2019 - Métropole--Antilles--Guyane--Polynésie}}
  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 10 points}

\medskip

Lorsqu'un fil électrique est parcouru par un courant électrique d'intensité constante, celui-ci s'échauffe par effet Joule et sa température varie en fonction du temps. On note $f(t)$ la température, exprimée en degré Celsius, du conducteur à l'instant $t$, exprimé en seconde, avec $t$ variant dans l'intervalle $[0~;~ + \infty[$.

\smallskip

Dans cet exercice, on se propose d'étudier l'évolution de la température du conducteur en
fonction du temps.

\begin{center}

\textbf{Les trois parties de cet exercice peuvent être traitées de façon indépendante}\end{center}

\medskip

\textbf{A. Résolution d'une équation différentielle}

\medskip

À l'instant $t = 0$ de la mise sous tension, la température du conducteur est celle du milieu ambiant, c'est-à-dire 18 degrés Celsius. Ainsi, on a $f(0) = 18$.

Dans les conditions de l'expérience, la fonction $f$ est solution de l'équation différentielle $(E)$ :

\[y' + 0,05y = 2,\]

où $y$ est une fonction inconnue de la variable $t$, définie et dérivable sur l'intervalle $[0~;~ + \infty[$, et
$y'$ sa fonction dérivée.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Résoudre l'équation $\left(E_0\right)\: :\quad  y' + 0,05y = 0$.
		
On fournit la formule suivante :
		\begin{center}
\begin{tabularx}{\linewidth}{|*{2}{>{\centering \arraybackslash}X|}}\hline
Équation différentielle &Solutions sur un intervalle $I$\\ \hline
$ay' + by = 0$&$f(t) = k \text{e}^{- \frac{b}{a} t}$\\ \hline
\end{tabularx}
\end{center}

		\item Vérifier que la fonction $g$ définie sur $[0~;~ +\infty[$ par $g(t) = 40$ est une solution de l'équation différentielle $(E)$.
		\item En déduire les solutions définies sur l'intervalle $[0~;~+ \infty[$ de l'équation différentielle $(E)$.
	\end{enumerate}
\item  On rappelle que la température initiale du conducteur est $18$\degres{} Celsius. 
	
Ainsi, la fonction $f$ exprimant la température du conducteur est la solution de l'équation différentielle $(E)$ vérifiant la condition initiale $f(0) = 18$.
	
Déterminer alors une expression de la fonction $f$.
\end{enumerate}

\bigskip

\textbf{B. Étude de la fonction }\boldmath $f$\unboldmath

\medskip

On admet que la fonction donnant la température du conducteur est la fonction $f$ définie sur $[0~;~+ \infty[$ par:

\[f(t) = -22 \text{e}^{-0,05t} + 40.\]

On note $C$ la courbe représentative de la fonction $f$ dans le plan muni d'un repère orthogonal.

La courbe $C$ est tracée en annexe.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item On admet que $\displaystyle\lim_{t \to + \infty} \text{e}^{-0,05t} = 0$.
		
Déterminer alors la limite de la fonction $f$ en $+ \infty$.
		\item En déduire que la courbe $C$ admet une asymptote dont on donnera une équation.

Tracer cette asymptote sur la représentation graphique donnée en annexe.
	\end{enumerate}
\item Un logiciel de calcul formel permet d'obtenir ci-dessous une expression de la dérivée $f'$ de la fonction $f$.

\begin{center}
\begin{tabular}{l  c l}
1			& $f(t)$			&:= $-22\text{e}^{-0,05t} + 40$\\
$\bullet$	&$\to  f(t)$ 		&:= $-22 \text{e}^{-\frac{1}{20}t} + 40$\\ \hline
2			&Dérivée$(f(t), t)$	&\\
			&$\to$				& $\dfrac{11}{10}\text{e}^{-\frac{1}{20}t}$\\ \hline
\end{tabular}
\end{center}

	\begin{enumerate}
		\item En admettant ce résultat, étudier les variations de la fonction $f$ sur $[0~;~+ \infty[$.
		\item Dresser le tableau de variation de $f$ sur $[0~;~+ \infty[$.
	\end{enumerate}	
\item Ce même logiciel de calcul formel affiche la partie régulière du développement limité à l'ordre 2 de la fonction $f$ au voisinage de zéro.

\begin{center}
\begin{tabular}{l  l}\hline
3&PolynômeTaylor$(f(t), t, 0, 2)$\\
 &$\to 18 + \dfrac{11}{10} t - \dfrac{11}{400} t^2$\\ \hline
\end{tabular}
\end{center}

\emph{Les deux questions suivantes sont des questions à choix multiples. Une seule réponse est exacte. Recopier sur la copie la réponse qui vous parait exacte. On ne demande aucune justification.\\
La réponse juste rapporte $0,5$ point. Une réponse fausse ou une absence de réponse ne
rapporte ni n'enlève de point.}

	\begin{enumerate}
		\item Une équation de la tangente $T$ à la courbe $C$ au point d'abscisse $0$ est :
		
\begin{center}
\begin{tabularx}{\linewidth}{|*{3}{>{\centering \arraybackslash}X|}}\hline
$y = 18$&$y = 18 + \dfrac{11}{10}t$&$y = 18 + \dfrac{11}{10}t - \dfrac{11}{400}t^2$\rule[-3mm]{0mm}{9mm}\\ \hline
\end{tabularx}
\end{center}
		\item La vitesse de chauffe, exprimée en degré Celsius par seconde, à l'instant initial est égale à $f'(0)$. Cette vitesse vaut :
\begin{center}
\begin{tabularx}{\linewidth}{|*{3}{>{\centering \arraybackslash}X|}}\hline
18&$\dfrac{11}{10}$&$\dfrac{11}{400}$\rule[-3mm]{0mm}{9mm}\\ \hline
\end{tabularx}
\end{center}
 	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{C. Dépassement d'un seuil et algorithmique}

\medskip

On cherche à déterminer le premier instant $t$, en seconde, à partir duquel la température du fil du conducteur dépasse $21$\degres Celsius.

À cette fin, on considère l'algorithme suivant.

\begin{center}
\begin{tabular}{l}
$t \gets 0$\\
Tant que $f(t) \leqslant  21$\\
\hspace{1cm} $t \gets t + 1$\\ 
Fin de Tant que\\
\end{tabular}
\end{center}

Remarque : dans cet algorithme $t \gets 0$ signifie que $t$ prend la valeur $0$.

\medskip

\begin{enumerate}
\item Faire tourner cet algorithme \og à la main \fg{} en complétant le tableau donné en annexe.
\item Quelle sera la valeur contenue dans la variable $t$ à la fin de l'algorithme ?

Interpréter ce résultat dans le contexte de l'exercice.
\end{enumerate}

\vspace{0,5cm}

\textbf{EXERCICE 2 \hfill 10 points}

\medskip

Dans les entreprises, l'énergie électrique est fournie par des onduleurs qui alimentent une multitude de récepteurs (ordinateurs, lampes basse consommation ... ) générant des courants harmoniques. Sans une installation adaptée et une utilisation de récepteurs optimisés, l'accumulation d'harmoniques de rangs multiples de 3 conduit au déséquilibre du triphasé. Cela peut engendrer de graves problèmes: surchauffe du fil portant le neutre, phénomène d'interférence, augmentation des pertes d'énergie, ouverture des fusibles ou interruptions automatiques. 

\bigskip

\textbf{A. Représentation graphique d'un signal déphasé}

\medskip 

Un onduleur à commande asynchrone délivre une tension périodique $f(t)$ de période $2\pi$ selon la représentation graphique suivante. 
\begin{center}
\psset{xunit=0.48cm,yunit=0.35cm}
\begin{pspicture}(-13,-5)(13,7)
\psgrid[griddots=7,linewidth=0.2pt,subgriddiv=1,gridlabels=0](-13,-5)(13,5)
\rput(0,6.4){Représentation graphique de $t \mapsto f(t)$}
\psaxes[linewidth=1.25pt,Dx=20,Dy=10]{->}(0,0)(-13,-5)(13,5) 
\uput[l](0,2){0,5}\uput[l](0,4){1}\uput[l](0,-4){-1}
\uput[d](-12,0){$-2\pi$} \uput[d](-6,0){$-\pi$}\uput[d](-2,0){$- \frac{\pi}{3}$}
\uput[d](2,0){$\frac{\pi}{3}$}\uput[d](6,0){$\pi$}\uput[d](12,0){$2\pi$}
\psset{linecolor=blue,linewidth=1.2pt}
\psline{-(}(-13,4)(-9,4)\psline{*-(}(-3,4)(3,4)\psline{*-}(9,4)(13,4)
\psline{*-(}(-9,-4)(-3,-4)\psline{*-(}(3,-4)(9,-4)
\end{pspicture}
\end{center}
Une représentation graphique de la fonction $t \mapsto f(t)$ est redonnée en annexe 2. 

\medskip

\begin{enumerate}
\item À l'aide de la représentation graphique de la fonction $f$, compléter le tableau de valeurs donné en annexe 2. 
\item Une représentation graphique de la fonction $t \mapsto f\left(t + \dfrac{2\pi}{3}\right)$ est donnée en annexe 2. 

Construire en annexe 2 la représentation graphique de la fonction$t \mapsto f\left(t + \dfrac{4\pi}{3}\right)$ sur l'intervalle $[- 2 \pi~;~2\pi]$ dans le repère prévu à cet effet. 
\item En régime triphasé, l'onduleur soumet la phase 1 à la tension $f(t)$, la phase 2 à la tension $f\left(t + \dfrac{2\pi}{3}\right)$ et la phase 3 à la tension $f\left(t + \dfrac{4\pi}{3}\right)$. Le neutre est soumis à la somme $S(t)$ des tensions des trois phases définie par: 

\[S(t) = f(t) + f\left(t + \dfrac{2\pi}{3}\right) + f\left(t + \dfrac{4\pi}{3}\right).\] 

La système triphasé est équilibré si, pour tout nombre réel $t$,\, $S(t) = 0$. 
	\begin{enumerate}
		\item Calculer $S(0)$. 
		\item Le système triphasé étudié dans cette partie est-il équilibré ? 
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{B. Développement en série de Fourier }

\medskip

Pour garantir l'équilibrage d'un système triphasé, on peut utiliser un ondule ur à commande décalée. Ainsi, nous considérons dans cette partie que la tension délivrée est un signal $g$ de période $2\pi$ dont une représentation graphique figure ci-dessous. 


\begin{center}
\psset{xunit=0.48cm,yunit=0.35cm}
\begin{pspicture}(-13,-5)(13,7)
\psgrid[griddots=7,linewidth=0.2pt,subgriddiv=1,gridlabels=0](-13,-5)(13,5)
\rput(0,6.4){Représentation graphique de $t \mapsto g(t)$}
\psaxes[linewidth=1.25pt,Dx=20,Dy=10]{->}(0,0)(-13,-5)(13,5) 
\uput[l](0,2){0,5}\uput[l](0,4){1}\uput[l](0,-4){-1}
\uput[d](-12,0){$-2\pi$} \uput[d](-6,0){$-\pi$}\uput[d](-2,0){$- \frac{\pi}{3}$}
\uput[d](2,0){$\frac{\pi}{3}$}\uput[d](6,0){$\pi$}\uput[d](12,0){$2\pi$}
\psset{linecolor=blue,linewidth=1.5pt}
\psline{-(}(-13,4)(-10,4)\psline{*-(}(-2,4)(2,4)\psline{*-}(10,4)(13,4)
\psline{*-(}(-10,0)(-8,0)\psline{*-(}(-4,0)(-2,0)\psline{*-(}(2,0)(4,0)\psline{*-(}(8,0)(10,0)
\psline{*-(}(-8,-4)(-4,-4)\psline{*-(}(4,-4)(8,-4)
\uput[u](12.8,0){$t$}
\end{pspicture}
\end{center}

Dans la suite de l'exercice, $a_0,\, a_n$ et $b_n$ désignent les coefficients du développement en série de Fourier de la fonction $g$ avec les notations du formulaire donné en bas de page. 

\medskip

\begin{enumerate}
\item Donner la parité de la fonction $g$. En déduire la valeur des coefficients $b_n$ pour tout entier naturel $n$ non nul. 
\item Donner la valeur de $g(t)$ sur chacun des trois intervalles $\left[0~;~\dfrac{\pi}{3}\right[, \,\left[\dfrac{\pi}{3}~;~\dfrac{2\pi}{3}\right[$ et $\left[\dfrac{2\pi}{3}~;~\pi\right[$.
\item 
	\begin{enumerate}
		\item Déterminer $a_0$. 
		\item Un logiciel de calcul formel fournit, pour tout entier naturel $n$ non nul: 

\[a_n = \dfrac{2}{\pi} \times \dfrac{\sin \left(\dfrac{n\pi}{3} \right) + \sin \left(\dfrac{2n\pi}{3} \right)}{n}. 
\]

En admettant le résultat précédent, justifier que, pour tout entier naturel $k$ non nul, $a_{3k} = 0$. 
		\item On démontre que ce qui empêche un système d'être équilibré est la présence d'harmoniques non nulles de rangs multiples de 3 dans le développement en série de Fourier de la fonction $g$. 

En admettant cette règle, peut-on considérer que le système est équilibré ? 
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Formulaire pour les séries de Fourier} 

\medskip

\begin{tabularx}{\linewidth}{|X|}\hline
$f$ fonction périodique de période $T = \dfrac{2\pi}{\omega}$.\\
Développement en série de Fourier: \\
\multicolumn{1}{|c|}{$s(t) = a_0 + \displaystyle\sum_{n=1}^{+ \infty}\left(a_n \cos(n \omega t)+ b_n \sin (n \omega t)\right)$,}\\
$a_0 = \dfrac{1}{T}\displaystyle\int_{a}^{a+T} f(t)\:\text{d}t ; \quad a_n = \dfrac{2}{T}\displaystyle\int_{a}^{a+T} f(t)\cos (n \omega t)\:\text{d}t\:\:(n \in \N)$\\
$b_n = \dfrac{2}{T}\displaystyle\int_{a}^{a+T} f(t)\sin (n \omega t)\:\text{d}t\:\:(n \in \N)$\\ \hline
\end{tabularx}

\newpage

\begin{center}
\textbf{ANNEXE 1 À RENDRE AVEC LA COPIE \\[5pt]
EXERCICE 1 QUESTION B. 1. b.} 

\bigskip

\psset{xunit=0.1cm,yunit=0.25cm}
\begin{pspicture}(-8,-3)(112,45)
\multido{\n=0+2}{58}{\psline[linewidth=0.2pt](\n,0)(\n,48)}
\multido{\n=0+10}{11}{\psline[linewidth=0.4pt](\n,0)(\n,48)}
\multido{\n=0+1}{49}{\psline[linewidth=0.2pt](0,\n)(112,\n)}
\multido{\n=0+1}{10}{\psline[linewidth=0.4pt](0,\n)(112,\n)}
\psaxes[linewidth=1.25pt,Dx=10,Dy=5](0,0)(0,0)(112,45)
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=blue]{0}{112}{40 22 2.71828 0.05 x mul exp div sub}
\end{pspicture}

\vspace{2cm}

\textbf{EXERCICE 1 QUESTION C. 1.}

\medskip

\begin{tabularx}{\linewidth}{|*{3}{>{\centering \arraybackslash}X|}}\hline
Valeur de $t$& Valeur de $f(t)$ arrondie à $10^{-2}$&Condition $f(t) \leqslant 21$\\\hline
0&18&Vraie\\ \hline
1&&\\ \hline
&&\\ \hline
&&\\ \hline
\end{tabularx}
\end{center}

\newpage

\begin{center}
\textbf{ANNEXE 2 À RENDRE AVEC LA COPIE}

Représentation graphique de $t \longmapsto f(t)$ :

\medskip

\psset{xunit=0.48cm,yunit=0.35cm}
\begin{pspicture}(-13,-5)(13,5)
\psgrid[griddots=7,linewidth=0.2pt,subgriddiv=1,gridlabels=0](-13,-5)(13,5)
%\rput(0,6.4){Représentation graphique de $t \mapsto g(t)$}
\psaxes[linewidth=1.25pt,Dx=20,Dy=10]{->}(0,0)(-13,-5)(13,5) 
\uput[l](0,2){0,5}\uput[l](0,4){1}\uput[l](0,-4){-1}
\uput[d](-12,0){$-2\pi$} \uput[d](-6,0){$-\pi$}\uput[d](-2,0){$- \frac{\pi}{3}$}
\uput[d](2,0){$\frac{\pi}{3}$}\uput[d](6,0){$\pi$}\uput[d](12,0){$2\pi$}
\psset{linecolor=blue,linewidth=1.2pt}
\psline{-(}(-13,4)(-9,4)\psline{*-(}(-3,4)(3,4)\psline{*-}(9,4)(13,4)
\psline{*-(}(-9,-4)(-3,-4)\psline{*-(}(3,-4)(9,-4)
\uput[u](12.5,0){$t$}
\end{pspicture}

\medskip

\textbf{EXERCICE 2 QUESTION A. 1.}

\medskip

\begin{tabularx}{\linewidth}{|*{8}{>{\centering \arraybackslash}X|}}\hline
$t$&$- \dfrac{4\pi}{3}$&$- \pi$&0&$\dfrac{\pi}{3}$&$\pi$&$\dfrac{4\pi}{3}$&$\dfrac{5\pi}{3}$\rule[-3mm]{0mm}{9mm}\\ \hline
$t + \dfrac{4\pi}{3}$\rule[-3mm]{0mm}{9mm}&&&&&&&\\ \hline
$f\left(t + \dfrac{4\pi}{3}\right)$&\rule[-3mm]{0mm}{9mm}&&&&&&\\ \hline
\end{tabularx}

\bigskip

\textbf{EXERCICE 2 QUESTION A. 2.}

Représentation graphique de $t\longmapsto  f\left(t + \dfrac{2\pi}{3}\right)$ :

\psset{xunit=0.48cm,yunit=0.35cm}
\begin{pspicture}(-13,-5)(13,7)
\psgrid[griddots=7,linewidth=0.2pt,subgriddiv=1,gridlabels=0](-13,-5)(13,5)
\psaxes[linewidth=1.25pt,Dx=20,Dy=10]{->}(0,0)(-13,-5)(13,5) 
\uput[l](0,2){0,5}\uput[l](0,4){1}\uput[l](0,-4){-1}
\uput[d](-12,0){$-2\pi$} \uput[d](-6,0){$-\pi$}\uput[d](-2,0){$- \frac{\pi}{3}$}
\uput[d](2,0){$\frac{\pi}{3}$}\uput[d](6,0){$\pi$}\uput[d](12,0){$2\pi$}
\psset{linecolor=blue,linewidth=1.2pt}
\psline{*-(}(-7,4)(-1,4)\psline{*-(}(5,4)(11,4)
\psline{*-(}(-13,-4)(-7,-4)\psline{*-(}(-1,-4)(5,-4)\psline{*-}(11,-4)(13,-4)
\end{pspicture}

\bigskip

Repère pour la représentation de $t\longmapsto  f\left(t + \dfrac{4\pi}{3}\right)$ :

\psset{xunit=0.48cm,yunit=0.35cm}
\begin{pspicture}(-13,-5)(13,5)
\psgrid[griddots=7,linewidth=0.2pt,subgriddiv=1,gridlabels=0](-13,-5)(13,5)
\psaxes[linewidth=1.25pt,Dx=20,Dy=10]{->}(0,0)(-13,-5)(13,5) 
\uput[l](0,2){0,5}\uput[l](0,4){1}\uput[l](0,-4){-1}
\uput[d](-12,0){$-2\pi$} \uput[d](-6,0){$-\pi$}\uput[d](-2,0){$- \frac{\pi}{3}$}
\uput[d](2,0){$\frac{\pi}{3}$}\uput[d](6,0){$\pi$}\uput[d](12,0){$2\pi$}
\psset{linecolor=blue,linewidth=1.2pt}
\uput[u](12.5,0){$t$}
\end{pspicture}
\end{center}
\end{document}