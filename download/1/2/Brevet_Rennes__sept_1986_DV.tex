\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{lscape}
\usepackage{multicol}
\usepackage{diagbox}
\usepackage{multirow} 
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pstricks,pst-plot,pst-text,pst-tree,pstricks-add}
\usepackage{pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm,headheight=14pt]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
%\usepackage[colorlinks=true,pdfstartview=FitV,linkcolor=blue,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Brevet},
pdftitle = {Rennes septembre 1986},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[french]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{}
\lhead{\small L'année 1986}
\rfoot{\small Rennes}
\lfoot{\small septembre 1986}
\pagestyle{fancy}
\thispagestyle{empty}
\begin{center} {\Large \textbf{\decofourleft~Brevet Rennes septembre 1986 \decofourright}}
\end{center}

\bigskip

\textbf{Activités numériques}

\medskip

\textbf{Exercice 1}

\medskip

Calculer les nombres suivants.

On demande les valeurs exactes les plus simples possibles et non des valeurs approchées.

\begin{center}
\renewcommand\arraystretch{2}
\begin{tabularx}{\linewidth}{X X}
$\dfrac75 + 0,12 =$ 					&$\left(3\sqrt 2 + 7\right)^2 =$\\
$\dfrac43 + 12$							&$\left(2\sqrt 7\right)^3 =$\\
$|7 - 9,2| -|-13| = $					&$3^2 + \left(- \dfrac12\right)^2 =$\\
$\dfrac{1- \dfrac14}{1 + \dfrac14}=$	&$\sqrt{169-25} = $\\
$5\times  10^{-3} + 8 \times 10^{-2}= $	&$11\sqrt 5 + 7\sqrt{75} = $\\
$\dfrac{4 \times 10^2}{5 \times 10^{-3}}=$&$\sqrt{\dfrac{81}{49}} =$\\
$19 - 9 \times 0,2^2$					&$\dfrac{\sqrt{72}}{\sqrt 2}$\\
\end{tabularx}
\renewcommand\arraystretch{1}
\end{center}

\medskip

\textbf{Exercice 2}

\medskip

Voici une partie de la représentation graphique (ci-dessous) d'une application de $\R$ dans $\R$.

\medskip

\begin{enumerate}
\item Donner les coordonnées des points A, B et C.
\item Quelles sont les images des nombres : $-1$ ; 0 ; $\sqrt 2$ ?
\item Quels sont les antécédents des nombres: 1 ; 2 ; 3 ?
\end{enumerate}

\begin{center}
\psset{unit=5mm,arrowsize=2pt 3}
\begin{pspicture}(-8,-5)(11,10)
\psgrid[gridlabels=0pt,subgriddiv=1,gridwidth=0.1pt]
\psaxes[linewidth=1.25pt,Dx=25,Dy=25](0,0)(-9,-5)(11,10)
\psaxes[linewidth=1.25pt,Dx=25,Dy=25]{->}(0,0)(1,1)
\psline[linewidth=1.5pt](-6,8)(-2,4)(2,6)(4,6)(7,0)(9.5,-5)
\uput[d](-2,4){A}\uput[u](2,6){B}\uput[dl](7,0){C}\uput[u](10.5,0){$x$}
\uput[l](0,9.5){$y$}\uput[dl](0,0){O}\uput[d](0.5,0){$\vect{\imath}$}\uput[l](0,0.5){$\vect{\jmath}$}
\end{pspicture}
\end{center}

\bigskip

\textbf{Activités géométriques}

\medskip

\textbf{Exercice 1}

\medskip

\begin{enumerate}
\item Construire un triangle (ABC) tel que

\begin{center}AB = 6,\qquad  AC = 8,\qquad  BC = 4\: (unité : 1 cm).\end{center}

\item Construire le point D tel que  (ABDC) soit un parallélogramme.
\item Placer le milieu I de [BD] et le symétrique E de B par rapport à C.
\item Soit K le point d'intersection des droites (CD) et (IE).

Que représente K pour le triangle (BDE) ? Expliquer votre réponse.
\end{enumerate}

\medskip

\textbf{Exercice 2}

\medskip

Tracer un triangle (ABC) rectangle en A tel que AB = 4, AC = 6  (unité : 1~cm).

Soit M le milieu de [AC).

\medskip

\begin{enumerate}
\item Calculer la longueur BM.
\item Calculer à un degré près l'angle $\widehat{\text{ABM}}$ et en déduire

l'angle $\widehat{\text{AMB}}$ puis l'angle $\widehat{\text{BMC}}$.

Pour cela on donne les tangentes des angles suivants : 

\begin{center}
\begin{tabular}{l l}
tg $35\degres = 0,700$\:;& tg $37\degres = 0,754$\:;\\
tg $36\degres = 0,727$\:;& tg $38\degres = 0,781$.\\
\end{tabular}
\end{center}
\item Construire le trapèze (BMCE) tel que E appartienne à la droite (AB). 

Déterminer la longueur BE.
\end{enumerate}

\bigskip

\textbf{Problème}

\medskip

On considère les applications $f$ et $g$ de $\R$ dans $\R$ définies par

\begin{center}$f(x) = (2x + 3)^2 - (x + 6)^2 \:;\quad
g(x) = (x + 3)(5 - x) + (2x + 6).$\end{center}


\begin{enumerate}
\item Factoriser $f(x)$ et $g(x)$.
\item Développer et réduire $f(x)$ et $g(x)$.
\item En utilisant les expressions de $f(x)$ et $g(x)$ qui vous
semblent les mieux adaptées, résoudre dans $\R$ les équations :

\begin{center}$f(x) = 0\:;\quad g(x) = 21\:;\quad f(x) = g(x).$ \end{center}

\item Résoudre dans $\R$ les systèmes d'inéquations simultanées:

\[\left\{\begin{array}{l c l}
x + 3 &\geqslant&0\\
 7 -x &\geqslant &0
\end{array}\right.\:\text{puis}\qquad \: \left\{\begin{array}{l c l}
x + 3 &\leqslant& 0\\
 7 - x&\leqslant& 0.
\end{array}\right.\]

En déduire les valeurs de $x$ pour lesquelles $g(x)$ est positif ou nul.
\end{enumerate}
\end{document}