%!TEX encoding = UTF-8 Unicode
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{graphicx}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength{\textheight}{23,5cm}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\setlength{\voffset}{-1,5cm}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\setlength\parindent{0mm}
\begin{document}
\lhead{\small Brevet de technicien supérieur }
\lfoot{\small{Géomètre topographe}}
\rfoot{\small{juin 2004}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}
\begin{center}\Large\textbf{\decofourleft~Brevet de technicien supérieur~\decofourright\\Géomètre topographe session 2004}  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 10 points}

\medskip

\emph{Le but de cet exercice est l'étude d'une courbe plane, appelée lemniscate, que l'on peut rencontrer lors de l'élaboration de certains raccordements routiers.}

\medskip

Le plan est muni d'un repère orthonormal direct \Oij{} (unité 5~cm). On note $\mathcal{C}$ la courbe définie en coordonnées polaires par  :

\[r = f(\theta) = \sqrt{\sin (2\theta)}~~ \text{pour}~~ \theta \in  \left[0~;~\dfrac{\pi}{2}\right] \cup  \left[\pi~;~\dfrac{3\pi}{2}\right]\]

\begin{enumerate}
\item  Soit $\theta$ un réel de l'intervalle $\left[0~;~\dfrac{\pi}{2}\right]$.
	\begin{enumerate}
		\item  À quel intervalle $\theta + \pi$ appartient-il ?
		\item Calculer $f(\theta + \pi)$ en fonction de $f(\theta)$  ;  en déduire une propriété de symétrie de la courbe $\mathcal{C}$.
	\end{enumerate}
\item Soit $\theta$ un réel de l'intervalle $\left[0~;~\dfrac{\pi}{4}\right]$.
	\begin{enumerate}
		\item À quel intervalle $\dfrac{\pi}{2} - \theta$	 appartient-il ?
		\item Calculer $f(\theta + \pi)$ en fonction de $f(\theta)$ ; en déduire une propriété de symétrie de la courbe $\mathcal{C}$.
	\end{enumerate}
\item Soit $\mathcal{C}_{1}$ l'arc de courbe obtenu pour $\theta \in  \left[0~;~\dfrac{\pi}{4}\right]$. Comment obtient-on $\mathcal{C}$ à partir de $\mathcal{C}_{1}$ ?
\item Montrer que pour tout $\theta$ appartenant à $\left[0~;~\dfrac{\pi}{4}\right],~r' = f'(\theta) = \dfrac{\cos 2\theta}{\sqrt{\sin 2\theta}}$.
\item Étudier le signe de $f'(\theta)$ sur $\left[0~;~\dfrac{\pi}{4}\right]$. En déduire les variations de $f$ pour $\theta \in  \left[0~;~\dfrac{\pi}{4}\right]$, puis dresser le tableau de variations de $f$ sur ce dernier intervalle.
\item  On pose, pour tout $\theta \in  \left[0~;~\dfrac{\pi}{4}\right],~ x = r \cos \theta$ et $y = r \sin \theta$. Montrer que :

\[x' = \dfrac{\cos 3\theta}{\sqrt{\sin 2 \theta}}~~	 \text{et}~~ y' =  \dfrac{\sin 3\theta}{\sqrt{\sin 2 \theta}}.\]

\item  Déterminer un vecteur directeur de chacune des deux tangentes suivantes à la courbe $\mathcal{C}$, l'une au point A de paramètre $\theta = \dfrac{\pi}{6}$ et l'autre au point B de paramètre $\theta = \dfrac{\pi}{4}$.
\item  Calculer $\displaystyle\lim_{\theta \to 0} \dfrac{y'}{x'}$. On admet que cette limite est le coefficient directeur de la tangente à $\mathcal{C}$ au point O. Caractériser la tangente à $\mathcal{C}$ au point O.
\item  Tracer soigneusement la courbe $\mathcal{C}$, ainsi que les tangentes aux points O, A, B.
\item  On admet que le rayon de courbure au point B vaut $\dfrac{1}{3}$. Déterminer un vecteur directeur $\vect{n}$ de la normale au point B. Sur la même figure, construire le cercle de courbure en B.
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 2 \hfill 10 points}

\medskip

L'espace est rapporté au repère orthonormal direct \Oijk. Les axes de coordonnées sont les axes (O$x$), (O$y$) et (O$z$). Les plans de coordonnées sont les plans (O$xy$), (O$xz$) et (O$yz$). La notation $M(x~;~y~;~z)$ désigne le point $M$ de coordonnées $x,~y$ et $z$.

\medskip

\emph{La figure sera construite et complétée sur l'annexe au fur et à mesure de l'avancement des questions.}

\medskip

Soit $\Delta$ la droite incluse dans le plan (O$y$z), d'équations	$\left\{\begin{array}{l c l}
x&=&0\\
12y - 5z&=&0\\
\end{array}\right.$\\
Soit $\Sigma$ la sphère de centre 	$\Omega(0~;~3~;~ 2)$, tangente en T(0~;~  3~ ;~0) au plan (O$xy$).

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item  Montrer que la sphère $\Sigma$ a pour équation $x^2 + y^2 + z^2 - 6y - 4z + 9 = 0$.
		\item Montrer que la droite $\Delta$ et la sphère $\Sigma$ ont pour unique point commun A$\left(0~;~\dfrac{15}{13}~;~\dfrac{36}{13}\right)$.
		
En déduire la position de la droite $\Delta$ par rapport à la sphère $\Sigma$.
	\end{enumerate}
\item Soit $P$ le plan perpendiculaire au point A à la droite $\Delta$.
	\begin{enumerate}
		\item  Montrer que $P$ a pour équation $5y + 12z -  39 = 0$.
		\item  On note $\Delta_{1},~ \Delta_{2},~ \Delta_{3}$ les droites intersections respectives de $P$ avec les plans (O$yz$), (O$xz$) et (O$xy$). Déduire de la question a. un système d'équations cartésiennes de chacune des droites $\Delta_{1},~ \Delta_{2},~ \Delta_{3}$.
		\item  Compléter la figure donnée en annexe. En particulier, on reconnaîtra et on nommera les droites $\Delta_{1},~ \Delta_{2},~ \Delta_{3}$.
 	\end{enumerate}
\item Soit B le point de la sphère $\Sigma$, diamétralement opposé à T.

On note T$'$ le point de coordonnées (3 ; 0 ; 0).
	\begin{enumerate}
		\item  Déterminer les coordonnées de B et placer ce point sur la figure en annexe.
		\item  Écrire une équation du plan $P'$ déterminé par les droites (TB) et (TT$'$).
		\item  Montrer que les plans $P$ et $P'$ sont sécants suivant une droite $D$ d'équations paramétriques 
$\left\{\begin{array}{l c l}
x&=&12t\\
y &=&- 12t + 3\\
z&=&5t + 2\\
\end{array}\right.$
		\item  Déterminer les coordonnées du point I, intersection de $D$ avec le plan (O$xz$).
	 \end{enumerate}
\item Soit $\Gamma_{1}$ le grand cercle intersection du plan $P$ et de la sphère $\Sigma$. Soit $\Gamma_{2}$ le grand cercle intersection du plan $P'$ et de la sphère $\Sigma$. Les deux grands cercles $\Gamma_{1}$ et $\Gamma_{2}$ se coupent en deux points. On note C le point d'abscisse positive. On obtient ainsi un triangle sphérique ABC tracé sur la sphère $\Sigma$.
	\begin{enumerate}
		\item  Compléter la figure donnée en annexe ; reconnaître et nommer $\Gamma_{1}$ et $\Gamma_{2}$.

\medskip

\emph{Avec les notations habituelles, on rappelle les \og formules fondamentales \fg}

$\left\{\begin{array}{l c l}	
\cos a&=&\cos b \cos c + \sin b \sin c \cos \widehat{A}\\
\cos \widehat{A}&=& - \cos \widehat{B} \cos \widehat{C} + \sin\widehat{B}  \sin\widehat{C} \cos a\\
\end{array}\right.$

		\item  Montrer que le triangle sphérique ABC est rectangle en A. Calculer le produit scalaire $\vect{\Omega\text{A}} \cdot \vect{\Omega\text{B}}$.\\
En déduire une valeur approchée de la mesure en degrés de l'angle géométrique $\widehat{\text{A}\Omega\text{B}}$.
		\item  Nommer deux plans de la figure déterminant l'angle $\widehat{\text{B}}$ du triangle sphérique ABC. 
		\item En déduire que sa mesure est 45~\degres. Calculer des valeurs approchées de $\widehat{\text{B}},~ b,~ a$.
	\end{enumerate}
\end{enumerate}

\newpage

\begin{center}

\textbf{ANNEXE à rendre avec la copie}

\vspace{3cm}

Figure de l'exercice 2

\vspace{1cm}

\psset{unit=0.95cm}
\begin{pspicture}(13,10.5)
\psline{->}(0,5)(13,5)\uput[d](13,5){$y$}
\psline{->}(5,0)(5,10.5)\uput[l](5,10.5){$z$}
\psline(13,4)(10.3,1.3)(1.2,5)(5,8.7)(13,5.4)
\psline{->}(6.75,6.75)(3.3,3.3)\uput[dl](3.3,3.3){$x$}
\pscircle(8.45,7.3){2.3}\uput[u](8.45,7.3){$\Omega$}
\uput[d](8.45,5){T}
\multido{\n=1.55+1.15}{9}{\psline(\n,4.95)(\n,5.05)}
\multido{\n=1.55+1.15}{8}{\psline(4.95,\n)(5.05,\n)}
\multirput(3.5,3.5)(0.375,0.375){9}{\psline(-0.05,0.05)(0.05,-0.05)}
\pscurve(6.35,8.2)(6.2,7.5)(6.5,7)(6.75,6.75)(7.3,6.4)(8,6.1)(9,5.95)(10,6.05)(10.6,6.4)
\pscurve[linestyle=dotted](6.35,8.2)(7,8.6)(8,8.7)(9,8.5)(10,8.1)(10.7,7.4)(10.8,6.8)(10.6,6.4)
\psdots[dotstyle=+,dotangle=45](8.45,7.3)(6.35,8.2)
\end{pspicture}
\end{center}
\end{document}