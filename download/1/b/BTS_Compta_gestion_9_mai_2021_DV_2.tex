\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage[dvips]{graphicx}
\usepackage{tabularx}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo}
\usepackage{pst-plot,pst-text,pst-tree,pstricks-add}%
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{fancyhdr}
%Tapuscrit : Denis Vergès
%Relecture : François Hache
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS Comptabilité et gestion},
pdftitle = {Métropole  - 10 mai  2019},
allbordercolors = white,
pdfstartview=FitH} 
\usepackage[frenchb]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur Métropole}
\lfoot{\small{Comptabilité et gestion}}
\rfoot{\small{10 mai 2021}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~Brevet de technicien supérieur Métropole~\decofourright\\[5pt]10 mai 2021 - Comptabilité et gestion\footnote{Candidats libres ou établissement privé hors contrat}}}

\end{center}

\vspace{0.25cm}

\textbf{Exercice 1 \hfill 10 points}

\bigskip

Dans cet exercice, on s'intéresse d'une part, à l'évolution du chiffre d'affaires d'une entreprise et d'autre part, au développement de son activité.

Le tableau suivant, où $x_i$ désigne le rang de l'année mesuré à partir de l'année 2013, donne le chiffre d'affaires $y_i$ (en milliers d'euros) de l'entreprise pour chaque année entre 2013 et 2018.

\begin{center}
\begin{tabularx}{\linewidth}{|m{3cm}|*{6}{>{\centering \arraybackslash}X|}}\hline
Année 						&2013 	&2014 	&2015 	&2016 	&2017 	&2018\\ \hline
Rang $x_i$					&0		&1		&2		&3		&4		&5\\ \hline
Chiffre d'affaires 
(en milliers d'euros) $y_i$	&\np{1254} &\np{1317} &\np{1395} &\np{1472} &\np{1575} &\np{1655}\\ \hline
\end{tabularx}
\end{center}

\textbf{Les deux parties de cet exercice sont indépendantes}

\bigskip

\textbf{Partie A :}

\medskip

\begin{enumerate}
\item Calculer le taux global d'évolution du chiffre d'affaires de cette entreprise entre 2013 et 2018, exprimé en pourcentage et arrondi à l'unité.
\item Montrer que le taux moyen annuel d'évolution du chiffre d'affaires de cette entreprise entre 2013 et 2018, arrondi à 0,1\,\% est de 5,7\,\%.
\item On suppose que le chiffre d'affaires de l'entreprise augmente chaque année de 5,7\,\% à partir de 2018.

On note $u_n$ le chiffre d'affaires de l'entreprise pour l'année $2018 +n$. Ainsi $u_0 = \np{1655}$.
	\begin{enumerate}
		\item Calculer $u_1$ arrondi à l'unité. Interpréter ce résultat dans le contexte de l'exercice.
		\item Quelle est la nature de la suite $\left(u_n\right)$ ? Préciser sa raison.
		\item Donner, pour tout entier naturel $n$,\: $u_n$ en fonction de $n$.
		\item Déterminer le chiffre d'affaires que peut prévoir l'entreprise en 2022, arrondi au millier d'euros près.
		\item À partir de quelle année le chiffre d'affaires de l'entreprise dépassera-t-il $2,5$~millions d'euros ?
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie B :}

\medskip

À partir des données du tableau fourni au début de l'énoncé :

\medskip

\begin{enumerate}
\item Donner, sans justifier, le coefficient de corrélation linéaire $r$ de la série statistique $\left(x_i~;~y_i\right)$. 

Arrondir à 0,001 près. 

Expliquer pourquoi ce résultat permet d'envisager un ajustement affine.
\item Donner, sans justifier, l'équation de la droite de régression de $y$ en $x$ par la méthode des moindres carrés sous la forme $y = ax + b$, où $a$ et $b$ sont à arrondir à $0,1$ près.
\item On décide d'ajuster ce nuage de points par la droite $D$ d'équation $y = 82x + \np{1241}$.
	\begin{enumerate}
		\item Calculer, à l'aide de ce modèle, le chiffre d'affaires que peut prévoir l'entreprise en 2021.
		\item Selon ce modèle, à partir de quelle année le chiffre d'affaires de l'entreprise dépassera-t-il $2,2$ millions d'euros ?
	\end{enumerate}
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 2 \hfill 10 points}

\bigskip

\textbf{Les trois parties de cet exercice sont indépendantes}

\bigskip

\textbf{Partie A :}

\medskip

Une entreprise qui fabrique des dragées possède une chaîne de production qui réalise, en fonction des besoins: des dragées blanches ou roses, aux amandes ou au chocolat. Actuellement la machine est réglée de la manière suivante :

\setlength\parindent{1cm}
\begin{itemize}
\item[$\bullet~~$] 55\,\% de la production sont des dragées blanches;
\item[$\bullet~~$] parmi les dragées blanches, 50\,\% sont aux amandes;
\item[$\bullet~~$] parmi les dragées roses, 60\,\% sont aux amandes.
\end{itemize}
\setlength\parindent{0cm}

\smallskip

On s'intéresse à une dragée prise au hasard. On considère les évènements suivants :

\setlength\parindent{1cm}
\begin{itemize}
\item[\starredbullet~]$B$ : \og La dragée choisie est blanche \fg{} ;
\item[\starredbullet~]$A$ : \og La dragée choisie est aux amandes \fg{} ;
\end{itemize}
\setlength\parindent{0cm}

\medskip

\begin{enumerate}
\item Donner la valeur des probabilités $P(B)$, $P_B(A)$ et $P_{\overline{B}}(A)$.
\item Réaliser un arbre de probabilité représentant la situation.
\item Calculer la probabilité que la dragée choisie soit blanche et aux amandes.
\item Calculer $P(A)$.
\item Le directeur constate que plus de la moitié de ses ventes sont des dragées au chocolat.
Le réglage de la machine lui permet-il de satisfaire l'ensemble de ses clients?
\item Sachant que la dragée est au chocolat, quelle est la probabilité qu'elle soit blanche ?

Arrondir le résultat à $0,001$ près.
\end{enumerate}

\bigskip

\textbf{Partie B :}

\medskip

L'entreprise réalise des sachets avec un assortiment de $100$ dragées prises au hasard. Ce tirage est assimilé à un tirage avec remise car le nombre de dragées est très grand.

On suppose que la probabilité qu'une dragée soit aux amandes est de $0,545$.

Soit $X$ la variable aléatoire qui, dans un sachet de $100$ dragées, associe le nombre de dragées aux amandes.

\medskip

\begin{enumerate}
\item Justifier que la variable aléatoire $X$ suit une loi binomiale dont on donnera les paramètres.
\item Déterminer le nombre moyen de dragées aux amandes par sachet.
\item Calculer $P(X = 60)$, arrondie à 0,001 près. Interpréter ce résultat.
\item Déterminer la probabilité d'obtenir au moins 50 dragées aux amandes dans un sachet.

Arrondir la probabilité à $0,001$ près.
\end{enumerate}

\bigskip

\textbf{Partie C :}

\medskip

L'entreprise souhaite proposer une nouvelle gamme de dragées, elle décide donc d'emprunter \np{150000}~\euro{} auprès d'un établissement financier afin de se développer.

L'emprunt, au taux annuel de 3\,\%, sera remboursé en 6 ans par versement annuel constant, nommé annuité $a$.

On rappelle la formule de calcul d'une annuité constante : $a = C \times \dfrac{t}{1 - (1 + t)^{-n}}$.

où $C$ est le capital emprunté, $t$ le taux annuel et $n$ le nombre d'annuités.

L'établissement financier établit le tableau d'amortissement suivant (la cellule C1 est au format pourcentage) :

\begin{center}
\begin{tabularx}{\linewidth}{|c|*{5}{>{\centering \arraybackslash}X|}}\hline
	&A		&B			&C			&D			&E\\ \hline
1	&		&Taux annuel&3\,\%		&			&\\ \hline
2	&		&			&			&			&\\ \hline
3	&Année	&\small Capital restant dû en début d'année&\small Intérêts de l'année & \small Amortissement du capital &\small Annuité constante\\ \hline
4	&1		&\np{150000,00} \euro&	&\np{23189,63} \euro&\np{27689,63} \euro\\ \hline
5	&2		& &			\np{3804,31} \euro &\np{23885,31} \euro&\np{27689,63} \euro\\ \hline
6	&3	&\np{102925,05} \euro&\np{3087,75} \euro&\np{24601,87} \euro&\np{27689,63} \euro\\ \hline
7	&4		&\np{78323,19} \euro&\np{2349,70} \euro & &\np{27689,63} \euro\\ \hline
8	&5		&\np{52983,26} \euro	&\np{1589,50} \euro	&\np{26100,13} \euro	&\np{27689,63} \euro\\ \hline
9	&6		&\np{26883,13} \euro	&806,49 \euro	&\np{26883,13} \euro	&\np{27689,53} \euro\\ \hline
\end{tabularx}
\end{center}

\smallskip

\begin{enumerate}
\item Montrer que le montant de l'annuité constante est d'environ \np{27689,63} euros.
\item Donner les formules à saisir en cellule C4, D7 et B5, qui, recopiées vers le bas, permettent de compléter le tableau d'amortissement.
\item Calculer les valeurs obtenues en cellules C4, B5 et D7.
\item Quel est le coût total de ce crédit ?
\end{enumerate}
\end{document}