%!TEX encoding = UTF-8 Unicode
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text,pst-eucl,pst-node,pst-circ}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS groupement A},
pdftitle = {2017},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[french]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}
\rhead{\textbf{A. P{}. M. E. P.{}}}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Électrotechnique}}
\rfoot{\small{2017}}

\pagestyle{fancy}
\thispagestyle{empty}

\begin{center}{\Large\textbf{Brevet de technicien supérieur  2017\\[6pt] Nouvelle Calédonie  groupement A}}
  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 4 points}

\medskip

\emph{\textbf{Cet exercice est un questionnaire à choix multiples}.\\
Pour chacune des quatre questions posées, une seule des réponses proposées est exacte.\\
Indiquer sur \textbf{la copie le numéro de la question et recopier la réponse choisie}. On ne demande aucune justification.\\
Chaque réponse juste rapporte $1$ point. Une réponse fausse ou une absence de réponse ne rapporte ni n'enlève aucun point.}

\medskip

On modélise une tension en aval d'un convertisseur Continu - Alternatif, appelé onduleur à commande décalée, par la fonction $v$ de la variable $t$ définie sur $\R$.

$t$ est le temps, exprimé en seconde, et $v(t)$ la tension, exprimée en volt, à l'instant $t$.

La courbe ci-dessous, symétrique par rapport à l'origine du repère, représente la fonction $v$.

\begin{center}
\psset{xunit=200cm,yunit=0.01cm,comma=true}
\begin{pspicture}(-0.028,-300)(0.036,300)
\multido{\n=-300+20}{31}{\psline[linestyle=dotted](-0.028,\n)(0.036,\n)}
\psaxes[Dx=0.004,Dy=50,labelFontSize=\scriptstyle]{->}(0,0)(-0.028,-300)(0.036,300)
\psaxes[Dx=0.004,Dy=50,labelFontSize=\scriptstyle](0,0)(-0.028,-300)(0.036,300)
\multido{\n=-0.022+0.010,\na=-0.018+0.010}{6}{\psline[linewidth=1.75pt](\n,0)(\na,0)}
\multido{\n=-0.018+0.020}{3}{\psline[linewidth=1.5pt,linestyle=dotted](\n,0)(\n,220)}
\multido{\n=-0.012+0.020}{3}{\psline[linewidth=1.5pt,linestyle=dotted](\n,0)(\n,220)}
\multido{\n=-0.008+0.020}{2}{\psline[linewidth=1.5pt,linestyle=dotted](\n,0)(\n,-220)}
\multido{\n=-0.002+0.020}{2}{\psline[linewidth=1.5pt,linestyle=dotted](\n,0)(\n,-220)}
%\multido{n=-0.018+0.020,\na=-0.012+0.020}{3}{\psline[linewidth=1.5pt](\n,220)(\na,220)}
%\multido{n=-0.008+0.020,\na=-0.002+0.020}{2}{\psline[linewidth=1.5pt](\n,-220)(\na,-220)}
\psline[linewidth=1.5pt](-0.018,220)(-0.012,220)
\psline[linewidth=1.5pt](0.002,220)(0.008,220)
\psline[linewidth=1.5pt](0.022,220)(0.028,220)
\psline[linewidth=1.5pt](-0.008,-220)(-0.002,-220)
\psline[linewidth=1.5pt](0.012,-220)(0.018,-220)
\end{pspicture}
\end{center}
\medskip

On admet que la fonction $v$ est périodique de période $T$, est développable en série de Fourier et vérifie, pour tout réel $t$ : 

\[v(t) = a_0 + \sum_{n=1}^{+ \infty} \left(a_n \cos(n\omega t) + b_n \sin (n\omega t)\right), \:\text{avec }\:\omega = \dfrac{2\pi}{T}.\]

On rappelle que :

\setlength\parindent{9mm}
\begin{itemize}
\item[$\bullet~~$] $a_0$ est la valeur moyenne de la fonction $v$ sur l'intervalle $[\alpha~;~\alpha + T]$ , quelle que soit la
valeur de la constante réelle $\alpha$. On a donc : $a_0 = \dfrac{1}{T}\displaystyle\int_{\alpha}^{\alpha + T} v(t)\:\text{d}t$.
\item[$\bullet~~$] La valeur efficace $V_{\text{ef}}$ de la fonction $v$ vérifie : $V_{\text{ef}}^2 = \dfrac{1}{T}\displaystyle\int_0^T [v(t)]^2\:\text{d}t$.
\end{itemize}
\setlength\parindent{0mm}

\medskip

\begin{enumerate}
\item La fonction $v$ est :

\begin{center}
\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} impaire &\textbf{b.~~} paire &\textbf{c.~~} ni paire ni impaire &\textbf{d.~~} paire et impaire
\end{tabularx}
\end{center}
\item  La pulsation $\omega$ (en rad/s) est égale à :
\begin{center}
\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} $100\pi$ &\textbf{b.~~} $110\pi$ &\textbf{c.~~} $125\pi$ &\textbf{d.~~} $200\pi$
\end{tabularx}
\end{center}
\item Le coefficient $a_0$ est égal à :
\begin{center}
\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} 132 &\textbf{b.~~} 66 &\textbf{c.~~} 0 &\textbf{d.~~} 110
\end{tabularx}
\end{center}
\item  La valeur efficace $V_{\text{ef}}$, exprimée en volt et arrondie à l'unité, est égale à :
\begin{center}
\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} 220 &\textbf{b.~~}170 &\textbf{c.~~}120 &\textbf{d.~~} 0
\end{tabularx}
\end{center}
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 2 \hfill 9 points}

\medskip

\textbf{Les parties A, B et C de cet exercice peuvent être traitées de manière indépendante.}

\medskip

On s'intéresse à une entreprise qui fabrique des tablettes tactiles avec un écran de $9,7$ pouces et des étuis pour ces tablettes.

\bigskip

\textbf{Partie A : Étude des dimensions des tablettes}

\medskip

Le service Packaging de l'entreprise réalise une étude sur la longueur et la largeur des tablettes fabriquées afin qu'elles soient compatibles avec les dimensions des étuis produits.

On note :
\begin{itemize}
\item[$\bullet~~$] $L$ la variable aléatoire qui associe à chaque tablette fabriquée sa longueur en millimètre (mm),
\item[$\bullet~~$] $l$ la variable aléatoire qui associe à chaque tablette fabriquée sa largeur en millimètre.
\end{itemize}

\medskip

\begin{enumerate}
\item La longueur d'une tablette est compatible avec celle de son étui lorsqu'elle est comprise entre $241,9$~mm et $243,1$~mm.

On admet que la variable aléatoire $L$ suit la loi normale de moyenne $242,5$ et d'écart-type $0,2$.
	\begin{enumerate}
		\item Calculer $P(241,9 \leqslant L \leqslant 243,1)$. Arrondir à $10^{-4}$.
		\item Interpréter la valeur obtenue.
 	\end{enumerate}	
\item  On admet que la variable aléatoire $l$ suit la loi normale de moyenne $166,8$ et d'écart-type $0,1$.
	
Pour mettre en place la norme de fabrication relative à la largeur des étuis, le service Packaging recherche le plus petit nombre décimal à deux chiffres après la virgule, noté $\alpha$ , tel que :
	
\[P(166,8 - \alpha \leqslant l \leqslant 166,8 + \alpha) > 0,995.\]

	\begin{enumerate}
		\item Recopier et compléter le tableau de valeurs ci-dessous. On arrondira à $10^{-4}$.
		
\begin{center}
\begin{tabularx}{0.8\linewidth}{|c|*{4}{>{\centering \arraybackslash}X|}}\hline	
$a$& 0,27 &0,28 &0,29 &0,30\\ \hline
$P(166,8 - \alpha \leqslant l \leqslant 166,8 + \alpha)$&&&&\\ \hline
\end{tabularx}
\end{center}
		\item En déduire la valeur du nombre a cherché et l'intervalle $[166,8 - \alpha~;~166,8 + \alpha]$ associé.
 	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie B : Étude d'un défaut de l'étui}

\medskip

L'étui est fabriqué en cuir recouvert de polyuréthane appelé \og cuir PU \fg.

Un certain nombre d'étuis a été retourné à l'entreprise pour défaut de rigidité. L'enquête statistique conduite par le service Qualité de l'entreprise permet de considérer que la probabilité $p$ qu'un étui comporte un défaut de rigidité est égale à $0,015$.

On prélève un lot de $100$ étuis. La production de l'entreprise est suffisamment importante pour que l'on puisse assimiler ce prélèvement à 100 tirages aléatoires avec remise d'un étui.

On appelle $R$ la variable aléatoire qui, à chaque lot ainsi prélevé, associe le nombre d'étuis
présentant un défaut de rigidité contenus dans le lot.

\medskip

\begin{enumerate}
\item Sans justifier, indiquer la loi suivie par la variable aléatoire R et ses paramètres.
\item Donner la valeur de $P(R = 2)$, arrondie à $10^{-3}$, puis interpréter ce résultat.
\item Peut-on affirmer que la probabilité qu'un lot de $100$ étuis prélevés dans la production comporte plus de $5$ étuis présentant un défaut de rigidité est inférieure à $0,02$ ? Justifier la
réponse.
\end{enumerate}

\bigskip

\textbf{Partie C : Étude des délais de livraison}

\medskip

La production journalière des tablettes et de leurs étuis étant constante, le stock est suffisamment important et les délais de livraison ne dépendent que du nombre de commandes qui sont à traiter.

On note $D$ la variable aléatoire qui, à chaque commande, associe le nombre de jours écoulés entre le jour où le client a effectué sa commande et le jour où il a été livré.

La variable aléatoire $D$ permet donc d'étudier les délais de livraison de l'entreprise.

\smallskip

On admet que pour tout réel positif ou nul $a$ : $P(0 \leqslant D \leqslant a) = \displaystyle\int_0^a \dfrac{1}{8}\text{e}^{- \frac{1}{8}t} \:\text{d}t$.

\medskip

\begin{enumerate}
\item Quelle est la probabilité que le client reçoive sa livraison dans les 15 jours suivant sa commande ? 

Écrire les étapes du calcul effectué et arrondir la réponse à $10^{-3}$.
\item  Pour tout réel positif $x$ on pose : $I(x) = \displaystyle\int_0^x \dfrac{1}{8}t\text{e}^{- \frac{1}{8}t} \:\text{d}t$.
	\begin{enumerate}
		\item Soit la fonction $G$ définie, pour tout réel positif $t$, par: $G(t) = -t\text{e}^{- \frac{1}{8}t} - 8\text{e}^{- \frac{1}{8}t}$.

Montrer que la fonction $G$ est une primitive de la fonction $t \longmapsto \dfrac{1}{8}t\text{e}^{- \frac{1}{8}t}$.
		\item En déduire l'expression de $I(x)$ en fonction de $x$.
 	\end{enumerate}
\item  On admet que l'espérance de la variable aléatoire $D$, que l'on notera $E(D)$, est définie par :

\[E(D) = \displaystyle\lim_{x \to + \infty}  I(x).\]

	\begin{enumerate}
		\item On rappelle que : $\displaystyle\lim_{x \to + \infty} x\text{e}^{-x} = 0$.

Donner la valeur de $E(D)$. Aucune justification n'est attendue.
		\item  Que représente la valeur de $E(D)$ pour le client ?
	\end{enumerate} 
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 3 \hfill 7 points}

\medskip

On rappelle que la fonction échelon unité est définie pour tout réel $t$ par : 
$\left\{\begin{array}{l c l}
U(t)&=&0\quad  \text{si } t < 0\\
U(t)&=&1\quad  \text{si } t \geqslant 0\\
\end{array}\right.$

On donne, dans le tableau ci-dessous, quelques transformées de Laplace de fonctions usuelles et quelques formules pouvant être utiles. 

\begin{center}
\begin{tabularx}{\linewidth}{|c|>{\centering \arraybackslash}X|}\hline
Fonction								&Transformée de Laplace de la fonction\\ \hline
$t \mapsto U(t)$						&$p \mapsto \dfrac{1}{p}$\\ \hline 
$t \mapsto tU(t)$						&$p \mapsto \dfrac{1}{p^2}$\\ \hline
$t \mapsto \text{e}^{-at}U(t)$, $a$ réel&$p \mapsto \dfrac{1}{p + a}$\\ \hline
$t \mapsto U(t-\alpha)$, $\alpha$ réel  &$p \mapsto \dfrac{1}{p}\text{e}^{- \alpha p}$\\ \hline
\multicolumn{2}{c}{}\\ \hline
Fonction								&Transformée de Laplace de la fonction\\ \hline
$t \mapsto f(t)$						&$p \mapsto F(p)$\\ \hline
$t \mapsto f(t - \alpha)U(t - \alpha)$, $\alpha$\: réel&$p \mapsto F(p)\text{e}^{- \alpha p}$ \\ \hline
\end{tabularx}
\end{center}

Un système linéaire est composé d'un variateur de vitesse et d'un moteur à forte inertie.

On lui soumet une consigne en tension dépendant du temps $t$ et modélisée par la fonction causale $e$ définie par :

\[e(t) = tU(t) - (t- 10)U(t - 10) \:\text{pour tout réel }\: t.\]

Le temps $t$ est exprimé en seconde. La consigne en tension $e(t)$ est exprimée en volt.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item En remplaçant $U(t)$ et $U(t - 10)$ par leurs valeurs, simplifier l'expression de $e(t)$ en faisant les cas nécessaires.
		\item Tracer la courbe représentant la fonction $e$ dans le repère fourni sur le document réponse.
		
La vitesse du moteur, en tour par minute (tr/min), est modélisée par la fonction $s$ obtenue à la sortie du système.

On note $E : p \mapsto E(p)$ et $S : p \mapsto S(p)$ les transformées de Laplace des fonctions $e$ et $s$.

La fonction de transfert simplifiée est définie par : $H (p) = \dfrac{150}{10p+1}$.

\textbf{On rappelle que :} \boldmath $S(p) = H(p) \times E(p)$.\unboldmath

\begin{center}
\begin{tabularx}{\linewidth}{|X|X|}\hline
\psset{unit=1cm}
\begin{pspicture}(5,2)
\psline[linewidth=1.5pt](0.5,1)(2,1)\uput[u](1.25,1){$e(t)$}
\rput(3.25,1.15){Système}\rput(3.25,0.85){linéaire}
\psframe(2,0.5)(4.5,1.75)
\psline[linewidth=1.5pt](4.5,1)(6,1)\uput[u](5.25,1){$s(t)$}
\end{pspicture}&
\psset{unit=1cm}
\begin{pspicture}(5,2)
\psline[linewidth=1.5pt](0.5,1)(2,1)\uput[u](1.5,1){$E(p)$}
\rput[u](3.5,1){$H(p) = \dfrac{150}{10p + 1}$}
\psframe(2,0.5)(5.,1.75)
\psline[linewidth=1.5pt](5,1)(6,1)\uput[u](5.5,1){$S(p)$}
\end{pspicture}\\ \hline
\end{tabularx}
\end{center}
	\end{enumerate}
\item Déterminer $E(p)$ puis $S(p)$ en fonction de $p$.
\item On note : $F(p) = \dfrac{1}{p^2(10p+ 1)}$.

Un logiciel de calcul formel donne une nouvelle expression de $F(p)$ comme somme de trois \og éléments simples \fg.

\begin{center}
\begin{tabularx}{0.6\linewidth}{|c|X|}\hline
\multicolumn{2}{|l|}{$\blacktriangleright$ Calcul formel}\\ \hline
1&$F(p) = 1/(p \verb+^+ 2(10p + 1)$\\
 &$\to \: F(p) : = \dfrac{1}{10p^3 + p^2}$\\ \hline
2&ÉlémentsSimples$[F(p)]$\\
 &$\to \dfrac{1}{p^2} - \dfrac{10}{p} + \dfrac{100}{10p + 1}$\\ \hline
\end{tabularx}
\end{center}

En remarquant que $\dfrac{100}{10p + 1} = \dfrac{10}{p + 0,1}$, 
déterminer l'original $f(t)$ de $F(p)$.
\item On remarque que : $S(p) = 150F(p) - 150F(p)\text{e}^{-10p}$.
	\begin{enumerate}
		\item Exprimer $s(t)$ en utilisant les fonctions $f$ et $U$.
		\item Montrer que pour $t \geqslant 10 \::\: s(t) = \np{1500} + \np{1500}\left(\text{e}^{-0,1t} - \text{e}^{-0,1t+1}\right)$.
	\end{enumerate}
\item On a représenté ci-dessous la courbe de la fonction $s$.
	\begin{enumerate}
		\item Par lecture graphique, conjecturer la limite de la fonction $s$ lorsque $t$ tend vers $+ \infty$.
		\item Prouver la conjecture à l'aide de l'expression de $s(t)$ pour $t \geqslant 10$. 
		\item Interpréter la valeur de cette limite pour le système étudié.
	\end{enumerate}
\end{enumerate}


\begin{center}
\psset{xunit=0.2cm,yunit=0.005cm}
\begin{pspicture}(-3,-100)(58,1800)
\multido{\n=0+2}{29}{\psline[linewidth=0.2pt](\n,0)(\n,1800)}
\multido{\n=0+100}{19}{\psline[linewidth=0.2pt](0,\n)(58,\n)}
\psaxes[linewidth=1.25pt,Dx=2,Dy=100,labelFontSize=\scriptstyle]{->}(0,0)(0,0)(58,1800)
\psaxes[linewidth=1.25pt,Dx=2,Dy=100,labelFontSize=\scriptstyle](0,0)(0,0)(58,1800)
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=blue]{10}{58}{1 2.71828 0.1 x mul neg exp add 2.71828 1 0.1 x mul sub exp sub 1500 mul}
\pscurve[linewidth=1.25pt,linecolor=blue](-2,0)(0,0)(2,25)(4,102)(6,218)(8,385)(10,551.819)
\end{pspicture}
\end{center}
\newpage

\begin{center}
{\large \textbf{Document réponse à rendre avec la copie\\ [5pt] 
Exercice 3 - question 1. b.}}

\vspace{2cm}

\psset{xunit=0.8cm,yunit=0.6cm}
\begin{pspicture}(-3,-0.5)(15,13)
\multido{\n=-3+1}{19}{\psline[linestyle=dashed,linewidth=0.5pt](\n,0)(\n,13)}
\multido{\n=0+2}{7}{\psline[linestyle=dashed,linewidth=0.5pt](-3,\n)(15,\n)}
\psaxes[linewidth=1.25pt,Dy=2]{->}(0,0)(-3,0)(15,13)
\psaxes[linewidth=1.25pt,Dy=2](0,0)(-3,0)(15,13)
\end{pspicture}
\end{center}
\end{document}