\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo}
%Tapuscrit : Denis Vergès
%Relecture : François Hache
\usepackage{pst-plot,pst-text,pst-node,pst-tree,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\addtolength{\topmargin}{-1.59999pt}
\newcommand{\barre}[1]{\overline{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS},
pdftitle = {Métropole 16 mai 2024},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[french]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur Polynésie}
\lfoot{\small{Services informatiques aux organisations\\Épreuve de mathématiques approfondies}}
\rfoot{\small{16 mai 2024}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~BTS Polynésie 16 mai 2024~\decofourright\\[7pt]Services informatiques aux organisations}\\[7pt]}

\medskip

\textbf{Épreuve de mathématiques approfondies}

\vspace{0,25cm}

\textbf{L'usage de calculatrice avec mode examen actif est autorisé}

\textbf{L'usage de calculatrice sans mémoire \og type collège \fg{} est autorisé}

\textbf{Durée : 2 heures}

\end{center}

\smallskip

\textbf{Exercice 1 \hfill 10 points}

\medskip

Une entreprise fabrique entre \np{1000} et \np{15000} composants pour téléphones portables par jour. On admet que si l'entreprise fabrique $x$ milliers de composants par jour le bénéfice de l'entreprise en centaines d'euros est modélisé par la fonction $f$ définie sur l'intervalle [1~;~15] par:

\[f(x) = - x \ln (x) + 2x.\]

On note $f'$ la fonction dérivée de la fonction $f$.

\bigskip

\textbf{Partie A}

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Un logiciel de calcul formel donne l'expression suivante pour la dérivée de la fonction $x \longmapsto  x \ln (x)$ :

\begin{center}	
\begin{tabular}{|m{1cm}|m{3cm}|}\hline
1&$x \ln (x)$\\ \hline
~&Dérivée : $\ln (x) + 1$\\ \hline
\end{tabular}
\end{center}

En déduire l'expression de $f'(x)$ pour tout $x$ appartenant à l'intervalle [1~;~15].

		\item Étudier le signe de $f'(x)$ sur l'intervalle [1~;~15].
		\item En déduire le tableau de variation de la fonction $f$ (les images seront, si besoin, arrondies au centième).
		\item Déterminer la valeur du maximum de $f$ sur l'intervalle [1~;~15] et préciser pour quelle valeur ce maximum est atteint.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Démontrer que l'équation $f(x) = 0$ admet une unique solution $a$ dans l'intervalle [1~;~15], puis en déterminer, à l'aide de la calculatrice, une valeur approchée au centième.
		\item En déduire le signe de $f$ sur l'intervalle [1~;~15].
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Vérifier que la fonction $F$ définie sur [1~;~15], par 
		
		\[F(x) = x^2 \left(\dfrac54 - \dfrac12 \ln (x)\right)\] est une primitive de la fonction $f$ sur l'intervalle [1~;~15].
		\item Donner une valeur approchée de l'intégrale 
		\[\displaystyle\int_2^6 f(x) \:\text{d}x\]
à $10^{-2}$ près.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

\begin{enumerate}
\item Déterminer le bénéfice maximal à l'euro près réalisé par l'entreprise et le nombre de composants pour le réaliser.
\item On considère que la production journalière de l'entreprise est comprise entre \np{2000} et \np{6000} composants.
	\begin{enumerate}
		\item Justifier alors que l'entreprise réalise un bénéfice positif.
		\item Pour une telle production, on admet que le bénéfice moyen de l'entreprise, en
centaines d'euros, est donné par:

\[\mu = \dfrac14 \displaystyle\int_2^6 f(x)\:\text {d}x.\]

Déterminer, à l'euro près, la valeur de ce bénéfice moyen.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Exercice 2 \hfill 10 points}

\medskip

\textbf{Partie A}

\medskip

Une entreprise possède deux chaînes de fabrication, notées $a$ et $b$, qui produisent des composants électroniques.

La chaine $a$ produit 55\,\% des composants. On estime que 5\,\% des pièces produites par la chaîne $a$ sont défectueuses et 4\,\% des pièces produites par la chaîne $b$ sont défectueuses.

On choisit au hasard un composant électronique parmi ceux produits par cette entreprise. On considère les évènements suivants:

\begin{itemize}[label=$\bullet~~$]
\item $A$ : \og le composant est produit par la chaine $a$ \fg
\item $B$: \og le composant est produit par la chaîne $b$ \fg
\item $D$ : \og le composant présente un défaut \fg{};
\end{itemize}

Dans cette partie les résultats seront arrondis à $10^{-4}$.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Modéliser cette situation par un arbre pondéré.
		\item Déterminer les probabilités $P(A \cap D)$, $P(B \cap D)$ et en déduire la probabilité qu'un composant soit défectueux.
		\item On a prélevé un composant défectueux, déterminer la probabilité qu'il
provienne de la chaîne $a$.
	\end{enumerate}
\item On constitue un lot de composants provenant des deux chaînes de fabrication en
prélevant $100$ composants.

On considère que le nombre de composants produits par les deux chaines de fabrication est suffisamment important pour que ce prélèvement soit assimilé à un tirage avec remise.

On considère que la probabilité qu'une pièce soit défectueuse est $p = \np{0,0455}$.

On note $X$ la variable aléatoire, qui a chaque lot de $100$ composants, associe le nombre de pièces défectueuses de ce lot.
	\begin{enumerate}
		\item Justifier que $X$ suit une loi binomiale et préciser les paramètres de cette loi.
		\item Déterminer les probabilités $P (X = 0)$ et $P(X \leqslant 10)$.
		\item En déduire la probabilité qu'il y ait entre 1 et 10 pièces défectueuses dans le
lot.
		\item Calculer l'espérance de la variable aléatoire $X$ et interpréter ce résultat dans
le cadre de cet exercice.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

Une société commercialise des liseuses utilisant les composants électroniques étudiés dans la partie A.
Le tableau ci-dessous, où $x_i$ désigne le rang de l'année mesuré à partir de l'année 2015, donne le nombre $y_i$ de liseuses de cette marque vendues annuellement depuis 2015.

\begin{center}
\begin{tabularx}{\linewidth}{|m{3.5cm}|*{7}{>{\centering \arraybackslash}X|}}\hline
Année							&2015 		&2016 		&2017		& 2018	& 2019&2020& 2021\\ \hline
Rang de l'année $x_i$			&0 			&1			& 2			& 3		& 4			&5 &6\\ \hline
Nombre $y_i$ de liseuses
 vendues&\np{1245} 				&\np{1320}	&\np{1421}	&\np{1480} &\np{1530}&\np{1680}& \np{1710}\\ \hline
\end{tabularx}
\end{center}

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer le coefficient de corrélation linéaire $r$ de la série statistique $(x_i~;~y_i)$. Arrondir le résultat au centième.
		\item Expliquer pourquoi le résultat obtenu permet d'envisager un ajustement affine.
	\end{enumerate}
\item Déterminer, à l'aide d'une calculatrice, une équation de la droite de régression de $y$
en $x$, sous la forme $y = ax + b$. Les coefficients $a$ et $b$ seront arrondis au dixième. 
\item 
	\begin{enumerate}
		\item À l'aide de l'équation de la droite de régression trouvée précédemment, estimer le nombre de liseuses vendues en 2023.
		\item À l'aide de l'équation de la droite de régression trouvée précédemment, estimer le rang de l'année à partir duquel le nombre de liseuses vendues deviendrait supérieur à \np{3000}.
	\end{enumerate}
\end{enumerate}
\end{document}