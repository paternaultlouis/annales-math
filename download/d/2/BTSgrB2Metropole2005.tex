%!TEX encoding = UTF-8 Unicode
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{graphicx}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength{\textheight}{23,5cm}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\setlength{\voffset}{-1,5cm}
\usepackage{fancyhdr}
\usepackage[np]{numprint}
\setlength\parindent{0mm}
\usepackage[frenchb]{babel}
\begin{document}
\rhead{\textbf{A. P. M. E. P.}}
\lhead{\small Brevet de technicien supérieur Métropole--Antilles-Guyane}
\lfoot{\small{Groupement B2 Conception et industrialisation en microtechniques}}
\rfoot{\small{juin 2005}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}
\begin{center} \Large \textbf{\decofourleft~Brevet de technicien supérieur~\decofourright\\ Groupement B2 session 2005\\
Conception et industrialisation en microtechniques}  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 11 points}

\medskip

\textbf{Les trois parties de cet exercice peuvent être
traitées de façon indépendante.}

\bigskip

\emph{A. Résolution d'une équation différentielle}

\medskip

On considère l'équation différentielle $(E)$  :%

\[\left(  1+x\right)  y^{\prime}+y=\frac{1}{1+x}\]

où $y$ est une fonction de la variable réelle $x,$ définie et
dérivable sur $\left]  -1;+\infty\right[  $ et $y^{\prime}$ sa fonction
dérivée.

\medskip

\begin{enumerate}
\item  Démontrer que les solutions sur $\left]  -1;+\infty\right[  $ de
l'équation différentielle $\left(E_{0}\right)  :$%

\[(1+x)  y^{\prime}+ y = 0 \]

sont les fonctions définies par $h(x)  =\dfrac{k}{x+1}$ où
$k$ est une constante réelle quelconque.
\item  Soit $g$ la fonction définie sur $]  -1~;~+\infty[  $ par
$g(x)  =\dfrac{\ln (1 + x)}{1 + x}$

Démontrer que la fonction $g$ est une solution particulière de
l'équation différentielle $(E).$
\item  En déduire l'ensemble des solutions de l'équation
différentielle $(E)$.
\item  Déterminer la solution $f$ de l'équation différentielle
$\left(E\right)  $ qui vérifie la condition initiale $f(0)
= 2.$
\end{enumerate}

\bigskip

\emph{B. Étude d'une fonction}

\medskip

Soit $f$ la fonction définie sur $\left]  -1;+\infty\right[  $ par

\[f\left(x\right)  =\frac{2 + \ln\left(  1 + x\right)}{1+x}.\]

Sa courbe représentative $\mathcal{C}$, dans un repère orthonormal
où l'unité graphique est 1 cm, est donnée ci-dessous.

\begin{center}
\begin{pspicture*}(-1.2,-2.2)(9.5,3.2)
\psgrid
[gridcolor=blue,gridlabels=0,griddots=8,subgriddiv=0](0,0)(-1.2,-2.2)(9.5,3.2)
\psaxes[linewidth=1pt,labels=none]{->}(0,0)(-1.2,-2.2)(9.5,3.2)
\psplot[linewidth=1.5pt,linecolor=blue,plotstyle=curve,plotpoints=5000]{-0.891}
{9.5}{1 x add ln 2 add 1 x add div}
\rput(1.2,1.8){$\mathcal{C}$}
\uput[l](0,1){1}
\uput[d](1,0){1}
\uput[dl](0,0){O}
\end{pspicture*}
\end{center}

\begin{enumerate}
\item  On admet que $\displaystyle\lim_{x\rightarrow-1}f\left(  x\right)  =-\infty$
et que $\displaystyle\lim_{x\rightarrow+\infty}f\left(  x\right)  =0.$\\\
Que peut-on en déduire pour la courbe $\mathcal{C}$ ?

\item
\begin{enumerate}
\item  Démontrer que, pour tout $x$ de $]  -1~;~+\infty[
,\; f^{\prime}\left(  x\right)  =\dfrac{-1-\ln(1+x)}{(
1+x)^{2}}$
\item  Résoudre dans $]  -1~;~+\infty[  $ l'inéquation
$-1-\ln(1+x)  \geqslant 0.$\\
En déduire le signe de $f^{\prime}(x)$ lorsque $x$ varie dans $]  -1~;~+\infty[.$
\item  Établir le tableau de variation de $f.$
\end{enumerate}
\item  Un logiciel de calcul formel donne le développement limité,
à l'ordre 2, au voisinage de 0, de la fonction $f:$%
\[f\left(  x\right)  =2-x+\frac{1}{2}x^{2}+x^{2}\,\varepsilon(x)
\text{ avec } \displaystyle\lim_{x \rightarrow 0}\varepsilon(x)  = 0\]

\emph{Ce résultat, admis, n'a pas à être démontré.}
	\begin{enumerate}
		\item  En déduire une équation de la tangente $\mathcal{T}$ à la
courbe $\mathcal{C}$ au point d'abscisse $0$.
		\item  Étudier la position relative de $\mathcal{C}$ et $\mathcal{T}$ au voisinage de leur point d'abscisse 0.$\bigskip$
	\end{enumerate}
\end{enumerate}

\bigskip

\emph{C. Calcul intégral}

\medskip

\begin{enumerate}
\item  Déterminer la dérivée de la fonction $G$ définie sur
$]  -1~;~+\infty[  $ par :%

\[G(x)  =\dfrac{1}{2}\left[\ln (1 + x)\right]^{2}\]

\item  En déduire qu'une primitive de $f$ sur $]  -1~;~+\infty[
$ est définie par :%

\[F(x)  = 2\ln(1 + x)  +\dfrac{1}{2}\left[  \ln(1 + x) \right]^{2}\]

\item
	\begin{enumerate}
		\item  On note $I = \displaystyle\int_{0}^{2}f(x)  \:\text{d}x.$ Démontrer que
$I = \dfrac{1}{2}(\ln 3)^{2}+2 \ln 3.$
		\item  Donner la valeur approchée arrondie à 10$^{-2}$ de $I.$
		\item  Donner une interprétation graphique du résultat obtenu au
\text{b.}
	\end{enumerate}
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 2 \hfill 9 points}

\medskip

\emph{Le but de cet exercice est de résoudre une équation différentielle intervenant en mécanique ou en électronique en utilisant deux méthodes.}

\begin{center} 
\textbf{Les deux parties de cet exercice peuvent être traitées de façon indépendante}
\end{center}

\medskip
 
On se propose de déterminer la fonction $f$ de la variable réelle $t$, définie et deux fois dérivable sur $[0~;~+\infty[$, vérifiant :

\[\begin{array}{l l}
\text{a})& 4 f''(t) + 8f'(t) + 5f(t) = 20~ \text{pour tout}~ t~ \text{de}~ [0~;~+\infty[,\\ 
\text{b})& f(0) = 0  \quad \text{et} \quad  f'(0) = 0.\\
\end{array}\]

\medskip
 
\emph{A. Première méthode}

\medskip
 
\begin{enumerate}
\item Déterminer l'ensemble des solutions, définies sur $[0~;~+\infty[$, de l'équation différentielle $\left(\text{E}_{0}\right)$ :
 
\[4 y''+ 8y'+ 5 y = 0.\] 

\item  Déterminer la constante $k$ telle que la fonction $g$ définie sur $[0~;~+\infty[$ par $g(t) = k$ soit une solution particulière de l'équation différentielle (E) :
 
\[4 y''+ 8y'+ 5 y = 20.\]
 
\item  En déduire l'ensemble des solutions, définies sur $[0~;~+\infty[$, de l'équation différentielle (E) : 
\item Déterminer la solution $f$ de l'équation différentielle (E) qui vérifie les conditions initiales $f(0) = 0$ et $f'(0) = 0$.
\end{enumerate}

\bigskip
 
\emph{B. Deuxième méthode}

\medskip
 
\begin{enumerate}
\item On admet que la fonction $f$, nulle sur $]- \infty~;~0]$, et ses dérivées $f'$ et $f''$ ont des transformées de Laplace. On note $F(p) = \mathcal{L}[f(t)]$ où $F$ est la transformée de $f$. 
	\begin{enumerate}
		\item Donner, à l'aide du formulaire figurant ci-dessous, les expressions de $\mathcal{L}[f''(t)]$ et $\mathcal{L}[f'(t)]$ en fonction de $F(p)$.
		 
En déduire $\mathcal{L}[4f''(t) + 8f'(t) + 5f(t)]$ en fonction de $F(p)$. 
		\item Déterminer $\mathcal{L}[20 \mathcal{U}(t)]$ où $\mathcal{U}$ est l'échelon unité. 
		\item Déduire des questions a. et b. que $F(p) = \dfrac{20}{}
p\left(4p^2 + 8p + 5\right)$.
	\end{enumerate} 
\item Dans la suite, on admet que $F(p)$ peut aussi s'écrire :
 
\[F(p) = 4 \left(\dfrac{1}{p} - \dfrac{p + 1}{(p + 1)^2+0,25} - 2\dfrac{0,5}{(p + 1)^2+0,25}\right).\]  

	\begin{enumerate}
		\item Déterminer $\mathcal{L}^{-1}\left(\dfrac{1}{p}\right)$
 où $\mathcal{L}^{-1}$ désigne la transformation de Laplace inverse. 
		\item Déterminer $\mathcal{L}^{-1}\left(\dfrac{p}{p^2 + (0,5)^2}	 \right)$ et en déduire $\mathcal{L}^{-1}\left(\dfrac{p + 1}{(p + 1)^2 + 0,25}\right)$. 
		\item Déterminer $\mathcal{L}^{-1}\left(\dfrac{0,5}{p^2 + (0,5)^2}	 \right)$ et en déduire $\mathcal{L}^{-1}\left(\dfrac{0,5}{(p + 1)^2 + 0,25}\right)$
		\item Déterminer alors la fonction $f$.
	\end{enumerate}
\end{enumerate}

\vspace{0,5cm}
 
\textbf{Formulaire}

\medskip
 
\fbox{\begin{minipage}{\linewidth} On rappelle les formules suivantes sur la transformation de Laplace.
 
\[\mathcal{L}[\mathcal{U}(t)] = \dfrac{1}{p}~ ;~ \mathcal{L}[\sin (\omega t) \mathcal{U}(t)] = \dfrac{\omega}{p^2 + \omega^2}~;~\mathcal{L}[\cos (\omega t) \mathcal{U}(t)] = \dfrac{p}{p^2 + \omega^2}.\]

Plus généralement, si on note $\mathcal{L}[f(t)\mathcal{U}(t)] = F(p)$ alors,
 
\[\mathcal{L} [f(t) \text{e}^{-at} \mathcal{U}(t)] = F(p + a) ;\]
 
\[\mathcal{L} [f'(t) \mathcal{U}(t)] = pF(p) - f(O^+) ;\]
 
\[\mathcal{L} [f''(t) \mathcal{U}(t)] = p^2 F(p) - pf\left(0^+\right) - f'\left(0^ +\right).\]
\end{minipage}} 
\end{document}