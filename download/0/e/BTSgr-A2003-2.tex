%!TEX encoding = UTF-8 Unicode
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength{\textheight}{23,5cm}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\setlength{\voffset}{-1,5cm}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur S}
\lfoot{\small{Métropole}}
\rfoot{\small{mai 2003}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center}
{\Large \textbf{\decofourleft~BTS Groupement A   mai 2003~\decofourright}}
\end{center}

\vspace{0,25cm}

\textbf{\textsc{Exercice 1} \hfill 10 points}

\medskip
 
\emph{Le but de cet exercice est de déterminer les premiers
 coefficients de Fourier et les principales harmoniques d'un
 signal. }
 
\textbf{Partie A}

\medskip

Pour tout entier naturel $n$, on considère les intégrales :

\[I_n=\int_{\frac{\pi}{2}}^{\pi}\cos (nx)\:\text{d}x
 ~\text{et}~J_n=\int_{0}^{\frac{\pi}{2}}x\cos (nx)\:\text{d}x \]
 
\begin{enumerate}
\item  Montrer que $I_n=-\dfrac{1}{n}\sin n\dfrac{\pi}{2}$.
\item  À l'aide d'une intégration par partie, montrer que

$J_n = \dfrac{\pi}{2n}\sin\left(n \dfrac{\pi}{2} \right)
        +\dfrac{1}{n^2}\cos\left(n \dfrac{\pi}{2} \right) -\dfrac{1}{n^2}$
\item  Déterminer $I_1$, $I_2$ et $I_3$, puis $J_1$, $J_2$ et $J_3$.
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

Soit $f$ la fonction numérique définie sur $\R$, paire,
 périodique de période $2\pi$, telle que :

\[\left\{\begin{array}{l}
\text{si}~0\leqslant t \leqslant \frac{\pi}{2},~~f(t)=\dfrac{2E}{\pi} t \\
\text{si}~\dfrac{\pi}{2} <  t \leqslant \pi,~~f(t)=E
\end{array}\right.\]

où $E$ est un nombre réel donné, strictement positif.

\medskip

\begin{enumerate}
\item  Tracer, dans un repère orthogonal, la représentation graphique
de la fonction $f$ sur l'intervalle $[-\pi~;~+\pi]$ (on prendra $E=2$ uniquement pour construire la courbe représentant $f$).
\item  Soit $a_0$ et pour tout entier naturel supérieur ou égal à $1$,
   $a_n$ et $b_n$ les coefficients de Fourier associés à $f$.
	\begin{enumerate}
		\item  Calculer $a_0$.
		\item  Pour tout $n\geqslant 1$, donner la valeur de $b_n$.
		\item  En utilisant la partie A, vérifier que pour tout $n\geqslant  1$, $a_n = \dfrac{2E}{\pi^2}\left(2J_n+\pi I_n \right)$.
		
Calculer $a_{4k}$ pour tout entier $k \geqslant 1$.
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{Partie C}

\medskip

\begin{enumerate}
\item  Déterminer les coefficients $a_1$, $a_2$, $a_3$.
\item  Calculer $F^2$, carré de la valeur efficace de la fonction $f$ sur une période.
   
On rappelle que dans le cas où $f$ est paire, périodique de période $T$, on a :

\[F^2=\frac{2}{T}\int_0^{\frac{T}{2}}f^2(t)\:\text{d}t\]
   
  \item  On sait par ailleurs que la formule de Bessel-Parseval donne :

\[F^2=a_0^2+\sum_{n=1}^{+\infty}\dfrac{a_n^2+b_n^2}{2}\]

Soit $P$ le nombre défini par $P = a_0^2+\dfrac{1}{2}\left(a_1^2 + a_2^2 + a_3^2\right)$.

Calculer $P$, puis  donner la valeur décimale arrondie au millième du rapport $\dfrac{P}{F^2}$.

\emph{Ce dernier résultat très proche de $1$, justifie que dans la pratique, on peut négliger les harmoniques d'ordre supérieur à $3$.}
\end{enumerate}
 
\vspace{0,5cm}

\textbf{\textsc{Exercice 2} \hfill 10 points}

\medskip

On note j le nombre complexe de module $1$ et d'argument  $\dfrac{\pi}{2}$.

On considère la fonction $H$ définie, pour tout nombre complexe
  $p$ distinct de $0$ et de $-1$, par :

\[H(p)=\dfrac{1}{p(p+1)}.\]

Dans toute la suite de l'exercice on prend $p = \text{j}\omega$, où
  $\omega$ désigne un réel strictement positif.
  
\medskip
  
\begin{enumerate}
\item  On note $r(\omega)$ le module du nombre complexe $H(\text{j}\omega)$   et on considère la fonction $G$ définie, pour tout réel
   $\omega$ par :

\[G(\omega)=\frac{20}{\ln 10}\ln r(\omega).\]

	\begin{enumerate}
		\item Montrer que $G(\omega)= -\dfrac{20}{\ln 10}
\ln \left( \omega \sqrt{1+\omega^2}\right)$.
		\item Déterminer les limites de la fonction $G$ en $0$ et en   $+ \infty$.
		
Montrer que la fonction $G$ est strictement décroissante sur $]0~;~+ \infty[$.
	\end{enumerate}
\item
	\begin{enumerate}
		\item Montrer qu'un argument $\varphi(\omega)$ de $H(j\omega)$ est :
\[\varphi(\omega)=-\frac{\pi}{2}-\arctan \omega\]
		\item Étudier les variations de la fonction $\varphi$ sur  $]0~;~+\infty[$ (on précisera les limites en $0$ et en $+\infty$).
	\end{enumerate}
\item
On considère la courbe $\mathcal C$ définie par la représentation
   paramétrique :
\[\left\{\begin{array}{l}
x(\omega) = -\dfrac{\pi}{2}-\arctan \omega \\
y(\omega) = -\dfrac{20}{\ln 10} \ln \left( \omega \sqrt{1+\omega^2}\right)
\end{array}\right.
\text{pour}~ \omega~ \text{strictement positif.}\]
	\begin{enumerate}
		\item Dresser le tableau des variations conjointes des fonctions $x$ et $y$.
		\item Recopier et compléter le tableau de valeurs suivant (on donnera des valeurs décimales arrondies au centième) :\\
		
\medskip

\[ \begin{tabularx}{0.75\linewidth}{|*{6}{>{\centering \arraybackslash}X|}}\hline
$\omega$   	& 0,5 	& 0,7 	& 0,786 	& 0,9 	& 1,5 \\ \hline
$x(\omega)$ 	&     	&     	& $-2,24$ 	&     	&     \\ \hline
$y(\omega)$ 	&     	&     	&   0   	&     	&     \\ \hline
\end{tabularx}\]

\medskip
	\item  Tracer la courbe $\mathcal C$ dans un repère orthogonal, on  prendra pour unités graphiques $5$~cm sur l'axe des abscisses et 1 cm sur l'axe des ordonnées.
	\end{enumerate}
\end{enumerate}

\emph{ La courbe $\mathcal C$ correspond au diagramme de Black associé à  la fonction de transfert $H$.}
 \end{document}