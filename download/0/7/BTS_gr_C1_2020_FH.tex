\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage[dvips]{graphicx}
\usepackage{tabularx}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo}
\usepackage{pst-plot,pst-text,pst-tree,pstricks-add}%
%Tapuscrit : François Hache
%Relecture : Denis Vergès
% Merci à Philippe Vercruysse et Ronan Charpentier pour le sujet
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{fancyhdr}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS Groupement C1-C2},
pdftitle = {2020},
allbordercolors = white,
pdfstartview=FitH} 
\usepackage[frenchb]{babel}
\DecimalMathComma
\usepackage[np]{numprint}

\newcommand{\e}{\,\text{e}}%%%               le e de l'exponentielle
\renewcommand{\d}{\,\text d}%%%              le d de l'intégration
\renewcommand{\i}{\,\text{i}\,}%%%           le i des complexes
\newcommand{\ds}{\displaystyle}

\begin{document}

\setlength\parindent{0mm}
\setlength\parskip{4pt}

\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Groupement C1-C2}}
\rfoot{\small{septembre 2020}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~Brevet de technicien supérieur Métropole~\decofourright\\[5pt]septembre 2020 - Groupement C1-C2}}

\end{center}

\vspace{1cm}

\textbf{\large Exercice 1 \hfill 10 points}

\bigskip

À la sortie d'un four, un solide dont la température est de 70~\degres{}C est placé, pour le refroidir, dans une pièce dont la température ambiante reste constante et égale à $T_{amb}= 20~$\degres{}C.
Le solide peut être emballé pour expédition dès que sa température passe au-dessous de
40\degres{}C.

On désigne par $T(t)$, la température, en degré Celsius (\degres{}C), du solide à l'instant $t$ ($t$ exprimé en minute).

$T'(t)$ représente la vitesse de refroidissement à l'instant $t$. La loi de Newton établit que cette vitesse est proportionnelle à la différence entre la température du solide et la température ambiante, soit :

\[T'(t) = k\left (T(t)-T_{amb}\strut\right )\]

où $k$ est une constante et $T_{amb}$ la température ambiante, en degré Celsius, de la pièce.

\bigskip

\textbf{Partie A}

\medskip

\begin{enumerate}
\item La constante $k$ dépend des matériaux. Pour le solide qui nous intéresse, $k = -0,07$.\\
Montrer que $T$ est solution de l'équation différentielle:

\[(E)\;:\quad y'+0,07y=1,4\]

où $y$ désigne une fonction de la variable $t$ définie et dérivable sur $\left [0~;~+\infty\strut\right [$ et $y'$ la fonction dérivée de $y$.

\item Résoudre, dans $\left [0~;~+\infty\strut\right [$, l'équation différentielle
$(E_0) :\quad y'+0,07y=0$.

\item \emph{Cette question est une question à choix multiples. Une seule des trois réponses proposées est exacte. Aucune justification n'est demandée. La bonne réponse rapporte un point. Une mauvaise réponse, plusieurs réponses ou l'absence de réponse ne rapportent ni n'enlèvent de point.\\
Indiquer sur la copie le numéro de la question et la réponse correspondante.}

\smallskip

Une solution particulière de $(E)$ est la fonction $f$ définie par:

\smallskip

{\renewcommand{\arraystretch}{2}
\begin{tabularx}{\linewidth}{|*3{>{\centering\arraybackslash}X|}}
\hline
\textbf{a.~~}$f(t)=20$ & \textbf{b.~~}$f(t)=1,4$ & \textbf{c.~~}$f(t)=20t$\\ 
\hline
\end{tabularx}
}

\item En déduire la solution générale de l'équation différentielle $(E)$.

\item
\begin{enumerate}
 \item  D'après l'énoncé, donner $T(0)$.
\item Déterminer une expression de la température $T(t)$ du solide, à l'instant $t$. 
 \end{enumerate} 

\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

Dans cette partie, on admet que pour tout réel $t$ de l'intervalle $\left [0~;~+\infty\strut\right [$, $T(t)=50\e^{-0,07t}+20$.

On donne ci-dessous $\mathcal C$, la courbe représentative de la fonction $T$ dans le plan muni d'un repère orthogonal.

\begin{center}
\psset{xunit=0.425cm, yunit=0.1cm, runit=1cm}
\def\xmin {-1.7}   \def\xmax {32}
\def\ymin {-5}   \def\ymax {80}
\begin{pspicture*}(\xmin,\ymin)(\xmax,\ymax)
%\psgrid[subgriddiv=0,  gridlabels=0, gridcolor=gray,subgridcolor=lightgray]
\multido{\i=0+2}{46}{\psline[linecolor=lightgray](0,\i)(\xmax,\i)}
\multido{\i=0+10}{18}{\psline[linecolor=gray](0,\i)(\xmax,\i)}
\multido{\n=0+0.2}{175}{\psline[linecolor=lightgray](\n,0)(\n,\ymax)}
\multido{\n=0+1}{36}{\psline[linecolor=gray](\n,0)(\n,\ymax)}
\multido{\i=2+2}{15}{\uput[d](\i,0){\footnotesize \i}}
\multido{\i=10+10}{7}{\uput[l](0,\i){\footnotesize \i\degres{}C}}
\psaxes[ticks=none, labels=none](0,0)(\xmin,\ymin)(\xmax,\ymax) 
\uput[dl](0,0){\footnotesize 0}
\def\f{50 2.7183 0.07 neg x mul exp mul 20 add}                           % définition de la fonction
\psplot[plotpoints=3000,linecolor=blue]{0}{\xmax}{\f}
\uput[u](29,0){\textbf{Temps (en min)}}
\uput[r](0,75){\textbf{Température}}
\end{pspicture*}
\end{center}

\begin{enumerate}
\item À l'aide du graphique ci-dessus :
\begin{enumerate}
\item déterminer la température du solide au bout de 10 minutes.
\item déterminer au bout de combien de temps le solide peut être emballé pour expédition.
\end{enumerate}

\item Un logiciel de calcul formel a permis d'obtenir les résultats ci-dessous que l’on pourra
utiliser dans les questions suivantes :

\begin{center}
\begin{tabular}{|c| l c c c}
\hline
1 & \texttt{f(x):=exp((-0,07)*x)} & \hspace*{1.5cm} & & \hspace*{0.5cm} \\
\cline{2-5}
   & & & \emph{x} -> exp(-0,07*\emph{x}) & \\
\hline\hline
2 & \texttt{deriver(f(x))} &  & & \\
\cline{2-5}
   & & &  -0.07*exp(-0,07*\emph{x}) & \\
\hline\hline
3 & \texttt{limite(f(x),x,+infinity)} &  & & \\
\cline{2-5}
   & & & 0 &\\
\hline\hline
4 & \texttt{integration(f(x),x,0,10)} &  & & \\
\cline{2-5}
   & & & 7,19163851727\\
\hline
\end{tabular}
\end{center}

\begin{enumerate}
\item En reliant $T$ à \texttt{f}, établir les variations de la fonction $T$ sur $\left [0~;~+\infty\strut\right [$.
\item Expliquer pourquoi la température du solide ne peut atteindre 18~\degres{}C .
\item Déterminer au dixième près la température moyenne du solide lors des dix premières
minutes.

\emph{On rappelle que la valeur moyenne d'une fonction $g$ sur un intervalle $\left [a~;~b \strut\right ]$ est:}

\hfill$\dfrac{1}{b-a} \ds\int_{a}^{b} g(t) \d t$.\hfill\,
\end{enumerate}

\end{enumerate}

\newpage

\textbf{\large Exercice 2 \hfill 10 points}

\bigskip

\emph{Les parties A, B et C sont indépendantes.}

\bigskip

\textbf{Partie A}

\medskip

Le tableau ci-dessous donne l'évolution des ventes de vélos à assistance électrique en France
entre 2007 et 2017.

\begin{center}
\begin{tabular}{|m{6cm}|*{6}{c|}}
\hline
Année & 2007 & 2009 & 2011 & 2013 & 2015 & 2017\\
\hline
Rang de l'année: $x_i$ & 0 & 2 & 4 & 6 & 8 & 10\\
\hline
Nombre de vélos à assistance électrique vendus (en milliers): $n_i$ & 10 & 23 & 37 & 57 & 102 & 278\\
\hline
\end{tabular}

\hspace*{1cm}\emph{\small Données: Observatoire du Cycle}\hfill\,
\end{center}

\begin{enumerate}
\item On a représenté, en annexe 1, à rendre avec la copie, le nuage des trois premiers points
associé à la série $(x_i~;\, n_i$.
\begin{enumerate}
\item Compléter le nuage de points.
\item Expliquer pourquoi un ajustement affine ne semble pas envisageable.
\end{enumerate}
\item On pose $y_i=\ln(x_i)$. Compléter au centième près le tableau donné en annexe 2, à rendre avec la copie.
\item On s'intéresse à l'ajustement affine de $y_i$ en fonction de $x_i$.

Voici le résultat obtenu à l'aide d'une calculatrice:

\smallskip

\hspace*{4cm} \texttt{LinearReg}\\
\hspace*{5cm} \texttt{a = 0,30742857}\\
\hspace*{5cm} \texttt{b = 2,35285714}\\
\hspace*{5cm} \texttt{r = 0,98986741}\\
\hspace*{5cm} \texttt{r$^2$= 0,9798375}\\
\hspace*{4.6cm} \texttt{MSe = 0,03403428}\\
\hspace*{4cm} \texttt{y = ax + b}

\smallskip

Donner une équation de la droite de régression de $y$ en $x$ (on arrondira les coefficients au
dixième).

\item Si l'évolution se poursuit de la même façon, quel devrait être, en milliers, le nombre de vélos à assistance électrique vendus en France en 2020?
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

Une entreprise produit en grande série des vélos à assistance électrique équipés de batteries au lithium-ion.\\
On propose d'étudier l'autonomie en kilomètre de ces vélos à assistance électrique en se plaçant dans des conditions usuelles de fonctionnement.\\
Soit $X$, la variable aléatoire qui, à chaque vélo à assistance électrique pris au hasard dans la production, associe son autonomie en kilomètre.\\
On admet que cette variable aléatoire $X$ suit la loi normale de moyenne $\mu=81$ et d'écart type $\sigma = 4$.

\smallskip

\textbf{Dans cette partie toutes les probabilités seront arrondies au millième.}

\smallskip

\begin{enumerate}
\item Déterminer la probabilité que l'autonomie d'un vélo à assistance électrique pris au hasard dans la production soit supérieure à 84 kilomètres.
\item
\begin{enumerate}
\item  \emph{Cette question est une question à choix multiples. Une seule des trois réponses
proposées est exacte. Aucune justification n'est demandée. La bonne réponse rapporte un
point. Une mauvaise réponse, plusieurs réponses ou l'absence de réponse ne rapportent ni
n'enlèvent de point.\\
Indiquer sur la copie le numéro de la question et la réponse correspondante.}

\smallskip

Une valeur approchée à l'unité du réel $d$ tel que: $P(X\leqslant d)  = 0,1$ est:

\begin{center}
{\renewcommand{\arraystretch}{2}
\begin{tabularx}{\linewidth}{|*3{>{\centering\arraybackslash}X|}}
\hline
\textbf{a.~~}$88$ & \textbf{b.~~}$81$ & \textbf{c.~~}$76$\\ 
\hline
\end{tabularx}
}
\end{center}

\item Interpréter le résultat dans le cadre de cette étude.
\end{enumerate}

\end{enumerate}

\bigskip

\textbf{Partie C}

\medskip

Dans cette partie, on considère que 4\,\% des batteries au lithium-ion présentent un défaut et sont qualifiées de \og non conformes\fg.\\
Soit $Y$ la variable aléatoire qui, à tout lot de 100 batteries pris au hasard dans la production, associe le nombre de batteries non conformes.\\
La production est assez importante pour qu'on puisse assimiler un tel prélèvement de 100
batteries à un tirage avec remise.

\begin{enumerate}
\item Quelle loi suit la variable aléatoire $Y$ ? Justifier et donner les paramètres de cette loi.
\item
\begin{enumerate}
 \item Déterminer $P(Y \leqslant 5)$.
 \item Interpréter ce résultat.
 \end{enumerate}
 
\item Déterminer la probabilité que, dans un prélèvement au hasard de 100 batteries, toutes les batteries soient conformes.

\item Calculer $E(Y)$. Interpréter le résultat.
\end{enumerate}

\newpage

\begin{center}
\textbf{\Large Annexes à rendre avec la copie}
\end{center}

\begin{center}
\textbf{\large Annexe 1: exercice 2, partie A, question 1.}
\end{center}

\begin{center}
\psset{xunit=1cm, yunit=0.05cm}
\def\xmin {-0.99}   \def\xmax {13}
\def\ymin {-15}   \def\ymax {284}
\begin{pspicture*}(\xmin,\ymin)(\xmax,\ymax)
\psgrid[unit=0.5cm,subgriddiv=0,  gridlabels=0, griddots=5,gridcolor=black](-2,-20)(26,57)
\psaxes[arrowsize=3pt 3,tickstyle=bottom,Dy=20]{->}(0,0)(\xmin,\ymin)(\xmax,\ymax) 
\uput{9pt}[dl](0,0){ 0}
\psdots[dotstyle=x,dotscale=2](0,10)(2,23)(4,37)
\uput[u](11,0){\textbf{Rang de l'année}}
\uput[r](0,280){\textbf{Nombre de vélos à assistance vendus}}
\end{pspicture*}
\end{center}

\vspace{0.5cm}

\begin{center}
\textbf{\large Annexe 2: exercice 2, partie A, question 2.}
\end{center}

\begin{center}
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{|m{6cm}|*{6}{c|}}
\hline
Année & 2007 & 2009 & 2011 & 2013 & 2015 & 2017\\
\hline
Rang de l'année: $x_i$ & 0 & 2 & 4 & 6 & 8 & 10\\
\hline
Nombre de VAE vendus (en milliers): $n_i$ & 10 & 23 & 37 & 57 & 102 & 278\\
\hline
$y_i=\ln(n_i)$ & 2,3 & 3,14 & 3,61 & & &\\
\hline
\end{tabular}
\end{center}


\end{document}
