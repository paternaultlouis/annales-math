\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Merci François Hache pour la relecture
\usepackage{pst-plot,pst-text,pst-eucl,pst-node,pst-circ,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS groupement B2},
pdftitle = {10 mai 2021},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Groupement B2}}
\rfoot{\small{10 mai 2021}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center}
{\Large\textbf{Brevet de technicien supérieur groupement B2 \\[6pt] 10 mai 2021 - Métropole--Antilles--Guyane--Polynésie}}

\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 10 points}

\medskip



Une étude est menée concernant le train d'atterrissage d'un certain type d'hélicoptère. 

Ce train d'atterrissage est composé d'une roue et d'un amortisseur oléopneumatique permettant d'absorber l'énergie de l'impact au moment de l'atterrissage.

\medskip

On note $f(t)$ la hauteur, en mètre, du centre de gravité de l'hélicoptère par rapport au sol à l'instant $t$ exprimé en seconde.

On suppose que $f$ est une fonction de la variable réelle $t$ définie et deux fois dérivable sur $[0~;~+\infty[$.
\begin{center}
\textbf{Les trois parties de cet exercice peuvent être traitées de façon indépendante}
\end{center}

\textbf{A. Résolution d'une équation différentielle}

\medskip

Une étude mécanique montre que la fonction $f$ est solution de l'équation différentielle 

\[(E) : \quad  y'' + 3y' + 2y = 4,\]

où $y$ est une fonction inconnue de la variable réelle $t$, définie et deux fois dérivable sur $[0~;~+\infty[$,  $y'$ la fonction dérivée de $y$ et $y''$ sa fonction dérivée seconde.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Résoudre dans $\R$ l'équation $r^2 + 3r + 2 = 0$,
		\item En déduire les solutions de l'équation différentielle 
		
\[\left(E_0\right) :\quad  y'' + 3y' + 2y = 0.\]

On fournit les formules suivantes.

\begin{center}
\begin{tabularx}{\linewidth}{|p{5cm}|X|}\hline
Équations	&Solutions sur un intervalle $I$\\ \hline
Équation différentielle :

$ay''+ by'+ cy = 0$,

Équation caractéristique:

$ar^2 + br + c = 0$ de discriminant $\Delta$.&
Si $\Delta > 0,\, y(t) = \lambda \text{e}^{r_1 t} + \mu \text{e}^{r_2 t}$ où $r_1$ et $r_2$ sont les racines de l'équation caractéristique.
 
Si $\Delta = 0,\, y(t) = (\lambda t+ \mu)\text{e}^{r t}$ où $r$ est la racine double de l'équation caractéristique,

Si $\Delta < 0,\, y(t) = [\lambda \cos(\beta t)+ \mu \sin(\beta t)]\text{e}^{\alpha t}$ où $r_1 = \alpha + \text{i}\beta$ et $r_2 = \alpha - \text{i}\beta$ sont les racines
complexes conjuguées de l'équation caractéristique.\\ \hline
\end{tabularx}
\end{center}

	\end{enumerate}
\item Soit $k$ un nombre réel. On définit la fonction constante $g$ sur $[0~;~+\infty[$ par $g(t) = k$.

Déterminer $k$ pour que la fonction $g$ soit solution de l'équation différentielle $(E)$.
\item En déduire l'ensemble des solutions de l'équation différentielle $(E)$.
\end{enumerate}

\bigskip

\textbf{B. Étude de la fonction }\boldmath$f$\unboldmath

\medskip

On admet que la fonction $f$ correspondant à la hauteur du centre de gravité de l'hélicoptère est définie sur $[0~;~+\infty[$ par  

\[f(t)= - \text{e}^{-t} + 1,5\text{e}^{-2t} + 2.\]

Sa courbe représentative $\mathcal{C}$ dans un repère orthogonal est donnée ci-dessous.

\begin{center}
\psset{unit=2cm,comma=true}
\begin{pspicture*}(-0.4,-0.3)(4.75,3.25)
\psgrid[gridwidth=0.5pt,subgridwidth=0.1pt,gridlabels=0pt,subgriddiv=2](0,0)(4.75,3.25)
\psaxes[linewidth=1.25pt,Dx=0.5,Dy=0.5]{->}(0,0)(0,0)(4.75,3.25)
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=red]{0}{4.75}{2 1.5 2.71828 2 x mul exp div add  2.71828 x neg exp sub}
\uput[u](0.5,2.1){\red $\mathcal{C}$}
\end{pspicture*}
\end{center}

\medskip

\begin{enumerate}
\item Déterminer la hauteur du centre de gravité de l'hélicoptère au moment de l'atterrissage à
l'instant $t=0$.
\item On admet que $\displaystyle\lim_{t \to + \infty} \text{e}^{-t} = \displaystyle\lim_{t \to + \infty} \text{e}^{-2t}= 0$.
	\begin{enumerate}
		\item Calculer $\displaystyle\lim_{t \to + \infty} f(t)$.
		\item En déduire que la courbe $\mathcal{C}$ admet une droite asymptote dont on donnera une équation.


	\end{enumerate}
\item 
	\begin{enumerate}
		\item À l'aide du graphique, conjecturer le sens de variation de la fonction $f$ sur $[0~;~ + \infty]$.
		\item Un logiciel de calcul formel donne ci-dessous une expression de la dérivée $f'$ de la fonction $f$. Ce résultat est admis.
		
\begin{center}
\renewcommand{\arraystretch}{1.5}
$\begin{array}{l}
1\quad f(t) : - \text{e}^{-t} + 1,5\text{e}^{-2t} + 2\\
 \quad \to f(t) := - \text{e}^{-t} + \dfrac{3}{2\rule[-5pt]{0pt}{0pt}}\text{e}^{-2t} + 2\\ \hline
2\quad \text{Dérivée}(f(t),t)\\
\quad \to \text{e}^{-t} - 3\text{e}^{-2t}\\
\end{array}$
\end{center}

Montrer que cette dérivée peut aussi s'écrire : $f'(t) = \text{e}^{-2t}\left(\text{e}^{t} - 3\right)$.
		\item Résoudre sur $[0~;~ + \infty]$ l'inéquation $\text{e}^{t} - 3 \geqslant 0$. 
		\item En déduire le signe de $f'(t)$ sur $[0~;~ + \infty]$.
		\item Dresser le tableau de variation de la fonction $f$ sur $[0~;~ + \infty]$.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Étude locale}

\medskip

On rappelle que la fonction $f$ est définie sur $[0~;~ + \infty]$ par  

\[f(t) = - \text{e}^{-t} + 1,5\text{e}^{-2t} + 2\]

et que sa courbe représentative $\mathcal{C}$ dans un repère orthogonal est donnée dans la partie B.

Un logiciel de calcul formel affiche la partie régulière du développement limité à l'ordre 2 de la fonction $f$ au voisinage de zéro.

\begin{center}
\renewcommand{\arraystretch}{2}
$\begin{array}{l}
3 \quad \text{PolynômeTaylor}(f(t), t, 0, 2)\\
\quad \dfrac{5}{2} -2t + \dfrac{5}{2} t^2\\
\end{array}$
\end{center}

\smallskip

\begin{enumerate}
\item \emph{Les deux questions suivantes sont des questions à choix multiples. Une seule réponse est exacte. Recopier sur la copie la réponse qui vous paraît exacte. On ne demande aucune justification.\\
La réponse juste rapporte $1$ point. Une réponse fausse ou une absence de réponse ne rapporte ni n'enlève de point.}

	\begin{enumerate}
		\item Le développement limité de la fonction $f$ à l'ordre 2 au voisinage de 0 est :
	\end{enumerate}
\begin{center}
\renewcommand{\arraystretch}{2.5}
\begin{tabularx}{\linewidth}{|*{3}{>{\centering \arraybackslash \small}X|}}\hline
$\dfrac{5}{2} - 2 t + \dfrac{5}{2}t^2$&
$\dfrac{5}{2} -2 t + \dfrac{5}{2} t^2+ t^2\epsilon(t)\, $

$\text{avec}\, \displaystyle\lim_{t \to + \infty}\epsilon(t) = 0$&$\dfrac{5}{2}  -2 t + \dfrac{5}{2} t^2+ t^2\epsilon(t)\,$

$ \text{avec}\, \displaystyle\lim_{t \to 0}\epsilon(t) = 0$\\ \hline
\end{tabularx}
\end{center}
	\begin{enumerate}[resume]
		\item Une équation de la tangente $T$ à la courbe $\mathcal{C}$ au point d'abscisse 0 est :
	\end{enumerate}	
\begin{center}
\renewcommand{\arraystretch}{2.5}
\begin{tabularx}{\linewidth}{|*{3}{>{\centering \arraybackslash}X|}}\hline		
$y= \dfrac{5}{2}$&$y= \dfrac{5}{2} - 2t$&$y= \dfrac{5}{2} - 2t + \dfrac{5}{2}t^2$\rule[-3mm]{0mm}{9mm}\\ \hline
\end{tabularx}
\end{center}

%	\end{enumerate}
\item Étudier la position relative, au voisinage du point d'abscisse 0, de la courbe $\mathcal{C}$ et de la tangente $T$.
\end{enumerate}

\vspace{0,5cm}

\textbf{EXERCICE 2 \hfill 10 points}

\medskip

Soit la fonction $f$, paire et périodique de période $2\pi$, telle que, pour tout $t$ de l'intervalle 
$[0~;~\pi]$,\, $f(t ) = t$. 

Cette fonction modélise un signal triangulaire.

\medskip

\begin{enumerate}
\item Tracer sur la feuille de copie, dans un repère orthonormal, la représentation graphique de
$f$ sur l’intervalle $[- 2\pi~;~2\pi]$.
\item Soit $s(t) =  a_0 + \displaystyle\sum_{n=1}^{+ \infty} \left(a_n \cos(n \omega t ) +  b_n \sin(n \omega t )\right)$ le développement en série de Fourier associé à~$f$.

\begin{center}

\emph{Un formulaire sur les séries de Fourier figure à la page suivante}
\end{center}

	\begin{enumerate}
		\item Calculer $\omega$.
		\item Calculer $a_0$.
		\item Justifier que $b_n = 0$ pour tout entier $n$ supérieur ou égal à 1.
		\item À l’aide d’un logiciel de calcul formel, on obtient : $a_n =  2\left(\dfrac{\cos(n \pi)}{n^2\pi} - \dfrac{1}{n^2 \omega}\right)$  pour tout entier $n$ supérieur ou égal à 1.
		
Ce résultat est admis et ne doit pas être démontré.

Calculer $a_1$ , $a_2$ et $a_3$ . On donnera les valeurs exactes.
		\item Le début du développement en série de Fourier de $f$ à l’ordre $n$ est noté :

\[s_n(t) = a_0 +  \displaystyle\sum_{k=1}^{+ \infty} \left(a_k \cos(k \omega t) + b_k \sin (k \omega t)\right).\]

Donner les expressions de $s_1(t)$ et de $s_3(t)$.
	\end{enumerate}
\item On admet que : $s_3(t) = \dfrac{\pi}{2} - \dfrac{4}{\pi} \cos (t) - \dfrac{4}{9\pi} \cos (3t)$. 

Recopier et remplir le tableau de valeurs suivant dans lequel les valeurs approchées sont à arrondir à $10^{–2}$ :

\begin{center}
\begin{tabularx}{0.75\linewidth}{|c|*{8}{>{\centering \arraybackslash}X|}}\hline
$t$			&0 		&0,5&1 	&1,5&2 	&2,5&3 	&$\pi$\\ \hline
$s_1(t)	$	&		&	&	&	&	&	&	&\\ \hline
$s_3(t)	$	&		&	&	&	&	&	&	&\\ \hline
\end{tabularx}
\end{center}

\item 
	\begin{enumerate}
		\item La valeur efficace $F$ du signal sur une période est donnée par la formule :
		
\[F^2 = \dfrac{1}{2\pi} \displaystyle\int_{-\pi}^{\pi} [f(t)]^2\:\text{d}t.\]

Montrer que $F = \dfrac{\pi}{\sqrt{3}}$.

		\item D’après la formule de Parseval, les valeurs efficaces $S_1$ et $S_3$ des fonctions $s_1$ et $s_3$ vérifient les égalités :
		
		\[\left(S_1 \right)^2 = a_0^2 + \dfrac{1}{2}a_1^2 \quad \text{et} \quad \left(S_3 \right)^2 = a_0^2 + \displaystyle\sum_{n=1}^3 \dfrac{a_n^2}{2}.\]
		
Calculer une valeur approchée à $10^{-4}$ près de chacun des rapports $\dfrac{S_1}{F}$ et  $\dfrac{S_3}{F}$.

Ranger par ordre croissant les trois nombres : 1, $\dfrac{S_1}{F}$ et  $\dfrac{S_3}{F}$. Commenter.
	\end{enumerate}
	
\vspace{1cm}

\begin{center}
\textbf{Formulaire sur les séries de Fourier}

\medskip

\begin{tabularx}{\linewidth}{|X|}\hline
$f$ : fonction périodique de période $T = \dfrac{2\pi}{\omega}$.\rule[-3mm]{0mm}{9mm}\\
Développement en série de Fourier :\\
$s(t) = a_0 + \displaystyle\sum_{n=1}^{+ \infty} \left(a_n \cos (n \omega t) + b_n \sin (n \omega t)\right)$,\, $(n  \in \N^{*})$.\\
$a_0 = \dfrac{1}{T} \displaystyle\int_a^{a + T} f(t)\:\text{d}t$ ;\\
$a_n = \dfrac{2}{T} \displaystyle\int_a^{a + T} f(t)\cos (n \omega t)\:\text{d}t$\, $(n  \in \N^{*})$ ;\\
$b_n = \dfrac{2}{T} \displaystyle\int_a^{a + T} f(t)\sin (n \omega t)\:\text{d}t$\rule[-4mm]{0mm}{9mm}\\ \hline
\end{tabularx}
\end{center}
\end{enumerate}
\end{document}