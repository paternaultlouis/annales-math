\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet} 
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage[normalem]{ulem}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{colortbl}
\usepackage{ulem}
\usepackage{lscape}
\usepackage{dcolumn}
\usepackage{textcomp}
\usepackage{enumitem}
\newcommand{\euro}{\eurologo{}}
\usepackage{pstricks,pst-plot,pst-tree,pstricks-add}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
% Tapuscrit Denis Vergès
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {TS Asie},
pdftitle = {20 juin 2019},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[np]{numprint}
\usepackage[frenchb]{babel}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat S}
\lfoot{\small{Asie}}
\rfoot{\small 20 juin 2019}
\pagestyle{fancy}
\thispagestyle{empty}
\begin{center}\textbf{Durée : 4 heures}

\vspace{0,5cm}

{\Large \textbf{\decofourleft~Baccalauréat S Asie 20 juin 2019~\decofourright}}
\end{center}

\vspace{0,5cm}

\textbf{Exercice 1 \hfill  6 points}

\textbf{Commun à  tous les candidats}

\medskip

La loi de refroidissement de Newton stipule que le taux d'évolution de la température d'un corps est proportionnel à la différence entre la température de ce corps et celle du milieu environnant. 

\smallskip

Une tasse de café est servie à une température initiale de 80~\degres C dans un milieu dont la température, exprimée en degré Celsius, supposée constante, est notée $M$. 

Le but de cet exercice est d'étudier le refroidissement du café en appliquant la loi de Newton suivant deux modèles. L'un, dans la partie A, utilise une suite; l'autre, dans la partie B, utilise une fonction. 

\medskip

\emph{Les parties {\rm A} et {\rm B} sont indépendantes.} 

\medskip

\textbf{Partie A}

\medskip 

Dans cette partie, pour tout entier naturel $n$, on note $T_n$ la température du café à l'instant $n$, avec $T_n$ 
exprimé en degré Celsius et $n$ en minute. On a ainsi $T_0 = 80$. 

On modélise la loi de Newton entre deux minutes consécutives quelconques $n$ et $n + 1$ par l'égalité: 

\[T_{n+1} - T_n = k\left(T_n - M\right)\] 

où $k$ est une constante réelle. 

Dans la suite de la partie A, on choisit $M = 10$ et $k = - 0,2$. 

Ainsi, pour tout entier naturel $n$, on a : $T_{n+1} - T_n = - 0,2 \left(T_n   -10\right)$. 

\medskip

\begin{enumerate}
\item D'après le contexte, peut-on conjecturer le sens de variations de la suite 
$\left(T_n\right)$ ? 
\item Montrer que pour tout entier naturel $n$ :\: $T_{n+1} = 0,8 T_n + 2$. 
\item  On pose, pour tout entier naturel $n$:\: $u_n = T_n - 10$. 
	\begin{enumerate}
		\item Montrer que $\left(u_n\right)$ est une suite géométrique. Préciser sa raison et son premier terme $u_0$.
		\item Montrer que, pour tout entier naturel $n$, on a : $T_n = 70 \times 0,8^n + 10$. 
		\item Déterminer la limite de la suite $\left(T_n\right)$. 
	\end{enumerate}
\item  On considère l'algorithme suivant : 

\begin{center}
\begin{tabularx}{0.35\linewidth}{|X|}\hline
Tant que $T \geqslant 40$\\ 
\hspace{0.8cm}$T\gets 0,8T+2$\\
\hspace{0.8cm}$ n \gets n+1$\\ 
Fin Tant que \\ \hline
\end{tabularx}
\end{center}

	\begin{enumerate}
		\item Au début, on affecte la valeur $80$ à la variable $T$ et la valeur $0$ à la variable $n$. 

Quelle valeur numérique contient la variable $n$ à la fin de l'exécution de l'algorithme ? 
		\item Interpréter cette valeur dans le contexte de l'exercice. 
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{Partie B}

\medskip 

Dans cette partie, pour tout réel $t$ positif ou nul, on note $\theta(t)$ la température du café à l'instant $t$, 
avec $\theta(t)$ exprimé en degré Celsius et $t$ en minute. On a ainsi $\theta(0) = 80$. 

\medskip

Dans ce modèle, plus précis que celui de la partie A, on suppose que $\theta$ est une fonction dérivable sur l'intervalle $[0~;~+\infty[$ et que, pour tout réel $t$ de cet intervalle, la loi de Newton se modélise par 
l'égalité : 

\[\theta'(t)= - 0,2(\theta(t) - M). \]

\smallskip

\begin{enumerate}
\item  Dans cette question, on choisit $M = 0$. On cherche alors une fonction $\theta$ dérivable sur l'intervalle $[0~;~+\infty[$ vérifiant $\theta(0) = 80$ et, pour tout réel $t$ de cet intervalle : $\theta'(t) = - 0,2\theta(t)$. 
	\begin{enumerate}
		\item Si $\theta$ est une telle fonction, on pose pour tout $t$ de l'intervalle $[0~;~+\infty[$, \:$f(t) = \dfrac{\theta(t)}{\text{e}^{- 0,2t}}$. 

Montrer que la fonction $f$ est dérivable sur $[0~;~+\infty[$ et que, pour tout réel $t$ de cet intervalle, $f'(t) = 0$. 
		\item En conservant l'hypothèse du \textbf{a.}, calculer $f(0)$. 

En déduire, pour tout $t$ de l'intervalle $[0~;~+\infty[$ , une expression de $f(t)$, puis de $\theta(t)$. 
		\item Vérifier que la fonction $\theta$ trouvée en \textbf{b.} est solution du problème. 
	\end{enumerate}
\item Dans cette question, on choisit $M = 10$. On admet qu'il existe une unique fonction $g$ dérivable sur $[0~;~+\infty[$, modélisant la température du café à tout instant positif $t$, et que, pour tout $t$ de 
l'intervalle $[0~;~+\infty[$ : 

\[g(t)=10 + 70\text{e}^{-0,2t},\: \text{où }\: t\: \text{est exprimé en minute et}\: g(t) \:\text{en degré Celsius.} \]

Une personne aime boire son café à $40$~\degres C. 

Montrer qu'il existe un unique réel $t_0$ dans $[0~;~+\infty[$ tel que $g\left(t_0\right) = 40$. 

Donner la valeur de $t_0$ arrondie à la seconde. 
\end{enumerate}

\bigskip

\textbf{Exercice 2 \hfill  4 points}

\textbf{Commun à  tous les candidats}

\medskip

Pour chacune des questions suivantes, une seule des quatre affirmations est exacte. Indiquer sur la copie le numéro de la question et recopier la lettre correspondant à l'affirmation exacte. Il est attribué un point si la lettre correspond à l'affirmation exacte, $0$ sinon. 
\smallskip

Dans tout l'exercice, on se place dans un repère orthonormé \Oijk{} de l'espace. 

\emph{Les quatre questions sont indépendantes.}
\textbf{ Aucune justification n'est demandée.}

\medskip 

\begin{enumerate}
\item On considère le plan $P$ d' équation cartésienne $3x + 2 y + 9 z - 5 = 0$ et la droite $d$ dont une représentation paramétrique est:
$\left\{\begin{array}{l c l}
x &= &4t + 3\\  y& =& - t + 2 \\z&=& -t + 9
\end{array}\right. , t \in \R$. 

\medskip

\textbf{Affirmation A} : l'intersection du plan $P$ et de la droite $d$ est réduite au point de coordonnées (3~;~2~;~9). 

\textbf{Affirmation B} : le plan $P$ et la droite $d$ sont orthogonaux. 

\textbf{Affirmation C} : le plan $P$ et la droite $d$ sont parallèles. 

\textbf{Affirmation D} : l'intersection du plan $P$ et de la droite $d$ est réduite au point de coordonnées $(-353~;~91~;~98)$.  

\item  ~

\parbox{0.57\linewidth}{On considère le cube ABCDEFGH représenté ci-contre et les points I, J et K définis par les égalités vectorielles : 

\[\vect{\text{AI}} = \dfrac{3}{4}\vect{\text{AB}},\:\vect{\text{DJ}}= \dfrac{1}{4}\vect{\text{DC}},\:\vect{\text{HK}} = \dfrac{3}{4}\vect{\text{HG}}.\]

\textbf{Affirmation A} : la section du cube ABCDEFGH par le plan (IJK) est un triangle. 

\textbf{Affirmation B} : la section du cube ABCDEFGH par le plan (IJK) est un quadrilatère. 

\textbf{Affirmation C} : la section du cube ABCDEFGH par le plan (IJK) est un pentagone. 

\textbf{Affirmation D} : la section du cube ABCDEFGH par le plan (IJK) est un hexagone. }\hfill
\parbox{0.375\linewidth}{\psset{unit=0.85cm}
\begin{pspicture}(6,5.5)
\psframe(0.25,0.25)(4.75,4.75)%ABCD
\psline(4.75,0.25)(5.75,1.25)(5.75,5.75)(4.75,4.75)%BFGC
\psline(5.75,5.75)(1.25,5.75)(0.25,4.75)%GHD
\psline[linestyle=dashed](0.25,0.25)(1.25,1.25)(1.25,5.75)%AEH
\psline[linestyle=dashed](1.25,1.25)(5.75,1.25)%EF
\uput[l](0.25,0.25){A} \uput[r](4.75,0.25){B} \uput[r](4.75,4.75){C} \uput[l](0.25,4.75){D} 
\uput[ur](1.25,1.25){E} \uput[ur](5.75,1.25){F} \uput[ur](5.75,5.75){G} \uput[ul](1.25,5.75){H}
\psdots(3.625,0.25)(1.375,4.75)(4.625,5.75)%IJK
\uput[u](3.625,0.25){I}  \uput[d](1.375,4.75){J} \uput[u](4.625,5.75){K} 
\end{pspicture}}

\item  On considère la droite $d$ dont une représentation paramétrique est $\left\{\begin{array}{l c r}
x&=&t + 2\\y &=& 2\\z&=&5t - 6
\end{array}\right.$ , avec $t \in \R$, et le 
point A$( - 2~;~1~;~0)$. Soit $M$ un point variable de la droite $d$. 

\medskip

\textbf{Affirmation A} : la plus petite longueur A$M$ est égale à $\sqrt{53}$ . 

\textbf{Affirmation B} : la plus petite longueur A$M$ est égale à $\sqrt{27}$. 

\textbf{Affirmation C} : la plus petite longueur A$M$ est atteinte lorsque le point $M$ a pour coordonnées $(-2~;~1~;~0)$. 

\textbf{Affirmation D} : la plus petite longueur A$M$ est atteinte lorsque le point $M$ a pour coordonnées $(2~;~2~;~-6)$. 

\item On considère le plan $P$ d'équation cartésienne $x+2y-3z+1=0$ et le plan $P'$ d'équation cartésienne $2x - y + 2 = 0$. 

\medskip

\textbf{Affirmation A} : les plans $P$ et $P'$ sont parallèles. 

\textbf{Affirmation B} : l'intersection des plans $P$ et $P'$ est une droite passant par les points A(5~;~12~;~10) et B (3~;~1~;~2). 

\textbf{Affirmation C} : l'intersection des plans $P$ et $P'$ est une droite passant par le point C(2~;~6~;~5) et dont un vecteur directeur est $\vect{u}(1~;~2~;~2)$. 

\textbf{Affirmation D} : l'intersection des plans $P$ et $P'$ est une droite passant par le point D$(-1~;~0~;~0)$ et dont un vecteur directeur est $\vect{v}(3~;~6~;~5)$. 
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 3 \hfill  5 points}

\textbf{Commun à  tous les candidats}

\medskip

\emph{Les parties {\rm A, B} et {\rm C} sont indépendantes}. 

Dans tout l'exercice, on arrondira les résultats au \textbf{millième}. 

\medskip

\textbf{Partie A}

\medskip 

En France, la consommation de produits bio croît depuis plusieurs années. 

En 2017, le pays comptait 52\,\% de femmes. Cette même année, 92\,\% des Français avaient déjà consommé des produits bio. De plus, parmi les consommateurs de produits bio, 55\,\% étaient des femmes. 

On choisit au hasard une personne dans le fichier des Français de 2017. On note : 

\begin{itemize}[leftmargin=15mm]
\item[$\bullet~~$] $F$ l'évènement \og la personne choisie est une femme \fg{} ; 
\item[$\bullet~~$] $H$ l'évènement \og la personne choisie est un homme\fg{} ; 
\item[$\bullet~~$] $B$ l'évènement \og la personne choisie a déjà consommé des produits bio \fg. 
\end{itemize}
\setlength\parindent{0mm}

\medskip

\begin{enumerate}
\item Traduire les données numériques de l'énoncé à l'aide des évènements $F$ et $B$. 
\item 
	\begin{enumerate}
		\item Montrer que $P(F \cap B) = 0,506$. 
		\item En déduire la probabilité qu'une personne ait consommé des produits bio en 2017, sachant que c'est une femme. 
	\end{enumerate}
\item  Calculer $P_H\left(\overline{B}\right)$. Interpréter ce résultat dans le contexte de l'exercice. 
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip 

Dans un supermarché, un chef de rayon souhaite développer l'offre de produits bio. 

Afin de justifier sa démarche, il affirme à son responsable que 75\,\% des clients achètent des produits bio au moins une fois par mois. 

Le responsable souhaite vérifier ses dires. Pour cela, il organise un sondage à la sortie du magasin. Sur \np{2000} personnes interrogées, \np{1421} répondent qu'elles consomment des produits bio au moins une fois par mois. 

Au seuil de 95\,\%, que peut-on penser de l'affirmation du chef de rayon ? 

\bigskip

\textbf{Partie C}

\medskip 

Pour promouvoir les produits bio de son enseigne, le responsable d'un magasin décide d'organiser un jeu qui consiste, pour un client, à remplir un panier avec une certaine masse d'abricots issus de l'agriculture biologique. Il est annoncé que le client gagne le contenu du panier si la masse d'abricots déposés est comprise entre $3,2$ et $3,5$~kilogrammes. 

La masse de fruits en kg, mis dans le panier par les clients, peut être modélisée par une variable aléatoire $X$ suivant la loi de probabilité de densité $f$ définie sur l'intervalle [3~;~4] par : 

\[f(x) = \dfrac{2}{(x - 2)^2}.\] 

Rappel : on appelle fonction de densité d'une loi de probabilité sur l'intervalle $[a~;~b]$ toute fonction $f$ définie, continue et positive sur 
$[a~;~b]$, telle que l'intégrale de $f$ sur $[a~;~b]$ est égale à 1. 

\medskip

\begin{enumerate}
\item Vérifier que la fonction $f$ précédemment définie est bien une fonction de densité d'une loi de probabilité sur l'intervalle [3~;~4]. 
\item Le magasin annonce : \og Un client sur trois gagne le panier ! \fg. 

Cette annonce est-elle exacte ? 
\item Cette question a pour but de calculer l'espérance mathématique E($X$) de la variable aléatoire $X$. 

On rappelle que, pour une variable aléatoire $X$ de densité $f$ sur l'intervalle $[a~;~b]$,\: E($X$) est 
donnée par : E$(X) = \displaystyle\int_a^b  x f(x)\:\text{d}x$. 
	\begin{enumerate}
		\item Vérifier que la fonction $G$, définie sur l'intervalle [3~;~4] par $G(x) = \ln (x - 2) - \dfrac{x}{x - 2}$, est une primitive de la fonction $x \longmapsto  \dfrac{x}{(x - 2)^2}$ sur cet intervalle. 
		\item En déduire la valeur exacte de E($X$), puis sa valeur arrondie au centième. 

Interpréter le résultat dans le contexte de l'exercice. 
	\end{enumerate}
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 4 \hfill  5 points}

\textbf{Candidats n'ayant pas suivi la spécialité mathématique}

\medskip

\begin{enumerate}
\item On considère dans l'ensemble des nombres complexes l'équation $(E)$ à l'inconnue $z$ : 

\[z^3 + \left( -2\sqrt{3} + 2\text{i}\right) z^2 + \left(4 - 4\text{i}\sqrt{3}\right) z + 8\text{i} = 0\quad  (E). \]

	\begin{enumerate}
		\item Montrer que le nombre $-2\text{i}$ est une solution de l'équation $(E)$. 
		\item Vérifier que, pour tout nombre complexe $z$, on a : 

\[z^3 + \left( -2\sqrt{3} + 2\text{i}\right) z^2 + \left(4 - 4\text{i}\sqrt{3}\right) z + 8\text{i} = (z + 2\text{i})\left(z^2  - 2\sqrt{3} z + 4\right).\] 

		\item Résoudre l'équation $(E)$ dans l'ensemble des nombres complexes. 
		\item Écrire les solutions de l'équation $(E)$ sous forme exponentielle. 
	\end{enumerate}
\end{enumerate}
	
\emph{Dans la suite, on se place dans le plan muni d'un repère orthonormé direct d'origine }O. 

\begin{enumerate}[resume]
\item On considère les points A, B, C d'affixes respectives $-2\text{i}$, $\sqrt{3} + \text{i}$ et $\sqrt{3} - \text{i}$. 
	\begin{enumerate}
		\item Montrer que A, B et C appartiennent à un même cercle de centre O dont on déterminera le rayon. 
		\item Placer ces points sur une figure que l'on complètera par la suite. 
		\item On note D le milieu du segment [OB]. Déterminer l'affixe $z_{\text{L}}$ du point L tel que AODL soit un parallélogramme. 
	\end{enumerate}
\item On rappelle que, dans un repère orthonormé du plan, deux vecteurs de coordonnées respectives $(x~;~y)$ et $(x'~;~y')$ sont orthogonaux si et seulement si $xx'+yy' = 0$. 
	\begin{enumerate}
		\item Soit $\vect{u}$ et $\vect{v}$ deux vecteurs du plan, d'affixes respectives $z$ et $z'$. 

Montrer que $\vect{u}$ et $\vect{v}$ sont orthogonaux si et seulement si $z \overline{z'}$ est un imaginaire pur. 
		\item À l'aide de la question \textbf{3. a.}, démontrer que le triangle AOL est rectangle en L. 
	\end{enumerate}
\end{enumerate}
%%%%%%%% spé
\vspace{0,5cm}

\textbf{Exercice 4 \hfill  5 points}

\textbf{Candidats ayant suivi la spécialité mathématique}

\medskip

On note $r$ l'ensemble des matrices colonnes à 2 lignes, à coefficients entiers. 

Soit $U = \begin{pmatrix}u_1\\u_2\end{pmatrix}$ et $V = \begin{pmatrix}v_1\\v_2\end{pmatrix}$ deux éléments de $r$. À $U$ et $V$, on associe la matrice $A = \begin{pmatrix}u_1&v_1\\u_2&v_2\end{pmatrix}$ et le nombre $d(A) = u_1 v_2 - u_2v_1$. 

On dit que $(U,~V)$ est une base de $r$ si et seulement si, pour tout élément $X$ de $r$, il existe un unique couple d'entiers relatifs $(a~;~b)$ tel que $X = aU + bV$. 

\medskip

\begin{enumerate}
\item Dans cette question, on pose $U = \begin{pmatrix}2\\1\end{pmatrix}$, \:$V = \begin{pmatrix}1\\2\end{pmatrix}$ et $X = \begin{pmatrix}10\\10\end{pmatrix}$
	\begin{enumerate}
		\item Montrer que $X$ ne peut pas s'écrire $X = a U + b V$, avec $a$ et $b$ entiers relatifs. 
		\item Le couple $(U,~V)$ est-il une base de $r$ ? 
	\end{enumerate}
\end{enumerate}	
	
\emph{Dans la suite de l'exercice, on souhaite illustrer sur un exemple la propriété : \og si $d(A) = 1$, alors $(U,~V)$ est une base de $r$ \fg.} 

\begin{enumerate}[resume]
\item En posant $U = \begin{pmatrix}6\\- 11\end{pmatrix}$ le but de cette question est de déterminer $V \begin{pmatrix}v_1\\v_2\end{pmatrix}$ tel que $d(A) = 1$. On rappelle dans ce cas que la matrice $A$ associée au couple $(U,~V)$ s'écrit : $A = \begin{pmatrix}6&v_1\\- 11&v_2\end{pmatrix}$. 
	\begin{enumerate}
		\item Exprimer la condition $d(A) = 1$ par une égalité reliant $v_1$ et $v_2$. 
		\item On considère l'équation $(E) :\: 11 x + 6 y = 1$, où $x$ et $y$ sont des entiers relatifs. 

Donner une solution particulière de l'équation $(E)$. 
		\item Résoudre l'équation $(E)$ dans l'ensemble des entiers relatifs. 
		\item Déterminer alors une matrice $V \begin{pmatrix}v_1\\v_2\end{pmatrix}$ de $r$ vérifiant d'une part l'égalité $d(A) = 1$ et, d' autre part, la condition $0 \leqslant v_1 \leqslant 10$. 
	\end{enumerate}
\item  Dans cette question, on pose $U = \begin{pmatrix}6\\- 11\end{pmatrix}$ et $V = \begin{pmatrix}5\\- 9\end{pmatrix}$. Ainsi $A = \begin{pmatrix}6&5\\- 11&-9\end{pmatrix}$. 
	\begin{enumerate}
		\item Montrer que la matrice $A$ est inversible et donner sa matrice inverse $A^{-1}$. 
		\item Soit $X$ un élément de $r$. 

Montrer que l'égalité $X = aU + b V$ s'écrit matriciellement $X = A\begin{pmatrix}a\\b\end{pmatrix}$. 
		\item Déduire des questions précédentes qu'il existe un unique couple d'entiers relatifs $(a~;~b)$ tel que $X = aU +bV$, c'est-à-dire tel que $(U,~ V)$ est une base de $r$. 
		\item Déterminer ce couple $(a~;~b)$ lorsque $X = \begin{pmatrix}2\\3\end{pmatrix}$. 
	\end{enumerate}
\end{enumerate}
\end{document}