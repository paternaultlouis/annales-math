\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Relecture : François Hache
\usepackage{pstricks,pst-plot,pst-tree,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
%\hypersetup{%
%pdfauthor = {APMEP},
%pdfsubject = {STI2D},
%pdftitle = {Mexique juin 2023},
%allbordercolors = white,
%pdfstartview=FitH}

%\DecimalMathComma
\usepackage[french]{babel}
\usepackage[np]{numprint}

\newcommand{\ds}{\displaystyle}
\setlength\parindent{0mm}
\setlength\parskip{5pt}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat STI2D}
\lfoot{\small{Mexique }}
\rfoot{\small{juin 2023}}
\pagestyle{fancy}
\thispagestyle{empty}
%\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~Baccalauréat STI2D Épreuve d'enseignement de spécialité~\decofourright\\[7pt]Mexique juin  2023}}

\vspace{0,25cm}

\textbf{\large Physique-Chimie et Mathématiques}

\end{center}

\textbf{\large EXERCICE 1\hfill physique-chimie et mathématiques\hfill 4 points}

\medskip

\textbf{Étude en laboratoire de la décharge d'un supercondensateur}

\medskip

On réalise un montage qui permet de charger un supercondensateur de  capacité égale à 372~F avec un générateur (interrupteur sur 1), puis de le décharger dans le conducteur ohmique de résistance $R$ (interrupteur sur 2).

\medskip

Le graphique ci-dessous représente l'enregistrement de l'évolution de la tension aux bornes du supercondensateur au cours de sa décharge.

\begin{center}
\psset{xunit=0.036cm,yunit=4.55cm,comma=true}
\begin{pspicture}(-20,-0.2)(360,2.7)
\multido{\n=0+10}{37}{\psline[linewidth=0.025pt](\n,0)(\n,2.5)}
\multido{\n=0.0+0.1}{26}{\psline[linewidth=0.025pt](0,\n)(360,\n)}
\psaxes[linewidth=1.25pt,Dx=50,Dy=0.5](0,0)(360,2.5)
\uput[u](360,0){$t$ (en s)}\uput[r](0,2.6){$u$ (en V)}
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=red]{0}{360}{2.3 2.71828 0.0112 x mul exp div}
\psplot[linestyle=dashed,linewidth=1.25pt]{0}{89}{2.3 0.02576 x mul sub}
\end{pspicture}
\end{center}

\textbf{Données }: énergie $W_c$ accumulée par le supercondensateur
\[W_c= \dfrac12 \times C \times u^2.\]


Avec $C$ : capacité du supercondensateur en farad (F)\\
\phantom{Avec }$u$ : tension aux bornes du supercondensateur en volt (V)\\
\phantom{Avec }$W_c$: énergie stockée dans le supercondensateur en joule (J)\\
\phantom{Avec }1 W h = \np{3600} J

\begin{enumerate}[start=3]
\item À partir du graphique, déterminer l'énergie initiale disponible dans le supercondensateur.
\item Sachant que sa masse est de $60$~g, montrer que son énergie massique est bien typique d'un supercondensateur.
\end{enumerate}

L'évolution de la tension aux bornes du supercondensateur, après fermeture de l'interrupteur K en position 2, est modélisée par la fonction $f$ définie sur l'intervalle $[0~;~+\infty[$ par:
\[f(x) = 2,3\text{e}^{-\np{0,0112}x},\]

 où $x$ représente le temps en seconde.
\begin{enumerate}[start=5]
\item Montrer qu'une équation de la tangente à la courbe représentative de la fonction $f$ au point d'abscisse 0 est : 
\[y = -\np{0,02576}x + 2,3.\]

On rappelle qu'une équation de la tangente à la courbe représentative d'une fonction $f$ au point d'abscisse $a$ est
\begin{center} $y = f'(a)(x - a) + f(a)$ où $f'$ est la fonction dérivée de $f$.\end{center}
\item Déterminer l'abscisse $\tau$ du point d'intersection de cette tangente avec
l'axe des abscisses.

On donnera une valeur approchée à $10^{-1}$ près.
\item Déterminer la capacité $C$ du supercondensateur sachant que
$\tau = RC$ et $R = 0,235~\Omega$.

Comparer la valeur obtenue à partir de ce modèle avec les données du constructeur.
\end{enumerate}

\bigskip

\textbf{\large EXERCICE 3\hfill  mathématiques \hfill 4 points}

\medskip

Les questions sont indépendantes.

\textbf{Question 1}

\medskip

On considère l'équation différentielle 
\[(E) :\qquad   y' = - 2y + 40.\]

\begin{enumerate}
\item Déterminer l'ensemble des solutions de l'équation différentielle $(E)$.
\item En déduire la solution $f$ de l'équation différentielle $(E)$ qui vérifie
$f(0) = 200.$
\end{enumerate}

\medskip

\textbf{Question 2}

\medskip

Soit $f$ la fonction définie sur $\R$ par 
\[f(x) = (x - 1)\text{e}^x.\]

$f$ est dérivable et sa dérivée est notée $f'$.

Justifier le signe de $f'(x)$ établi dans le tableau ci-dessous:

\begin{center}
\renewcommand{\arraystretch}{1.5}
\def\esp{\hspace*{1.5cm}}
$\begin{array}{|c | *{5}{c} |} 
\hline
x  & -\infty & \esp & 0 & \esp  & +\infty \\
\hline
f'(x) &  & \pmb{-} &  \vline\hspace{-2.7pt}{0} & \pmb{+} &    \\
\hline
\end{array}$
\renewcommand{\arraystretch}{1}
\end{center}

\medskip

\textbf{Question 3}

\medskip

On considère les nombres complexes 
\begin{center}$z_1 = 2\text{e}^{\text{i}\frac{\pi}{3}}$ \quad et \quad  $z_2 = \sqrt 2 \text{e}^{\text{i}\frac{\pi}{4}}$\end{center}

\begin{enumerate}
\item Exprimer sous forme exponentielle le produit $z_1  \times z_2$.
\item En déduire une forme trigonométrique de $z_1  \times z_2$.
\end{enumerate}

\medskip

\textbf{Question 4}

\medskip

L'évolution de l'effectif de la population d'un pays, exprimé en millions d'habitants, est modélisée par la fonction $f$ définie sur [0~;~40] comme suit : 
\[f(t) = 10\text{e}^{0,02t}, \]
où $t$ correspond au nombre d'années écoulées depuis le 1\up{er} janvier 2020.

\medskip

\begin{enumerate}
\item Estimer le nombre d'habitants donné par ce modèle au 1\up{er} janvier 2020 et au 1\up{er} janvier 2021.
\item D'après ce modèle, déterminer l'année durant laquelle l'effectif de la population dépassera 20 millions d'habitants.
\end{enumerate}
\end{document}