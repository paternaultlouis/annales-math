\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet} 
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage[normalem]{ulem}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{lscape}
\usepackage{dcolumn}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo{}}
\usepackage{pstricks,pst-plot,pst-tree,pst-node,pstricks-add,pst-func}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
% Tapuscrit Denis Vergès
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {ES/L Polynésie},
pdftitle = {22 juin 2018},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat ES/L}
\lfoot{\small{Polynésie}}
\label{Polynesie}
\rfoot{\small 22 juin 2018}
\pagestyle{fancy}
\thispagestyle{empty}
\begin{center}\textbf{Durée : 3 heures}

\vspace{0,5cm}

{\Large \textbf{\decofourleft~Baccalauréat Terminale ES Polynésie 22 juin 2018
~\decofourright}}
\end{center}

\vspace{0,5cm}

\textbf{Exercice 1 \hfill  5 points}

\textbf{Commun à  tous les candidats}

\medskip

On considère la fonction $f$ définie sur l'intervalle ]0~;~3] par 

\[f(x) = x^2(1 - \ln x).\]

On donne ci-dessous sa courbe représentative $\mathcal{C}$.

\begin{center}
\psset{unit=3cm,comma=true}
\begin{pspicture}(-0.4,-1.2)(4,2)
\multido{\n=0.0+0.1}{41}{\psline[linewidth=0.2pt](\n,-1.2)(\n,2)}
\multido{\n=0.0+0.5}{9}{\psline[linewidth=0.5pt](\n,-1.2)(\n,2)}
\multido{\n=-1.2+0.1}{33}{\psline[linewidth=0.2pt](0,\n)(4,\n)}
\multido{\n=0.0+0.5}{5}{\psline[linewidth=0.5pt](0,\n)(4,\n)}
\psaxes[linewidth=1pt,Dx=0.5,Dy=0.5](0,0)(0,-1.2)(4,2)
\psaxes[linewidth=1.5pt]{->}(0,0)(1,1)
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=blue]{0.01}{3.084}{1 x ln sub x dup mul mul}
\uput[u](0.75,0.8){\blue $\mathcal{C}$}
\end{pspicture}
\end{center}

\medskip

On admet que $f$ est deux fois dérivable sur ]0~;~3], on note $f'$ sa fonction dérivée et on admet que sa dérivée seconde $f''$  est définie sur ]0~;~3] par : $f''(x) = - 1- 2 \ln x$.

\medskip

Cet exercice est un questionnaire à choix multiples. Pour chacune des questions posées, une seule réponse est exacte. Aucune justification n'est demandée.

Une réponse exacte rapporte $1$ point, une réponse fausse ou l'absence de réponse ne rapporte ni n'enlève de point. Une réponse multiple ne rapporte aucun point.


\medskip

\begin{enumerate}
\item Sur ]0~;~3], $\mathcal{C}$ coupe l'axe des abscisses au point d'abscisse :

\medskip
\begin{tabularx}{\linewidth}{*{3}{X}}
\textbf{a.~~} e &\textbf{b.~~} 2,72 &\textbf{c.~~} $\dfrac{1}{2}\text{e} + 1$
\end{tabularx}
\medskip

\item  $\mathcal{C}$ admet un point d'inflexion d'abscisse :

\medskip
\begin{tabularx}{\linewidth}{*{3}{X}}
\textbf{a.~~} e &\textbf{b.~~} $\dfrac{1}{\sqrt{\text{e}}}$&\textbf{c.~~} $\sqrt{\text{e}}$
\end{tabularx}
\medskip

\item  Pour tout nombre réel $x$ de l'intervalle ]0~;~3] on a :

\medskip
\begin{tabularx}{\linewidth}{*{3}{X}}
\textbf{a.~~} $f'(x) = x(1 - 2\ln x)$&\textbf{b.~~} $f'(x)= - \dfrac{2}{x}$
&\textbf{c.~~} $f'(x) = - 2$
\end{tabularx}
\medskip

\item  Sur l'intervalle [1~;~3] :

\medskip
\begin{tabularx}{\linewidth}{*{3}{X}}
\textbf{a.~~}$f$ est convexe &\textbf{b.~~} $f$ est décroissante &\textbf{c.~~} $f'$ est décroissante
\end{tabularx}
\medskip

\item  Une équation de la tangente à $\mathcal{C}$  au point d'abscisse e s'écrit :

\medskip
\begin{tabularx}{\linewidth}{*{3}{X}}
\textbf{a.~~} $y= -x+ \text{e}$&\textbf{b.~~} $y= - \text{e}x$ &\textbf{c.~~} $y = - \text{e}x + \text{e}^2$
\end{tabularx}
\medskip
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 2 \hfill  5 points}

\textbf{Commun à  tous les candidats}

\medskip

Les parties de cet exercice peuvent être traitées de façon indépendante.

Les résultats numériques seront donnés, si nécessaire, sous forme approchée à 0,001 près.

\bigskip

\textbf{Partie A}

\medskip

Une entreprise est composée de 3 services A, B et C d'effectifs respectifs $450$, $230$ et $320$ employés.

Une enquête effectuée sur le temps de parcours quotidien entre le domicile des employés et
l'entreprise a montré que:

\begin{description}
\item[ ] 40\,\% des employés du service A résident à moins de 30 minutes de l'entreprise;
\item[ ] 20\,\% des employés du service B résident à moins de 30 minutes de l'entreprise;
\item[ ] 80\,\% des employés du service C résident à moins de 30 minutes de l'entreprise.
\end{description}

On choisit au hasard un employé de cette entreprise et on considère les évènements suivants :

\setlength\parindent{1cm}
\begin{description}
\item[ ] $A$ : \og l'employé fait partie du service A \fg{} ;
\item[ ] $B$ : \og l'employé fait partie du service B \fg{} ;
\item[ ] $C$ : \og l'employé fait partie du service C \fg{} ;
\item[ ] $T$ : \og l'employé réside à moins de 30 minutes de l'entreprise\fg.
\end{description}
\setlength\parindent{0cm}

On rappelle que si $E$ et $F$ sont deux évènements, la probabilité d'un évènement $E$ est notée $P(E)$ et celle de $E$ sachant $F$ est notée $P_F(E)$.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Justifier que $P(A) = 0,45$.
		\item Donner $P_A(T)$.
		\item Représenter la situation à l'aide d'un arbre pondéré en indiquant les probabilités associées à chaque branche.
 	\end{enumerate}
\item Déterminer la probabilité que l'employé choisi soit du service A et qu'il réside à moins de $30$ minutes de son lieu de travail.
\item Montrer que $P(T) = 0,482$.
\item Sachant qu'un employé de l'entreprise réside à plus de $30$ minutes de son lieu de travail, déterminer la probabilité qu'il fasse partie du service C.
\item On choisit successivement de manière indépendante $5$ employés de l'entreprise. On considère que le nombre d'employés est suffisamment grand pour que ce tirage soit assimilé à un tirage avec remise. Déterminer la probabilité qu'exactement $2$ d'entre eux résident à moins de 30 minutes de leur lieu de travail.
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

Soit $X$ la variable aléatoire qui, à chaque employé en France, associe son temps de trajet quotidien, en minutes, entre son domicile et l'entreprise. Une enquête montre que $X$ suit une loi normale d'espérance $40$ et d'écart type $10$.

\medskip

\begin{enumerate}
\item Calculer la probabilité que le trajet dure entre 20 minutes et 40 minutes.
\item  Déterminer$P(X > 50)$.
\item  À l'aide de la méthode de votre choix, déterminer une valeur approchée du nombre $a$ à l'unité près, tel que $P(X > a) = 0,2$. Interpréter ce résultat dans le contexte de l'exercice.
\end{enumerate}

\bigskip

\textbf{Partie C}

\medskip

Cette entreprise souhaite faire une offre de transport auprès de ses employés. Un sondage auprès de quelques employés est effectué afin d'estimer la proportion d'employés dans l'entreprise intéressés par cette offre de transport. On souhaite ainsi obtenir un intervalle de confiance d'amplitude strictement inférieure à $0,15$ avec un niveau de confiance de $0,95$. Quel est le nombre minimal d'employés à consulter ?

\vspace{0,5cm}

\textbf{Exercice 3 \hfill  4 points}

\textbf{Commun à  tous les candidats}

\medskip

En économie le résultat net désigne la différence entre la recette et les charges d'une entreprise sur une période donnée. Lorsqu'il est strictement positif, c'est un bénéfice.

Propriétaire d'une société, Pierre veut estimer son résultat net à la fin de chaque mois.

À la fin du mois de janvier 2018, celui-ci était de \np{10000} euros.

Pierre modélise ce résultat net par une suite $\left(u_n\right)$ de premier terme $u_0 = \np{10000}$ et de terme général $u_n$ tel que 

\[u_{n+1} = 1,02u_n - 500\]

où $n$ désigne le nombre de mois écoulés depuis janvier 2018.

\medskip

\begin{enumerate}
\item Quel est le montant du résultat net réalisé à la fin du mois de mars 2018 ?
\item Pour tout entier naturel $n$, on pose $a_n = u_n  - \np{25000}$.
	\begin{enumerate}
		\item Montrer que la suite $\left(a_n\right)$ est une suite géométrique dont on précisera le premier terme $a_0$ et la raison.
		\item  Exprimer $a_n$ en fonction de $n$ et montrer que, pour tout entier naturel $n$,\:
		
$u_n = \np{25000} - \np{15000} \times 1,02^n$.
		\item  Résoudre l'inéquation $\np{25000} - \np{15000} \times 1,02^n > 0$ où $n$ désigne un entier naturel.
		
Interpréter le résultat obtenu dans le contexte de l'exercice.
	\end{enumerate}
\item  À l'aide d'un algorithme, Pierre souhaite déterminer le cumul total des résultats nets mensuels de la société jusqu'au dernier mois où l'entreprise est bénéficiaire.

Recopier et compléter l'algorithme pour qu'à la fin de son exécution, la variable $N$ contienne le nombre de mois pendant lesquels l'entreprise est bénéficiaire et la variable $S$ le cumul total des résultats nets mensuels sur cette période.

\begin{center}
\begin{tabularx}{0.3\linewidth}{|X|}\hline
$U \gets \np{10000}$\\
$S \gets 0$\\
$N \gets 0$\\
Tant que \ldots\ldots\\
\hspace{1cm}$S$  \ldots\ldots\\
\hspace{1cm}$U$  \ldots\ldots\\
\hspace{1cm}$N$  \ldots\ldots\\
Fin Tant que\\ \hline
\end{tabularx}
\end{center}
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 3 \hfill  4 points}

\textbf{Candidats ayant suivi l'enseignement de spécialité}

\medskip

Un journaliste britannique d'une revue consacrée à l'automobile doit tester les autoroutes fran\c{c}aises. 

Pour remplir sa mission, il décide de louer une voiture et de circuler entre six grandes villes fran\c{c}aises : Bordeaux~(B), Lyon~(L), Marseille~(M), Nantes~(N), Paris~(P) et Toulouse(T).
\smallskip

Le réseau autoroutier reliant ces six villes est modélisé par le graphe ci-dessous sur lequel les sommets représentent les villes et les ar\^etes les liaisons autorouti\`eres entre ces villes.

\begin{center}
{\psset{unit=1cm}
\begin{pspicture}(-0.1,-0.5)(7,6.5)
\cnodeput(2.5,2){A}{N}
\cnodeput(2.,6){B}{P}
\cnodeput(3.5,3.5){C}{L}
\cnodeput(5.5,3.2){D}{M}
\cnodeput(1.5,.5){E}{B}
\cnodeput(6.5,0.5){F}{T}
\ncarc[arcangle=30]{A}{B}
\ncarc[arcangle=-30]{C}{B}
\ncarc[arcangle=70]{B}{F}
\ncarc[arcangle=-60]{B}{E}
\ncarc[arcangle=-30]{C}{A}
\ncarc[arcangle=90]{C}{E}
\ncarc[arcangle=30]{C}{F}
\ncarc[arcangle=-30]{D}{C}
\ncarc[arcangle=30]{D}{F}
\ncarc[arcangle=-30]{A}{E}
\ncarc[arcangle=30]{F}{E}
\end{pspicture}}
\end{center}
\bigskip

\textbf{Partie A}
\medskip

\begin{enumerate}
\item 
    \begin{enumerate}
        \item Quel est l'ordre du graphe ?
        \item Le graphe est-il complet ? Justifier la réponse.
    \end{enumerate}
\item  
    \begin{enumerate}
        \item On admet que le graphe est connexe. 
        
        Le journaliste envisage de parcourir chacune des liaisons modélisées sur le graphe une fois et une seule. 
        
        Est-ce possible ? Justifier la réponse.
        \item Le journaliste va-t-il pouvoir louer sa voiture dans un aéroport parisien, parcourir chacune des liaisons une et une seule fois puis rendre la voiture dans le même aéroport ? Justifier la réponse.
    \end{enumerate}
\item  On nomme $G$ la matrice d'adjacence du graphe (les villes étant rangées dans l'ordre
alphabétique). 

On donne :
\[G = \begin{pmatrix}
0 &\ldots &0 &1 &1 &1 \\
\ldots&0 &1 &1 &1 &1\\ 
0 &1 &\ldots &0 &\ldots &1\\ 
1 &1 &0 &0 &1 &0 \\
1 &1 &\ldots &1 &0 &1\\ 
1 &1 &1 &0 &1 &0 
\end{pmatrix} \quad 
\text{et} \quad G^3 = \begin{pmatrix}
10 &13 &5 &10 &11 &12\\
13 &12 &8 &11 &13 &12\\
5 &8 &2 &5 &5 &7\\
10 &11 &5 &6 &10 &7\\
11 &13& 5 &10 &10 &12\\
12 &12 &7 &7 &12 &8
\end{pmatrix}\]

    \begin{enumerate}
        \item Recopier et compléter la matrice d'adjacence.
        \item Alors qu'il se trouve à Paris, le rédacteur en chef demande au journaliste d'être à Marseille exactement trois jours plus tard pour assister à une course automobile. Le journaliste décide chaque jour de s'arrêter dans une ville différente. 
        
Déterminer le nombre de trajets possibles.
    \end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

On a indiqué sur le graphe ci-dessous le temps nécessaire en minutes pour parcourir chacune des liaisons autorouti\`eres.

\begin{center}
{\psset{unit=1.2cm}
\begin{pspicture}(-0.1,-0.5)(7,6.5)
\cnodeput(2.5,2){A}{N}
\cnodeput(2.,6){B}{P}
\cnodeput(3.5,3.5){C}{L}
\cnodeput(5.5,3.2){D}{M}
\cnodeput(1.5,.5){E}{B}
\cnodeput(6.5,0.5){F}{T}
\ncarc[arcangle=30]{A}{B}\ncput*{222} \ncarc[arcangle=-30]{C}{B}\ncput*{268}
\ncarc[arcangle=70]{B}{F}\ncput*{391}\ncarc[arcangle=-60]{B}{E}\ncput*{340}
\ncarc[arcangle=-30]{C}{A}\ncput*{396}\ncarc[arcangle=90]{C}{E}\ncput*{336}
\ncarc[arcangle=30]{C}{F}\ncput*{305}\ncarc[arcangle=-30]{D}{C}\ncput*{214}
\ncarc[arcangle=30]{D}{F}\ncput*{236}\ncarc[arcangle=-30]{A}{E}\ncput*{206}
\ncarc[arcangle=30]{F}{E}\ncput*{153}
\end{pspicture}}
\end{center}

Le journaliste se trouve à Nantes et désire se rendre le plus rapidement possible à Marseille.

Déterminer un trajet qui minimise son temps de parcours.

\vspace{0,5cm}

\textbf{Exercice 4 \hfill  5 points}

\textbf{Commun à  tous les candidats}

\begin{center}
Les parties de cet exercice peuvent être traitées indépendamment.
\end{center}

\medskip

Une usine qui fabrique un produit A, décide de fabriquer un nouveau produit B afin d'augmenter son chiffre d'affaires. La quantité, exprimée en tonnes, fabriquée par jour par l'usine est modélisée par :
\begin{itemize}
\item la fonction $f$ définie sur [0~;~14] par 

\[f(x) = \np{2000}\text{e}^{-0,2x}\]

 pour le produit A ;
\item  la fonction $g$ définie sur [0~;~14] par 

\[g (x)= 15x^2 + 50 x\]

pour le produit B, où $x$ est la durée écoulée depuis le lancement du nouveau produit B exprimée en mois.
\end{itemize}

Leurs courbes représentatives respectives $\mathcal{C}_f$ et $\mathcal{C}_g$ sont données ci-dessous.

\begin{center}
\psset{xunit=0.7cm,yunit=0.002cm}
\begin{pspicture}(-1,-200)(17.5,3700)
\multido{\n=0+1}{18}{\psline[linewidth=0.2pt](\n,0)(\n,3500)}
\multido{\n=0+100}{36}{\psline[linewidth=0.2pt](0,\n)(17,\n)}
\multido{\n=0+500}{7}{\psline[linewidth=0.5pt](0,\n)(17,\n)}
\psaxes[linewidth=1.25pt,Dy=500]{->}(0,0)(0,0)(17,3500)
\uput[r](0,3600){$y$ en tonnes} 
\uput[u](16.2,0){$x$ en mois}
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=blue]{0}{14}{2000 2.71828 0.2 x mul exp div}\uput[u](2,1400){\blue $\mathcal{C}_f$}
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=red]{0}{14}{x dup mul 15 mul 50 x mul add}\uput[u](2,250){\red $\mathcal{C}_g$}
\end{pspicture}
\end{center}

\bigskip

\textbf{Partie A}

\medskip

Par lecture graphique, sans justification et avec la précision permise par le graphique :

\medskip

\begin{enumerate}
\item Déterminer la durée nécessaire pour que la quantité de produit B dépasse celle du produit A.
\item L'usine ne peut pas fabriquer une quantité journalière de produit B supérieure à \np{3000}~tonnes.

Au bout de combien de mois cette quantité journalière sera atteinte?
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

Pour tout nombre réel $x$ de l'intervalle [0~;~14] on pose $h(x) = f(x) + g(x)$.

On admet que la fonction $h$ ainsi définie est dérivable sur [0~;~14].

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Que modélise cette fonction dans le contexte de l'exercice ?
		\item Montrer que, pour tout nombre réel $x$ de l'intervalle [0~;~14]
$h'(x) = - 400\text{e}^{-0,2x} + 30x + 50$.
	\end{enumerate}
\item On admet que le tableau de variation de la fonction $h'$ sur l'intervalle [0~;~14] est :

\begin{center}
{\renewcommand{\arraystretch}{1.3}
\psset{nodesep=3pt,arrowsize=2pt 3}
\def\esp{\hspace*{3cm}} \def\hauteur{0pt}
$\begin{array}{|c| *3{c}|}
\hline
 x & 0   & \esp & 14 \\
% \hline
%f'(x) &   & \pmb{+} & \\  
\hline
  &   &    & \Rnode{max}{h'(14) \approx 446}   \\
\shortstack{variations\\ de $h'$} & &  &  \rule{0pt}{\hauteur} \\
 &     \Rnode{min}{-350} & & \rule{0pt}{\hauteur}
\ncline{->}{min}{max}
%\rput*(-3,0.62){\Rnode{zero}{\red 0}}
%\rput(-3,1.9){\Rnode{alpha}{\red \alpha}}
%\ncline[linestyle=dotted, linecolor=red]{alpha}{zero}
\\
\hline
\end{array}$
}
\end{center}

	\begin{enumerate}
		\item Justifier que l'équation $h'(x)= 0$ admet une unique solution $\alpha$ sur l'intervalle [0~;~14] et donner un encadrement d'amplitude $0,1$ de $\alpha$.
		\item  En déduire les variations de la fonction $h$ sur l'intervalle [0~;~14].
	\end{enumerate}
\item Voici un algorithme :

\begin{center}
\begin{tabularx}{0.5\linewidth}{|X|}\hline
$Y \gets -400 \,\text{exp}(- 0,2X) + 30X + 50$\\
Tant que $Y \leqslant 0$\\
\hspace{0.7cm}$X \gets X + 0,1$\\
\hspace{0.7cm}$Y \gets  -400 \,\text{exp}(- 0,2X)+ 30X +50$\\
Fin Tant que\\ \hline
\end{tabularx}
\end{center}

	\begin{enumerate}
		\item Si la variable $X$ contient la valeur 3 avant l'exécution de cet algorithme, que contient la variable $X$ après l'exécution de cet algorithme ?
		\item En supposant toujours que la variable $X$ contient la valeur $3$ avant l'exécution de cet algorithme, modifier l'algorithme de façon à ce que X contienne une valeur approchée à $0,001$ près de $\alpha$ après l'exécution de l'algorithme.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Vérifier qu'une primitive $H$ de la fonction $h$ sur [0~;~14] est :
		
\[H(x) = - \np{10000} \text{e}^{- 0,2x} + 5x^3 + 25x^2.\]

		\item  Calculer une valeur approchée à l'unité près de
$\dfrac{1}{12} \displaystyle\int_0^{12}  h(x)\:\text{d}x$.
		\item  Donner une interprétation dans le contexte de l'exercice.
	\end{enumerate}
\end{enumerate}
\end{document}