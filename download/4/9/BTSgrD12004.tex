%!TEX encoding = UTF-8 Unicode
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{graphicx}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength{\textheight}{23,5cm}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\setlength{\voffset}{-1,5cm}
\usepackage{fancyhdr}
\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS groupement D1},
pdftitle = {mai 2004},
allbordercolors = white
}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Groupement D1}}
\rfoot{\small{juin 2004}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}
\begin{center}
\Large\textbf{\decofourleft~Brevet de technicien supérieur~\decofourright\\ Groupement D1 Analyses biologiques - Biochimiste session 2004} 
\end{center}

\textbf{Exercice  1\hfill 10 points}

\medskip

\begin{center}
\textbf{Les deux parties de cet exercice peuvent être traitées de façon indépendante.}
\end{center}

\textbf{Partie A : Résolution d'une équation différentielle}

\medskip

On considère l'équation différentielle $(E)$ :
 
\[y'-y=2 (x+1) \text{e}^x\]

où $y$ désigne une fonction de la variable réelle $x$ définie et dérivable sur  $\R$ , $y'$ sa fonction dérivée.

\medskip

\begin{enumerate}
\item Résoudre l'équation différentielle $(E_0)$ : $y'-y=0$.
\item Déterminer les réels $a$ et $b$ de façon que la fonction $g$ définie sur $\R$ par
 
\[g(x) = \left(a x^2 + b x\right) \text{e}^x\]

soit une solution particulière de $(E)$.
\item En déduire la solution générale de l'équation différentielle $(E)$.
\item Déterminer la solution $f$ de l'équation $(E)$ qui vérifie la condition initiale : $f'(0)=3$.
\end{enumerate}

\bigskip

\textbf{Partie B : étude d'une fonction}

\medskip

Soit $f$ la fonction définie sur $\R$ par
 
\[f(x) = (x+1)^2\text{e}^x.\]

On note $\mathcal {C}$ la courbe représentative de $f$ dans le plan rapporté à un repère orthogonal \Oij{} d'unités graphiques : 1cm en abscisse et 4~cm en ordonnée.

\medskip

\begin{enumerate}
\item Déterminer la limite de $f$ en $+\infty$ et la limite de $f$ en $-\infty$ (on rappelle que, pour $\alpha > 0$, $\displaystyle\lim_{x \rightarrow -\infty}x^{\alpha}\text{e}^x=0$).

En déduire l'existence d'une asymptote à la courbe $\mathcal {C}$.
\item Montrer que $f'(x) = (x + 1)(x + 3)\text{e}^x$.
\item Etudier les variations de $f$ sur $\R$, puis dresser le tableau de variations de la fonction $f$.
\item Tracer la courbe $\mathcal {C}$ dans le plan repéré par \Oij.
\item Calcul d'aire :
	\begin{enumerate}
		\item Vérifier que $F(x) = \left(x^2 + 1\right)\text{e}^x$ est une primitive de $f$ sur $\R$.
		\item En déduire l'aire exacte $\mathcal {A}$, en $\text{cm}^2$, de la partie du plan limitée par la courbe $\mathcal {C}$, l'axe $(Ox)$ et les droites d'équations respectives $x = -1$ et $x = 0$.
		\item Donner la valeur arrondie de $\mathcal {A}$ à $10^{-2}$ près.
	\end{enumerate}
\end{enumerate}

\vspace{1.35cm}

\textbf{Exercice 2 \hfill 10 points}

\begin{center}
\textbf{Les trois parties de cet exercice peuvent être traitées de façon indépendante.}
\end{center}

\emph{Une entreprise fabrique en grande quantité des tiges en plastique de longueur théorique $100$~mm.\\
Les résultats seront arrondis au centième le plus proche.}

\medskip

\textbf{Partie A : Loi normale}

\medskip

Une tige est considérée comme conforme pour la longueur lorsque sa longueur, exprimée en millimètres, est dans l'intervalle $[99,64~;~100,36]$.

On note $X$ la variable aléatoire qui, à chaque tige prise au hasard dans la production, associe sa longueur. On suppose que $X$ suit une loi normale de moyenne $100$ et d'écart-type $0,16$.

\medskip

\begin{enumerate}
\item Calculer la probabilité qu'une tige prélevée au hasard dans la production soit conforme pour la longueur.
\item Déterminer le nombre réel $a$ tel que $P(X < a) = 0,96$.
\end{enumerate}

\bigskip

\textbf{Partie B : Loi binomiale et loi de Poisson}

\medskip

Dans un lot de ce type de tiges, 2\,\% des tiges n'ont pas une longueur conforme. On prélève au hasard $n$ tiges de ce lot pour vérification de  longueur. Le lot est assez important pour que l'on puisse assimiler ce prélèvement à un tirage avec remise de $n$ tiges.

On considère la variable aléatoire $Y$ qui, à tout prélèvement de $n$ tiges, associe le nombre de tiges de longueur non conforme.

\medskip

\begin{enumerate}
\item Pour cette question on prend $n = 50$.
	\begin{enumerate}
		\item Justifier que la variable aléatoire $Y$ suit une loi binomiale dont on donnera les paramètres.
		\item Calculer $P(Y = 3)$.
	\end{enumerate}
\item Pour cette question on prend $n = 100$. La variable aléatoire $Y$ suit alors une loi binomiale que l'on décide d'approcher par une loi de Poisson .
	\begin{enumerate}
		\item Déterminer le paramètre $\lambda$ de cette loi de Poisson .
		\item On désigne par $Z$ une  variable aléatoire suivant la loi de Poisson de paramètre $\lambda$ où $\lambda$ est le paramètre obtenu à la question 2. a. À l'aide de l'approximation de $Y$ par $Z$, calculer la probabilité d'avoir au plus 4~tiges de longueur non conforme.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie C : Text d'hypothèse}

\medskip

Un client reçoit un lot important de tiges de ce type. Il veut vérifier que la moyenne $\mu$ de l'ensemble des longueurs, en mm, des tiges constituant ce lot est égale à la longueur théorique.

On note $L$ la variable aléatoire qui, à chaque tige prélevée au hasard dans le lot, associe sa longueur en mm. La variable aléatoire $L$ suit la loi normale de moyenne inconnue $\mu$ et d'écart-type $0,16$.

On désigne par $\overline{L}$ la variable aléatoire qui, à chaque échantillon aléatoire de 90~tiges prélevé dans le lot, associe la moyenne des longueurs de ces tiges (le lot est assez important pour que l'on puisse assimiler ces prélèvements à des tirages avec remise). $\overline{L}$ suit la loi normale de moyenne $\mu$ et d'écart-type 
$\sigma = \dfrac{0,16}{\sqrt{90}} \approx 0,017$.

Le client construit un test d'hypothèse :

\setlength\parindent{5mm}
\begin{itemize}
\renewcommand{\labelitemi}{$\bullet$}
\item L'hypothèse nulle est $\textbf{H}_0$ : $\mu = 100$.
\item L'hypothèse alternative est $\textbf{H}_1$ : $\mu \neq 100$.
\item Le seuil de signification est fixé à 5\,\%.
\end{itemize}
\setlength\parindent{0mm}

\medskip

\begin{enumerate}
\item Sous l'hypothèse nulle $\textbf{H}_0$ déterminer le réel positif $h$ tel que :

\[P(100 - h < \overline{L} < 100 + h) = 0,95.\]

\item Énoncer la règle de décision permettant d'utiliser ce test.
\item Le client prélève un échantillon aléatoire de 90 tiges dans la livraison et il constate que la moyenne des longueurs de l'échantillon est de 100,04 mm. Le client estime que le fournisseur n'a pas respecté ses engagements et renvoie tout le lot.

Le client a-t-il raison ? Justifier votre réponse.
\end{enumerate}
\end{document}
