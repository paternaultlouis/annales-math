\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,makeidx}
\usepackage[normalem]{ulem}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
%Tapuscrit : Denis Vergès
\usepackage{fancybox}
\usepackage{ulem}
\usepackage{calc}
\usepackage{textcomp}
\usepackage{diagbox}
\usepackage{tabularx}
\newcommand{\euro}{\eurologo{}}
\usepackage{pstricks,pst-plot,pst-text}
\usepackage[body={15cm,23.5cm}]{geometry}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\setlength{\voffset}{-1,5cm}
\usepackage{fancyhdr} 
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{A. P. M. E. P.}
\lhead{\small Baccalauréat anticipé Première L }
\lfoot{\small{Polynésie}}
\rfoot{\small{juin 2008}}
\renewcommand \footrulewidth{.2pt}%
\pagestyle{fancy}
\thispagestyle{empty}
\begin{center} { \Large \textbf{\decofourleft~Baccalauréat Mathématiques-informatique \decofourright\\  Polynésie  juin 2008}}
    \end{center}
    
\vspace{0,5cm}

\textbf{\textsc{Exercice 1} \hfill 9 points}

\medskip
 
On étudie l'évolution de l'effectif d'une population de bactéries (estimé en milliers d'individus) en fonction du temps (exprimé en heures). On commence les relevés à $15$~h et on fait un relevé toutes les heures.

On appelle $n$ la durée, exprimée en heures, écoulée depuis $15$~h.

On note $u_{n}$ l'effectif de la population de bactéries, exprimé en milliers d'individus, relevé après $n$ heures. Ainsi $u_{1}$ est l'effectif de la population de bactéries, exprimé en milliers d'individus, relevé à $16$~h.

L'objectif de cet exercice est de réfléchir sur deux modèles qui essaient de décrire l'évolution de la population observée.

\medskip

\textbf{Partie A}

\medskip
 
Les premiers relevés permettent de dresser le tableau suivant :

\medskip

\renewcommand{\arraystretch}{1.25}
\begin{tabularx}{\linewidth}{|c|*{5}{>{\centering \arraybackslash}X|}}\hline
Heure					&15 h	&	16 h &17 h&18 h &19 h\\ \hline
$n$ (durée en h écoulée depuis 15 h)	&0	&1&2 &3& 4\\ \hline
$u_{n}$ (nombre de bactéries en milliers)& 6,9& 8,1&9,6&11,1&12,7\\ \hline
\end{tabularx}

\medskip

\begin{enumerate}
\item  Placer, dans le repère fourni en feuille annexe à rendre avec la copie, les points $M_{n}$ de coordonnées $\left(n, u_{n}\right)$.
\item  À quel type de croissance peut faire penser ce graphique ?
\end{enumerate}
 
\medskip

\textbf{Partie B}

\medskip
 
On saisit les données précédentes dans les colonnes A, B et C d'une feuille de calcul de tableur. Voir sa reproduction à la fin de l'exercice.

Les observations de la partie A suggèrent de modéliser l'évolution du nombre de bactéries, exprimé en milliers d'individus, après une durée de $n$ heures, à l'aide de la suite $\left(v_{n}\right)$ définie par : $v_{0} = 6{,}9$ et $v_{n+1} = v_{n} + 1{,}4$.
\begin{enumerate}
\item 
	\begin{enumerate}
		\item  Calculer $v_{1}$ et $v_{2}$.
		\item  Quelle est la nature de la suite $\left(v_{n}\right)$ ?
 	\end{enumerate}
\item Dans le tableau fourni à la fin  de l'exercice, on a saisi dans la cellule D3 la valeur de $v_{0} : 6{,}9$. Donner une formule à inscrire dans la cellule D4 qui permet d'obtenir, en recopiant vers le bas, les valeurs de la suite $\left(v_{n}\right)$ dans la colonne D.
\item Quel est le nombre de bactéries que l'on peut prévoir à $7$~h, le lendemain du jour où a commencé l'étude, si on utilise ce modèle ? Justifier.
\end{enumerate}
 
\medskip

\textbf{Partie C}

\medskip
 
En fait, les relevés effectués à partir de $7$~h, le lendemain du jour où a commencé l'étude, donnent des valeurs sensiblement différentes des prévisions fournies par le modèle étudié à la partie B comme le montre le tableau ci-dessous : 

\medskip

\begin{tabularx}{\linewidth}{|c|*{4}{>{\centering \arraybackslash}X|}}\hline
Heure&7 h 	&8 h 	& 9 h& 	10 h\\ \hline
$n$ (durée en h écoulée depuis $15$~h) & 	16& 	17& 	18& 	19\\ \hline
$u_{n}$ (nombre de bactéries en milliers)& 	51&	62& 	68&	79\\ \hline
\end{tabularx}

\medskip

On décide donc de modéliser différemment l'évolution du nombre de bactéries, exprimé en milliers d'individus, après une durée de $n$~heures, et de se servir pour cela de la suite $\left(w_{n}\right)$
définie par : $w_{0} = 6{,}9$ et $w_{n} = 6{,}9 \times 1{,}136^n$.
\medskip

 \textbf{Dans cette partie, les valeurs des termes de la suite \boldmath $\left(w_{n}\right)$ \unboldmath seront arrondies au dixième.}

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Calculer $w_{1}$ et $w_{2}$.
		\item Quelle est la nature de la suite $\left(w_{n}\right)$ ?
	\end{enumerate}
\item Dans la feuille de calcul reproduite ci-dessous, on a saisi $1{,}136$ dans la cellule E1 et $6{,}9$ dans la cellule E3.

Parmi les formules suivantes, quelles sont celles qui permettent, en les inscrivant dans la cellule E4 et en recopiant vers le bas, d'obtenir les valeurs de la suite $\left(w_{n}\right)$ dans la colonne E ?

\medskip
	 
\begin{tabularx}{1.1\linewidth}{*{4}{X}}
\textbf{a.}~ =E3*E1&	 \textbf{b.}~ =E3*E\$1&	 \textbf{c.}~ =E\$3*(E\$1$\wedge$ A4)& \textbf{d.}~	=E\$3*(E\$1 $\wedge$ B4)\\
\end{tabularx}

\medskip

\item Calculer $w_{16}$.
\item Calculer l'écart relatif, en pourcentage arrondi au dixième, entre $w_{16}$ et la valeur $u_{16}$ relevée à $7$~h.
\end{enumerate}

\vspace{0,25cm}


\textbf{Reproduction de la feuille de calcul sur tableur (parties B et C de l'exercice 1.}

\medskip

\begin{tabularx}{\linewidth}{|c|*{5}{>{\centering \arraybackslash}X|}}\hline
&A&B&C&D&E\\ \hline
1&&&&&	1,136\\ \hline
2&	heure&durée $n$&$u_{n}$&$v_{n}$&$w_{n}$\\ \hline
3&	15 h	&0	&6,9	&6,9	&6,9\\ \hline
4&	16 h	&1	&8,1	&	&\\ \hline
5&	17 h	&2	&9,6	&	&\\ \hline
6&	18 h	&3	&11,1	&	&\\ \hline
7&	19 h	&4	&12,7	&	&\\ \hline
8&	20 h	&5	&	&	&\\ \hline
9&	21 h	&6	&	&	&\\ \hline
10&	22 h	&7	&	&	&\\ \hline
11&	23 h	&8	&	&	&\\ \hline
\end{tabularx}
 
\vspace{0,5cm}

\textbf{\textsc{Exercice 2} \hfill 11 points}

\medskip
 
Le tableau (incomplet), fourni en feuille annexe, donne la répartition d'une population de $800$~utilisateurs d'Internet pour le téléchargement selon leur âge et leur volume de téléchargement mensuel.

Le volume de téléchargement est exprimé en Giga-octets (notés Go) et l'âge en années.

\medskip

\textbf{Partie A}

\medskip

\begin{enumerate}
\item Compléter le tableau donné en feuille annexe à rendre avec la copie. Aucune justification n'est demandée.
\item Les pourcentages demandés dans cette question seront arrondis à l'unité.
	\begin{enumerate}
		\item  Parmi ces utilisateurs d'Internet, quel pourcentage est dans la tranche d'âge [30~;~40[ ?
		\item  Parmi les utilisateurs d'Internet qui téléchargent entre $0$ et $2$ Go par mois, combien représentent, en pourcentage, ceux âgés de $40$~ans ou plus ?
 	\end{enumerate}
\end{enumerate}

\medskip

\textbf{Partie B}

\medskip

\begin{enumerate}
\item  Dans la population observée, combien d'utilisateurs d'internet ont moins de 30~ans ?

Expliquer alors pourquoi l'âge médian (la médiane) de cette population est nécessairement compris entre $20$ et $30$~ans.
\item  Pour déterminer cet âge médian, on donne, la répartition des âges dans la classe [20~;~30[. Elle est fournie par le tableau suivant :
 
 \medskip
 
\begin{tabularx}{\linewidth}{|l|*{10}{>{\centering \arraybackslash}X|}}\hline 
Âge	&20&	21&	22&23&	24&	25&26	&27& 	28&29\\ \hline
Effectif& 	25&	26&	30&	22&	34&	21&	19&	20&	14& 	12\\ \hline
\end{tabularx}

\medskip

Justifier que l'âge médian vaut $24$~ans.
\item   Les diagrammes en boîte des âges des utilisateurs d'internet qui téléchargent entre $0$ et $2$ ~Go et entre $6$ et $8$~Go sont représentés ci-dessous :

%\begin{center}
%\psset{xunit=0.18cm,yunit=0.4cm}
%\begin{pspicture}(10,-1)(70,10)
%\multido{\n=10+2}{31}{\psline[linestyle=dashed,linewidth=0.4pt](\n,-1)(\n,10)}
%\multido{\n=-1+1}{11}{\psline[linestyle=dashed,linewidth=0.4pt](10,\n)(70,\n)}
%\multido{\n=10+2}{31}{\uput[d](\n,0){\footnotesize \n}}
%\psline[linewidth=1.2pt](11,8)(13,8) \psline[linewidth=1.2pt](25,8)(52,8)
%\psframe[linewidth=1.2pt](13,7)(25,9) \psline[linewidth=1.2pt](22,7)(22,9)
%\psline[linewidth=1.2pt](14,4)(27,4) \psline[linewidth=1.2pt](55,4)(59,4)
%\psframe[linewidth=1.2pt](27,3)(55,5) \psline[linewidth=1.2pt](44,3)(44,5)
%\rput(58,8){Tranche [6 ; 8[} \rput(66,4){Tranche [0 ; 2[}
%\end{pspicture}


\begin{center}
\psset{xunit=0.2cm,yunit=0.4cm}
\begin{pspicture}(10,-1)(70,10)
\multido{\n=10+2}{31}{\psline[linestyle=dashed,linewidth=0.4pt](\n,0)(\n,10)}
\multido{\n=0+1}{11}{\psline[linestyle=dashed,linewidth=0.4pt](10,\n)(70,\n)}
\multido{\n=10+2}{31}{\uput[d](\n,0){\footnotesize \n}}
\psline[linewidth=1.2pt](11,8)(13,8) \psline[linewidth=1.2pt](25,8)(52,8)
\psframe[linewidth=1.2pt](13,7)(25,9) \psline[linewidth=1.2pt](22,7)(22,9)
\psline[linewidth=1.2pt](14,4)(27,4) \psline[linewidth=1.2pt](55,4)(59,4)
\psframe[linewidth=1.2pt](27,3)(55,5) \psline[linewidth=1.2pt](44,3)(44,5)
\psframe[fillstyle=solid,fillcolor=white,framearc=0.5](54,7)(65,9) \rput(59.5,8){Tranche [6 ; 8[}
\psframe[fillstyle=solid,fillcolor=white,framearc=0.5](62,3)(73,5) \rput(67.5,4){Tranche [0 ; 2[}
\end{pspicture}
\end{center}
%\end{center}

Les propositions suivantes sont-elles vraies ou fausses ? Justifier les réponses.

\medskip

\textbf{Proposition a :} l'écart interquartile de la série des   âges des 
utilisateurs qui téléchargent entre $0$ et $2$~Go est plus du double de la série des  âges des  utilisateurs qui téléchargent entre $0$ et $2$ Go.

\medskip

\textbf{Proposition b :} plus de 75\,\% des utilisateurs qui téléchargent entre $0$ et $2$~Go ont plus de 26 ans.

\medskip

\textbf{Proposition c :} plus de la moitié des utilisateurs qui téléchargent entre $6$ et $8$~Go sont mineurs.

\end{enumerate}

\newpage

\begin{center}
\textbf{ANNEXE à rendre avec la copie}

\vspace{0,5cm}

Exercice 1, partie A, question 1

\medskip

%\begin{pspicture}(-0.5,4)(8.5,17)
%\psaxes[linewidth=1.5pt,Oy=5](0,5)(-0.5,4)(8.5,17)
%\multido{\n=-0.5+0.25}{37}{\psline[linestyle=dotted](\n,4)(\n,17)}
%\multido{\n=4+0.5}{27}{\psline[linestyle=dotted](-0.5,\n)(8.5,\n)}
%\uput[l](0,5){5}
%%\multido{\n=0+1}{9}{\uput[d](\n,5){\n}}
%%\multido{\n=5+1}{12}{\uput[l](0,\n){\n}}
%\end{pspicture}
%\end{center}

\begin{pspicture}(-0.5,4)(17,16.5)
\psset{xunit=1.8cm}
\multido{\n=0+0.25}{33}{\psline[linewidth=0.5pt](\n,5)(\n,16)}
\multido{\n=5+0.5}{23}{\psline[linewidth=0.5pt](0,\n)(8,\n)}
\multido{\n=5+1}{12}{\uput[l](0,\n){\n}}
\multido{\n=0+1}{9}{\uput[d](\n,5){\n}}
\psline{->}(0,4.5)(0,16.5) \psline{->}(-0.25,5)(8.25,5)
\multido{\n=0+1}{9}{\psline(\n,4.85)(\n,5.15)}
\multido{\n=5+1}{12}{\psline(-0.075,\n)(0.075,\n)}
\end{pspicture}
\end{center}

\vspace{0,5cm}

\textbf{Exercice 2, partie A, question 1}

\medskip

\renewcommand{\arraystretch}{1.25}
 \begin{tabularx}{\linewidth}{|c|*{5}{>{\centering \arraybackslash}X|}}\hline
\backslashbox{Tranche d'âge}{Volume en Go}&[0 ; 2[&[2 ; 4[&[4 ; 6[&[6 ; 8[&Total\\ \hline
[10 ; 20[&21	&51	&80	&125	&277\\ \hline
[20 ; 30[&17	&	&	&107	&223\\ \hline
[30 ; 40[&22	&44	&50	&47	&163\\ \hline
[40 ; 50[&30 	& 20	&20	& 12	& \\ \hline
[50 ; 60[&42	&	&2 	&8	&\\ \hline
Total	&132 	&158	&	&299	&800\\ \hline
\end{tabularx}
\end{document}