%!TEX encoding = UTF-8 Unicode
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} 
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength{\textheight}{23,5cm}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\setlength{\voffset}{-1,5cm}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P. M. E. P.}}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Géomètre  topographe}}
\rfoot{\small{mai 2013}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}

\begin{center}{\Large\textbf{Brevet de technicien supérieur session 2013\\ Géomètre  topographe}}
  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 : courbe plane \hfill 9 points}

\medskip
 
Le plan est rapporté à un repère orthonormal \Oij{} représenté en annexe 1 . 

On considère la courbe $C$, d'équation polaire 

\[r(\theta) = 4\sin \theta \cos^2\theta \quad  \text{où}\: \: \theta \in \R.\]
 
On note $M_{\theta}$ le point de $C$ de coordonnées polaires $(r~;~\theta)$. 

Le but de cet exercice est d'étudier quelques propriétés de la courbe $C$ et de la tracer.

\bigskip
 
\textbf{A. Détermination de l'intervalle d'étude}

\medskip
 
\begin{enumerate}
\item Montrer que $r$ est une fonction périodique de période $2\pi$. 
\item Étudier la parité de la fonction $r$. 

Que peut-on en conclure pour les points $M_{\theta}$ et $M_{- \theta}$ de la courbe $C$ ? 
\item Déduire des deux questions précédentes que l'intervalle d'étude de $r$ peut être réduit à $[0~;~\pi]$ et donner une propriété géométrique de la courbe $C$. 
\item Comparer $r(\pi - \theta)$ et $r(\theta)$. 

Que peut-on en conclure pour les points $M_{\theta}$ et $M_{\pi -\theta}$ de la courbe $C$ ?
 
En déduire que l'étude de la fonction $r$ peut être réduite à l'intervalle 
$\left[0~;~ \dfrac{\pi}{2}\right]$.
\end{enumerate}

\bigskip
 
\textbf{B. Étude et tracé de la courbe } \boldmath $C$ \unboldmath

\medskip
 
\begin{enumerate}
\item On note $r'$ la fonction dérivée de la fonction $r$. 

Montrer que $r'(\theta) = 4 cos\theta \left(1 - 3 \sin^2 \theta\right)$. 
\item Étudier le signe de la fonction $r'$ pour tout réel de l'intervalle $\left[0~;~ \dfrac{\pi}{2}\right]$.

Dresser le tableau des variations de la fonction $r$ sur l'intervalle $\left[0~;~\dfrac{\pi}{2}\right]$.
\item Compléter le tableau de valeurs de l'annexe 1  avec des valeurs exactes. 
\item Justifier que la tangente à la courbe $C$ au point $M_{0}$ est l'axe (O$x$).
 
On admettra qu'aux points $M_{\pi/6}$ et $M_{\pi/2}$ la tangente à la courbe $C$ est \og verticale \fg{} et qu'au point $M_{\pi/4}$ elle est \og horizontale \fg. 
\item Placer les points $M_{0} ; M_{\pi/6}, M_{\pi/4}$ et $M_{\pi/2}$ sur le repère de l'annexe 1.
 
Tracer les tangentes à la courbe $C$ en ces quatre points. 
\item Montrer que le rayon de courbure de la courbe $C$ au point $M_{0}$ est $2$. 
 
On admettra que $ r''(\theta) = 4 \sin \theta \left(2 - 9 \cos^2 \theta \right)$ et pour rappel : $R = \dfrac{\left(r^2 + r'^2\right)^{3/2}}{r^2 + 2r'^2 - rr''}$.
Donner les coordonnées cartésiennes du centre du cercle osculateur à la courbe C au point $M_{0}$. 
\item Tracer le cercle osculateur à la courbe $C$ au point $M_{0}$.
 
Tracer la courbe $C$. 
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 2 : géométrie sphérique \hfill 11 points}

\medskip

Dans l'espace, rapporté à un repère orthonormal direct \Oijk, la Terre est assimilée à une sphère $\sum$ de centre O et de rayon 1.
 
Tout point de $\sum$ est alors repéré par le couple $(\theta~;~\varphi)$ où $\theta$ est sa longitude et $\varphi$ sa latitude (en radians).
 
Soient les points N$\left(0~;~\dfrac{\pi}{2}\right)$ ; S$\left(0~;~- \dfrac{\pi}{2}\right)$ ; A$\left(- \dfrac{\pi}{2}~;~0\right)$ ; B$\left(\dfrac{\pi}{2}~;~\dfrac{\pi}{4}\right)$ ; C$\left(0~;~\dfrac{\pi}{3}\right)$ et D$\left(\dfrac{\pi}{2}~;~0\right)$.

\bigskip
 
\textbf{A. Trigonométrie sphérique}

\medskip
 
\begin{enumerate}
\item 
	\begin{enumerate}
		\item Placer les points N, S, A, B, C et D sur la figure de l'annexe 2 page 5. 
		\item Tracer le triangle sphérique NBC.
	\end{enumerate} 
\item
	\begin{enumerate}
		\item Donner les coordonnées cartésiennes des points N, S, A et D.
		\item Déterminer les coordonnées cartésiennes des points B et C.
	\end{enumerate}		 
\item  Déterminer les six éléments du triangle sphérique NBC. Donner les valeurs exactes et ensuite les valeurs approchées en arrondissant à $10^{-2}$.

\end{enumerate}

\bigskip
 
\textbf{B. Projection stéréographique de pôle A}

\medskip
 
On note $I$ l'inversion de pôle A et de puissance $2$.

\medskip
 
\begin{enumerate}
\item 
	\begin{enumerate}
		\item Justifier que les points N, C et S sont des points invariants par $I$. 
		\item Déterminer les coordonnées cartésiennes de l'image du point D par $I$. 

On admettra que B$'$ image du point B par $I$ a pour coordonnées $\left(0~;~0~;~\sqrt{2} - 1\right)$ .
	\end{enumerate} 
\item Déterminer l'image de la sphèreL privée du point A par $I$. Justifier votre réponse.

\medskip
 
L'objectif des deux questions suivantes est de représenter les images par $I$ des deux triangles sphériques NDC et NBC. 
\item 
	\begin{enumerate}
		\item Quelle est l'image par $I$ du grand cercle $\Gamma_{1}$ privé du point A passant par N et D ? Justifier votre réponse. 
		\item Quelle est l'image par $I$ du grand cercle $\Gamma_{2}$ privé du point A passant par C et D ? Justifier votre réponse. 
Quelle est l'image par 1 du grand cercle $\Gamma_{3}$ passant par C et N ? Justifier votre réponse.
 
Tracer sur le repère de l'annexe 2  l'image du triangle sphérique NDC.
	\end{enumerate} 
\item 
	\begin{enumerate}
		\item Quelle est la nature de l'image par $I$ du grand cercle $\Gamma_{4}$ passant par B et C ? Justifier votre réponse. 
		\item En remarquant que le point CI diamétralement opposé au point C est invariant par $I$, construire l'image de l'arc $\widearc{\text{BC}}$ dans le repère de l'annexe 2.
		 
Laisser apparents les traits de construction et surligner d'une couleur différente de celle utilisée dans la question 3.  d. les contours de l'image du triangle sphérique NBC. 
	\end{enumerate}
\end{enumerate}

\newpage
\begin{center}

\textbf{ANNEXE 1 (à rendre avec la copie)}

\vspace{1cm}
 
\begin{flushleft}\textbf{Exercice 1. Question B. 3 }\end{flushleft}

\bigskip

\renewcommand\arraystretch{2}
\begin{tabularx}{0.8\linewidth}{|c|*{4}{>{\centering \arraybackslash}X|}}\hline
$\theta$&0&$\dfrac{\pi}{6}$&$\dfrac{\pi}{4}$&$\dfrac{\pi}{2}$\\ \hline
$r$&&&&\\ \hline
$r'$&&&&\\ \hline
\end{tabularx}

\vspace{1cm}

\begin{flushleft}\textbf{Exercice 1. Questions B. 5 et B. 7} \end{flushleft}

\vspace{0,5cm}
\psset{unit=2cm}
\begin{pspicture*}(-2.25,-0.25)(2.75,3.25)
\psaxes[linewidth=1.5pt]{->}(0,0)(-2.25,-0.25)(2.75,3.25)
\psgrid[gridlabels=0pt,subgriddiv=4,gridcolor=orange,subgridcolor=orange](-2.25,0)(2.75,3.25)
\uput[d](2.7,0){$x$}\uput[l](0,3.2){$y$}
\uput[dl](0,0){O}
\end{pspicture*}
\end{center}

\newpage
\begin{center}

\textbf{ANNEXE 2 (à rendre avec la copie)}

\space{1cm}

\begin{flushleft}\textbf{Exercice 2. Question A. 1}\end{flushleft}

\bigskip

\psset{unit=0.9cm}
\begin{pspicture}(12,8.5)
\psline{->}(0,4.8)(12,3.5)
\psline{->}(8.2,6.4)(3.8,1.8)
\psline{->}(6,0)(6,8.5)
\pscircle(6,4.1){4.1}
\rput(0,3.1){\scalebox{.99}[0.3]{\psarc(6.1,4.1){4.1}{180}{0}}%
\scalebox{.99}[0.3]{\psarc[linestyle=dashed](6.1,4.1){4.1}{0}{180}}}
\rput{-100}(6,10.25){\scalebox{.99}[0.3]{\psarc(6.1,4.1){4.1}{180}{0}}%
\scalebox{.99}[0.3]{\psarc[linestyle=dashed](6.1,4.1){4.1}{0}{180}}}
\rput(5.8,4.3){O} \rput(3.5,1.8){$x$}\rput(12.2,3.5){$y$}
\rput(6,8.7){$z$}
\end{pspicture} 

\vspace{2cm}

\begin{flushleft}\textbf{Exercice 2. Questions B. 3 et B. 4}\end{flushleft}

\bigskip
\psset{unit=2.5cm}
\begin{pspicture*}(-1.2,-2.2)(2.25,1.25)
\psaxes[linewidth=1.5pt]{->}(0,0)(-1.2,-2.2)(2.25,1.25)
\psgrid[gridlabels=0pt,subgriddiv=1,gridcolor=orange](-2.25,-2.2)(2.75,3.25)
\uput[d](2.2,0){$x$}\uput[l](0,1.2){$y$}\uput[dl](0,0){O}
\end{pspicture*}
 \end{center}
\end{document}