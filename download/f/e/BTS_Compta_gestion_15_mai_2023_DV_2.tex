\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage[dvips]{graphicx}
\usepackage{tabularx}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo}
\usepackage{pst-plot,pst-text,pst-tree,pst-func,pstricks-add}%
\usepackage{color,colortbl}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
% Tapuscrit : Denis Vergès
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\setlength\headheight{14pt}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{fancyhdr}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS Comptabilité et gestion},
pdftitle = {Métropole  - 15 mai  2023},
allbordercolors = white,
pdfstartview=FitH} 
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}

\begin{document}

\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur Métropole}
\lfoot{\small{Comptabilité et gestion}}
\rfoot{\small{15 mai 2023}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~Brevet de technicien supérieur Métropole~\decofourright\\[7pt]15 mai 2023 - Comptabilité et gestion\footnote{Candidats libres ou établissement privé hors contrat}}}
\end{center}

\vspace{0.25cm}

\textbf{\large{}Exercice 1 \hfill 10 points}

\emph{Les deux parties de cet exercice sont indépendantes}

\medskip

On s'intéresse à deux aspects des services de communication mobiles en France.

\bigskip

\textbf{Partie A : Données consommées sur les réseaux mobiles en France}

\medskip

Le tableau ci-dessous donne le volume total en exaoctets, noté Eo (1 exaoctet $= 10^{18}$ octets), des données consommées sur les réseaux mobiles pour la période 2017--2021.

\begin{center}
\begin{tabularx}{\linewidth}{|m{2.75cm}|*{5}{>{\centering \arraybackslash}X|}}\hline
Année &2017 &2018 &2019 &2020 &2021\\ \hline
Rang de l'année $x_i$&0&1&2&3&4\\ \hline
Volume de données $y_i$ (en Eo)&2,20 &3,64 &5,24 &7,13 &8,66\\ \hline
\multicolumn{6}{r}{(\emph{source: ARCEP }2022)}\\
\end{tabularx}
\end{center}

La calculatrice est nécessaire pour la plupart des calculs demandés.

\medskip

\begin{enumerate}
\item Déterminer un ajustement affine de $y$ en fonction de $x$ selon la méthode des moindres carrés.
Les coefficients de l'équation de la droite seront arrondis à $0,001$~près.
\item On décide d'ajuster le nuage de points de cette série statistique $\left(x_i~;~y_i\right)$ par la droite d'équation : $y = 1,6x + 2,1$.

Utiliser ce modèle pour répondre aux questions suivantes:
	\begin{enumerate}
		\item Estimer le volume total des données consommées en 2022.
		\item Estimer à partir de quelle année le volume total des données dépassera $15$ Eo.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie B : Nombre de SMS émis en France}

\medskip

Le tableau ci-dessous, extrait d'une feuille de calcul d'un tableur, donne le nombre de milliards de SMS émis chaque année en France pour la période 2017-2021.

Les cellules de la ligne 3 sont au format pourcentage à deux décimales.

\begin{center}
\begin{tabularx}{\linewidth}{|c|>{\centering \arraybackslash}m{3cm}|*{5}{>{\centering \arraybackslash}X|}}\hline
&\cellcolor{lightgray}A &\cellcolor{lightgray}B&\cellcolor{lightgray}C&\cellcolor{lightgray}D&\cellcolor{lightgray}E&\cellcolor{lightgray}F\\ \hline
\cellcolor{lightgray}1&Année&2017 &2018&2019 &2020&2021\\ \hline
\cellcolor{lightgray}2&Nombre de SMS émis (en milliards)&184,5& 171,3&159,9 &136,5& 119,6\\ \hline
\cellcolor{lightgray}3&Taux d'évolution par rapport à l'année 2017 \newline (en \,\%)&\cellcolor{lightgray}&&&&\\ \hline
\multicolumn{7}{r}{(\emph{source: ARCEP }2022)}\\
\end{tabularx}
\end{center}

\begin{enumerate}
\item Écrire une formule à saisir en C3 qui permet, par recopie vers la droite, de calculer les taux d'évolution du nombre de SMS émis chaque année par rapport à l'année 2017.
\item Justifier que, sur la période 2017 à 2021, le nombre de SMS émis a diminué d'environ 35,2\,\%. 
\item Calculer le taux d'évolution annuel moyen du nombre de SMS émis entre 2017 et 2021.
Arrondir à 0,01\,\% près.
\item La suite $\left(u_n\right)$ modélise le nombre de milliards de SMS émis pour l'année $(2021 + n)$. On a ainsi: $u_0 = 119,6$.

On suppose qu'à partir de l'année 2021, le nombre $u_n$ de SMS émis diminue chaque année de 10,3\,\%.
	\begin{enumerate}
		\item Calculer $u_1$ puis $u_2$. Arrondir à 0,1 milliard près. 
		
Interpréter ces résultats dans le contexte de l'exercice.
		\item Quelle est la nature de la suite $\left(u_n\right)$ ? Préciser sa raison. Justifier.
		\item Pour tout entier naturel $n$, exprimer $u_n$ en fonction de $n$.
		\item D'après ce modèle, quel serait le nombre de SMS émis en 2027 ? Arrondir à $0,1$~milliard près.
		\item D'après ce modèle, en quelle année le nombre de SMS émis passera-t-il pour la première fois sous les $50$~milliards ?
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{\large{}Exercice 2 \hfill 10 points}

\medskip

\emph{Les trois parties de cet exercice sont indépendantes}

\medskip

On s'intéresse à la production d'un producteur de noix (nuciculteur) du Périgord.

\bigskip

\textbf{Partie A : Probabilités conditionnelles}

\medskip

Pour ce nuciculteur, 62\,\% des noix récoltées sont de la variété \og Franquette \fg, 27\,\% des noix récoltées sont de la variété \og Corne \fg{} et le reste sont des noix de la variété \og Marbot \fg,

Une étude statistique a montré que 3\,\% des noix de la variété \og Franquette \fg, 4\,\% des noix de la variété \og Corne\fg et 2\,\% des noix de la variété \og Marbot \og{} sont vides quand elles sont récoltées.

On choisit une noix au hasard dans la récolte de ce nuciculteur. Toutes les noix ont la même probabilité d'être choisies.

On s'intéresse alors aux évènements suivants:

\begin{itemize}
\item $F$ : la noix est de la variété \og Franquette \fg 
\item $C$ : la noix est de la variété \og Corne \fg
\item $M$ : la noix est de la variété \og Marbot \fg
\item $V$ : la noix est vide; $\overline{V}$ est l'évènement contraire de $V$.
\end{itemize}

\begin{enumerate}
\item Recopier et compléter l'arbre de probabilité suivant :

\begin{center} \pstree[treemode=R,nodesepA=0pt,nodesepB=5pt,levelsep=3.5cm,treesep=1cm]{\TR{}}
{
\pstree[nodesepA=5pt]{\TR{$F$}\naput{$0,62$}}
	{
	\TR{$V$}\naput{$0,03$}
	\TR{$\overline{V}$}\nbput{$\ldots$}
	}
\pstree[nodesepA=5pt]{\TR{$C$}\naput{$\ldots$}}
	{
	\TR{$V$}\naput{$\ldots$}
	\TR{$\overline{V}$}\nbput{$\ldots$}
	}
\pstree[nodesepA=5pt]{\TR{$M$}\nbput{$\ldots$}}
	{
	\TR{$V$}\naput{$\ldots$}
	\TR{$\overline{V}$}\nbput{$\ldots$}
	}
}
\end{center}

\item
	\begin{enumerate}
		\item Calculer la probabilité que la noix soit de la variété \og Franquette\fg{} et qu'elle soit vide.
		\item Calculer la probabilité $P\left(F \cap \overline{V}\right)$. Interpréter ce résultat dans le contexte de l'énoncé.
	\end{enumerate}
\item Démontrer que $P(V) = \np{0,0316}$.
\item On suppose que la noix choisie n'est pas vide.

Quelle est la probabilité qu'elle soit de la variété \og Corne\fg{} ? Arrondir le résultat à $0,001$ près.
\end{enumerate}

\bigskip

\textbf{Partie B : Loi binomiale}

\medskip

On prélève au hasard $100$ noix dans la récolte de ce nuciculteur.

La quantité de noix est assez grande pour assimiler ce prélèvement à un tirage avec remise.

On considère la variable aléatoire $X$ qui, à tout prélèvement de $100$ noix dans la récolte de ce nuciculteur, associe le nombre de noix vides.

On admet que la probabilité pour qu'une noix soit vide est égale à $0,03$.

\medskip

\begin{enumerate}
\item Justifier que la variable $X$ suit une loi binomiale dont on précisera les paramètres.
\item Calculer l'espérance de la variable aléatoire $X$ et interpréter ce résultat dans le contexte de l'exercice.
\item Calculer $P(X = 0)$, arrondi à $0,001$ près et interpréter ce résultat dans le contexte de l'exercice.
\item Calculer $P(X \leqslant 3)$. Arrondir à $0,001$ près.
\item Calculer la probabilité pour que, dans un tel prélèvement, au moins quatre noix soient vides.
Arrondir à $0,001$ près.
\end{enumerate}

\bigskip

\textbf{Partie C : Loi normale}

\medskip

On note $Y$ la variable aléatoire qui, à chaque noix récoltée par ce nuciculteur, associe sa masse en grammes. 

On admet que $Y$ suit la loi normale d'espérance $28$ et d'écart-type $4$.

\medskip

\begin{enumerate}
\item Le nuciculteur vend une partie de sa récolte sous la forme de filets contenant 100 noix chacun. Quelle est la masse moyenne d'un tel filet?
\item Déterminer $P(20 \leqslant Y \leqslant 36)$. Arrondir à $0,01$ près.
\item Le nuciculteur décide de ne pas utiliser les noix dont la masse est inférieure à $18$ grammes.

Déterminer la probabilité pour qu'une noix prise au hasard dans la production soit ainsi rejetée. Arrondir à $0,001$ près.
\end{enumerate}
\end{document}