%!TEX encoding = UTF-8 Unicode
\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{graphicx}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\setlength\parindent{0mm}
\usepackage[np]{numprint}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\begin{document}
\lhead{\small Brevet de technicien supérieur}
\rhead{\small Métropole}
\lfoot{\small{Comptabilité et gestion des organisations}}
\rfoot{\small{juin 2007}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}
\begin{center} \Large \textbf{\decofourleft~Brevet de technicien supérieur Métropole~\decofourright\\ Comptabilité et gestion des organisations session 2007}  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 10 points}

\begin{center}
\textbf{Les trois parties de cet exercice sont indépendantes.}
\end{center}

Un atelier d'assemblage de matériel informatique s'approvisionne en pièces d'un certain modèle.

\medskip

\textbf{A. Évènements indépendants et probabilités conditionnelles}

\medskip

L'atelier reçoit ce modèle de pièces en grande quantité. Chaque pièce peut présenter deux défauts que l'on appelle défaut $a$ et défaut $b$.

On prélève une pièce au hasard dans une importante livraison.

On note $A$ l'évènement : \og l'appareil présente le défaut $a$ \fg{} et on note $B$ l'évènement : \og l'appareil présente le défaut $b$ \fg.
 
On admet que les probabilités des évènements $A$ et $B$ sont $P(A) = 0,02$ et
  
$P(B) = 0,01$.
  
On suppose que les deux évènements $A$ et $B$ sont indépendants.

\medskip
\begin{enumerate}
\item  Calculer la probabilité de l'évènement $E_{1}$ : \og la pièce présente le défaut $a$ et le défaut $b$ \fg.
\item  Calculer la probabilité de l'évènement $E_{2}$ \og la pièce est défectueuse, c'est-à-dire qu'elle présente au moins un des deux défauts \fg.
\item  Calculer la probabilité de l'évènement $E_{3}$ : \og la pièce ne présente aucun défaut \fg.
\item  Calculer la probabilité que la pièce présente les deux défauts sachant qu'elle est défectueuse. Arrondir à $10^{-4}$.
\end{enumerate}

\begin{center}
\textbf{Dans ce qui suit, tous les résultats approchés sont à arrondir à}~\boldmath $10^{-3}$ \unboldmath
\end{center}
 
\medskip

\textbf{B. Loi binomiale}

\medskip

On note $D$ l'évènement : \og une pièce prélevée au hasard dans un stock important est défectueuse \fg.

On suppose que $P(D) =  0,03$.

On prélève au hasard $200$~pièces dans le stock pour vérification. Le stock est assez important pour que l'on puisse assimiler ce prélèvement à un tirage avec remise de $200$~pièces.

On considère la variable aléatoire $X$ qui, à tout prélèvement de $200$~pièces, associe le nombre de pièces de ce prélèvement qui sont défectueuses.

\medskip

\begin{enumerate}
\item  Justifier que la variable aléatoire $X$ suit une loi binomiale dont on déterminera les paramètres.
\item  Calculer la probabilité que dans un tel prélèvement ii y ait exactement une pièce défectueuse.
\item  Calculer la probabilité que dans un tel prélèvement il y ait au moins deux pièces défectueuses.
\end{enumerate}

\medskip

\textbf{C. Loi normale}

\medskip

On s'intéresse maintenant à la masse de ces pièces.

On note $Y$ la variable aléatoire qui à chaque pièce prélevée au hasard dans un lot important associe sa masse en grammes.

On suppose que la variable aléatoire $Y$ suit la loi normale de moyenne $500$ et d'écart type $4$.

\medskip

\begin{enumerate}
\item  Calculer $P(Y \leqslant  510)$.
\item  Une pièce de ce modèle est acceptable pour la masse lorsque celle-ci appartient à l'intervalle [490 ; 510]. Calculer la probabilité qu'une pièce prélevée au hasard soit acceptable pour la masse.
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 2 \hfill 10 points}

\medskip

\textbf{A. Étude d'une fonction}

\medskip

Soit $f$ la fonction définie sur $[0~;~+ \infty[$ par

\[f(x) = 4 - \text{e}^{-x}(x + 2)^2.\]

On note $\mathcal{C}$ la courbe représentative de la fonction $f$ dans le plan muni d'un repère orthogonal \Oij. On prend comme unités : 1~cm pour 1 sur l'axe des abscisses et 2~cm pour 1 sur l'axe des ordonnées.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item  On admet que $\displaystyle\lim_{x \to + \infty}\text{e}^{-x}(x + 2)^2 =  0$ ; en déduire $\displaystyle\lim_{x \to + \infty} f(x)$.

		\item  En déduire que la courbe $\mathcal{C}$ admet une asymptote $\Delta$ dont on donnera une équation.
	\end{enumerate}
\item
	\begin{enumerate}
		\item  Démontrer que pour tout réel $x$ de $[0~;~+ \infty[,~ f'(x) =  x(x + 2)\text{e}^{-x}$. 
		\item  Étudier le signe de $f'(x)$ pour tout réel $x$ de $[0~;~+ \infty[$. Établir le tableau de variations de la fonction $f$.
	\end{enumerate}
\item Compléter, après l'avoir reproduit, le tableau suivant dans lequel les valeurs approchées sont à arrondir à $10^{-2}$.\\

\medskip

\renewcommand{\arraystretch}{2}
\begin{tabularx}{\linewidth}{|*{10}{>{\centering \arraybackslash}X|}}\hline
$x$&	0&	1&	2&	3&	4&	5&	6&	7 &	8\\ \hline
$f(x)$&&&&&&&&&\\ \hline
\end{tabularx}

\medskip

\item  Construire la droite $\Delta$ et la courbe $\mathcal{C}$ sur une feuille de papier millimétré.
\end{enumerate}

\bigskip

\textbf{B. Calcul intégral}

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item  Soient $g$ et $h$ les fonctions définies sur $[0~;~+ \infty[$ respectivement par :
		\[g(x) = - \text{e}^{-x}(x + 2)^2~~ \text{et}~~ h(x) =  \text{e}^{-x}\left(x^2 + 6x + 10\right).\]
 Démontrer que $h$ est une primitive de $g$ sur $[0~;~+ \infty[$. 
		\item Déduire du \textbf{1. a.} une primitive $F$ de la fonction $f$ sur $[0~;~+ \infty[$.
	\end{enumerate}
\item
	\begin{enumerate}
		\item  Démontrer que la valeur moyenne de $f$ sur [0  : 8] est $V_{m} = \dfrac{11 + 61\text{e}^{-8}}{4}$.
		\item  Donner la valeur approchée, arrondie à $10^{-2}$, de $V_{m}$.
 	\end{enumerate}
\end{enumerate}
 
\medskip

\textbf{C, Application économique}

\medskip

Depuis le premier janvier 1999 une entreprise fabrique un produit noté $P$. Ce produit a été commercialisé dans une ville comportant \np{40000}~foyers acheteurs potentiels.

On admet que le nombre de foyers équipés du produit $P$ le premier janvier de l'année $(1999 + n)$ est égal à $\np{10000} \times f(n)$, où $f$ est la fonction définie dans la partie A.

\medskip

\begin{enumerate}
\item  Déterminer le pourcentage de foyers équipés du produit $P$ le premier janvier 2007 parmi les foyers acheteurs potentiels. Arrondir à 1\,\%.
\item  Déterminer le nombre de foyers qui se sont équipés entre le premier janvier 2001 et le premier janvier 2002.
\end{enumerate}
\end{document}