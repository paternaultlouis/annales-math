%!TEX encoding = UTF-8 Unicode
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{fancybox}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{lscape}
\usepackage{diagbox}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Relecture : François Hache
\usepackage{pst-plot,pst-tree,pstricks,pst-node,pst-text}
\usepackage{pstricks-add}
\newcommand{\R}{\textbf{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength{\textheight}{23.5cm}
\setlength{\voffset}{-1.5cm}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{multicol}

\usepackage{fp}% obligatoire pour des nombres aléatoires
\usepackage{xstring}
\usepackage{spreadtab}
%%%%   Commandes perso FH
\newcommand{\ds}{\displaystyle}%   displaystyle
\newcommand{\cg}{\texttt{]}}% crochet gauche
\newcommand{\cd}{\texttt{[}}% crochet droit
\newcommand{\pg}{\geqslant}%  plus grand ou égal
\newcommand{\pp}{\leqslant}%  plus petit ou égal
\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Baccalauréat S},
pdftitle = {Asie - 22 juin 2017},
allbordercolors = white,
pdfstartview=FitH} 
\usepackage[frenchb]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
\renewcommand{\d}{\,\text{d}}% le d de différentiation
\newcommand{\e}{\,\text{e}\,}% le e de l'exponentielle
\renewcommand{\i}{\text{\,i}}% le i des complexes
\begin{document}

\setlength\parindent{0mm}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat S}
\lfoot{\small{Asie}}
\rfoot{\small{22 juin 2017}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty} 

\begin{center} {\Large{\textbf{\decofourleft~Baccalauréat S -- Asie~\decofourright\\[10pt]22 juin 2017}}}
\end{center}

\vspace{0,5cm}

\subsection*{\textsc{Exercice 1} \hfill 5 points}
 
\textbf{Commun  à tous les candidats}

\bigskip

Un protocole de traitement d'une maladie, chez l'enfant, comporte une perfusion longue durée d'un
médicament adapté. La concentration dans le sang du médicament au cours du temps est modélisée
par la fonction $C$ définie sur l'intervalle $[0~;~+ \infty[$ par :

\[C(t) = \dfrac{d}{a}\left(1 - \text{e}^{-\frac{a}{80} t}\right)\]

\begin{list}{\textbullet}{où}
\item $C$ désigne la concentration du médicament dans le sang, exprimée en micromole par litre,

\item $t$ le temps écoulé depuis le début de la perfusion, exprimé en heure,

\item $d$ le débit de la perfusion, exprimé en micromole par heure,

\item $a$ un paramètre réel strictement positif, appelé clairance, exprimé en litre par heure.

\end{list}

Le paramètre $a$ est spécifique à chaque patient.

\medskip

En médecine, on appelle \og plateau \fg{} la limite en $+ \infty$ de la fonction $C$.

\bigskip

\textbf{Partie A : étude d'un cas particulier}

\medskip

La clairance $a$ d'un certain patient vaut 7, et on choisit un débit $d$ égal à $84$.

Dans cette partie, la fonction $C$ est donc définie sur $[0~;~+ \infty[$ par : 

\[C(t) =  12\left(1 - \text{e}^{-\frac{7}{80} t}\right).\]

\begin{enumerate}
\item Étudier le sens de variation de la fonction $C$ sur $[0~;~+ \infty[$.
\item Pour être efficace, le plateau doit être égal à 15. Le traitement de ce patient est-il efficace ?
\end{enumerate}

\bigskip

\textbf{Partie B : étude de fonctions}

\medskip

\begin{enumerate}
\item Soit $f$ la fonction définie sur $]0~;~+ \infty[$ par : 

\[f(x) = \dfrac{105}{x} \left(1 - \text{e}^{- \frac{3}{40}x}\right).\]

Démontrer que, pour tout réel $x$ de  $]0~;~+ \infty[$, \:  $f'(x) = \dfrac{105g(x)}{x^2}$, où $g$ est la fonction définie sur $[0~;~+ \infty[$ par : 

\[g(x) = \dfrac{3x}{40}\text{e}^{- \frac{3}{40}x}\ + \text{e}^{- \frac{3}{40}x} - 1.\]

\item  On donne le tableau de variation de la fonction $g$ :
%\begin{center}
%\psset{unit=1cm}
%\begin{pspicture}(7,2)
%\psframe(7,2) \psline(0,1.5)(7,1.5)\psline(0,2)(7,2)\psline(1,0)(1,2)
%\uput[u](0.5,1.4){$x$} \uput[u](1.1,1.4){$0$} \uput[u](6.5,1.4){$+ \infty$}
%\rput(0.5,0.75){$g(x)$} \uput[d](1.1,1.5){$0$}\uput[u](6.5,0){$- 1$}
%\psline{->}(1.25,1.25)(6.2,0.35) 
%\end{pspicture}
%\end{center}

\begin{center}
\renewcommand{\arraystretch}{1.3}
\psset{nodesep=3pt,arrowsize=2pt 3}  % paramètres
\def\esp{4cm}% pour modifier la largeur du tableau
\def\hauteur{10pt}% mettre au moins 12pt pour augmenter la hauteur
$\begin{array}{|c| *3{c}|}
\hline
 x & 0   & \hspace*{\esp} & +\infty \\
% \hline
%f'(x) &   & \pmb{-} & \\  
\hline
  &  \Rnode{max}{0} &    &    \\
g(x) & &  &  \rule{0pt}{\hauteur} \\
 &      & & \Rnode{min}{-1} \rule{0pt}{\hauteur}
\ncline{->}{max}{min}
%\rput*(-2,0.65){\Rnode{zero}{\red 0}}
%\rput(-2,1.75){\Rnode{alpha}{\red \alpha}}
%\ncline[linestyle=dotted, linecolor=red]{alpha}{zero}
\\
\hline
\end{array}$
\end{center}


En déduire le sens de variation de la fonction $f$.

\emph{On ne demande pas les limites de la fonction $f$}.
\item  Montrer que l'équation $f(x) = 5,9$ admet une unique solution sur l'intervalle [1~;~80].

En déduire que cette équation admet une unique solution sur l'intervalle $]0~;~+ \infty[$.

Donner une valeur approchée de cette solution au dixième près.
\end{enumerate}

\bigskip

\textbf{Partie C : détermination d'un traitement adéquat}

\medskip

Le but de cette partie est de déterminer, pour un patient donné, la valeur du débit de la perfusion qui permette au traitement d'être efficace, c'est-à-dire au plateau d'être égal à $15$.

Au préalable, il faut pouvoir déterminer la clairance $a$ de ce patient. À cette fin, on règle
provisoirement le débit $d$ à 105, avant de calculer le débit qui rende le traitement efficace.

On rappelle que la fonction $C$ est définie sur l' intervalle $[0~;~+ \infty[$ par : 

\[C(t) = \dfrac{d}{a}\left(1 - \text{e}^{-\frac{a}{80} t}\right)\]

\begin{enumerate}
\item On cherche à déterminer la clairance a d'un patient. Le débit est provisoirement réglé à $105$.
	\begin{enumerate}
		\item Exprimer en fonction de $a$ la concentration du médicament $6$ heures après le début de la perfusion.
		\item Au bout de $6$ heures, des analyses permettent de connaître la concentration du médicament dans le sang; elle est égale à $5,9$ micromole par litre.
		
Déterminer une valeur approchée, au dixième de litre par heure, de la clairance de ce
patient.
	\end{enumerate}
\item  Déterminer la valeur du débit $d$ de la perfusion garantissant l'efficacité du traitement.
\end{enumerate}

\vspace{0,5cm}

\subsection*{\textsc{Exercice 2} \hfill 3 points}
 
\textbf{Commun  à tous les candidats}

\bigskip

On considère la suite $\left(u_n\right)$ définie par: 

\[\left\{\begin{array}{l c l}
u_0 &= &1 \quad  \text{et, pour tout entier naturel } n,\\
\: u_{n+1} &=& \left(\dfrac{n+1}{2n+4}\right) u_n.
\end{array}\right.\]

On définit la suite $\left(v_n\right)$ par : pour tout entier naturel $n$,\: $v_n = (n + 1)u_n$.

\medskip

\parbox{0.55\linewidth}{\begin{enumerate}
\item La feuille de calcul ci-contre présente les valeurs des
premiers termes des suites $\left(u_n\right)$ et $\left(v_n\right)$, arrondies au cent-millième.

Quelle formule, étirée ensuite vers le bas, peut-on écrire  dans la cellule B3 de la feuille de calcul pour obtenir les  termes successifs de $\left(u_n\right)$ ?
\item  
	\begin{enumerate}
		\item Conjecturer l'expression de $v_n$ en fonction de $n$.
		\item Démontrer cette conjecture. 
	\end{enumerate}
\item  Déterminer la limite de la suite $\left(u_n\right)$.
\end{enumerate}} \hfill
\parbox{0.42\linewidth}{
\begin{tabularx}{\linewidth}{|c|*{3}{>{\centering \arraybackslash}X|}}\hline
&A&B&C\\ \hline
1 &$n$ 	&$u_n$ &$v_n$\\ \hline
2 &0	& \np{1,00000}&\np{1,00000}\\ \hline
3 &1 	& \np{0,25000} &\np{0,50000}\\ \hline
4 &2	&\np{0,08333} &\np{0,25000}\\ \hline
5 &3 	&\np{0,03125} &\np{0,12500}\\ \hline
6 &4 	&\np{0,01250} &\np{0,06250}\\ \hline
7 &5 	&\np{0,00521} &\np{0,03125}\\ \hline
8 &6 	&\np{0,00223} &\np{0,01563}\\ \hline
9 &7 	&\np{0,00098} &\np{0,00781}\\ \hline
10 &8 	&\np{0,00043} &\np{0,00391}\\ \hline
11 &9	&\np{0,00020} &\np{0,00195}\\ \hline
\end{tabularx}}


\vspace{0,5cm}

\subsection*{\textsc{Exercice 3} \hfill 4 points}
 
\textbf{Commun  à tous les candidats}

\bigskip

Pour chacune des quatre affirmations suivantes, indiquer si elle est vraie ou fausse, en justifiant la réponse. 

Il est attribué un point par réponse exacte correctement justifiée. 

Une réponse non justifiée n'est pas prise en compte. 

Une absence de réponse n'est pas pénalisée.

\medskip

\begin{enumerate}
\item On dispose de deux dés, identiques d'aspect, dont l'un est truqué de sorte que le 6 apparait avec la probabilité $\dfrac{1}{2}$. On prend un des deux dés au hasard, on le lance, et on obtient $6$.

\textbf{Affirmation 1} : la probabilité que le dé lancé soit le dé truqué est égale à $\dfrac{2}{3}$.
\item  Dans le plan complexe, on considère les points M et N d'affixes respectives $z_{\text{M}} = 2\text{e}^{- \text{i}\frac{\pi}{3}}$  et
$z_{\text{N}} = \dfrac{3 - \text{i}}{2 + \text{i}}$.

\textbf{Affirmation 2} : la droite (MN) est parallèle à l'axe des ordonnées.
\end{enumerate}

Dans les questions \textbf{3.} et \textbf{4.}, on se place dans un repère orthonormé \Oijk{} de l'espace et l'on considère la droite $d$ dont une représentation paramétrique est : $\left\{\begin{array}{l c l}
x &=& 1+ t\\
y &=& 2,\\
z &=& 3 + 2t
\end{array}\right.  t \in \R$.

\begin{enumerate}[resume]
\item  On considère les points A, B et C avec A$(-2~;~2~;~3)$, B (0~;~1~;~2) et C(4~;~2~;~0).

On admet que les points A, B et C ne sont pas alignés.

\textbf{Affirmation 3} : la droite $d$ est orthogonale au plan (ABC).
\item  On considère la droite $\Delta$ passant par le point D(1~;~4~;~1) et de vecteur directeur $\vect{v}(2~;~1~;~3)$.

\textbf{Affirmation 4} : la droite $d$ et la droite $\Delta$ ne sont pas coplanaires.
\end{enumerate}

\vspace{0,5cm}

\subsection*{\textsc{Exercice 4} \hfill 3 points}
 
\textbf{Commun  à tous les candidats}

\bigskip

L'objet du problème est l'étude des intégrales $I$ et $J$ définies par :

\[I = \displaystyle\int_0^1 \dfrac{1}{1 + x}\:\text{d}x\quad \text{ et }\quad  J = \displaystyle\int_0^1 \dfrac{1}{1 + x^2}\:\text{d}x.\]

\textbf{Partie A : valeur exacte de l'intégrale} \boldmath $I$\unboldmath

\medskip

\begin{enumerate}
\item Donner une interprétation géométrique de l'intégrale $I$.
\item Calculer la valeur exacte de $I$.
\end{enumerate}

\bigskip

\textbf{Partie B : estimation de la valeur de } \boldmath $J$\unboldmath

Soit $g$ la fonction définie sur l'intervalle [0~;~1] par $g(x) = \dfrac{1}{1 + x^2}$.

On note $\mathcal{C}_g$ sa courbe représentative dans un repère orthonormé du plan.

On a donc : $J = \displaystyle\int_0^1 g(x)\:\text{d}x$.

Le but de cette partie est d'évaluer l'intégrale $J$ à l'aide de la méthode probabiliste décrite ci-après.

On choisit au hasard un point M$\,(x~;~y)$ en tirant de façon indépendante ses coordonnées $x$ et $y$ au hasard selon la loi uniforme sur [0~;~1].

On admet que la probabilité $p$ qu'un point tiré de cette manière soit situé sous la courbe 
$\mathcal{C}_g$ est égale à l'intégrale $J$.

En pratique, on initialise un compteur $c$ à $0$, on fixe un entier naturel $n$ et on répète $n$ fois le processus suivant :

\begin{itemize}
\item on choisit au hasard et indépendamment deux nombres $x$ et $y$, selon la loi uniforme sur [0~;~1] ;
\item si M$\,(x~;~y)$ est au-dessous de la courbe $\mathcal{C}_g$ on incrémente le compteur $c$ de 1.
\end{itemize}

On admet que $f = \dfrac{c}{n}$ est une valeur approchée de $J$. \emph{C'est le principe de la méthode dite de Monte-Carlo.}


\parbox{0.42\linewidth}{La figure ci-contre illustre la méthode présentée pour
$n = 100$.

$100$ points ont été placés aléatoirement dans le carré.

Les disques noirs correspondent aux points sous la
courbe, les disques blancs aux points au-dessus de la
courbe. 

Le rapport du nombre de disques noirs par le
nombre total de disques donne une estimation de l'aire
sous la courbe.}\hfill
\parbox{0.55\linewidth}{
\psset{unit=6cm,comma=true}
\begin{pspicture}(-0.1,-0.1)(1.1,1.2)
\psgrid[gridlabels=0pt,gridwidth=0.2pt,subgridwidth=0.15pt](0,0)(1,1)
\multido{\n=0.1+0.2}{5}{\psline[linewidth=0.2pt](0,\n)(1,\n)}
\psaxes[linewidth=1.25pt,Dx=0.2,Dy=0.2,labelFontSize=\scriptstyle](0,0)(0,0)(1,1)
\psRandom[dotsize=4pt,dotstyle=o,fillstyle=solid,fillcolor=black,
  randomPoints=100](0,0)(1,1){%
    \pscustom[linestyle=none]{%
      \psline(0,1)
      \psplot[plotpoints=200]{0}{1}{1 x dup mul 1 add div }
      \psline(1,0)
}}
\psRandom[dotsize=4pt,dotstyle=o,fillcolor=white,
  randomPoints=100](0,0)(1,1){%
    \pscustom[linestyle=none]{%e
      \psline(0,1)(1,1)
      \psplot[plotpoints=200]{1}{0}{1 x dup mul 1 add div }
}}
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=cyan]{0}{1}{1 x dup mul 1 add div}
\rput(0.5,1.05){Illustration de la méthode avec $n = 100$}
\end{pspicture}
}

\begin{enumerate}
\item Recopier et compléter l'algorithme ci-après pour qu'il affiche une valeur approchée de $J$.
\begin{center}
\begin{tabularx}{0.8\linewidth}{|l|X|}\hline
\textbf{Variables}& $n$, $c$, $f$, $i$, $x$, $y$ sont des nombres\\ \hline
&Lire la valeur de $n$\\
&$c$ prend la valeur {}\ldots{}\\
&Pour $i$ allant de 1 à {}\ldots{} faire\\
&\hspace{0.5cm}$x$ prend une valeur aléatoire entre $0$ et $1$\\
\textbf{Traitement}
&\hspace{0.5cm}$y$ prend {}\ldots {}\\
&\hspace{0.5cm}Si {}\ldots {} alors\\
&\hspace{0.5cm}\ldots  prend la valeur {}\ldots{}\\
&\hspace{0.5cm}Fin si\\
&Fin pour\\
&$f$ prend la valeur {}\ldots{}\\ \hline
\textbf{Sortie}& Afficher $f$\\ \hline
\end{tabularx}
\end{center}

\item  Pour $n = \np{1000}$, l'algorithme ci-dessus a donné pour résultat : $f = 0,781$.

Donner un intervalle de confiance, au niveau de confiance de 95\,\%, de la valeur exacte de $J$.
\item  Quelle doit-être, au minimum, la valeur de $n$ pour que l'intervalle de confiance, au niveau de confiance de 95\,\%, ait une amplitude inférieure ou égale à $0,02$ ?
\end{enumerate}

\vspace{0,5cm}

\subsection*{\textsc{Exercice 5} \hfill 5 points}
 
\textbf{Candidats n'ayant pas suivi l'enseignement de spécialité}

\bigskip

\textbf{Question préliminaire}

Soit $T$ une variable aléatoire suivant une loi exponentielle de paramètre $\lambda$, où $\lambda$ désigne un réel strictement positif.

On rappelle que, pour tout réel $a$ positif, on a : $P( T \leqslant a) = \displaystyle\int_0^{a}\lambda \text{e}^{-\lambda t}\:\text{d}t$.

Démontrer que, pour tout réel $a$ positif, $P(T > a) = \text{e}^{-\lambda a}$.

\medskip

Dans la suite de l'exercice, on considère des lampes à led dont la durée de vie, exprimée en jour, est modélisée par une variable aléatoire $T$ suivant la loi exponentielle de paramètre $\lambda = \dfrac{1}{\np{2800}}$.

\begin{center}\emph{Les durées seront données au jour près, et les probabilités au millième près}
\end{center}

\textbf{Partie A : étude d'un exemple}

\medskip

\begin{enumerate}
\item Calculer la probabilité qu'une lampe fonctionne au moins 180 jours.
\item Sachant qu'une telle lampe a déjà fonctionné 180 jours, quelle est la probabilité qu'elle
fonctionne encore au moins 180 jours?
\end{enumerate}
 
\bigskip
 
\textbf{Partie B : contrôle de la durée de vie moyenne}
 
\medskip
 
Le fabricant de ces lampes affirme que, dans sa production, la proportion de lampes qui ont une
durée de vie supérieure à $180$~heures est de 94\,\%.
 
Un laboratoire indépendant qui doit vérifier cette affirmation fait fonctionner un échantillon
aléatoire de $400$~lampes pendant $180$~jours.
 
On suppose que les lampes tombent en panne indépendamment les unes des autres.
 
Au bout de ces $180$~jours, $32$ de ces lampes sont en panne.
 
Au vu des résultats des tests, peut-on remettre en cause, au seuil de 95\,\%, la proportion annoncée par le fabricant ?
 
\newpage
 
\textbf{Partie C : dans une salle de spectacle}
 
\medskip
 
Pour éclairer une salle de spectacle, on installe dans le plafond $500$ lampes à led.
 
On modélise le nombre de lampes fonctionnelles après $1$~an par une variable aléatoire $X$ qui suit la loi normale de moyenne $\mu = 440$ et d'écart-type $\sigma = 7,3$.
 
 \medskip
 
\begin{enumerate}
\item Calculer $P (X > 445)$, la probabilité que plus de $445$ lampes soient encore fonctionnelles après un an.
\item Lors de l'installation des lampes dans le plafond, la direction de la salle veut constituer un stock de lampes. 

Quelle doit-être la taille minimale de ce stock pour que la probabilité de pouvoir
changer toutes les lampes défectueuses, après un an, soit supérieure à 95\,\% ?
\end{enumerate}

\vspace{0,5cm}

\subsection*{\textsc{Exercice 5} \hfill 5 points}
 
\textbf{Candidats ayant  suivi l'enseignement de spécialité}

\begin{center}
\emph{Les deux parties sont indépendantes}
\end{center}

Un bit est un symbole informatique élémentaire valant soit 0, soit 1.

\bigskip

\textbf{Partie A : ligne de transmission}

\medskip

Une ligne de transmission transporte des bits de données selon le modèle suivant :

\begin{itemize}
\item elle transmet le bit de façon correcte avec une probabilité $p$ ;
\item elle transmet le bit de façon erronée (en changeant le $1$ en $0$ ou le $0$ en $1$) avec une probabilité $1- p$.
\end{itemize}

On assemble bout à bout plusieurs lignes de ce type, et on suppose qu'elles introduisent des erreurs de façon indépendante les unes des autres.

\smallskip

On étudie la transmission d'un seul bit, ayant pour valeur 1 au début de la transmission.

Après avoir traversé $n$ lignes de transmission, on note :

\begin{itemize}
\item $p_n$ la probabilité que le bit reçu ait pour valeur $1$ ;
\item $q_n$ la probabilité que le bit reçu ait pour valeur $0$.
\end{itemize}

On a donc $p_0 = 1$ et $q_0 = 0$.

On définit les matrices suivantes :

\[A = \begin{pmatrix}p& 1- p\\1 - p&p\end{pmatrix}\quad X_n = \begin{pmatrix}p_n\\q_n\end{pmatrix}
\quad  P = \begin{pmatrix}1&1\\1&- 1\end{pmatrix}.\]

On admet que, pour tout entier $n$, on a : $X_{n+1} = AX_n$ et donc, $X_n = A^n X_0$.

\medskip

\parbox{0.59\linewidth}{\begin{enumerate}
\item 
	\begin{enumerate}
		\item Montrer que $P$ est inversible et déterminer $P^{-1}$.
		\item On pose : $D = \begin{pmatrix} 1&0\\0 & 2p - 1\end{pmatrix}$.
		
Vérifier que : $A = PDP^{-1}$.
		\item Montrer que, pour tout entier $n \geqslant 1$,
		
\[A^n = PD^n P^{-1}.\]
		
		\item En vous appuyant sur la copie d'écran d'un logiciel de calcul formel donnée ci-contre, déterminer l'expression de $q_n$ en fonction de $n$.
	\end{enumerate}
\item On suppose dans cette question que $p$ vaut $0,98$. On
rappelle que le bit avant transmission a pour valeur 1. On souhaite que la probabilité que le bit reçu ait pour valeur $0$ soit inférieure ou égale à $0,25$. Combien peut-on, au maximum, aligner de telles lignes de transmission ?
\end{enumerate}}\hfill 
\parbox{0.4\linewidth}{\begin{tabularx}{\linewidth}{|c|X r|}\hline	
1	&$X0$ : = [[1], [0]]					&\\ \hline
	&$\begin{bmatrix}1\\0\end{bmatrix}$		&M\\ \hline
2	&P : = [[1, 1], [1, $-1$] ]				&\\ \hline
	&$\begin{bmatrix}1&1\\1&-1\end{bmatrix}$&M\\ \hline
3	&\small D : = [[1, 0],[0,$2*p-1$]]				&\\ \hline
	&$\begin{bmatrix}1&0\\0&2*p-1\end{bmatrix}$&M\\ \hline
4	&$P*(D\hat~n)*P\hat~(-1)*X0$					&\\ \hline
	&$\begin{bmatrix}\dfrac{(2*p - 1)^n + 1}{2}\\\dfrac{- (2*p-1)^n + 1}{2}\end{bmatrix}$&M\\ \hline
\end{tabularx}}

\bigskip

\textbf{Partie B : étude d'un code correcteur, le code de Hamming (7, 4)}

\medskip

On rappelle qu'un \textbf{bit} est un symbole informatique élémentaire valant soit $0$, soit $1$.

On considère un \og mot \fg{} formé de 4 bits que l'on note $b_1$, $b_2$, $b_3$ et $b_4$.

Par exemple, pour le mot \og 1101 \fg, on a $b_1 = 1$, $b_2 = 1$, $b_3 = 0$ et $b_4 = 1$.

On ajoute à cette liste une \emph{clé de contrôle} $c_1c_2c_3$ formée de trois bits :

\begin{itemize}
\item $c_1$ est le reste de la division euclidienne de $b_2 + b_3 + b_4$ par 2 ;
\item $c_2$ est le reste de la division euclidienne de $b_1 + b_3 + b_4$ par 2 ;
\item $c_3$ est le reste de la di vision euclidienne de $b_1 + b_2 + b_4$ par 2.
\end{itemize}

On appelle alors \og message \fg{} la suite de 7 bits formée des 4 bits du mot et des 3 bits de contrôle.

\medskip

\begin{enumerate}
\item Préliminaires
	\begin{enumerate}
		\item Justifier que $c_1$, $c_2$ et $c_3$ ne peuvent prendre comme valeurs que 0 ou 1.
		\item Calculer la clé de contrôle associée au mot 1001.
 	\end{enumerate}
\item Soit $b_1b_2b_3b_4$ un mot de 4 bits et $c_1c_2c_3$ la clé associée.
	
Démontrer que si on change la valeur de $b_1$ et que l'on recalcule la clé, alors :
	
\begin{itemize}
\item[$\bullet$~~] la valeur de $c_1$ est inchangée ;
\item[$\bullet$~~] la valeur de $c_2$ est modifiée ;
\item[$\bullet$~~] la valeur de $c_3$ est modifiée.
\end{itemize}
 
\item On suppose que, durant la transmission du message, au plus un des 7 bits a été transmis de
façon erronée. À partir des quatre premiers bits du message reçu, on recalcule les 3 bits de
contrôle, et on les compare avec les bits de contrôle reçus.

Sans justification, recopier et compléter le tableau ci-dessous. La lettre $F$ signifie que le bit de contrôle reçu ne correspond pas au bit de contrôle calculé, et $J$ que ces deux bits sont égaux.

\begin{center}
\begin{tabularx}{\linewidth}{|m{2.35cm}|*{8}{>{\centering \arraybackslash}X|}}\hline
\diagbox{\footnotesize Bit de \\\footnotesize contrôle\\\footnotesize  calculé}{\footnotesize Bit \\\footnotesize erroné}&$b_1$& $b_2$& $b_3$& $b_4$&$c_1$&$c_2$&$c_3$&\scriptsize Aucun\\ \hline
\multicolumn{1}{|c|}{$c_1$}	&$J$	&&&&&&&\\ \hline
\multicolumn{1}{|c|}{$c_2$}	&$F$	&&&&&&&\\ \hline
\multicolumn{1}{|c|}{$c_3$}	&$F$	&&&&&&&\\ \hline
\end{tabularx}
\end{center}
\item Justifier rapidement, en vous appuyant sur le tableau, que si un seul bit reçu est erroné, on peut dans tous les cas déterminer lequel, et corriger l'erreur.
\item Voici deux messages de 7 bits :

\[A = 0100010 \quad \text{et} \quad  B = 1101001.\]

On admet que chacun d'eux comporte au plus une erreur de transmission.

Dire s'ils comportent une erreur, et la corriger le cas échéant.
\end{enumerate}
\end{document}