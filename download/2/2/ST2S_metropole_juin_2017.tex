\documentclass[10pt,a4paper,french]{article}
\usepackage{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage{xspace}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{textcomp,enumitem}
\usepackage[table]{xcolor}
\usepackage{graphicx}
\usepackage{lastpage}
% Tapuscrit : Jean-Claude Souque
\newcommand{\euro}{\eurologo{}}
\usepackage{pst-plot,pst-tree,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Baccalauréat ST2S},
pdftitle = {Métropole - 16 juin 2016},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[np]{numprint}
\renewcommand\arraystretch{1}
\def\e{\text{e}}
\begin{document}
\setlength\parindent{0mm}

\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat  Sciences et Technologies de la Santé et du Social (ST2S)}
\lfoot{\small{Métropole}}
\rfoot{\small{16 juin 2017}}
\pagestyle{fancy}
\thispagestyle{empty}
\begin{center}\textbf{Durée : 2 heures}

\vspace{0,5cm}

{\Large \textbf{\decofourleft~Baccalauréat  ST2S  Métropole 16 juin 2017~\decofourright\\ }}
 \end{center}

\vspace{0,5cm}

\textbf{\textsc{Exercice 1}\hfill(8 points)}

\vspace{0.5cm}

La corpulence est mesurée à partir de l'indice de masse corporelle (IMC) qui est égal au
rapport entre la masse (en kilogramme) et le carré de la taille (en mètre). Les individus dont
l'IMC est supérieur à 30 sont considérés comme obèses.

On a réalisé en 2006 une étude à l'aide de questionnaires sur une population d'individus âgés
de 21 à 59 ans.

\medskip

\emph{Les deux parties de cet exercice peuvent être traitées de manière indépendante.}

\medskip

\textbf{Partie A}

\medskip

Dans cette partie, on choisit un questionnaire au hasard parmi ceux des femmes interrogées.

On note $E$ l'évènement : \og le questionnaire choisi correspond à une personne ayant un
emploi \fg{}.

On note $O$ l'évènement : \og le questionnaire choisi correspond à une personne considérée comme obèse \fg.

Selon les données de 2006, on sait que :

\begin{itemize}
\item[---] l'effectif total des femmes interrogées est de \np{2685}, dont \np{1920} ont un emploi ;
\item[---] 10,6\,\% des femmes interrogées sont considérées comme obèses ;
\item[---] parmi les femmes considérées comme non obèses, 72,7\,\% ont un emploi.
\end{itemize}

\medskip

\begin{enumerate}
\item \emph{On arrondira les résultats à l'entier le plus proche}.
\begin{enumerate}
\item  Justifier que le nombre total de femmes considérées comme obèses est égal à 285 et
que les femmes considérées comme non obèses et ayant un emploi sont au nombre de \np{1745}.
\item Compléter le tableau donné en \textbf{\textsc{ANNEXE} 1, à rendre avec la copie}.
\end{enumerate}
\item \emph{Dans les questions suivantes, les résultats seront arrondis au millième}.
\begin{enumerate}
\item Calculer la probabilité de l'évènement $E$.
\item Calculer la probabilité de l'évènement $O$.
\item Décrire par une phrase l'évènement $E \cap O$ et calculer la probabilité de cet évènement.
\item Justifier que les évènements $E$ et $O$ ne sont pas indépendants.
\end{enumerate}
\item \emph{Étude de l'influence de la corpulence sur le taux d'emploi des femmes en 2006 (les
probabilités seront arrondies au millième)}.
\begin{enumerate}
\item  Calculer la probabilité que le questionnaire choisi corresponde à une femme ayant un emploi sachant qu'elle est considérée comme obèse.
\item Déterminer la probabilité $P_{\overline{O}}(E)$.
\item En considérant les résultats précédents, que peut-on dire de l'influence de la
corpulence sur le taux d'emploi des femmes en 2006 ?
\end{enumerate}
\end{enumerate}

\medskip

\textbf{Partie B}

\medskip

Dans cette partie, on choisit un questionnaire au hasard parmi ceux des hommes interrogés.

On reprend les mêmes notations pour les évènements que dans la partie A, c'est-à-dire :

$E$ désigne l'évènement: \og le questionnaire choisi correspond à une personne ayant un
emploi \fg.

$O$ désigne l'évènement: \og le questionnaire choisi correspond à une personne considérée
comme obèse\fg.

On admet que les probabilités associées à cette expérience aléatoire sont représentées à l'aide
de l'arbre de probabilité suivant:

\begin{center}
\pstree[linecolor=blue,treemode=R,nodesepA=0pt,nodesepB=2.5pt,labelsep=0.1pt]{\TR{}}
{
	\pstree{\TR{$O$~}\taput{0,095}}
	  { 
		  \TR{$E$}\taput{0,839}
		  \TR{$\overline{E}$}\tbput{\np{0.161}}	   
	  }
	\pstree{\TR{$\overline{O}$~}\tbput{0,905}}
	  {
		  \TR{$E$}\taput{0,83}
		  \TR{$\overline{E}$}\tbput{\np{0.17}}
	  }
}
\end{center}

\begin{enumerate}
\item  Par lecture de l'arbre, donner la probabilité qu'un homme ait un emploi sachant qu'il est
considéré comme non obèse.
\item Le rapport d'étude conclut qu'il n'y a pas d'influence de la corpulence sur le taux
d'emploi des hommes en 2006. Comment peut-on le justifier à l'aide de l'arbre
précédent ?
\end{enumerate}

\vspace{0,5cm}

\textbf{\textsc{Exercice 2}\hfill(7 points)}

\vspace{0.5cm}

Une municipalité a ouvert au public, en novembre 2016, un parc composé d'un étang, d'un
arboretum et d'une maison de la nature permettant d'accueillir des expositions de
sensibilisation à la protection de l'environnement.

Pour des raisons de sécurité, la mairie devra affecter à ce parc un agent supplémentaire si le
nombre de visiteurs dépasse \np{2500} personnes par mois.

\medskip

\textbf{Partie A : ajustement affine}

\medskip

Afin d'anticiper le recrutement de l'agent supplémentaire, la municipalité a étudié la
fréquentation du parc depuis son ouverture. Ces données sont regroupées dans le tableau
suivant:

\begin{center}
\begin{tabularx}{\linewidth}{|m{3.1cm}|*{7}{>{\footnotesize  \centering \arraybackslash}X|}}\hline
\multirow{2}{2cm}{Mois} 	&Novembre 	&Décembre  	&Janvier&Février&Mars 	&Avril 	&Mai \\   
							&2016		&2016		&2017	&2017	&2017	&2017	&2017\\\hline
Rang du mois ($x_i$) 		&0 			&1			&2		&3 		&4 		&5		&6\\\hline
Nombre de visiteurs 
par mois ($y_i$)			&\np{1200}	&\np{1233}	&\np{1316}&\np{1360}&\np{1448}&\np{1457}&\np{1520}\\\hline
\end{tabularx}
\end{center}

\medskip


\textbf{Le nuage de points correspondant est donné en \textsc{Annexe} 2, à rendre avec la copie.}

\medskip

\begin{enumerate}
\item  Déterminer les coordonnées du point moyen G de ce nuage (on arrondira, si nécessaire, les
résultats à l'unité). Placer ce point dans le repère de l'\textbf{\textsc{Annexe} 2}.


\item On fait l'hypothèse que le nombre de visiteurs par mois de ce parc est correctement
modélisé à l'aide de la droite d'ajustement $D$ d'équation: $y = 54x + \np{1200}$,
$x$ représentant le rang du mois depuis l'ouverture.
	\begin{enumerate}
		\item  Tracer la droite $D$ dans le repère de l'\textbf{\textsc{Annexe} 2}. Préciser les points utilisés pour la
construction.
		\item En supposant cet ajustement fiable jusqu'en 2020, déterminer la date (mois, année) à
partir de laquelle la municipalité devra affecter un agent supplémentaire à ce parc.
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{Partie B : étude de l'impact d'une campagne de communication à l'aide d'une suite}

\medskip

La municipalité met en place une campagne de communication et prévoit que le nombre de
visiteurs du parc augmentera de 5\,\% chaque mois à partir de mai 2017.

On modélise dans cette partie le nombre mensuel de visiteurs du parc à l'aide d'une suite
$\left(u_n\right)$. Ainsi $u_0$ représente le nombre de visiteurs en mai 2017 ($u_0 = \np{1520}$), $u_1$ représente le
nombre de visiteurs en juin 2017, etc.

Afin d'étudier l'évolution de la fréquentation du parc, la municipalité utilise la feuille de
calcul automatisé suivante :

\smallskip

\begin{tabularx}{\linewidth}{|c|m{3.87cm}|*{7}{>{\centering \arraybackslash}X|}}
\hline
						&\centering{A}		&B	&C&D &E&F& G&H\\\hline
\multirow{2}{0.2cm}{1}	&\multirow{2}{1.6cm}{Mois}&{\small Mai}& {\small Juin}&{\small Juillet}& {\small Août}& {\small Sept.}	&{\small Octobre}& {\small Nov.}\\
						&			&2017	& 2017 &2017 &2017& 2017& 2017& 2017\\\hline
2						&Estimation du nombre de visiteurs par mois, $u_n$&1520&&&&&&\\\hline
\end{tabularx}

\medskip

\begin{enumerate}
\item  Quelle formule peut-on entrer dans la cellule C2 de sorte que, recopiée vers la droite sur la plage C2 : H2, elle permette d'afficher les estimations du nombre de visiteurs par mois?
\item \emph{Utilisation de la suite } $\left(u_n\right)$
	\begin{enumerate}
		\item Déterminer une estimation du nombre de visiteurs en juin 2017.
		\item Indiquer, sans justification, la nature de la suite $\left(u_n\right)$. Donner la valeur de sa raison.
		\item Exprimer $u_n$ en fonction de $n$, pour tout entier naturel $n$.
		\item Déterminer une estimation du nombre de visiteurs dans ce parc en octobre 2017.
	\end{enumerate}
\item Résoudre dans l'ensemble des nombres réels l'inéquation: $\np{1520} \times 1,05^{x} \geqslant \np{2500}$.
\item Déterminer la date (mois, année) de recrutement d'un agent supplémentaire pour ce parc,
suite à la campagne de communication.
\end{enumerate}

\vspace{0,5cm}

\textbf{\textsc{Exercice 3}\hfill(5 points)}

\vspace{0.5cm}

\textbf{Partie A : Étude d'une fonction}

\medskip

Soit $f$ la fonction définie sur l'intervalle [0; 8] par :

\[f(x) = 0,5x^3 - 12x^2 + 65,625x + 20.\]

\begin{enumerate}
\item  On note $f'$ la fonction dérivée de la fonction $f$,

Déterminer $f'(x)$ pour tout réel $x$ appartenant à l'intervalle [0~;~8].
\item On admet que: $f'(x) = (x - 3,5)(1,5x - 18,75)$ pour tout nombre réel $x$ de
l'intervalle [0~;~8].

Compléter le tableau de signes suivant, après l'avoir recopié sur la copie, afin d'étudier le signe de $f' (x)$ pour $x$ appartenant à l'intervalle [0~;~8].

\smallskip
\begin{center}
\begin{tabular}[]{|c|m{5cm}|}
\hline
$x$& 0\hfill 8\\\hline
$x-3,5$&\\\hline
$1,5x -18,75$&\\\hline
$f'(x) = (x - 3,5)(1,5x - 18,75)$&\\\hline
\end{tabular}
\end{center}
\smallskip
\item En déduire le tableau de variation de la fonction $f$ sur l'intervalle [0~;~8].

\emph{On fera apparaître les valeurs de la fonction $f$ aux bornes de l'intervalle ainsi qu'aux
éventuels changements de variation.}
\end{enumerate}

\medskip

\textbf{Partie B : Application}

\medskip

L'OMS a fixé à 50 milligrammes par litre (mg/L) la concentration limite de nitrates dans l'eau
destinée à la consommation, considérant qu'au-delà il y a des risques pour la santé.

Suite à un incident industriel, une importante quantité de nitrates a été déversée dans un cours
d'eau sur lequel se situe un point de captage pour l'alimentation d'une ville.

Un expert indépendant est alors consulté afin de prévoir l'évolution du taux de nitrates dans
ce cours d'eau au niveau du point de captage pendant les 8 jours suivant l'incident.

L'expert décide de modéliser le taux de nitrates, $x$ jours après le début de l'incident, à l'aide
de la fonction $f$ étudiée en \textbf{partie A}.

\medskip

\begin{enumerate}
\item  D'après ce modèle, quel sera le taux maximal de nitrates atteint pendant la phase de
surveillance de 8 jours ?
\item En cas d'incident, un décret impose de fermer le point de captage pendant 8 jours.

D'après le modèle choisi par l'expert, sera-t-on au terme des 8 jours dans les conditions
fixées par l'OMS ?
\end{enumerate}

\newpage

\begin{center}
\textbf{\textsc{Annexes} à rendre avec la copie}
\end{center}
\medskip

\textbf{\textsc{Annexe}1}~: Corpulence et taux d'emploi des femmes en 2006.

\bigskip

\renewcommand\arraystretch{2}
\begin{tabularx}{\linewidth}{|*{4}{>{\centering \arraybackslash}X|}}\hline
&Obèse&Non obèse&Total\\\hline
Ayant un emploi&&&\\\hline
N'ayant pas un emploi&&&\\\hline
Total&285&&\np{2685}\\\hline
\end{tabularx}
\renewcommand\arraystretch{1}
\vspace{1cm}

\textbf{\textsc{Annexe}2}

\begin{center}
\psset{xunit=0.4cm,yunit=0.008cm,labelFontSize=\scriptstyle}
\begin{pspicture}(-0.75,-100)(30,1700)
\multido{\n=0+1}{29}{\psline[linewidth=0.75pt,linecolor=lightgray](\n,0)(\n,1670)}
\multido{\n=0+50}{34}{\psline[linewidth=0.75pt,linecolor=lightgray](0,\n)(28.5,\n)}
\psaxes[linewidth=0.95pt,Dx=2,Oy=1000,Dy=100,]{->}(0,0)(29.1,1650)
\psdots[dotstyle=+,dotscale =1.4,dotangle=45](0,200)(1,233)(2,316)(3,360)(4,448)(5,457)(6,520)
\uput[u](2,1640){\footnotesize Nombre de visiteurs par mois}
\uput[d](27,- 40){\footnotesize Rang du mois}
\end{pspicture}
\end{center}
\end{document}