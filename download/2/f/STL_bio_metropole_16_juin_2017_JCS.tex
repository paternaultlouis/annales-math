\documentclass[10pt,a4paper,french]{article}
\usepackage{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage{xspace}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,amstext,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage[euler]{textgreek}
\usepackage{textcomp,enumitem}
\usepackage[table]{xcolor}
\usepackage{lscape}
\usepackage{graphicx}
%tapuscrit Jean-Claude Souque
\newcommand{\euro}{\eurologo{}}
\usepackage{pst-tree,pst-plot,pst-text,pst-eucl,pst-func,pst-math,pstricks-add}
\usepackage[squaren, Gray, cdot]{SIunits}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\makeatletter
\def\hlinewd#1{%
\noalign{\ifnum0=`}\fi\hrule \@height #1 %
\futurelet\reserved@a\@xhline}
\makeatother

\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Baccalauréat STL biotechnologies},
pdftitle = {Métropole - 16 juin 2017 },
allbordercolors = white,
pdfstartview=FitH}
\usepackage[np]{numprint}
\renewcommand\arraystretch{1.2}
\newcommand{\e}{\text{e}}
\frenchbsetup{StandardLists=true}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat  STL spécialité Biotechnologies}
\lfoot{\small{Métropole}}
\rfoot{\small{16 juin 2017}}
\pagestyle{fancy}
\thispagestyle{empty}
\begin{center}{\Large \textbf{\decofourleft~Baccalauréat STL  biotechnologies  Métropole--La Réunion~\decofourright\\[4pt] 16 juin 2017}}
\end{center}

\vspace{0,25cm}
 
\textbf{\textsc{Exercice 1} \hfill 6 points}  

\vspace{0.25cm}

\emph{Les deux parties de cet exercice peuvent être traitées indépendamment.}

\medskip

\textbf{PARTIE A}

\medskip

Une société souhaite exploiter un nouveau détecteur qui permet de mesurer la désintégration de
noyaux radioactifs. Pour tester ce détecteur, la société l'utilise pour déterminer le nombre de
noyaux radioactifs présents dans un échantillon radioactif à des instants donnés. Voici les
résultats des relevés réalisés au cours des heures qui ont suivi le début du test:

\smallskip
\begin{center}
\begin{tabularx}{\linewidth}{|m{6cm}|*{6}{>{\centering \arraybackslash}X|}}\hline
Nombre $t_i$ d'heures écoulées
 depuis le début du test			&0			&2  	&4		&6		&8		&10\\\hline
Nombre de noyaux $N_i$  détectés
 dans l'échantillon (en milliards)	&500 		&440	&395 	&362	&316 	&279\\\hline
\end{tabularx}

\end{center}
\smallskip

\begin{enumerate}
\item \begin{enumerate}
\item  Recopier et compléter le tableau ci-dessous $\left(\text{on arrondira les valeurs à }10^{-3}\right)$ :

\begin{center}
\begin{tabularx}{0.75\linewidth}{|c|*{6}{>{\centering \arraybackslash}X|}}
\hline
$t_i$ 			&$0$ 	&$2$ 	&$4$ 	&$6$ 	&$8$&$10$\\\hline
$y_i = \ln N_i$	&		&		&		&		&	&\\\hline
\end{tabularx}
\end{center}

\item Représenter le nuage de points de coordonnées $\left(t_i~;~y_i\right)$ sur l'\textbf{annexe 1, (à rendre avec la
copie)}.
\item Un ajustement affine est-il envisageable ? Pourquoi ?
\item \label{equa} À l'aide de la calculatrice, déterminer une équation de la droite $D$ d'ajustement de $y$ en $t$
par la méthode des moindres carrés sous la forme $y = at + b$, où les coefficients $a$ et $b$
seront arrondis à $10^{-3}$.
\item Tracer alors la droite $D$ sur l'\textbf{annexe 1 (à rendre avec la copie)}.
\end{enumerate}

\item \begin{enumerate}
\item  On choisit la droite $D$ comme modèle d'ajustement du nuage de points $M_i\left(t_i~;~y_i\right)$.

À l'aide de la question \ref{equa}, montrer alors que, pour tout réel $t$ positif ou nul, le nombre
de noyaux, en milliards, détectés dans l'échantillon au bout de $t$ heures écoulées depuis
le début du test, est de la forme: $A\text{e}^{Bt}$ où $A$ (arrondi à l'unité) et $B$ (arrondi au millième)
sont deux réels à préciser.
\item La loi de désintégration assure que la fonction $f$, qui à tout réel $t$ positif ou nul, associe le
nombre de noyaux, en milliards, présents dans l'échantillon au bout de $t$ heures, est
définie par $f(t) = 500\text{e}^{-0,06t}$ .
Le test réalisé doit-il conduire la société à exploiter le nouveau détecteur? Pourquoi?
\end{enumerate}
\end{enumerate}

\medskip

\textbf{PARTIE B}

\medskip

On étudie à présent la fonction $f$ définie sur $[0~;~~+\infty[ $ par 

\[f(t) = 500\text{e}^{- 0,06t}.\]

\smallskip
\begin{enumerate}
\item On admet que : $\displaystyle \lim_{t\to +\infty} \text{e}^{- 0,06t} = 0$.

Déterminer et interpréter graphiquement la limite de $f$ en $+\infty$.
\item Calculer $f'(t$) où $f'$ est la fonction dérivée de $f$.
\item En déduire le tableau de variations de la fonction $f$.
\item On rappelle que $f(t)$ est le nombre de noyaux, en milliards, présents dans l'échantillon
radioactif $t$ heures après le début du test.
\begin{enumerate}
\item Calculer le nombre de noyaux présents dans l'échantillon 24 heures après le début du test. On arrondira à l'unité.
\item Au bout de combien d'heures la moitié des noyaux présents dans l'échantillon au début du test aura-t-elle disparu ? On justifiera la réponse par un calcul et on arrondira à l'heure.
\end{enumerate}

\end{enumerate}

\vspace{0,25cm}
    
\textbf{\textsc{Exercice 2} \hfill 6 points}  

\vspace{0.25cm}

On s'intéresse à une modélisation de la concentration d'un médicament, injecté dans le sang d'un
patient, en fonction du temps.

À 7 heures du matin, on injecte le médicament au patient. Toutes les heures, on relève la
concentration de médicament dans le sang, exprimée en $\micro \mathrm{g}\cdot \mathrm{mL}^{-1}$. À l'injection, cette concentration est égale à $\np[\micro g\cdot mL^{-1}]{3,4}$.

Le nuage de points ci-dessous donne la concentration de ce médicament dans le sang en fonction du temps écoulé depuis l'injection.

\begin{center}
\psset{xunit=1.7cm,yunit=1.6cm,comma=true,labelFontSize=\scriptstyle}
\begin{pspicture}(0,-0.9)(6.6,4.5)
\multido{\n=0+1}{7}{\psline[linewidth=0.75pt,linecolor=lightgray](\n,0)(\n,4)}
\multido{\n=0+0.5}{9}{\psline[linewidth=0.75pt,linecolor=lightgray](0,\n)(6,\n)}
\psaxes[linewidth=0.85pt,Dx=1,Dy=1,labels=x]{-}(0,0)(6,4)
\multido{\n=0+0.500}{9}{\uput[l](0,\n){\scriptsize \np{\n}}}
\psdots[dotstyle=square,fillcolor=gray,dotscale =1.4,dotangle=45](0,3.4)(1,2.72)(2,2.176)(3,1.741)(4,1.393)(5,1.114)
\uput[r](0.1,3.4){\np{3.400}} \uput[r](1.1,2.72){\np{2.720}}\uput[r](2.1,2.176){\np{2.176}}\uput[r](3.1,1.741){\np{1,741}}\uput[r](4.1,1.393){1,393}\uput[r](5.1,1.114){1,114}\uput[d](3,-0.25){\footnotesize temps en heures} \uput[l](-0.5,2){\rotatebox{90}{\footnotesize Concentration en $\micro \mathrm{g}\cdot\mathrm{mL}^{-1}$}}
\end{pspicture}
\end{center} 

\medskip

\textbf{PARTIE A}

\medskip

Dans cette partie, on modélise la concentration de ce médicament par une fonction définie sur l'intervalle [0~;~5].

Parmi les trois modélisations proposées, une seule est correcte. Laquelle? Justifier.

\begin{itemize}
\item[a.] $f :x\mapsto  0,6x+3,4$
\item[b.] $g: x \mapsto 3,4\text{e}^{ -0,223x}$
\item[c.] $h: x\mapsto \dfrac{9}{3+x}$
\end{itemize}
\medskip

\textbf{PARTIE B}

\medskip

Dans cette partie, on choisit de modéliser la concentration du médicament par une suite, en
prenant, pour valeurs des trois premiers termes de la suite, les valeurs données par le graphique
placé avant la partie A.

\begin{enumerate}
\item  Pour tout entier naturel $n$, on note $C_n$ la concentration, exprimée en $\micro \mathrm{g}\cdot \mathrm{mL}^{-1}$, au bout
de $n$ heures, de ce médicament dans le sang. Une partie de ce médicament est éliminée toutes les heures.
	\begin{enumerate}
		\item  Par lecture du graphique, donner les valeurs de $C_0$, $C_1$ et $C_2$.
		\item Que peut-on alors conjecturer sur la nature de la suite $\left(C_n\right)$ ? Pourquoi ?
	\end{enumerate}
\end{enumerate}
On admet qu'à chaque heure, la concentration du médicament restante baisse de 20\,\%.

\begin{enumerate}[resume]
\item Pour tout entier naturel $n$, exprimer $C_n$ en fonction de $n$.
\item Déterminer alors la limite de la suite $\left(C_n\right)$ lorsque $n$ tend vers l'infini. Interpréter
cette limite dans le contexte de l'exercice.
\item Soit l'algorithme suivant:

\begin{center}
\begin{tabular}{|l|}
\hline
\begin{minipage}{6cm}

\textbf{Variables:}%\rule[-3mm]{0mm}{8mm}

\hspace{1.2em}$n$ entier naturel

\hspace{1.2em}$C$ réel

\textbf{Initialisation :}

\hspace{1.2em}Affecter à $n$ la valeur 0

\hspace{1.2em}Affecter à $C$ la valeur 3,4

\textbf{Traitement:}

\hspace{1.2em}Tant que $C$ est supérieur à 1

\hspace{3.2em}Affecter à $n$ la valeur $n+1$

\hspace{3.2em}Affecter à $C$ la valeur $0,8\times C$

\hspace{1.2em}Fin tant que

\textbf{Sortie:}

\hspace{1.2em}Afficher $n$\rule[-3mm]{0mm}{8mm}

\end{minipage}\\
\hline
\end{tabular}
\end{center}
Quelle valeur affiche l'algorithme ? Interpréter le résultat dans le contexte de cet exercice.
\item Pour des raisons d'efficacité, le patient reçoit immédiatement une nouvelle injection de médicament dès que, lors d'un relevé à une heure donnée, la concentration $\mathbf{c}$ du médicament
dans le sang est inférieure ou égale à \np[\mu \mathrm{g}\cdot mL^{-1}]{1}. À la nouvelle injection, la concentration du
médicament dans le sang est alors égale à $\mathbf{c} + 3,4\, \mu \mathrm{g}\cdot \mathrm{mL}^{-1}$.
	\begin{enumerate}
		\item À quelle heure le patient devra-t-il recevoir une deuxième injection?
		\item Quelle est la concentration du médicament à cette deuxième injection ? 

On arrondira le résultat à \np[\mu g\cdot mL^{-1}]{0.1}.
		\item À quelle heure le patient devra-t-il recevoir une troisième injection?
	\end{enumerate}
\end{enumerate}

\vspace{0,25cm}

\textbf{\textsc{Exercice 3} \hfill 4 points}  

\vspace{0.25cm}

La Direction de la recherche, des études, de l'évaluation et des statistiques (Drees) affirme qu'en France : 7 adultes sur 10 portent des lunettes.

On prélève au hasard un échantillon de 40 adultes parmi la population française. On assimile ce
prélèvement à un tirage avec remise.

Soit $X$ la variable aléatoire qui, à tout échantillon de ce type, associe le nombre de porteurs de
lunettes dans l'échantillon.

\medskip

\begin{enumerate}
\item \begin{enumerate}
\item  Montrer que $X$ suit une loi binomiale dont on précisera les paramètres.
\item Calculer la probabilité qu'il y ait au moins 30 porteurs de lunettes dans un tel
échantillon de 40 adultes. On donnera la valeur arrondie à $10^{-3}$.
\end{enumerate}
On admet que la loi binomiale de la variable aléatoire $X$ précédente peut être approchée par une loi normale de paramètres \textmu{} et $\sigma$.
\item On a représenté ci-dessous un diagramme en bâtons et une courbe C. L'une de ces deux représentations est la représentation de la loi binomiale suivie par $X$; l'autre celle de la loi normale de paramètres \textmu{} et $\sigma$.

\begin{center}

\psset{xunit=0.35cm,yunit=40cm,arrowsize=5pt,yAxis=false}
\begin{pspicture*}(17,-0.03)(42,0.15)
%\rput(16,0.07){$\mathcal{N}(6;2,19)$}
\psaxes[Dx=2,Dy=0.05,dy=0.05\psyunit]{->}(0,0)(-1,0)(40,0.20)
\psGauss[linecolor=red,linewidth=1.5pt,mue=28,sigma=2.898]{18}{40}
\psBinomial[linewidth=1.2pt,markZeros,fillstyle=vlines]{40}{0.7}  
\end{pspicture*} 

\end{center}

\begin{enumerate}
\item  À laquelle des deux représentations est associée la loi binomiale ? Pourquoi ?
\item Donner, par lecture graphique, la valeur de \textmu. Justifier,
\item On affirme que l'écart type $\sigma$ de la loi normale est égal à 8. Cette affirmation est-elle
correcte ? Pourquoi ?
\end{enumerate}
\item \begin{enumerate}
\item  Déterminer un intervalle de fluctuation asymptotique à 95\,\% de la fréquence des
porteurs de lunettes dans un échantillon aléatoire de 40 adultes en France. On
arrondira les bornes de l'intervalle à $10^{-3}$.
\item Dans un échantillon de 40 adultes en France, on compte 24 porteurs de lunettes.
Déduire de la question précédente si cet échantillon remet en cause l'affirmation de la
Drees qui figure au début de l'exercice.
\end{enumerate}
\end{enumerate}

\vspace{0,25cm}
    
\textbf{\textsc{Exercice 4} \hfill 4 points}  

\vspace{0.25cm}

Soient les fonctions $f$ et $g$ définies sur [0~;~7] par 

\[ f(x) = 20x\e^{-x}\qquad \text{ et }\qquad g(x)= 20x^2\e^{-x}.\]

 On note $C_f$ et $C_g$ les courbes représentatives respectives des fonctions $f$ et $g$ représentées en \textbf{annexe~2}.

\begin{enumerate}
\item  On note :

\begin{itemize}
\item  $D_1$ l'aire du domaine délimité par la courbe $C_f$, l'axe des abscisses et les droites  d'équations $x = 1$ et $x = 3$ ;
\item $D_2$ l'aire du domaine délimité par les courbes $C_g$, $C_f$ et les droites d'équation $x = 3$
et $x = 6$.
\end{itemize}
\begin{enumerate}
\item  Hachurer les domaines $D_l$ et $D_2$ sur le graphique donné en \textbf{annexe 2, à rendre avec la copie}.
\item Encadrer, par deux entiers consécutifs, les aires, en unités d'aire, des domaines $D_1$
et $D_2$.
\end{enumerate}
\item La commande $Int(f(x),x,a,b)$ d'un logiciel de calcul formel permet de calculer la valeur de
l'intégrale $\displaystyle \int_{a}^{b} f(x)\mathrm{d}x$.

 On obtient alors les résultats suivants pour quatre intégrales:

\begin{center}

\renewcommand\arraystretch{1.3}

\begin{tabular}{|c| m{7cm}|}\hlinewd{1mm}
1& $Int(20x\text{e}^{-x},x,1,2)$\\\hline
&$40\text{e}^{-1}-60\text{e}^{-2}$\\\hlinewd{1mm}
2&$Int(20x\e^{-x}, x,2,3$)\\\hline
&$60\text{e}^{-2}-80\text{e}^{-3}$\\\hlinewd{1mm}
3& $Int(20x\text{e}^{-x}, x,3,6)$\\
&$80\text{e}^{-3} -140\text{e}^{-6}$\\\hlinewd{1mm}
4& $Int(20x^2\text{e}^{-x} ,x,3,6)$\\
&$340\text{e}^{-3} -\np{1 000}\text{e}^{-6}$\\\hlinewd{1mm}
\end{tabular}
\renewcommand\arraystretch{1}

\end{center}
	\begin{enumerate}
		\item Déterminer les aires des domaines $D_1$ et $D_2$ en justifiant la réponse. On donnera les valeurs exactes.
		\item Comparer les valeurs des deux aires obtenues.
	\end{enumerate}
\end{enumerate}

\pagebreak
\begin{landscape}
\begin{center}
\textbf{Annexe 1 \hspace{1em}(exercice 1)}

\textbf{(À rendre avec la copie)}

\psset{xunit=1cm,yunit=10cm,comma=true}
\begin{pspicture}(0,0)(19,1)
\multido{\n=0+1}{19}{\psline[linewidth=0.75pt,linecolor=lightgray,linestyle=dashed](\n,0)(\n,0.8)}
\multido{\n=0+0.1}{9}{\psline[linewidth=0.75pt,linecolor=lightgray,linestyle=dashed](0,\n)(19,\n)}
\psaxes[linewidth=1.5pt,Oy=5.5,Dy=0.1]{->}(0,0)(19,0.8)
\uput[dr](0,0.8){$y_i$}\uput[ur](18.5,0){$t_i$}
\end{pspicture}
\end{center} 
\end{landscape}

\pagebreak
\begin{landscape}
\begin{center}
\textbf{Annexe 2\hspace{1em}(exercice 4)}

\textbf{(À rendre avec la copie)}

\psset{xunit=2.75cm,yunit=1.cm,comma=true}
\begin{pspicture}(0,0)(7.5,13)
\multido{\n=0+0.5}{15}{\psline[linewidth=0.75pt,linecolor=lightgray,linestyle=dashed](\n,0)(\n,12.1)}
\multido{\n=0+1}{13}{\psline[linewidth=0.75pt,linecolor=lightgray,linestyle=dashed](0,\n)(7.1,\n)}
\psaxes[linewidth=1pt,Dx=0.5,Dy=1](0,0)(7.3,12.5)
\psaxes[linewidth=1.5pt,Dx=0.5,Dy=1]{->}(0,0)(1,1)
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=blue]{0}{7}{ 2.71828 x neg exp x mul 20 mul }
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=cyan]{0}{7}{2.71828 x neg exp x 2 exp mul 20 mul}

\uput[ur](0.2,5.9){\blue $C_f$}\uput[ur](2.5,9){\cyan $C_g$}
\end{pspicture}
\end{center} 
\end{landscape}
\end{document}