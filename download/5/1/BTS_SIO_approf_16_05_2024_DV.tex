\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo}
%Tapuscrit : Denis Vergès
%Relecture : 
\usepackage{pst-plot,pst-text,pst-node,pst-tree,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\newcommand{\barre}[1]{\overline{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS},
pdftitle = {Métropole 16 mai 2024},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[french]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur Métropole}
\lfoot{\small{Services informatiques aux organisations\\Épreuve de mathématiques approfondies}}
\rfoot{\small{16 mai 2024}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~BTS Métropole 16 mai 2024~\decofourright\\[7pt]Services informatiques aux organisations}\\[7pt]}

\medskip

\textbf{Épreuve de mathématiques approfondies}

\vspace{0,25cm}

\textbf{L'usage de calculatrice avec mode examen actif est autorisé}

\textbf{L'usage de calculatrice sans mémoire \og type collège \fg{} est autorisé}

\textbf{Durée : 2 heures}

\end{center}

\smallskip

\textbf{Exercice 1 \hfill 10 points}

\medskip

Un magasin vend des téléphones portables et souhaite proposer à ses acheteurs de souscrire un contrat d'assurance.

\bigskip

\textbf{Partie A : Étude de marché}

\medskip

Avant la date de de proposition de son assurance, le magasin a commandé une étude permettant d'observer les habitudes d'un échantillon représentatif d'acheteurs. Elle a obtenu les informations suivantes:

\begin{itemize}[label=$\bullet~$]
\item 78\,\% des joueurs de l'échantillon ont moins de 30 ans ;
\item parmi les acheteurs de moins de 30 ans, 4\,\% souscrivent un contrat d'assurance ;
\item 14\,\% des acheteurs qui ont 30 ans ou plus souscrivent un contrat d'assurance.
\end{itemize}

\medskip

On choisit un acheteur au hasard dans cet échantillon.

On considère les évènements suivants:
\begin{description}
\item[ ] $T$ : \og l'acheteur a moins de 30 ans \fg{} ;
\item[ ] $A$ : \og l'acheteur souscrit un contrat d'assurance \fg.
\end{description}

On rappelle que, quel que soit l'évènement $E$, on note $\overline{E}$ son évènement contraire.

\medskip

\begin{enumerate}
\item Recopier et compléter l'arbre pondéré suivant:

\begin{center}
\pstree[treemode=R,levelsep=3cm]{\TR{}}
{\pstree{\TR{$T$~~}\taput{\ldots}}
	{\TR{$A$}\taput{\ldots}
	\TR{$\overline{A}$}\tbput{\ldots}
	}
\pstree{\TR{$\overline{T}$~~}\tbput{\ldots}}
	{\TR{$A$}\taput{\ldots}
	\TR{$\overline{A}$}\tbput{\ldots}
	}
}
\end{center}

\item Calculer la probabilité que l'acheteur ait moins de 30 ans et souscrive un contrat d'assurance.
\item Montrer que la probabilité que l'acheteur souscrive un contrat d'assurance est égale à $0,062$.
\item Sachant qu'un acheteur a souscrit un contrat d'assurance, quelle est la probabilité qu'il ait moins de 30 ans ?

Arrondir le résultat à $10^{-3}$.
\end{enumerate}

\bigskip

\textbf{Partie B : Étude après commercialisation}

\medskip

Pour la première phase de commercialisation de son contrat d'assurance, le magasin a
vendu 100 téléphones.

On considère que cette phase de commercialisation revient à effectuer un prélèvement de 100 acheteurs avec remise parmi un très grand nombre d'acheteurs.

On suppose que l'échantillon de la partie A était représentatif, c'est-à-dire que la probabilité qu'un acheteur de téléphone prélevé au hasard souscrive un contrat d'assurance est égale à $0,062$.

On note $X$ la variable aléatoire qui, à chaque lot de $100$~acheteurs, associe le nombre d'acheteurs qui ont souscrit un contrat d'assurance.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Justifier que la variable aléatoire $X$ suit une loi binomiale de paramètres $n =100$ et $p = 0,062$.
		\item Calculer la probabilité qu'exactement 7 acheteurs souscrivent un contrat d'assurance. Arrondir le résultat à $10^{-3}$.
		\item Déterminer l'espérance de la variable aléatoire $X$.
		\item Pour chaque téléphone vendu, le magasin reçoit une prime de $7,50$~euros si l'acheteur souscrit un contrat d'assurance.
		
En moyenne, quel est le montant total des primes que percevra le magasin ?
	\end{enumerate}
\item Pour approfondir son étude, la société décide d'approcher la loi de la variable
 aléatoire $X$ par la loi de Poisson de paramètre $\lambda = 6,2$.
 
 On note $Y$ la variable aléatoire suivant cette loi de Poisson.
	\begin{enumerate}
		\item Justifier le choix de $\lambda = 6,2$.
		\item Calculer $P(Y \geqslant 5)$, puis interpréter ce résultat. Arrondir le résultat à $10^{-3}$.
		\item Calculer la probabilité qu'au moins un acheteur souscrive un contrat d'assurance.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Exercice 2 \hfill 10 points}

\medskip

Une société conçoit et commercialise un jeu vidéo d'aventure.
Partie A : Gain de niveau et points d'expérience - étude statistique
Ce jeu est constitué de plusieurs niveaux. Un joueur débute au niveau 1 et peut passer au niveau supérieur en gagnant des points d'expérience. , .
Pour tout entier naturel $n$,non nul, on note $X_n$ le nombre de points d'expérience nécessaires (en milliers) pour passer du niveau $n$ au niveau $n + 1$.

Le tableau ci-dessous indique, pour quelques niveaux, le nombre de points d'expérience nécessaires (en milliers) pour passer au niveau suivant:

\begin{center}
\begin{tabularx}{\linewidth}{m{4cm}|*{5}{>{\centering \arraybackslash}X|}}\hline
Niveau $n$& 2& 4& 6& 8& 10\\ \hline
Nombre de points d'expérience nécessaires (en
milliers) pour passer du niveau $n$ au niveau $n + 1$&220 &256& 283& 304& 321\\ \hline
\end{tabularx}
\end{center}

\begin{enumerate}
\item Déterminer le coefficient de corrélation linéaire $r$ de la série statistique $\left(n~;~X_n\right)$. Arrondir le résultat au centième.
\item Déterminer, à l'aide d'une calculatrice, une équation de la droite de régression de $X_n$ en $n$,sous la forme $X_n = an + b$.
\item À l'aide de l'équation de la droite de régression trouvée précédemment, estimer le nombre de points d'expérience nécessaires pour passer du niveau 20 au niveau 21.

Arrondir le résultat à 1 près.
\end{enumerate}

\bigskip

\textbf{Partie B : Gain de niveau et points d'expérience - étude de fonction}

\medskip

La société n'est pas satisfaite de la valeur de $r$ trouvée en partie A, qu'elle estime trop éloignée de 1.

Elle choisit une autre méthode.

Pour cela, elle considère la fonction $f$ définie sur [1~;~100] par 
\[f(x) = 100 \ln (2x + 5).\]

On admet que pour tout entier naturel $n$, non nul, $f(n)$ modélise le nombre de points d'expérience nécessaires(en milliers) pour passer du niveau $n$ au niveau $n+1$.

On note $f'$ la fonction dérivée de la fonction $f$.

\medskip

\begin{enumerate}
\item Justifier que pour tout $x$ appartenant à l'intervalle [1~;~100], $f'(x) = \dfrac{200}{2x + 5}$.
\item 
	\begin{enumerate}
		\item Justifier que la fonction $f$ est strictement croissante sur [1~;~100].
		\item Dresser le tableau de variation de la fonction $f$ sur [1~;~100]. On arrondira la
valeur des images à l'unité.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Résoudre sur [1~;~100], l'équation $f(x) = 500$.
		
On donnera la valeur exacte et une valeur approchée à 1 près.
		\item En déduire le premier niveau à partir duquel au moins $500$points d'expérience
sont nécessaires pour passer au niveau suivant.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie C : Étude des gains d'Ether}

\medskip

La monnaie du jeu s'appelle l'Ether (symbolisé par $E$). Un joueur débute avec une somme de \np{50000} $E$.

Les règles sont telles que chaque nouvelle heure supplémentaire jouée augmente la somme détenue par le joueur de 2\,\%.

Pour tout entier naturel, $u_n$ modélise la somme détenue par le joueur après avoir joué $n$ heures.

Ainsi on a $u_0 = \np{50000}$.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Calculer $u_1$.
		\item Vérifier que $u_2 = \np{52020}$ puis interpréter ce résultat dans le contexte de l'exercice.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Pour tout entier naturel $n$, exprimer $u_{n+1}$ en fonction de $u_n$.
		\item Déterminer la nature de la suite $\left(u_n\right)$·
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Pour tout entier naturel $n$, exprimer $u_n$ en fonction de $n$.
		\item En déduire, à l'unité près, la somme détenue après avoir joué $7$~heures.
	\end{enumerate}
\end{enumerate}
\end{document}