\documentclass[11pt,a4paper,french]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{makeidx}
\usepackage{amsmath,amssymb}
\usepackage{fancybox}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{lscape}
\usepackage{multicol}
\usepackage{mathrsfs}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{enumitem}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : François Kriegk (avec le site https://mathpix.com/pdf-to-latex)
%Sujet fourni par : Jennifer Faber
%Relecture : Denis Vergès
\usepackage{pst-plot,pst-tree,pstricks,pst-node,pst-text}
\usepackage{pst-eucl,pst-3dplot,pstricks-add}
\usepackage{tikz}
\usepackage{esvect}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=2cm, bottom=3cm]{geometry}
\headheight15 mm
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\newcommand{\e}{\text{e}}
\usepackage{fancyhdr}
%\usepackage[dvips]{hyperref}
%\hypersetup{%
%	pdfauthor = {APMEP},
%	pdfsubject = {Baccalauréat Spécialité},
%	pdftitle = {Amérique du Nord Sujet 1 27 mars 2023},
%	allbordercolors = white,
%	pdfstartview=FitH}

%\frenchsetup{StandardLists=true}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat spécialité sujet 2}
\lfoot{\small{Amérique du Nord}}
\rfoot{\small{28 mars 2023}}

\usepackage{babel}
\usepackage[np]{numprint}

\begin{document}
\pagestyle{fancy}
\thispagestyle{empty}
\setlength\parindent{0mm}
\begin{center}{\Large\textbf{\decofourleft~Baccalauréat Amérique du Nord 28 mars 2023~\decofourright\\[7pt]  Sujet 2\\[7pt] ÉPREUVE D'ENSEIGNEMENT DE SPÉCIALITÉ}}
\end{center}

\bigskip

\textbf{\textsc{Exercice 1} \hfill 5 points}

\medskip

\textbf{\large Partie A}

\medskip

Le plan est muni d'un repère orthogonal.

On considère une fonction $f$ définie et dérivable sur $\mathbb{R}$. On note $f'$ sa fonction dérivée. On donne ci-dessous la courbe représentative de la fonction dérivée $f'$.

\begin{center}
\psset{unit=1cm,arrowsize=2pt 3}
\begin{pspicture*}(-7.1,-8)(3.1,3)
\psaxes[linewidth=1.25pt,labelFontSize=\scriptstyle]{->}(0,0)(-7,-7.95)(3,3)
\psgrid[gridlabels=0pt,subgriddiv=1,gridwidth=0.3pt]
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=red]{-7}{3}{x dup mul 3 x mul sub 1 add 2.71828 x exp mul}
\end{pspicture*}
%\includegraphics[max width=\textwidth]{2023_03_29_f608f13ad2bd4a2a2c83g-1}
\end{center}

Dans cette partie, les résultats seront obtenus par lecture graphique de la courbe représentative de la fonction dérivée $f'$. Aucune justification n'est demandée.

\medskip

\begin{enumerate}
\item Donner le sens de variation de la fonction $f$ sur $\mathbb{R}$. On utilisera des valeurs approchées si besoin.
\item Donner les intervalles sur lesquels la fonction $f$ semble être convexe.
\end{enumerate}

\medskip

\textbf{\large Partie B}

\medskip

On admet que la fonction $f$ de la partie $\mathbf{A}$ est définie sur $\mathbb{R}$ par 
\[f(x)=\left(x^{2}- 5 x + 6\right) \mathrm{e}^{x}.\]

On note $\mathcal{C}$ la courbe représentative de la fonction $f$ dans un repère.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer la limite de la fonction $f$ en $+\infty$.
		\item Déterminer la limite de la fonction $f$ en $-\infty$.
	\end{enumerate}
\item Montrer que, pour tout réel $x$, on a $f'(x)=\left(x^{2}-3 x+1\right) \mathrm{e}^{x}$.
\item En déduire le sens de variation de la fonction $f$.
\item Déterminer l'équation réduite de la tangente $(\mathcal{T})$ à la courbe $\mathcal{C}$ au point d'abscisse 0.
\end{enumerate}

On admet que la fonction $f$ est deux fois dérivable sur $\mathbb{R}$. On note $f''$ la fonction dérivée seconde de la fonction $f$. On admet que, pour tout réel $x$, on a $f''(x) = (x + 1)(x - 2) \mathrm{e}^{x}$.

\begin{enumerate}[resume]
\item 
	\begin{enumerate}
		\item Étudier la convexité de la fonction $f$ sur $\mathbb{R}$.
		\item Montrer que, pour tout $x$ appartenant à l'intervalle $[-1~;~2]$, on a $f(x) \leqslant x + 6$.	
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{\textsc{Exercice 2} \hfill 5 points}

\medskip

On étudie un groupe de \np{3000} sportifs qui pratiquent soit l'athlétisme dans le club A, soit le basketball dans le club B.

En 2023, le club A compte \np{1700} membres et le club B en compte \np{1300}.

\smallskip

On décide de modéliser le nombre de membres du club A et du club B respectivement par deux suites $\left(a_{n}\right)$ et $\left(b_{n}\right)$, où $n$ désigne le rang de l'année à partir de 2023.

L'année 2023 correspond au rang 0. On a alors $a_{0}= \np{1700}$ et $b_{0} = \np{1300}$.

Pour notre étude, on fait les hypothèses suivantes :
\begin{itemize}
\item durant l'étude, aucun sportif ne quitte le groupe ;
\item chaque année, $15\,\%$ des sportifs du club A quittent ce club et adhèrent au club B ;
\item chaque année, $10\,\%$ des sportifs du club B quittent ce club et adhèrent au club $A$.
\end{itemize}

\medskip

\begin{enumerate}
\item Calculer les nombres de membres de chaque club en 2024.
\item Pour tout entier naturel $n$, déterminer une relation liant $a_{n}$ et $b_{n}$.
\item Montrer que la suite $\left(a_{n}\right)$ vérifie la relation suivante pour tout entier naturel $n$, on a : 
\[a_{n+1}= 0,75 a_{n} + 300.\]

\item 
	\begin{enumerate}
		\item Démontrer par récurrence que pour tout entier naturel $n$, on a :
\[1200 \leqslant a_{n+1} \leqslant a_{n} \leqslant \np{1700}.\]
		\item En déduire que la suite $\left(a_{n}\right)$ converge.
	\end{enumerate}
\item Soit $\left(v_{n}\right)$ la suite définie pour tout entier naturel $n$ par $v_{n}=a_{n}- \np{1200}$.
	\begin{enumerate}
		\item Démontrer que la suite $\left(v_{n}\right)$ est géométrique.
		\item Exprimer $v_{n}$ en fonction de $n$.
		\item En déduire que pour tout entier naturel $n, a_{n}= 500 \times 0,75^{n}+ \np{1200}$.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer la limite de la suite $\left(a_{n}\right)$.
		\item Interpréter le résultat de la question précédente dans le contexte de l'exercice.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Recopier et compléter le programme Python ci-dessous afin qu'il renvoie la plus petite valeur de n à partir de laquelle le nombre de membres du club A est strictement inférieur à \np{1280}.

\begin{center}
\psset{unit=1cm}
\begin{tabular}{l l}
\textbf{def}&seuil() :\\
&n = 0\\&A = \np{1700}\\
&\textbf{while ...} :\\
&\qquad n=n+1\\
&\qquad A = \ldots\\
&\textbf{return}\ldots\\
\end{tabular}
%\includegraphics[max width=\textwidth]{2023_03_29_f608f13ad2bd4a2a2c83g-2}
\end{center}
		\item Déterminer la valeur renvoyée lorsqu'on appelle la fonction seuil.
	\end{enumerate}
\end{enumerate}	
	
\textbf{\textsc{Exercice 3} \hfill 5 points}

\medskip

Dans l'espace muni d'un repère orthonormé d'unité $1 \mathrm{~cm}$, on considère les points

\begin{center}$\mathrm{D}(3~;~1~;~5)$, \qquad 
$\mathrm{E}(3~;~-2~;~-1)$, \qquad 
$\mathrm{F}(-1~;~2~;~1)$, \qquad 
$\mathrm{G}(3~;~2~;~-3)$.\end{center}

\smallskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer les coordonnées des vecteurs $\overrightarrow{\text{EF}}$ et $\overrightarrow{\text{FG}}$.
		\item Justifier que les points $\mathrm{E}, \mathrm{F}$ et $\mathrm{G}$ ne sont pas alignés.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer une représentation paramétrique de la droite (FG).
		\item On appelle $\mathrm{H}$ le point de coordonnées $(2~;~2~;~-2)$.

Vérifier que $\mathrm{H}$ est le projeté orthogonal de E sur la droite (FG) .
		\item Montrer que l'aire du triangle EFG est égale à $12\mathrm{~cm}^{2}$.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Démontrer que le vecteur $\vec{n}\begin{pmatrix}2 \\ 1 \\ 2\end{pmatrix}$ est un vecteur normal au plan (EFG).
		\item Déterminer une équation cartésienne du plan (EFG) .
		\item Déterminer une représentation paramétrique de la droite $(d)$ passant par le point $\mathrm{D}$ et orthogonale au plan (EFG) .
		\item On note K le projeté orthogonal du point D sur le plan (EFG).

À l'aide des questions précédentes, calculer les coordonnées du point $\mathrm{K}$.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Vérifier que la distance $D K$ est égale à $5 \mathrm{~cm}$.
		\item En déduire le volume du tétraèdre DEFG.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{\textsc{Exercice 4} \hfill 5 points}

\medskip

\emph{Cet exercice est un questionnaire à choix multiple. \\Pour chaque question, une seule des quatre réponses proposées est exacte.\\Le candidat indiquera sur sa copie le numéro de la question et la réponse choisie.\\Aucune justification n'est demandée.\\
Une réponse fausse, une réponse multiple ou l'absence de réponse à une question ne rapporte ni n'enlève de point.\\Les cinq questions sont indépendantes.}

\medskip

\begin{enumerate}
\item On considère la fonction $f$ définie sur l'intervalle $]1~;~+\infty[$ par 
\[f(x)= 0,05 - \dfrac{\ln x}{x - 1}.\]

La limite de la fonction $f$ en $+\infty$ est égale à :

\begin{center}
\begin{tabularx}{\linewidth}{*{4}{X}}
\textbf{a.~~} $+\infty$&\textbf{b.~~}0,05&\textbf{c.~~}$-\infty$&\textbf{d.~~}0
\end{tabularx}
\end{center}

\item On considère une fonction $h$ continue sur l'intervalle $[-2 ; 4]$ telle que :

\[h(-1)=0, \qquad h(1) = 4, \qquad h(3) = -1.\]

On peut affirmer que :

\begin{center}
\begin{tabularx}{\linewidth}{X}
\textbf{a.~~} la fonction $h$ est croissante sur l'intervalle $[-1~;~1]$.\\
\textbf{b.~~} la fonction $h$ est positive sur l'intervalle $[-1~;~1]$.\\
\textbf{c.~~} il existe au moins un nombre réel $a$ dans l'intervalle $[1~;~3]$ tel que $h(a) = 1$.\\
\textbf{d.~~} l'équation $h(x)=1$ admet exactement deux solutions dans l'intervalle 

$[-2~;~4]$.
\end{tabularx}
\end{center}

\item On considère deux suites $\left(u_{n}\right)$ et $\left(v_{n}\right)$ à termes strictement positifs telles que 

$\displaystyle\lim _{n \rightarrow+\infty} u_{n}=+\infty$ et $\left(v_{n}\right)$ converge vers 0 .

On peut affirmer que :

\begin{center}
\begin{tabularx}{\linewidth}{*{2}{X}}
\textbf{a.~~}la suite $\left(\frac{1}{v_{n}}\right)$ converge.&
\textbf{b.~~} la suite $\left(\frac{v_{n}}{u_{n}}\right)$ converge.\\
\textbf{c.~~} la suite $\left(u_{n}\right)$ est croissante.&
\textbf{d.~~} $\displaystyle\lim _{n \rightarrow+\infty}\left(-u_{n}\right)^{n}=-\infty$
\end{tabularx}
\end{center}

\item Pour participer à un jeu, un joueur doit payer $4$ \euro.

Il lance ensuite un dé équilibré à six faces :

\begin{itemize}
\item s'il obtient 1, il remporte $12$ \euro{};
\item s'il obtient un nombre pair, il remporte $3$\euro{};
\item sinon, il ne remporte rien.
\end{itemize}

En moyenne, le joueur :

\begin{center}
\begin{tabularx}{\linewidth}{*{2}{X}}
\textbf{a.~~}gagne $3,50$ \euro&\textbf{b.~~}perd $3$ \eurologo.\\
\textbf{c.~~}perd $1,50$ \euro&\textbf{d.~~}perd $0,50$ \euro.
\end{tabularx}
\end{center}

\item On considère la variable aléatoire $X$ suivant la loi binomiale $\mathcal{B}(3~;~p)$.

On sait que $P(X = 0) = \dfrac{1}{125}$. On peut affirmer que :

\begin{center}
\renewcommand\arraystretch{1.9}
\begin{tabularx}{\linewidth}{*{2}{X}}
\textbf{a.~~}$p = \dfrac{1}{5}$&\textbf{b.~~}$P(X = 1) =\dfrac{124}{125}$\\
\textbf{c.~~}$p = \dfrac{4}{5}$&\textbf{d.~~}$P(X= 1) =\dfrac{4}{5}$
\end{tabularx}
\end{center}
\end{enumerate}
\end{document}