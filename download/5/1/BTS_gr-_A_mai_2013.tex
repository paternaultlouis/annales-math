\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
\usepackage{graphicx}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text,pst-circ,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}
\lhead{\textbf{A. P. M. E. P.}}
\rhead{\small Brevet de technicien supérieur}
\lfoot{\small{Groupe A1}}
\rfoot{\small{14 mai 2013}}

\pagestyle{fancy}
\thispagestyle{empty}

\begin{center}{\Large\textbf{Brevet de technicien supérieur\\[5pt]session 2013 - groupement A}}
  
\end{center}

\vspace{0,25cm}

\textbf{Spécialités :}
\setlength\parindent{5mm}
\begin{itemize}
\item Contrôle industriel et régulation automatique
\item Informatique et réseaux pour l'industrie et les services techniques
\item Systèmes électroniques
\item Électrotechnique
\item Génie optique
\item Techniques physiques pour l'industrie et le laboratoire
\end{itemize}
\setlength\parindent{0mm}

\bigskip

\textbf{Exercice 1 \hfill 10 points}

\medskip

Cet exercice comporte 2 parties indépendantes. Il traite de l'équilibre de systèmes triphasés. 

Aucune connaissance sur ces systèmes n'est nécessaire pour traiter l'intégralité de cet exercice.

\bigskip
 
\textbf{Partie A}

\medskip
 
Un onduleur à commande asynchrone délivre une tension périodique $f(t)$ de période $2\pi$ selon la représentation graphique suivante :

\begin{center}
\psset{xunit=1cm,yunit=2cm,comma=true}
\begin{pspicture}(-6.5,-1.1)(6.5,1.2)
\psaxes[linewidth=1.25pt,Dx=10,Dy=0.5]{->}(0,0)(-6.5,-1.1)(6.5,1.1)
\psaxes[linewidth=1.25pt,Dx=10,Dy=0.5](0,0)(-6.5,-1.1)(6.5,1.1)
\multido{\n=-6.2832+0.5236}{27}{\psline[linestyle=dotted](\n,-1)(\n,1)}
\multido{\n=-1.0+0.5}{5}{\psline[linestyle=dotted](-6.2832,\n)(6.2832,\n)}
\psline[linewidth=1.25pt,linecolor=blue](-6.5,1)(-4.712,1)(-4.712,-1)(-1.5708,-1)(-1.5708,1)(1.5708,1)(1.5708,-1)(4.712,-1)(4.712,1)(6.5,1)
\uput[dl](-6.282,0){$- 2\pi$}\uput[dl](-3.141,0){$-\pi$}
\uput[dl](-1.0472,0){$\frac{-\pi}{3}$}\uput[dl](1.0472,0){$\frac{\pi}{3}$}
\uput[dl](3.141,0){$\pi$}\uput[dl](6.282,0){$2\pi$}
\end{pspicture}
\end{center} 
 
\begin{enumerate}
\item Sur l'annexe \no 1, on a représenté graphiquement sur $[-2\pi~;~2\pi]$ la tension $f(t)$ et la tension $f\left(t + \dfrac{2\pi}{3}\right)$.

Sur le document réponse, compléter le tableau de valeurs et construire la représentation graphique de la tension $f\left(t + \dfrac{4\pi}{3}\right)$ sur $[-2\pi~;~2\pi]$. 
\item \emph{En régime triphasé, l'onduleur soumet la phase 1 à la tension $f(t)$, la phase 2 à la tension $f\left(t + \dfrac{2\pi}{3}\right)$ et la phase 3 à $f\left(t + \dfrac{4\pi}{3}\right)$. Le neutre, quant à lui, est soumis à la somme $S(t)$ des tensions des phases, définie par}
 
\[S(t) = f(t) + f\left(t + \dfrac{2\pi}{3}\right) + f\left(t + \dfrac{4\pi}{3}\right).\]

\emph{Si cette somme est nulle pour tout nombre réel $t$, le système triphasé est équilibré. Sinon le système est déséquilibré.} 
	\begin{enumerate}
		\item Calculer $S(0)$. 
		\item Le système triphasé étudié dans cette partie est-il équilibré ?
	\end{enumerate}
\end{enumerate}

\bigskip
 
\textbf{Partie B}

\medskip
 
Pour garantir l'équilibrage d'un système triphasé, on peut utiliser un onduleur à commande décalée. Ainsi, nous considérons dans cette partie que la tension délivrée est un signal $g$ de période $2\pi$, dont la représentation graphique sur $[-2\pi~;~2\pi]$ figure ci-après : 

\begin{center}
\psset{xunit=0.8cm,yunit=2cm,comma=true}
\begin{pspicture}(-6.5,-1.1)(6.5,1.1)
\psaxes[linewidth=1.25pt,Dx=10,Dy=0.5]{->}(0,0)(-6.5,-1.1)(6.5,1.1)
\multido{\n=-6.2832+0.5236}{27}{\psline[linestyle=dotted](\n,-1)(\n,1)}
\multido{\n=-1.0+0.5}{5}{\psline[linestyle=dotted](-6.2832,\n)(6.2832,\n)}
\psline[linewidth=1.25pt,linecolor=blue](-6.282,1)(-5.236,1)(-5.236,0)(-4.189,0)(-4.189,-1)(-2.0944,-1)(-2.0944,0)(-1.0472,0)(-1.0472,1)(1.0472,1)(1.0472,0)(2.0944,0)(2.0944,-1)(4.189,-1)(4.189,0)(5.236,0)(5.236,1)(6.282,1)
\uput[dl](-6.282,0){$- 2\pi$}\uput[dl](-3.141,0){$-\pi$}
\uput[dl](-1.0472,0){$\frac{-\pi}{3}$}\uput[dl](1.0472,0){$\frac{\pi}{3}$}
\uput[dl](-2.0944,0){$\frac{-2\pi}{3}$}\uput[dl](2.0944,0){$\frac{2\pi}{3}$}
\uput[dl](3.141,0){$\pi$}\uput[dl](6.282,0){$2\pi$}
\end{pspicture}
\end{center} 

On s'intéresse au développement en série de Fourier du signal $g$.
 
Dans la suite de l'exercice, $a_{0} , a_{n}$ et $b_{n}$ désignent les coefficients du développement en série de Fourier de ce signal $g$, avec les notations du formulaire. 

\medskip

\begin{enumerate}
\item Déterminer $a_{0}$. 
\item Préciser la valeur des coefficients $b_{n}$ pour tout entier naturel $n$ non nul. 
\item
	\begin{enumerate}
		\item Donner la valeur de $g(t)$ sur chacun des intervalles 
$\left]0~;~\dfrac{\pi}{3}\right[, \: \left]\dfrac{\pi}{3}~;~\dfrac{2\pi}{3}\right[$ et $\left]\dfrac{2\pi}{3}~;~\pi \right[$. 
		\item Montrer que, pour tout entier naturel $n$ non nul on a :
		
\[a_{n} = \dfrac{2}{\pi} \times \dfrac{\sin \left(\frac{n\pi}{3} \right) + \sin \left(\frac{2n\pi}{3} \right)}{n}\]
 
	\end{enumerate}
\item
	\begin{enumerate}
		\item Vérifier que $a_{3k} = 0$, pour tout nombre entier naturel $k$ non nul. 
		\item On démontre que ce qui empêche un signal d'être nul dans le neutre est la présence d'harmoniques non nulles de rangs multiples de 3 dans le développement en série de Fourier du signal $g$.
		 
Peut-on considérer que le système triphasé est équilibré, c'est à dire que la tension sur le neutre est nulle ?
	\end{enumerate} 
\end{enumerate}

\emph{Remarque : Dans les hôpitaux, les banques, les lycées, etc., l'énergie électrique est fournie par des transformateurs ou par les onduleurs qui alimentent une multitude de récepteurs (ordinateurs, lampes basse-consommation ...) qui génèrent des courants harmoniques. Sans une installation adaptée et sans une utilisation de récepteurs optimisés, l'accumulation d'harmoniques de rangs multiples de 3 conduit au déséquilibre du système triphasé. Ceci peut engendrer de graves problèmes .' surchauffe du fil portant le neutre, phénomènes d'interférence, augmentation des pertes d'énergie, ouverture des fusibles ou interrupteurs automatiques ...} 

\vspace{0,5cm}

\textbf{Exercice 2 \hfill 10 points}

\medskip


Les parties A, B et C de cet exercice peuvent être traitées de manière indépendante.

On notera $U$ la fonction échelon unité définie pour tout nombre réel $t$ par : 

\[ \left\{\begin{array}{l c l c l}
U(t) &=& 0 &\text{si}& t < 0\\
U(t) &=& 1 &\text{si}& t \geqslant 0
\end{array}\right.\]
 
Une fonction définie sur l'ensemble des nombres réels est dite causale lorsque cette fonction est nulle sur l'intervalle $]- \infty~;~0[$. On considère un système entrée-sortie où les signaux d'entrée et sortie sont modélisés par des fonctions causales notées respectivement $e$ et $s$. Ce système est du second ordre, c'est à dire que les fonctions $e$ et $s$ sont liées sur l'intervalle $[0~;~+ \infty[$ par une équation différentielle du type 

\[s''(t) + bs'(t) + cs(t) = ce(t),\]
 
où $b$ et $c$ désignent des constantes réelles.
 
On suppose de plus dans tout l'exercice que $s(0) = 0$ et $s'(0) = 0$.

\bigskip
 
\textbf{Partie A : résolution d'une équation différentielle du second ordre}

\medskip
 
Dans cette partie, on suppose que $b = 1$ et $c = 0,25$. De plus, le signal d'entrée, constant est défini pour tout nombre réel $t$ de l'intervalle $[0~;~+ \infty[$ par $e(t) = 10$.
 
La fonction causale $s$ est donc solution sur l'intervalle $[0~;~+ \infty[$ de l'équation différentielle 

\[(E) \::\quad  y'' + y' + 0,25y = 2,5.\]
 
\begin{enumerate}
\item 

Déterminer une fonction constante sur $[0~;~+ \infty[$ solution particulière de l'équation différentielle $(E)$.
 
Résoudre l'équation différentielle $\left(E_{0}\right)\: :\quad y'' + y' + 0,25y = 0$. 

En déduire la forme générale des solutions de l'équation différentielle (E). 
Parmi les quatre expressions ci-dessous, laquelle est celle de s(t) sur l'intervalle $[0~;~+ \infty[$ ?
 
Recopier la réponse choisie sur la copie.

\begin{center}
\begin{tabularx}{0.8\linewidth}{X X}
$\bullet~~5t\text{e}^{-0,5t}$&$\bullet~~ 10 - (5t + 10)\text{e}^{-0,5t}$\\ 
$\bullet~~10 - (2,5t + 10)\text{e}^{-0,25t}$&$\bullet~~10 - (10t + 10)\text{e}^{-0,5t}$
\end{tabularx}
\end{center} 
\end{enumerate}
\bigskip
 
\textbf{Partie B : utilisation de la transformation de Laplace}

\medskip
 
Dans cette partie, on suppose que $b = 0$ et $c = 9$. De plus, le signal d'entrée, sinusoïdal est défini pour tout nombre réel $t$ par

\[e(t) = \sin(2t) U(t).\]
 
La fonction causale $s$ est donc solution de l'équation différentielle 

\[\left(E'\right)\: :\quad s''(t) + 9s(t) = 9\sin (2t)U(t).\]
 
On note $S$ la transformée de Laplace de la fonction $s$.
 
\begin{enumerate}
\item En appliquant la transformation de Laplace aux deux membres de l'équation différentielle $\left(E'\right)$, montrer que 
 
\[S(p) = \dfrac{18}{\left(p^2 + 4\right)\left(p^2+ 9\right)}.\]
 
\item Déterminer les nombres réels $a$ et $b$ tels que, pour tout nombre réel $p$, on ait 
 
\[S(p) = \dfrac{a}{p^2 + 4} +  \dfrac{b}{p^2 + 9}.\]
 
\item En déduire l'expression de $s(t)$ pour tout nombre réel $t$ positif ou nul.
\end{enumerate}
 
\bigskip
 
\textbf{Partie C : détermination de l'amplitude du signal de sortie}

\medskip
 
On note $f$ la fonction causale définie sur l'ensemble des nombres réels par : 

\[f(t) = (1,8\sin (2t) - 1,2\sin (3t)) U(t).\]
 
Cette fonction est périodique de période $2\pi$ sur l'intervalle $[0~;~+\infty[$.
 
Sur l'annexe 2 sont tracées deux représentations graphiques de la fonction $f$.
 
Les points $M_{1}, M_{2}, M_{3}, M_{4}$ indiqués sur le graphique correspondent aux extremums locaux de la fonction $f$ sur l'intervalle $[0~;~ 2\pi]$.
 
Le but de cette partie est de déterminer la valeur maximale $A$ atteinte par $f(t)$ quand $t$ varie dans l'intervalle $[0~;~+\infty[$.
 
\begin{enumerate}
\item En utilisant la figure 1 de l'annexe 2, déterminer une valeur approchée de $A$ à $0,1$ près.
\item Pour tout nombre réel $t$ positif ou nul, calculer une expression de $f'(t)$. 




\item 
	\begin{enumerate}
		\item Montrer que, pour tout nombre réel positif ou nul $t, f'(t)$ peut se mettre sous la forme 
		
\[f'(t) = \alpha \sin \left(\dfrac{5t}{2}\right) \sin \left(\dfrac{t}{2}\right) .\]		
où $\alpha$ est un nombre réel strictement positif.
 
En déduire la valeur de $f'\left(\dfrac{2k\pi}{5}\right)$ pour tout nombre entier naturel $k$. 
		\item Déterminer les valeurs exactes des abscisses des points $M_{1}, M_{2}, M_{3}, M_{4}$. 
		
En déduire une valeur approchée de $A$ à $10^{-3}$ près.
	\end{enumerate} 
\end{enumerate} 
\newpage

\begin{center}
\textbf{Annexe 1}
\vspace{2cm}
 
Représentation graphique de la tension $f(t)$

\vspace{0,5cm} 

\psset{xunit=1cm,yunit=2cm,comma=true}
\begin{pspicture}(-6.5,-1.1)(6.5,1.1)
\psaxes[linewidth=1.25pt,Dx=10,Dy=0.5]{->}(0,0)(-6.5,-1.1)(6.5,1.1)
\psaxes[linewidth=1.25pt,Dx=10,Dy=0.5](0,0)(-6.5,-1.1)(6.5,1.1)
\multido{\n=-6.2832+0.5236}{27}{\psline[linestyle=dotted](\n,-1)(\n,1)}
\multido{\n=-1.0+0.5}{5}{\psline[linestyle=dotted](-6.2832,\n)(6.2832,\n)}
\psline[linewidth=1.25pt,linecolor=blue](-6.5,1)(-4.712,1)(-4.712,-1)(-1.5708,-1)(-1.5708,1)(1.5708,1)(1.5708,-1)(4.712,-1)(4.712,1)(6.5,1)
\uput[dl](-6.282,0){$- 2\pi$}\uput[dl](-3.141,0){$-\pi$}\uput[d](-6.282,0){$2\pi$}
\uput[dl](-1.0472,0){$\frac{-\pi}{3}$}\uput[dl](1.0472,0){$\frac{\pi}{3}$}
\uput[dl](3.141,0){$\pi$}\uput[dl](6.282,0){$2\pi$}
\end{pspicture}
\vspace{2cm}

Représentation graphique de la tension $f\left(t + \dfrac{2\pi}{3}\right)$

\vspace{0,5cm} 

\psset{xunit=1cm,yunit=2cm,comma=true}
\begin{pspicture}(-6.5,-1.1)(6.5,1.1)
\psaxes[linewidth=1.25pt,Dx=10,Dy=0.5]{->}(0,0)(-6.5,-1.1)(6.5,1.1)
\psaxes[linewidth=1.25pt,Dx=10,Dy=0.5](0,0)(-6.5,-1.1)(6.5,1.1)
\multido{\n=-6.2832+0.5236}{27}{\psline[linestyle=dotted](\n,-1)(\n,1)}
\multido{\n=-1.0+0.5}{5}{\psline[linestyle=dotted](-6.2832,\n)(6.2832,\n)}
\psline[linewidth=1.25pt,linecolor=blue](-6.283,-1)(-3.665,-1)(-3.665,1)(-0.5236,1)(-0.5236,-1)(2.618,-1)(2.618,1)(5.7596,1)(5.7596,-1)(6.283,-1)
\uput[dl](-6.282,0){$- 2\pi$}\uput[dl](-3.141,0){$-\pi$}\uput[d](-6.282,0){$2\pi$}
\uput[dl](-1.0472,0){$\frac{-\pi}{3}$}\uput[dl](1.0472,0){$\frac{\pi}{3}$}
\uput[dl](3.141,0){$\pi$}\uput[dl](6.282,0){$2\pi$}
\end{pspicture}

\end{center}
\newpage
\begin{center}
\textbf{Document réponse\\ 
(à rendre avec la copie)}


\vspace{3cm}
 
Tableau des valeurs prises par $f\left(t + \dfrac{4\pi}{3}\right)$ pour certaines valeurs de $t$

\vspace{0,5cm}

\renewcommand\arraystretch{2}
\begin{tabularx}{\linewidth}{|c|*{8}{>{\centering \arraybackslash}X|}}\hline 
$t$&$- \dfrac{4\pi}{3}$&$- \pi$&$- \dfrac{\pi}{2}$&$0$&$\dfrac{\pi}{3}$&$\pi$&$ \dfrac{4\pi}{3}$&$ \dfrac{5\pi}{3}$\\ \hline
$f\left(t + \dfrac{4\pi}{3}\right)$&&1&&&&&&$- 1$\\ \hline
\end{tabularx}

\vspace{3cm}

Repère pour représenter $f\left(t + \dfrac{4\pi}{3}\right)$

\vspace{0,5cm}

\psset{xunit=1cm,yunit=2.5cm,comma=true}
\begin{pspicture}(-6.5,-1.1)(6.5,1.2)
\psaxes[linewidth=1.5pt,Dx=10,Dy=0.5]{->}(0,0)(-6.5,-1.1)(6.5,1.2)
\multido{\n=-6.2832+0.5236}{25}{\psline[linestyle=dotted](\n,-1)(\n,1)}
\multido{\n=-1.0+0.5}{5}{\psline[linestyle=dotted](-6.2832,\n)(6.2832,\n)}
\uput[dl](-6.282,0){$- 2\pi$}\uput[dl](-3.141,0){$-\pi$}\uput[d](-6.282,0){$2\pi$}
\uput[dl](-1.0472,0){$\frac{-\pi}{3}$}\uput[dl](1.0472,0){$\frac{\pi}{3}$}
\uput[dl](3.141,0){$\pi$}\uput[dl](6.282,0){$2\pi$}
\end{pspicture}
\end{center}

\newpage
\begin{center}
\textbf{Annexe 2}

\vspace{1cm}
 
Figure 1

\vspace{0,5cm}

\psset{xunit=0.5\pstRadUnit,yunit=1cm}
\begin{pspicture}(-0.5,-3.5)(20,3.5)
%\psgrid
\psaxes[linewidth=1.5pt,Dx=2]{->}(0,0)(-0.5,-3.5)(20,3.5)
\psplot[plotpoints=8000,linewidth=1.25pt,linecolor=blue]{0}{20}{ x 2 mul RadtoDeg  sin 1.8 mul  x  3 mul RadtoDeg sin 1.2 mul sub}
\psdots(1.25,1.8)(2.5,-2.82)(3.8,2.82)(5,-1.8)
\uput[u](1.25,1.8){$M_{1}$}\uput[d](2.5,-2.82){$M_{2}$}
\uput[u](3.8,2.82){$M_{3}$}\uput[d](5,-1.8){$M_{4}$}

\end{pspicture} 

\vspace{2cm}

\psset{xunit=\pstRadUnit,yunit=1cm}
\begin{pspicture}(-0.25,-3.5)(6.5,3.5)
\psaxes[linewidth=1.5pt,Dx=2]{->}(0,0)(-0.25,-3.5)(6.25,3.5)
\psplot[plotpoints=8000,linewidth=1.25pt,linecolor=blue]{0}{6.5}{ x 2 mul RadtoDeg  sin 1.8 mul  x  3 mul RadtoDeg sin 1.2 mul sub}
\psdots(1.25,1.8)(2.5,-2.82)(3.8,2.82)(5,-1.8)
\uput[u](1.25,1.8){$M_{1}$}\uput[d](2.5,-2.82){$M_{2}$}
\uput[u](3.8,2.82){$M_{3}$}\uput[d](5,-1.8){$M_{4}$}
\end{pspicture} 

\end{center}
\end{document}