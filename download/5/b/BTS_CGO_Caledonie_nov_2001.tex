\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}%ATTENTION codage en utf8 !
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{lscape}
\usepackage{graphicx}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès 
\usepackage{pst-plot,pst-text}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength{\textheight}{23,5cm}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\setlength{\voffset}{-1,5cm}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\lhead{\small Brevet de technicien supérieur}
\rhead{\small Nouvelle-Calédonie}
\lfoot{\small{Comptabilité et gestion}}
\rfoot{\small{Session 2001}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}
\begin{center} \Large \textbf{\decofourleft~Brevet de technicien supérieur Nouvelle-Calédonie~\decofourright\\Comptabilité et gestion  session 2001}  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 11 points}

\medskip

On considère un produit dont le prix unitaire, exprimé en euros, est noté $x$.

La demande $f(x)$ est la quantité de ce produit, exprimée en centaines d'unités, que les consommateurs sont prêts à acheter au prix unitaire de $x$ euros. 

L'offre $g(x)$ est la quantité de ce produit, exprimée en centaines d'unités, que les producteurs sont prêts à vendre au prix unitaire de $x$ euros. 

On appelle prix d'équilibre de ce produit le prix pour lequel l'offre et la demande sont égales. L'objectif de cet exercice est de déterminer un prix d'équilibre.

\medskip
 
\emph{Les deux parties de cet exercice peuvent être traitées de façon indépendante.}

\medskip
 
\textbf{Partie I : Étude statistique}

\medskip

\emph{Pour cette partie, on utilisera les fonctions de la calculatrice. Le détail des calculs n'est pas demandé.} 

\medskip

Une étude statistique a permis de relever les résultats suivants, où $x_{i}$  représente le prix de vente unitaire en euros et $y_{i}$ la quantité demandée, en centaines d'unités, de ce produit.

\medskip
\begin{tabularx}{\linewidth}{|l|*{6}{>{\centering \arraybackslash}X|}}\hline  
Prix unitaire en euros : $x_{i}$&1,1	&1,25&1,4	&2		&2,45&3\\ \hline 
Quantité en centaines : $y_{i}$	&9,75	&8,50&4,50	&3,00	&2,6&2,5\\ \hline
\end{tabularx}
\medskip
 
Le plan est rapporté à un repère orthogonal \Oij{} d'unités graphiques: 5~cm pour 1~euro en abscisse et 1~cm pour 1~centaine d'unités en ordonnée.
 
Le nuage de points $M_{i}$ de coordonnées $\left(x_{i}~;~y_{i}\right)$ est représenté sur la feuille annexe. Vu la disposition des points, on ne cherche pas à remplacer ce nuage par une droite, c'est-à-dire à réaliser un ajustement affine. 

On effectue le changement de variable $Y_{i} = \ln y_{i}$ où ln désigne la fonction logarithme népérien.

\medskip
 
\begin{enumerate}
\item Compléter le tableau de valeurs ci-dessous, sous le nuage de points : les valeurs de $Y_{i}$ seront arrondies à $10^{- 3}$ près.

\medskip
\begin{tabularx}{\linewidth}{|l*{6}{>{\centering \arraybackslash}X|}}\hline 
Prix unitaire en euros : $x_{i}$&1,1&1,25	&1,4&2	&2,45	&3\\ \hline 
Quantité en centaines : $Y_{i}$	&	&		&	&	&		&\\ \hline
\end{tabularx}
\medskip 

\item Donner le coefficient de corrélation linéaire $r$ de la série statistique $\left(x_{i}~;~Y_{i}\right)$. On en donnera la valeur décimale approchée à $10^{- 2}$ près par défaut. Le résultat trouvé permet d'envisager un ajustement affine. 
\item Donner, par la méthode des moindre, carrés, une équation de la droite de régression de $Y$ en $x$ sous la forme $Y = a x + b$ ; on 
donnera la valeur décimale approchée de $a$ à $10^{- 2}$ près par défaut ; $b$ sera arrondi à l'entier le plus proche. 
\item En déduire une estimation de la quantité demandée $y$, en centaines d'unités, en fonction du prix unitaire $x$, sous la forme 
$y = k \text{e}^{ - \lambda x}$ où $k$ et $\lambda$ sont des constantes ; $k$ sera arrondi à l'entier le plus proche. 
\item En déduire la quantité demandée que l'on peut estimer pour un prix unitaire de $2,90$ euros. On donnera la valeur arrondie à une unité près.
\end{enumerate}
 
\medskip
 
\textbf{Partie II : Recherche du prix d'équilibre}

\medskip
 
Dans cette partie, on considère que la demande, exprimée en centaines d'unités, pour un prix unitaire de $x$ euros est $f(x)$, où $f$ est la fonction définie sur l'intervalle [1~;~3] par :

\[f(x) = 20 \text{e}^{- 0,7 x}.\]
 
De même, l'offre, exprimée en centaines d'unités, pour un prix unitaire de $x$ euros est $g(x)$, où $g$ est la fonction définie sur l'intervalle [1~;~3] par : 
\[g(x) = 0,15 x + 2,35.\]
 
\begin{enumerate}
\item On désigne par $f'$ 1 la fonction dérivée de $f$. 
	\begin{enumerate}
		\item Calculer  $f'(x)$. En déduire le sens de variation de la fonction $f$ sur l'intervalle [1~;~3]. 
		\item Sur le graphique donné en annexe, tracer les représentations graphiques $C$ et $\Delta$ des fonctions $f$ et $g$. 
		\item Déterminer graphiquement, en faisant figurer les tracés utiles, une valeur approchée, arrondie à $10^{-1}$ près, de l'abscisse du point d'intersection de $C$ et $\Delta$.
	\end{enumerate} 
\item Soit la fonction $h$ définie sur l'intervalle [1~;~3] par 

\[h(x) = f(x) - g(x).\]
 
	\begin{enumerate}
		\item Étudier le sens de variation de la fonction $h$ sur l'intervalle [1~;~3]. 
		\item En déduire, en justifiant, que l'équation $h(x) = 0$ admet dans l'intervalle [1~;~3] une solution unique,  notée $\alpha$, dont on donnera la valeur décimale approchée à $10^{- 2}$ près par défaut. Vérifier que cette valeur est compatible avec la valeur lue sur le graphique au 1. 
		\item Donner, à $10^{- 2}$ près, le prix d'équilibre en euros, c'est-à-dire le prix pour lequel l'offre et la demande sont égales. Calculer 
l'offre correspondant au prix d'équilibre.
	\end{enumerate} 
\end{enumerate}

\begin{center}
\psset{xunit=2cm,yunit=0.4cm,comma=true}
\begin{pspicture}(4,13)
\multido{\n=0.0+0.5}{9}{\psline[linewidth=0.3pt,linecolor=orange](\n,0)(\n,12)}
\multido{\n=0+2}{7}{\psline[linewidth=0.3pt,linecolor=orange](0,\n)(4,\n)}
\psaxes[linewidth=1.5pt,Dx=0.5,Dy=2](0,0)(4,13)
\psdots[dotscale=1.5,dotstyle=+,dotangle=45](1.1,9.75)(1.25,8.5)(1.4,4.5)(2,3)(2.45,2.6)(3,2.5)
\end{pspicture}
\end{center}

\vspace{0,5cm}

\textbf{Exercice 2 \hfill 9 points}

\begin{center}
\textbf{Les trois parties de cet exercice sont indépendantes}
\end{center}

Une centrale d'achat fournit trois types de poulets à une chaîne d'hypermarchés :

\setlength\parindent{8mm}
\begin{itemize}
\item des poulets \og biologiques \fg, dits poulets $P_{1}$; 
\item des poulets de Bresse, dits poulets $P_{2}$ ; 
\item des poulets élevés en plein air, dits poulets $P_{3}$.
\end{itemize}
\setlength\parindent{0mm}
 
Une étude de marché a montré qu'un poulet se vend mal lorsque son poids est inférieur ou égal à 1~kilogramme.
 
Avant leur conditionnement et leur mise en vente en grande surface, les poulets sont stockés dans un entrepôt frigorifique.

Dans la suite, on s'intéresse aux stocks de ces trois types de poulets, une journée donnée.

\bigskip
 
\textbf{Partie 1 : Etude des poulets $P_{1}$}

\medskip
 
On note $X$ la variable aléatoire qui, à chaque poulet prélevé au hasard dans le stock de poulets $P_{1}$ associe son poids en kg. On admet que $X$ suit la loi normale de moyenne $1,46$ et d'écart-type $0,30$.
 
Calculer, à $10^{-2}$ près, la probabilité de l'évènement $A$ : \og un poulet prélevé au hasard dans le stock de poulets $P_{1}$ a un poids inférieur ou égal à 1~kg \fg.

\bigskip
 
\textbf{Partie II : Étude des poulets $P_{2}$}

\medskip
 
On note $B$ l'évènement : \og un poulet prélevé au hasard dans le stock de poulets $P_{2}$ a un poids inférieur ou égal à 1 kg \fg. 

On suppose que la probabilité de l'évènement $B$ est $0,03$.
 
On prélève au hasard $100$ poulets dans le stock de poulets $P_{2}$. Le stock est assez important pour que l'on puisse assimiler ce prélèvement à un tirage avec remise de $100$ poulets.
 
On considère la variable aléatoire $Y$ qui, à tout prélèvement de $100$ poulets ainsi défini, associe le nombre de poulets ayant un poids inférieur ou égal à 1 kg.

\medskip
 
\begin{enumerate}
\item Expliquer pourquoi $Y$ suit une loi binomiale. En déterminer les paramètres. 
\item On approche la loi de la variable aléatoire $Y$ par la loi de Poisson de même espérance mathématique.
 
Donner le paramètre de cette loi. 
\item Utiliser cette approximation pour calculer, à $10^{-2}$ près, la probabilité de l'évènement $C$ : 
\og parmi 100 poulets prélevés au hasard dans le stock de poulets $P_{2}$ il y a au plus 4 poulets ayant un poids inférieur ou égal à 1~kg \fg.
\end{enumerate}
 
\bigskip
 
\textbf{Partie III : Étude des poulets $P_{3}$}

\medskip
 
Dans cette partie, on cherche à estimer le pourcentage $p$ inconnu de poulets du stock dont le poids est inférieur ou égal à 1 kg.
 
On considère un échantillon de $100$ poulets prélevés au hasard et avec remise dans le stock de poulets $P_{3}$ on constate qu'il contient 4 poulets dont le poids est inférieur ou égal à 1~kg.

\medskip
 
\begin{enumerate}
\item Donner une estimation ponctuelle du pourcentage $p$ de poulets du stock de poulets $P_{3}$ dont le poids est inférieur ou égal à 1~kg. 
\item Soit $F$ la variable aléatoire qui, à tout échantillon de $100$ poulets prélevés au hasard et avec remise dans le stock de poulets $P_{3}$ 
associe le pourcentage de poulets de cet échantillon dont le poids est inférieur ou égal à 1~kg.
 
On suppose que $F$ suit la loi normale $\mathcal{N}\left(p~;~ \sqrt{\dfrac{p(1 - p)}{n}}\right)$ où $p$ est le pourcentage inconnu de poulets du stock de poulets $P_{3}$ dont le poids est inférieur ou égal à 1~kg. 
	\begin{enumerate}
		\item Déterminer un intervalle de confiance du pourcentage $p$ avec le coefficient de confiance 95\,\%, les bornes seront données à $10^{-3}$ 
près. 
		\item On considère l'affirmation suivante : \og le pourcentage $p$ est obligatoirement dans l'intervalle de confiance obtenu à la question 
a. \fg. Cette affirmation est-elle vraie ?
	\end{enumerate} 
\end{enumerate}
\end{document}