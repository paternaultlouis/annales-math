\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{graphicx,colortbl}
\usepackage{textcomp} 
\usepackage{multirow}
\usepackage{enumitem}
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Relecture François Hache
%Merci à Ronan Charpentier pour le sujet
\usepackage{pst-plot,pst-text,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS Opticien-lunetier},
pdftitle = {septembre 2020},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
\renewcommand{\d}{\mathrm{\,d}}%     le d de différentiation
\newcommand{\e}{\mathrm{\,e\,}}%      le e de l'exponentielle
\renewcommand{\i}{\mathrm{\,i\,}}%    le i des complexes
\newcommand{\ds}{\displaystyle}

\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\footnotesize Brevet de technicien supérieur }
\lfoot{\footnotesize{Opticien--lunetier}}
\rfoot{\footnotesize{septembre 2020}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}
\begin{center} \Large \textbf{\decofourleft~Brevet de technicien supérieur Opticien--lunetier~\decofourright\\[5pt]septembre 2020}  
\end{center}
%Tapuscrit Ronan Charpentier

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 10 points}

\medskip

\textbf{Les quatre parties de cet exercice peuvent être traitées de façon indépendante}

\medskip

La fabrication de verre minéral résulte d'une fusion de trois éléments (quartz, potasse et oxyde). Tous
ces éléments sont chauffés jusqu'à leur température de fusion de \np{1500}~degrés Celsius dans un four à cuve de vitrification, puis brassés pendant plusieurs heures. Le verre liquide est ensuite acheminé à
une presse automatique qui produit des ébauches de verres, lesquels sont refroidis lentement jusqu'à
température ambiante dans un four de recuit.

\medskip

Dans les parties A, B et C, on s'intéresse à la mise en fusion des éléments constitutifs du verre minéral dans le four à cuve. Dans la partie D, on étudie le refroidissement du verre dans le four de recuit.

\bigskip

\textbf{Partie A. Statistiques à 2 variables}

\medskip

On a mesuré la température du verre minéral dans la cuve de vitrification depuis le début du processus
de chauffage jusqu'à la fusion. On obtient les résultats suivants :

\begin{center}
\begin{tabularx}{\linewidth}{|m{3.25cm}|*{9}{>{\centering \arraybackslash}X|}}\hline
Temps $t$ (en minutes) &0 &1 &2 &3 &4 &5 &10 &15 &20\\ \hline
Température du verre 
$T$ (en degrés Celsius)&24 &354 &614 &816 &943 &\np{1066} &\np{1395} &\np{1459} &\np{1491}\\ \hline
\end{tabularx}
\end{center}

\smallskip

Représentation de la série $(t~;~T)$ :

\begin{center}
\psset{xunit=0.4cm,yunit=0.006cm}
\begin{pspicture}(-5,-100)(24,1600)
\multido{\n=0+5}{5}{\psline[linewidth=0.1pt](\n,0)(\n,1500)}
\multido{\n=0+500}{4}{\psline[linewidth=0.1pt](0,\n)(24,\n)}
\psaxes[linewidth=1.25pt,Dx=5,Dy=500](0,0)(0,0)(24,1600)
\psdots(0,0)(1,354)(2,614)(3,816)(4,943)(5,1066)(10,1395)(15,1459)(20,1491)
\uput[u](20,0){Temps $t$ en minutes}
\rput{90}(-3,1300){Température $T$ en ~\degres C}
\end{pspicture}
\end{center}

\smallskip

Le coefficient de corrélation linéaire de la série $(t~;~T)$ est $r_1 \approx 0,865$.

\medskip

L'allure du nuage de points représentant la série conduit à procéder à un changement de variable, en
posant:

\[z = \ln \left(\dfrac{\np{1500}}{\np{1500} - T}\right).\]

On obtient le tableau de valeurs suivant (les résultats ont été arrondis à $10^{-3}$ près).

\begin{center}
\begin{tabularx}{\linewidth}{|m{3.25cm}|*{9}{>{\centering \arraybackslash}X|}}\hline
Temps $t$ (en minutes) &0 &1 &2 &3 &4 &5 &10 &15 &20\\ \hline
$z$						&0,016 	&0,269 	&0,527 	&0,785 	&0,991 	&1,240 	&2,659 	&3,600 	&5,116\\ \hline
\end{tabularx}
\end{center}

\medskip

\begin{enumerate}
\item Le coefficient de corrélation linéaire de cette nouvelle série $(t~;~z)$ est $r_2 \approx 0,999$.

Expliquer pourquoi le changement de variable est pertinent.
\item Donner, à l'aide de la calculatrice, une équation de la droite de régression de $z$ en $t$ selon la méthode des moindres carrés, sous la forme $z = at + b$, où $a$ et $b$ sont arrondis au millième.
\item En déduire, en utilisant le changement de variable, une expression de $T$ en fonction de $t$ de la forme
\[T = A\text{e}^{-0,251t} + \np{1500},\]

où $A$ est arrondi à l'unité.
\end{enumerate}

\bigskip

\textbf{Partie B. Résolution d'une équation différentielle}

\medskip

Le processus de chauffage commencé, la loi de propagation de la chaleur nous permet d'étudier la
température du verre minéral dans la cuve en fonction du temps.

On modélise ainsi la température du verre minéral (en degré Celsius) par une fonction du temps (en
minutes) solution de l'équation différentielle suivante :

\[(E) :\quad  y' + 0,25y = 375\]

où $y$ est une fonction inconnue de la variable $t$, définie et dérivable sur l'intervalle 
$[0~;~ +\infty[$, et $y'$ sa fonction dérivée.

\medskip

\begin{enumerate}
\item Déterminer les solutions de l'équation différentielle :

\[\left(E_0\right) :\quad  y' + 0,25y = 0.\]

On fournit la formule suivante :

\begin{center}
\begin{tabularx}{\linewidth}{|*{2}{>{\centering \arraybackslash}X|}}\hline
Équation différentielle& Solutions sur un intervalle I\\ \hline$
ay' + by = 0$&$f(t) = k\text{e}^{- \frac{b}{a}t}$\\ \hline
\end{tabularx}
\end{center}

\item 
	\begin{enumerate}
		\item Soit $g$ la fonction définie sur $[0~;~ +\infty[$ par $g(t) = \np{1500}$.
		
Vérifier que $g$ est une solution de l'équation différentielle $(E)$.
		\item En déduire les solutions de l'équation différentielle $(E)$.
 	\end{enumerate}
\item À l'instant $t = 0$ (démarrage du four à cuve), les éléments constitutifs du verre minéral sont à
température ambiante c'est-à-dire 24 degrés Celsius.

Déterminer la fonction $h$ donnant la température du verre minéral (en degrés Celsius) en fonction du
temps $t$ (en min) vérifiant cette condition initiale.
\end{enumerate}

\bigskip

\textbf{Partie C. Étude d'une fonction}

\medskip

Dans cette partie, on modélise l'évolution de la température du verre minéral en fonction du temps $t$,
par la fonction $f$ définie sur $[0~;~ +\infty[$ par:

\[f(t) = \np{1500} - \np{1476}\text{e}^{-0,25t}.\]

On désigne par $\mathcal{C}$ la courbe représentative de la fonction $f$ dans un repère orthogonal.

Un logiciel de calcul formel fournit les résultats ci-dessous. Ces résultats sont admis et pourront être utilisés pour répondre aux questions qui suivent.

\begin{center}
\begin{tabularx}{0.6\linewidth}{|c|X|}\hline
\multicolumn{2}{|l|}{Calcul formel}\\ \hline
1&Dérivée$(\np{1500}- \np{1476}\text{exp}(-0,25t))$

$\to 369\text{e}^{- \frac{1}{4}t}$\\ \hline
2&Intégrale$(\np{1500}-\np{1476}\text{exp}(-0,25t))$

$\to \np{5904}\text{e}^{- \frac{1}{4}t} + \np{1500}t + c_1$\\ \hline
3&Limite$(\np{1500}-\np{1476}\text{exp}(-0,25t), +\text{inf})$

$\to \np{1500}$\\ \hline
4&PolynômeTaylor$(\np{1500}-\np{1476}\text{exp}(-0,25t), t, 0,2)$

$\to  24 + 369 t - \dfrac{369}{8} t^2$\\ \hline
\end{tabularx}
\end{center}

\medskip

\begin{enumerate}
\item Déterminer le sens de variation de la fonction $f$ sur $[0~;~ +\infty[$.
\item \emph{Les questions }a., b., et c. \emph{suivantes sont des questions à choix multiples. Pour chaque question, une seule réponse est exacte. Recopier sur la copie la réponse qui vous parait exacte. On ne demande aucune justification. Une réponse fausse ou une absence de réponse n'enlève pas de point.}

	\begin{enumerate}
		\item 
		
\begin{tabularx}{\linewidth}{|*{4}{X|}}\hline
\rule[-9pt]{0pt}{20pt}$\displaystyle\lim_{t \to + \infty} f(t) = 0$ &$\displaystyle\lim_{t \to + \infty} f(t) = 24$ &$\displaystyle\lim_{t \to + \infty} f(t) = \np{1500}$ &$\displaystyle\lim_{t \to + \infty} f(t)  = \np{1476}$ \\ \hline
\end{tabularx}
		\item La courbe $\mathcal{C}$ admet une asymptote dont l'équation est:
		
\begin{tabularx}{\linewidth}{|*{4}{X|}}\hline	
$t =\np{1500}$& $y = 0$& $y =  \np{1476}$& $y = \np{1500}$\\ \hline
\end{tabularx}
		\item Une équation de la tangente à la courbe $\mathcal{C}$ au point d'abscisse $0$ est:
		
\begin{tabularx}{\linewidth}{|*{4}{X|}}\hline
$y = 369t + 24$& $y = 369t - 24$& $y = 369 + 24t$& $y = 369t -\dfrac{369}{8}t^2$\rule[-3mm]{0mm}{9mm}\\ \hline
\end{tabularx}
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie D. Étude d'une suite}

\medskip

Après le brassage du verre liquide pendant plusieurs heures, on commence le processus de
refroidissement. La température du verre doit subir un abaissement régulier afin que le verre se solidifie sans se cristalliser.

On modélise l'évolution de la température du verre en fonction du temps à l'aide de la suite 
$\left(u_n\right)$ définie
par:
\[\left\{\begin{array}{l c l}
u_0& =& \phantom{0,9u_n }\np{1500}\\
u_{n+1} &=& 0,9u_n + 2,4 \:\text{pour tout entier naturel }\: n
\end{array}\right.\]

Ainsi, pour tout entier naturel $n$, on note $u_n$ la température du verre en degrés Celsius au bout de $n$ minutes et $u_0$ désigne la température initiale du processus de refroidissement.

Pour tout entier naturel $n$, on pose $v_n = u_n - 24$.

\medskip

\begin{enumerate}
\item Montrer que $\left(v_n\right)$ est une suite géométrique dont on précisera la raison et le premier terme.
\item  
	\begin{enumerate}
		\item En déduire une expression de $v_n$ en fonction de $n$.
		\item Montrer que pour tout entier naturel $n$,\: $u_n = \np{1476} \times 0,9^n + 24$.
	\end{enumerate}
\item  Déterminer la limite de la suite $\left(u_n\right)$.

Interpréter, dans le contexte, le résultat obtenu.
\item  On considère l'algorithme suivant:

\begin{center}
\begin{tabular}{l}
$n \gets 0$\\
$u \gets \np{1500}$\\
Tant Que $u > 25$\\
\qquad $u \gets u \times 0,9 + 2,4$\\
\qquad $n \gets n+1$\\
Fin Tant Que
\end{tabular}
\end{center}

Remarque: dans cet algorithme $n \gets 0$ signifie que la valeur 0 est affectée à la variable $n$.

\smallskip

En sortie de cet algorithme, la valeur de $n$ est $70$. Que représente ce nombre dans le contexte ?
\end{enumerate}

\bigskip

\textbf{EXERCICE 2 \hfill 10 points}

\medskip

\textbf{Les quatre parties de cet exercice peuvent être traitées de façon indépendante}

\smallskip

\textbf{Les résultats approchés seront à arrondir au millième}

\smallskip

Une entreprise fabrique en grande quantité des tiges en titane de longueur $80$~mm.

\bigskip

\textbf{Partie A : Loi normale}

\medskip

Une tige est considérée comme conforme lorsque sa longueur, exprimée en millimètres, est dans
l'intervalle [79,63~;~80,37].

On note $X$ la variable aléatoire qui, à chaque tige prise au hasard dans la production, associe sa
longueur. On suppose que $X$ suit une loi normale de moyenne $80$ et d'écart-type $0,16$.

\medskip

\begin{enumerate}
\item Calculer la probabilité qu'une tige prélevée au hasard dans la production ne soit pas conforme.
\item Calculer la probabilité $P(X \leqslant 80,28)$.
\end{enumerate}

\bigskip

\textbf{Partie B : Loi binomiale et loi de Poisson}

\medskip

Dans un lot de ce type de tiges, on considère que 2\,\% des tiges n'ont pas une longueur conforme. On
prélève au hasard n tiges de ce lot pour vérification de longueur. Le lot est assez important pour que
l'on puisse assimiler ce prélèvement à un tirage avec remise de n tiges.

On considère la variable aléatoire $Y$ qui, à tout prélèvement de $n$ tiges, associe le nombre de tiges de longueur non conforme.

\medskip

\begin{enumerate}
\item Justifier que la variable aléatoire $Y$ suit une loi binomiale.
\item Pour cette question, on prend $n = 50$.

Calculer la probabilité qu'il y ait exactement $2$ tiges de longueur non conforme dans ce
prélèvement.
\item On considère maintenant que $n = 200$.

On admet que la loi de la variable aléatoire $Y$ peut être approchée par une loi de Poisson.
	\begin{enumerate}
		\item Déterminer le paramètre $\lambda$ de cette loi de Poisson.
		\item On désigne par $Z$ la variable aléatoire suivant cette loi de Poisson.
À l'aide de l'approximation de $Y$ par $Z$, calculer la probabilité d'avoir au plus $3$ tiges de longueur
non conforme.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie C : Test d'hypothèse}

\medskip

Un client reçoit un lot important de tiges de ce type. On souhaite construire un test d'hypothèse bilatéral pour décider, au seuil de 5\,\%, si on peut considérer que la moyenne des longueurs des tiges reçues est égale à la longueur théorique de $80$~mm.

On note $L$ la variable aléatoire qui, à une tige prélevée au hasard dans le lot, associe sa longueur en mm. On admet que $L$ suit la loi normale de moyenne inconnue $\mu$ et d'écart-type $0,16$.

On désigne par $\overline{L}$ la variable aléatoire qui, à chaque échantillon aléatoire de $100$ tiges prélevé dans le lot, associe la moyenne des longueurs de ces $100$ tiges.

On admet que $\overline{L}$ suit une loi normale de moyenne $\mu$ et d'écart-type $\sigma = \dfrac{0,16}{\sqrt{100}} = 0,016$.

Pour ce test :

\setlength\parindent{9mm}
\begin{itemize}[label=\textbullet]
\item L'hypothèse nulle est: $H_0 :\quad  \og \mu = 80 \fg.$
\item L'hypothèse alternative est: $H_1 :\quad  \og \mu \ne 80 \fg.$
\item Le seuil de signification du test est fixé à 5\,\%.
\end{itemize}
\setlength\parindent{0mm}

\medskip

\begin{enumerate}
\item Sous l'hypothèse nulle $H_0$, déterminer le réel positif $h$ tel que
$P(80 - h < L < 80 + h) = 0,95$.
\item Énoncer la règle de décision permettant d'utiliser ce test.
\item Le client prélève un échantillon aléatoire de $100$ tiges dans la livraison et il constate que la
moyenne des longueurs de cet échantillon est de $80,02$~mm.

Quelle est la conclusion du test ?
\end{enumerate}

\bigskip

\textbf{Partie D : évènements indépendants}

\medskip

En fin de processus, en plus du défaut de longueur, on contrôle aussi le défaut de courbure des tiges.

On constate sur la production d'une journée que 2\,\% des tiges ont un défaut de longueur et que 5\,\%
des tiges ont un défaut de courbure.

On prélève une tige au hasard dans cette production.

On note $B$ l'évènement: \og la tige prélevée présente un défaut de longueur \fg.

On note $C$ l'évènement: \og la tige prélevée présente un défaut de courbure \fg.

On admet que les évènements $B$ et $C$ sont indépendants.

\medskip

\begin{enumerate}
\item Calculer $P(B \cap C)$.
\item Calculer la probabilité que la tige contrôlée ait au moins un des deux défauts.
\item Calculer la probabilité que la tige contrôlée n'ait aucun des deux défauts.
\end{enumerate}
\end{document}