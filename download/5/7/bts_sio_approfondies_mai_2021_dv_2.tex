\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} 
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo}
%Tapuscrit : Denis Vergès
%Relecture : François Hache
\usepackage{pst-plot,pst-text,pst-node,pst-tree,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\newcommand{\barre}[1]{\overline{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS},
pdftitle = {Polynésie mai 2021},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[french]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur Polynésie}
\lfoot{\small{Services informatiques aux organisations\\ mathématiques approfondies}}
\rfoot{\small{mai 2021}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~BTS Polynésie mai 2021 -- mathématiques approfondies~\decofourright\\[5pt]Services informatiques aux organisations }}

\vspace{0,25cm}

\textbf{L'usage de calculatrice avec mode examen actif est autorisé} 

\textbf{L'usage de calculatrice sans mémoire \og type collège \fg{} est autorisé}

\end{center}

\smallskip

\textbf{Exercice 1\hfill 10 points}

\medskip

\emph{Les trois parties de cet exercice sont indépendantes}

\emph{Dans cet exercice, sauf mention contraire, les résultats sont à arrondir à}  $10^{-3}$.

\medskip

Dans un groupe d'assurances, on s'intéresse aux sinistres susceptibles de survenir, une année donnée, aux véhicules de la flotte d'une importante entreprise de livraison de colis.

\bigskip

\textbf{Partie A - Étude du nombre de sinistres dans une équipe de 15 conducteurs}

\medskip

On choisit au hasard $15$ conducteurs de l'entreprise. Le nombre de conducteurs est suffisamment important pour que ce choix soit assimilé à un tirage avec remise.

On note $E$ l'évènement: \og un conducteur choisi au hasard dans l'ensemble des conducteurs de l'entreprise n'a pas de sinistre pendant l'année considérée \fg.

On suppose que la probabilité de l'évènement $E$ est égale à $0,6$.

On considère la variable aléatoire $X$ qui, parmi les $15$ conducteurs choisis, comptabilise le
nombre de conducteurs n'ayant pas de sinistre pendant l'année considérée.

\medskip

\begin{enumerate}
\item Justifier que la variable aléatoire $X$ suit une loi binomiale dont on déterminera les paramètres.
\item Calculer la probabilité que, parmi les $15$ conducteurs choisis, $10$ conducteurs exactement n'aient pas de sinistre pendant l'année considérée.
\item Calculer la probabilité que, parmi les $15$ conducteurs choisis, $13$ conducteurs au moins n'aient pas de sinistre.
\item Calculer l'espérance mathématique de la variable aléatoire $X$ et interpréter le résultat obtenu.
\end{enumerate}
\bigskip

\textbf{Partie B - Étude du coût des sinistres}

\medskip

On considère la variable aléatoire $C$ qui, à chaque sinistre tiré au hasard parmi les sinistres survenus, associe son coût en euros.

On suppose que la variable aléatoire $C$ suit la loi normale d'espérance \np{1200} et d'écart type $200$.

\medskip

\begin{enumerate}
\item Déterminer $P(800 \leqslant C \leqslant \np{1600})$ à $10^{-2}$ près, puis interpréter cette valeur.
\item Calculer la probabilité qu'un sinistre tiré au hasard parmi les sinistres survenus coûte plus de \np{1500} euros.
\end{enumerate}

\bigskip

\textbf{Partie C - Nombre de sinistres pendant la première année de mise en service}

\medskip

Pour les véhicules de la flotte de cette entreprise, on a relevé le nombre de sinistres par véhicule pendant la première année de mise en service.

\smallskip

Pour les véhicules ayant eu au plus quatre sinistres, on a obtenu :

\begin{center}
\begin{tabularx}{\linewidth}{|l|*{5}{>{\centering \arraybackslash}X|}}\hline
Nombre de sinistres : $x_i$& 0			&1	&2		&3	&4\\ \hline
Nombre de véhicules: $n_i$ &\np{1345}	&508& 228 	&78 &35\\ \hline
\end{tabularx}
\end{center}

Le nuage de points $\left(x_i~;~n_i\right)$ suggère de procéder à un ajustement exponentiel. 

On pose donc $y = \ln n_i$.

\medskip

\begin{enumerate}
\item Compléter après l'avoir reproduit, le tableau suivant en arrondissant les valeurs à
$10^{-3}$.

\begin{center}
\begin{tabularx}{\linewidth}{|l|*{5}{>{\centering \arraybackslash}X|}}\hline
Nombre de sinistres : $x_i$	&0	&1	&2	&3	&4\\ \hline
$y = \ln n_i$				&	&	&	&	&\\ \hline
\end{tabularx}
\end{center}

\item Déterminer, à l'aide d'une calculatrice, une équation de la droite de régression de $y$ en $x$ sous la forme $y = ax + b$, où $a$ et $b$ sont à arrondir à $10^{-2}$.
\item Justifier que le nombre $n$ de véhicules ayant subi $x$ sinistres peut être modélisé par une égalité de la forme $n = A \times B^x$  où $A = \np{1326}$ à 1 près et $B = 0,4$ à $0,1$ près.
\item À l'aide de l'équation précédente, estimer le nombre de véhicules ayant eu six sinistres pendant leur première année de mise en circulation.
 
\end{enumerate} 
 
\vspace{0,5cm}

\textbf{Exercice 2\hfill 10 points}

\medskip

Pour un promoteur immobilier, le coût de production, en millions d'euros, pour $n$ villas construites, $0 \leqslant n \leqslant 10$, est modélisé par:

\[C(n) = 0,2n + 0,45\ln (8n+ 1).\]

 Ce promoteur vend chaque villa \np{600000}~\euro.

\bigskip

\textbf{Partie A}

\medskip

\begin{enumerate}
\item Calculer $C(4)$ puis $C(8)$, on arrondira les résultats à $0,01$ près.
\item Le coût de production est-il proportionnel au nombre de villas construites ?
Pourquoi ?
\item On appelle $R(n)$, la recette, en millions d'euros, générée par la vente de $n$ villas. Expliquer pourquoi $R(n) = 0,6n$.
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

On modélise le coût de production des villas et la recette générée par leur vente (en millions d'euros) par les fonctions $f$ et $g$ de la variable réelle $x$ définies sur l'intervalle [0~;~10] par :

\[f(x) = 0,2x + 0,45\ln (8x + 1)\quad  \text{et}\quad  g(x) = 0,6x.\]

Dans le graphique ci-dessous, on a représenté la courbe $\Gamma$ représentative de la fonction $f$,
ainsi que la droite $(D)$) représentative de la fonction $g$.

\begin{center}
\psset{unit=1cm}

\begin{pspicture}(-0.5,-0.5)(10,6)
\psgrid[gridlabels=0pt,subgriddiv=5,gridcolor=gray](0,0)(10,6)
\psaxes[linewidth=1.25pt,labelFontSize=\scriptstyle]{->}(0,0)(0,0)(10,6)
\psaxes[linewidth=1.25pt,labelFontSize=\scriptstyle](0,0)(0,0)(10,6)
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=red]{0}{10}{0.2 x mul x 8 mul 1 add ln 0.45 mul add}\uput[dr](9,3.7){\red $\Gamma$}
\psline[linewidth=1.25pt](10,6)\uput[ul](9,5.4){$D$}
\end{pspicture}
\end{center}

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Par lecture graphique, déterminer le coût de production pour la construction de 2 villas ainsi que la recette générée par la vente de 2 villas.
		\item Déterminer si le bénéfice obtenu pour la construction et la vente de 2 villas est positif. Expliquer.
	\end{enumerate}
\item Déterminer graphiquement le nombre minimum de villas qu'il faut construire et vendre pour avoir un bénéfice positif.
\end{enumerate}

\bigskip

\textbf{Partie C}

\medskip

Dans cette partie on suppose que le promoteur a construit et vendu au moins une villa.

\medskip

\begin{enumerate}
\item Montrer que le bénéfice réalisé pour la construction et la vente de $n$ villas est, en millions
d'euros:

\[B(n) = 0,4n - 0,45\ln (8n + 1).\]

\item On considère la fonction $h$, définie sur l'intervalle [1~;~10] par:

\[h(x) = 0,4x - 0,45\ln (8x + 1).\]

	\begin{enumerate}
		\item On note $h'$ la fonction dérivée de $h$. Démontrer que pour tout $x$ appartenant à
l'intervalle [1; 10],
 \[h'(x)= \dfrac{3,2(x -1)}{8x+1}.\]
 
		\item Étudier le signe de $h'(x)$ pour tout $x$ appartenant à l'intervalle [1~;~10].
		\item Établir le tableau de variation de $h$ sur l'intervalle [1~;~10].
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Démontrer que l'équation $h(x) = 0$ admet une unique solution $\alpha$ appartenant à l'intervalle ]1~;~10[.
		\item Donner une valeur approchée à 1 près de $\alpha$.
		\item À partir de combien de villas vendues, le promoteur sera t-il bénéficiaire ?
	\end{enumerate}
\end{enumerate}
\end{document}