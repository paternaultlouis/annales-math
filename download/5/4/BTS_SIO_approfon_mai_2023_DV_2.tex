\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} 
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo}
%Tapuscrit : Denis Vergès
%Relecture : François Hache
\usepackage{pst-plot,pst-text,pst-node,pst-tree,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\newcommand{\barre}[1]{\overline{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS},
pdftitle = {Métropole 17 mai 2023},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[french]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur Métropole}
\lfoot{\small{Services informatiques aux organisations\\Mathématiques approfondies}}
\rfoot{\small{17 mai 2023}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~BTS Métropole 17 mai 2023~\decofourright\\[5pt]Services informatiques aux organisations}}

\medskip

\textbf{Mathématiques approfondies}

\vspace{0,25cm}

\textbf{Seuls les points supérieurs à \boldmath$10$\unboldmath sont pris en compte}

\medskip

\textbf{L'usage de calculatrice avec mode examen actif est autorisé} 

\textbf{L'usage de calculatrice sans mémoire \og type collège \fg{} est autorisé}

\end{center}

\smallskip

\textbf{Exercice 1 :  \hfill 10 points}

\medskip

\textbf{Les parties A et B sont indépendantes}

\medskip

\textbf{Partie A}

\medskip

En 2022, une usine a assemblé \np{300000} ordinateurs. L'entreprise souhaite optimiser les lignes de production dans les années futures et prévoit d'augmenter le nombre d'ordinateurs assemblés de 2\,\% par an.

On modélise la production annuelle de l'usine par une suite $\left(P_n\right)$ telle que pour tout entier naturel $n,\: P_n$ représente le nombre d'ordinateurs assemblés exprimé en milliers, au cours de l'année $2022 + n$. Ainsi $P_0 = 300$.
\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item  Calculer $P_1$ puis interpréter ce résultat dans le contexte de l'exercice.
		\item Pour tout entier naturel $n$, exprimer $P_{n+1}$ en fonction de $P_n$.
		\item Déterminer la nature de la suite $\left(P_n\right)$.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item Pour tout entier naturel $n$, exprimer $P_n$ en fonction de $n$.
		\item En déduire, à l'unité près, le nombre d'ordinateurs assemblés en 2030 selon
ce modèle.
	\end{enumerate}

\item Déterminer le plus petit entier naturel $n$ pour lequel $P_n > 400$.

Interpréter ce résultat dans le contexte de l'exercice.
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

On s'intéresse plus précisément à l'une des lignes d'assemblage de l'usine. 

Cette ligne permet d'assembler entre \np{20000} et \np{40000} ordinateurs par an.

On admet que si cette ligne d'assemblage permet de produire $x$ milliers d'ordinateurs par an, le bénéfice associé, exprimé en milliers d'euros, est modélisé par la fonction $f$ définie sur l'intervalle [20~;~40] par:
\[f(x) = (45 - x)\text{e}^{0,1x} - 10.\]

On note $f'$ la fonction dérivée de la fonction $f$.

\medskip

\begin{enumerate}
\item Recopier et compléter le tableau suivant en arrondissant les résultats au centième:

\begin{center}
\begin{tabularx}{\linewidth}{|*{6}{>{\centering \arraybackslash}X|}}\hline
$x$		&20	&25	&30	&35	&40\\ \hline
$f(x)$	&	&	&	&	&\\ \hline
\end{tabularx}
\end{center}
\item
	\begin{enumerate}
		\item Montrer que pour tout $x$ appartenant à [20~;~ 40],\:$f'(x) = (-0,1x + 3,5)\text{e}^{0,1x}$.
		\item Étudier le signe de $f'(x)$ sur l'intervalle [20~;~40].
		\item En déduire le tableau de variation de la fonction $f$ sur ce même intervalle. 
		
		Les valeurs des images seront arrondies au centième.
	\end{enumerate}	

\item Déterminer le nombre d'ordinateurs que la ligne d'assemblage doit fabriquer par an afin d'obtenir un bénéfice associé maximal.

Donner, à la dizaine d'euros près, la valeur de ce bénéfice maximal.
\item On considère la fonction $F$ définie sur [20~;~40] par 
\[F(x) = (550 - 10x)\text{e}^{0,1x} - 10x.\]

On admet que $F$ est une primitive de la fonction $f$ sur l'intervalle [20~;~40].
	\begin{enumerate}
		\item Montrer que la valeur exacte de $\displaystyle\int_{30}^{40} f(x)\: \text{d}x$ est $150\text{e}^4 - 250\text{e}^3 - 100$.
		\item En déduire une valeur approchée à la dizaine d'euros du bénéfice moyen réalisé lorsque la ligne d'assemblage produit entre \np{30000} et \np{40000} ordinateurs.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Exercice 2 :  \hfill 10 points}

\medskip

\textbf{Partie A}

\medskip

Le tableau suivant, où $x_i$ désigne le rang de l'année mesuré à partir de l'année 2015, donne le nombre $y_i$ d'appareils connectés, exprimé en milliards, dans le monde entre 2015 et 2021.

\begin{center}
\begin{tabularx}{\linewidth}{|m{3.5cm}|*{7}{>{\centering \arraybackslash}X|}}\hline
Année						&2015 	&2016 &2017 &2018	&2019&2020&2021\\ \hline
$x_i$ : rang de l'année 	&0 		&1 	  &2 	&3		&4& 5& 6\\ \hline
$y_i$ : nombre d'appareils
 (en milliards)				&15,4 	&17,7 &20,4 &23,1&26,7 &30,7 &35,8\\ \hline
\end{tabularx}
\end{center}

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer le coefficient de corrélation linéaire $r$ de la série statistique $\left(x_i~;~ y_i\right)$.  Arrondir le résultat au centième.
		\item Expliquer pourquoi le résultat obtenu permet d'envisager un ajustement affine.
	\end{enumerate}	
\item Déterminer, à l'aide d'une calculatrice, une équation de la droite de régression de $y$ en $x$, sous la forme $y = ax +b$. Les coefficients $a$ et $b$ seront arrondis au dixième.
\item À l'aide de l'équation de la droite de régression trouvée précédemment, estimer le
nombre d'appareils qui seront connectés en 2023.

\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

Un fabricant commercialise des montres connectées.

Les batteries utilisées pour fabriquer ces montres proviennent de deux fournisseurs différents, notés A et B. 

75\,\% des batteries du stock du fabricant proviennent du fournisseur A, les autres proviennent du fournisseur B.

Le fabricant remarque des défauts de charge parmi les batteries de son stock. 

Après analyse, il constate que 1,2\,\% des batteries provenant du fournisseur A et 2\,\% de celles provenant du fournisseur B sont défectueuses.

On prélève au hasard une batterie dans le stock du fabricant.

On considère les évènements suivants :

\begin{description}
\item[ ] $A$ : \og la batterie prélevée provient du fournisseur A \fg{}; 
\item[ ] $B$ : \og la batterie prélevée provient du fournisseur B \fg{} ;
\item[ ] $D$ : \og la batterie prélevée est défectueuse \fg.
\end{description}

On note $\overline{D}$ l'évènement contraire de l'évènement $D$.

\medskip

\begin{enumerate}
\item Modéliser la situation par un arbre pondéré.
\item
	\begin{enumerate}
		\item Calculer la probabilité de l'évènement $B \cap D$.
		\item Interpréter le résultat obtenu par une phrase.
	\end{enumerate}	
\item Montrer que la probabilité que la batterie prélevée soit défectueuse est égale à $0,014$.
\item Sachant que la batterie prélevée est défectueuse, déterminer la probabilité que celle-ci provienne du fournisseur B. 

On arrondira le résultat au millième.
\item Le fabricant prélève au hasard $50$ batteries de son stock. 

Le stock est suffisamment important pour assimiler ce prélèvement à un tirage avec remise. 

On note $X$ la variable aléatoire qui, à chaque lot de $50$ batteries, associe le nombre de batteries défectueuses.
	\begin{enumerate}
		\item Sans justifier, préciser la loi suivie par la variable aléatoire $X$ ainsi que les paramètres de cette loi.
		\item Calculer la probabilité qu'au moins une batterie soit défectueuse. Arrondir le résultat au millième.
	\end{enumerate}
\end{enumerate}
\end{document}