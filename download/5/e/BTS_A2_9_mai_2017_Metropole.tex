\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx,mathrsfs}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{multirow}
\usepackage{textcomp}
\usepackage{lscape} 
\newcommand{\euro}{\eurologo{}}
\usepackage{graphicx}
%Tapuscrit : Denis Vergès
\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usetikzlibrary{hobby}
\usepackage{tkz-tab}
\usepackage{pstricks,pst-plot,pst-bezier,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
%echelon unité
\newcommand{\U}{\mathscr{U}}
\usepackage{pst-bezier,pst-tree,pstricks-add}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{fancyhdr}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS groupement A2},
pdftitle = {Métropole - 9 mai 2017},
allbordercolors = white,
pdfstartview=FitH} 
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
%signe inferieur ou egal francais
\renewcommand{\leq}{\leqslant}
%signe superieur ou egal francais
\renewcommand{\geq}{\geqslant}
\begin{document}
\setlength\parindent{0mm}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}
\rhead{\textbf{A. P{}. M. E. P.{}}}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Électrotechnique\\Systèmes photoniques}}
\rfoot{\small{9 mai 2017\\V. X. Jumel}}

\pagestyle{fancy}
\thispagestyle{empty}

\begin{center}{\Large\textbf{Brevet de technicien supérieur 9 mai 2017\\[5pt] groupement A2}}
  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 11 points}

\medskip

Les 4 parties de cet exercice peuvent être traitées de façon indépendante.
Dans cet exercice on s'intéresse à l'évolution, en fonction du temps, de la vitesse de
rotation d'un moteur à courant continu.

\bigskip

\textbf{Partie A: Étude de la vitesse de rotation du moteur lors de son démarrage}

\medskip

Dans un premier temps, le moteur à courant continu utilisé n'est soumis à aucune charge
mécanique. La vitesse de rotation de ce moteur, exprimée en tour par seconde (tour/s), est
notée $\omega$. Elle dépend du temps $t$ , exprimé en seconde (s), écoulé depuis le démarrage du
moteur.

La courbe ci-dessous représente l'évolution de cette vitesse en fonction du temps.

\begin{center}
\psset{xunit=2cm,yunit=0.5cm,comma=true}
\begin{pspicture}(-0.5,-3)(5.1,18)
\psgrid[gridlabels=0pt,subgriddiv=2](0,0)(5.1,18)
\psaxes[linewidth=1pt,Dx=0.5,Dy=5](0,0)(0,0)(5,18)
\psaxes[linewidth=1pt,Dx=0.5,Dy=5](0,0)(0,0)(5,18)
\psaxes[linewidth=1.5pt,Dx=0.5,Dy=5]{->}(0,0)(1,1)
\uput[r](0,17.5){vitesse en  tours par seconde}
\uput[u](4.25,0){temps en secondes}
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=blue]{0}{5}{15 30 x mul 15 add 2.71828 2 x mul exp div sub}
\end{pspicture}
\end{center}

\begin{enumerate}
\item Répondre aux questions suivantes à l'aide de la représentation graphique ci-dessus.
	\begin{enumerate}
		\item Quelle est la vitesse de rotation du moteur à l'instant t = 0 ?
		\item Quelle est la vitesse de rotation du moteur une seconde après le démarrage?
		\item Vers quelle valeur $\omega_{\text{S}}$ semble se stabiliser la vitesse de rotation du moteur ?
		\item Avec la précision permise par le graphique, déterminer au bout de combien de temps on
atteint 95\,\% de la vitesse stabilisée. Expliquer.
	\end{enumerate}
\item On admet que, dans les conditions de fonctionnement étudiées dans la partie A, la
vitesse de rotation du moteur est modélisée par la fonction $\omega$ définie pour $t \geqslant 0$ par :

\[\omega(t) = 15 - (30 t + 15)\text{e}^{- 2t}\]

	\begin{enumerate}
		\item On note $\omega'$ la fonction dérivée de $\omega$. Justifier que pour $t \geqslant 0 $:\: $\omega'(t) = 60 t \text{e}^{- 2 t}$.
		\item En déduire le sens de variation de la fonction $\omega$ sur $[0~;~+ \infty[$.
		\item Calculer $\omega'(0)$. Donner une interprétation graphique du résultat.
	\end{enumerate}
\end{enumerate}

\begin{center}\textbf{Le formulaire ci-dessous peut être utilisé pour les parties B et C de l'exercice}
\end{center}

\begin{tabularx}{\linewidth}{|p{6cm}|X|}\hline
\textbf{Équation différentielle sans second membre}& \textbf{Solutions sur $\R$}\\ \hline
$a y''+ by' + cy = 0$ avec $a$, $b$ et $c$ des constantes réelles.&$\bullet~~$Si $\Delta > 0$ :\: $t \longmapsto A \text{e}^{r_1 t}  + B \text{e}^{r_2 t}$, avec $A,\: B$ constantes réelles et $r_1$,\: $r_2$ les solutions de l'équation caractéristique.\\
Équation caractéristique : $ar^2 + br + c = 0$ de discriminant $\Delta$.&$\bullet~~$
Si $\Delta = 0$ :\: $t \longmapsto (At + B)\text{e}^{rt}$, avec $A$,\:$B$
 constantes réelles et $r$ la solution de l'équation caractéristique.\\
&$\bullet~~$Si $\Delta < 0$ : $t \longmapsto \text{e}^{\alpha t} [A \cos (\beta t) + B \sin(\beta t)$,
avec $A$,\: $B$ constantes réelles et $\alpha + \text{i} \beta$ et $\alpha - \text{i}\beta$ les
solutions de l'équation caractéristique.\\ \hline
\end{tabularx}

\bigskip

\textbf{Partie B : Résolution d'une équation différentielle permettant d'obtenir la vitesse de
rotation}

\medskip

Sous certaines conditions de charge, la vitesse de rotation d'un moteur à courant continu
soumis à une tension constante U, exprimée en Volt (V), est solution de l'équation
différentielle 

\[(E) :\quad  \dfrac{1}{4}y'' + y' + y = \dfrac{\text{U}}{k},\:\text{où $k$ est une valeur caractéristique du moteur.}\]

\begin{enumerate}
\item On note $\left(E_0\right)$ l'équation homogène associée à $(E)$. On a donc :

\[\left(E_0\right) :\quad  \dfrac{1}{4}y'' + y' + y  = 0.\]

Déterminer les solutions de l'équation différentielle $\left(E_0\right)$.
\item Vérifier que la fonction constante $g$: \:  $t \longmapsto \dfrac{\text{U}}{k}$  est une solution de l'équation différentielle $(E)$.
\item En déduire les solutions de l'équation différentielle de $(E)$.
\item En prenant $k = \dfrac{2}{3}$ et $\text{U} = 10$~V montrer que la fonction $\omega$ donnée dans la question A. 2. est la solution de l'équation différentielle $(E)$ vérifiant les conditions initiales $y(0) = 0$ et $y'(0) = 0$.
\end{enumerate}

\bigskip

\textbf{Partie C : Détermination de la vitesse de rotation d'un moteur à courant continu à
partir des principes de la physique}

\medskip

D'une manière plus générale on démontre que la vitesse de rotation du moteur alimenté par
une tension continue $U$ vérifie l'équation différentielle 

\[\left(E_1 \right) :\quad  \alpha^2 y'' + 2m\alpha y' + y = \dfrac{\text{U}}{k},\]

où $\alpha$, $m$ et $k$ sont des paramètres strictement positifs dépendant des caractéristiques
physiques du moteur étudié (résistance, inductance, moment d'inertie).

\smallskip

Dans cette partie on prend : $\text{U} = 10$ V ; $\alpha = 0,3$ ;\: $m = 0,6$ et $k = \dfrac{2}{3}$.

L'équation différentielle $\left(E_1 \right)$ s'écrit donc : 

\[0,09 y'' + 0,36 y' + y = 15.\]

\begin{enumerate}
\item Résoudre dans $\C$ l'équation : $0,09 z^2 + 0,36 z + 1 = 0$.
\item Parmi les quatre fonctions proposées ci-dessous, une seule est solution de l'équation
différentielle $\left(E_1 \right)$ et vérifie les conditions initiales $y(0) = 0$ et $y'(0) = 0$.

Quelle est cette fonction ? Justifier la réponse.

\begin{center}
\renewcommand\arraystretch{1.8}
\begin{tabularx}{0.75\linewidth}{l X}
\textbf{Fonction 1} :& $t \longmapsto 15\left[1- \text{e}^{ - \frac{8}{3}t}\left(\cos (2t)+ \frac{3}{4}\sin(2t)\right)\right]$\\
\textbf{Fonction 2} :& $t \longmapsto 15\left[1- \text{e}^{-2t}\left(\cos \left(\frac{8}{3}t\right)+ \frac{3}{4} \sin \left(\frac{8}{3}t\right)\right)\right]$\\
\textbf{Fonction 3} :& $t \longmapsto 15\text{e}^{\frac{2}{3}t} - 15\text{e}^{-\frac{14}{3}t}$\\
\textbf{Fonction 4} :& $t \longmapsto 15 - \text{e}^{-2t}\left[\cos \left(\frac{8}{3}t\right) + \frac{3}{4}\sin \left( \frac{8}{3}t\right)  \right]$
\end{tabularx}
\renewcommand\arraystretch{1}
\end{center}

\item La solution de l'équation différentielle $\left(E_1 \right)$ qui vérifie les conditions initiales $y(0) = 0$ et 

$y'(0) = 0$ modélise l'évolution de la vitesse du moteur en fonction du temps dans les conditions étudiées dans la partie C. Elle est représentée ci-dessous.

\begin{center}
\psset{xunit=2cm,yunit=0.5cm,comma=true}
\begin{pspicture}(-0.5,-3)(5.1,18)
\psgrid[gridlabels=0pt,subgriddiv=2](0,0)(5.1,18)
\psaxes[linewidth=1pt,Dx=0.5,Dy=5](0,0)(0,0)(5,18)
\psaxes[linewidth=1pt,Dx=0.5,Dy=5](0,0)(0,0)(5,18)
\psaxes[linewidth=1.5pt,Dx=0.5,Dy=5]{->}(0,0)(1,1)
\uput[r](0,17.5){vitesse en  tours par seconde}
\uput[u](4.25,0){temps en secondes}
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=blue]{0}{5}{15 1 8 x mul 3 div 180 mul 3.14159 div cos 8 x mul 3 div 180 mul 3.14159 div sin 3 mul 4 div add 2.71828 2 x mul exp div  sub mul}
\end{pspicture}
\end{center}

D'après cette modélisation, quelle est la vitesse maximale du moteur ?

À quel moment, environ, est-elle atteinte ?
\end{enumerate}

\bigskip

\textbf{Partie D : Comportement d'un moteur soumis à différents sauts de tension}

\medskip

Une boucle de régulation de vitesse permet à présent de faire fonctionner le moteur à
différentes vitesses. La tension d'entrée vaut successivement 10~V, 30~V puis 20~V. La
vitesse de rotation du moteur est alors analysée et illustrée par le graphique ci-dessous.

\begin{center}
\psset{xunit=0.8cm,yunit=0.2cm,comma=true}
\begin{pspicture}(-0.5,-3)(13,55)
%\psgrid[gridlabels=0pt,subgriddiv=1](0,0)(13,55)
\multido{\n=0+1}{14}{\psline[linewidth=0.3pt](\n,0)(\n,55)}
\multido{\n=0+5}{12}{\psline[linewidth=0.3pt](0,\n)(13,\n)}
\psaxes[linewidth=1.25pt,Dy=5]{->}(0,0)(0,0)(13,55)
\psaxes[linewidth=1.25pt,Dy=5](0,0)(0,0)(13,55)
\uput[r](0,53){vitesse en  tours par seconde}
\uput[u](11.5,0){temps en secondes}           
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=blue]{0}{3}{15 1 8 x mul 3 div 180 mul 3.14159 div cos 8 x mul 3 div 180 mul 3.14159 div sin 3 mul 4 div add 2.71828 2 x mul exp div  sub mul}
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=blue]{3}{7}{30 1 8 x 3 sub mul 3 div 180 mul 3.14159 div cos 8 x 3 sub mul 3 div 180 mul 3.14159 div sin 3 mul 4 div add 2.71828 2 x 3 sub mul exp div  sub mul 15 add}
\psplot[plotpoints=3000,linewidth=1.25pt,linecolor=blue]{7}{13}{45 15 1 2.71828 x 7 sub 2 mul neg exp 8 3 div x 7 sub mul 180 mul 3.14159 div cos x 7 sub 8 mul 3 div 180 mul 3.14159 div sin 0.75 mul   add mul sub mul sub}
\end{pspicture}
\end{center}

\begin{enumerate}
\item Déterminer à l'aide du graphique les trois instants où les tensions ont été modifiées. On
ne demande pas de justification.
\item Représenter sur le document réponse (page 9) la tension d'entrée $e$, exprimée en Volt,
appliquée aux bornes du moteur en fonction du temps $t$, exprimé en seconde.
\item On désigne par $\mathcal{U}$ la fonction causale unité. On rappelle que:

\[\mathcal{U}(t) = 0\:\:  \text{ si }\:t < 0 \:\:\text{ et } \mathcal{U}(t) = 1\: \text{ sinon}.\]

Pour exprimer la tension d'entrée $e(t)$ appliquée aux bornes du moteur à l'instant $t$ un
étudiant propose l'expression $e(t) = 10 \mathcal{U}(t) + 30 \mathcal{U}(t - 3) - 20 \mathcal{U}(t - 7)$ et remplit le tableau donné sur le document réponse.
	\begin{enumerate}
		\item Compléter, sur le document réponse, le tableau rempli par l'étudiant.
		\item Une fois qu'il a terminé de remplir le tableau, l'étudiant se rend compte qu'il a donné une expression inexacte de $e(t)$. Expliquer pourquoi.
		\item Donner l'expression exacte de $e(t)$. On n'attend pas de justification.
	\end{enumerate} 
\end{enumerate}

\newpage

\textbf{Exercice 2 \hfill 9 points}

\medskip

\begin{center}\textbf{Les deux parties suivantes sont indépendantes. Elles peuvent être traitées dans n'importe quel ordre}
\end{center}

\medskip

\textbf{PARTIE A :}

\medskip

On appelle $f$ la fonction définie sur $\R$, paire, périodique de période $\pi$, vérifiant :

\[f(t) = t \sin(t)\quad  \text{pour }\: t \in  \left[0~;~\dfrac{\pi}{2}\right].\]

\begin{enumerate}
\item Parmi les quatre courbes suivantes quelle est celle qui représente la fonction $f$ ? On
n'attend pas de justification.
    \begin{center}
      \begin{tikzpicture}[scale = 0.5,>=latex]
        \begin{scope}[xshift=-7cm]
          \node at (-4,2.6) [anchor = south west ] {Courbe 1} ;
          \draw [->,thick] (-4,0) -- (6.5,0) ;
          \draw [->,thick] (0,-2.5) -- (0,2.5) ;

          \draw [thin,gray] (-4,-2.5) grid [xstep = 1.57] (6.5,2.5) ;

          \draw [blue,thick,smooth,samples=750] plot [domain=-4:6.5] (\x,
            {2*cos(2*\x r)} ) ;
            \draw(1.56,0) node[below]{$\frac{\pi}{2}$};
             \draw(0,0) node[below]{\footnotesize $0$};
           \draw(0,1) node[left]{\footnotesize $1$};
        \end{scope}
        \begin{scope}[xshift=7cm]
          \node at (-3.14,3.2) [anchor = south west ] {Courbe 2} ;
          \draw [->,thick] (-3.14,0) -- (6.5,0) ;
          \draw [->,thick] (0,-1.7) -- (0,3.2) ;

          \draw [thin,gray] (-3.14,-1.7) grid [xstep = 1.57] (6.5,3.2) ;
          \draw [blue,thick,smooth] plot [domain=0:3.14] (\x,
            {\x*\x*sin(\x r)/2.2 } ) ;
          \draw [blue,thick,smooth] plot [domain=0:3.14] (\x+3.14,
            {\x*\x*sin(\x r)/2.2 } ) ;
          \draw [blue,thick,smooth] plot [domain=0:3.14] (\x-3.14,
            {\x*\x*sin(\x r)/2.2 } ) ;
           \draw(1.56,0) node[below]{$\frac{\pi}{2}$};
            \draw(0,0) node[below]{\footnotesize $0$};
           \draw(0,1) node[left]{\footnotesize $1$};
        \end{scope}
        \begin{scope}[xshift=-7cm,yshift=-7cm,scale=1.2]
          \node at (-3.14,2.5) [anchor = south west ] {Courbe 3} ;
          \draw [->,thick] (-3.14,0) -- (6.5,0) ;
          \draw [->,thick] (0,-2.5) -- (0,2.5) ;

           \draw [thin,gray] (-4,-2.5) grid [xstep = 1.57] (6.5,2.5) ;

           \draw [blue,thick,smooth] plot [domain=0:1.57] (\x,
             {\x*sin(\x r) } ) ;
           \draw [blue,thick,smooth] plot [domain=0:1.57] (\x-3.14,
             {\x*sin(\x r) } ) ;
           \draw [blue,thick,smooth] plot [domain=0:1.57] (\x+3.14,
             {\x*sin(\x r) } ) ;
           \draw [blue,thick,smooth] plot [domain=0:1.57] (-\x,
             {\x*sin(\x r) } ) ;
           \draw [blue,thick,smooth] plot [domain=0:1.57] (-\x+6.28,
             {\x*sin(\x r) } ) ;
           \draw [blue,thick,smooth] plot [domain=0:1.57] (-\x+3.14,
             {\x*sin(\x r) } ) ;
           \draw(1.56,0) node[below]{$\frac{\pi}{2}$};
            \draw(0,0) node[below]{\footnotesize $0$};
           \draw(0,1) node[left]{\footnotesize $1$};
        \end{scope}
        \begin{scope}[xshift=7cm,yshift=-7cm]
          \node at (-5,4.2) [anchor = south west ] {Courbe 4} ;
          \draw [->,thick] (-5,0) -- (5,0) ;
          \draw [->,thick] (0,-2.9) -- (0,4.1) ;

          \draw [thin,gray] (-5,-2.5) grid [xstep = 1.57] (5,4.1) ;

          \draw [blue,thick,smooth] plot [domain=0:5] (\x, {
            \x*sin(2*\x r) } ) ;
          \draw [blue,thick,smooth] plot [domain=0:5] (-\x, {
            \x*sin(2*\x r) } ) ;
           \draw(1.56,0) node[below]{$\frac{\pi}{2}$};
            \draw(0,0) node[below]{\footnotesize $0$};
           \draw(0,1) node[left]{\footnotesize $1$};
        \end{scope}
      \end{tikzpicture}
    \end{center}
\item On admet que la fonction $f$ est développable en série de Fourier.

On note $S$ son développement en série de Fourier.

On rappelle que :

\begin{center}
\begin{tabularx}{\linewidth}{|X|}\hline
$S(t) = a_0 + \displaystyle\sum_{n=1}^{+ \infty} \left(a_n \cos(n\omega t) + b_n \sin(n \omega t)\right)$, avec $\omega = \dfrac{2\pi}{T}$,\: $T$ période de $f$ ;\\
$a_0 = \dfrac{1}{T}\displaystyle\int_a^{a + T}  f(t)\:\text{d}t$. Pour $n \geqslant 1$ : 

$a_n = \dfrac{2}{T}\displaystyle\int_a^{a + T}  f(t)\cos (n \omega t)\:\text{d}t$ et 
$b_n = \dfrac{2}{T}\displaystyle\int_a^{a + T}  f(t)\sin (n \omega t)\:\text{d}t$\\
avec $a$ constante réelle quelconque.\\ \hline
\end{tabularx}
\end{center}

\begin{enumerate}
      \item Justifier que $b_n = 0$ pour tout $n$ entier naturel supérieur
        ou égal à 1.
      \item Montrer que la fonction $g$ définie pour tout réel $t$ par
        $g(t) = -t\cos(t) + \sin(t)$ est une primitive de la fonction
        définie sur $\R$ par $t\mapsto t\sin t$.
      \item La fonction étant paire et de période $\pi$, $a_0$ vérifie
        $a_0 = \frac2{\pi} \displaystyle\int_0^{\frac{\pi}2} f(t) \mathrm{d}t$.

        Vérifier que $a_0 = \frac2{\pi}$. Écrire les étapes du calcul
        effectué.
\end{enumerate}
\item On admet que pour tout entier naturel $n \geqslant 1$ : $a_n =
    \frac2{\pi}(-1)^n \left( \frac1{(2n+1)^2} + \frac1{(2n-1)^2}\right)$

Donner les valeurs de $a_1$ et $a_2$ arrondies au millième.
\item On note $f_e$ le nombre positif vérifiant $f_e^2 = \dfrac1{\pi}
    \displaystyle\int_0^{\pi} f^2(t) \mathrm{d}t$.

On admet que l'expression $a_0^2 + \frac12\displaystyle\sum_{n=1}^2\left(a_n^2 + b_n^2\right)$, obtenue d'après la formule de Parseval, permet d'obtenir la valeur approchée de $f_e^2$ au millième.
    \begin{enumerate}
		\item Calculer la valeur approchée de $f_e$ au millième.
		\item Si $f$ modélise un signal de période $\pi$, que représente
 $f_e$ ?
    \end{enumerate}
\end{enumerate}

\bigskip

\textbf{PARTIE B :}

\medskip

Une entreprise produit en grande série des axes de rotation pour un
modèle de moteur.

Ces axes sont fabriqués par dix machines identiques fonctionnant de
manière indépendante et toujours ensemble pendant une période appelée
\emph{cycle de fabrication}.

Le service de maintenance annonce que la probabilité qu'une machine
tombe en panne pendant un cycle de fabrication est de 0,02.

\medskip

\begin{enumerate}
  \item On admet que la variable aléatoire $X$ qui à chaque cycle de
    fabrication associe le nombre de machines tombant en panne pendant
    ce cycle suit une loi binomiale.

\begin{minipage}{0.45\textwidth}
\begin{enumerate}
\item Quels sont les paramètres $n$ et $p$ de cette loi
          binomiale ?

          En utilisant le graphique ci-contre, qui représente la loi de
          probabilité de la variable aléatoire $X$ :
\item indiquer quelle est la probabilité $p_1$ qu'aucune machine
          ne tombe en panne pendant un cycle de fabrication ;
\item déterminer quelle est la probabilité $p_2$ qu'au moins
          deux machines tombent en panne pendant un cycle de
          fabrication.
\end{enumerate}
\end{minipage}
\begin{minipage}{0.53\textwidth}
\begin{center}
%\begin{tikzpicture}[xscale=0.7,yscale=7]
%\draw (-1.5,0) -- (7,0) ;
%\draw (-1.5,0) -- (-1.5,0.9) ;
%
%\draw [fill=black] (-0.1,0) rectangle (0.1,0.817) ;
%\draw [fill=black] (0.9,0) rectangle (1.1,0.167) ;
%\draw [fill=black] (1.9,0) rectangle (2.1,0.015) ;
%\draw [fill=black] (2.9,0) rectangle (3.1,0.001) ;
%\draw [fill=black] (3.9,0) rectangle (4.1,0.001) ;
%\draw [fill=black] (4.9,0) rectangle (5.1,0.001) ;
%\draw [fill=black] (5.9,0) rectangle (6.1,0.001) ;
%
%%\foreach \i in {0,{0.1},{0.2},{0.3},{0.4},{0.5},{0.5},{0.6},{0.7},{0.8},{0.9}}
%%{ \draw (-1.6,\i) node [left] { \small \np{\i} } -- (-1.5,\i) ;
%%} ;
%\draw(-1.6,0.1) \node[left] {\np{0.1}} ;
%\foreach \i in {0,...,6} { \node at (\i, 0) [below] { \small
%  \i } ; } ;
%\foreach \i/\j in
%{0/0.817,1/0.167,2/0.015,3/0.001,4/0.000,5/0.000,6/0.000}
%{ \node at (\i,\j) [above] {\scriptsize \np{\j}};
%};
%\node at (3,1) { \footnotesize \textbf{Probabilité d'avoir $k$ succès (à
%  $10^{-3}$ près)} } ;
%\node at (3,-0.1) { \small \textbf{Nombre de succès} };
%\end{tikzpicture}
\psset{xunit=0.6cm,yunit=6cm,comma=true}
\begin{pspicture}(0,-0.15)(9,1)
\psaxes[linewidth=1.25pt,Dx=10,Dy=0.1](0,0)(0,0)(9,1)
\multido{\n=2+1,\na=0+1}{7}{\uput[d](\n,0){\na}}
\psline[linewidth=4pt](2,0)(2,0.817)\uput[u](2,0.817){\footnotesize$0,817$}
\psline[linewidth=4pt](3,0)(3,0.167)\uput[u](3,0.167){\footnotesize 0,167}
\psline[linewidth=4pt](4,0)(4,0.015)\uput[u](4,0.015){\footnotesize 0,015}
\psline[linewidth=4pt](5,0)(5,0.001)\uput[u](5,0.001){\footnotesize 0,001}
\psline[linewidth=4pt](6,0)(6,0.000)\uput[u](6,-0.01){\footnotesize 0,000}
\psline[linewidth=4pt](7,0)(7,0.000)\uput[u](7,-0.02){\footnotesize 0,000}
\rput(4.5,0.95){\footnotesize \textbf{Probabilité d'avoir $k$ succès (à $10^{-3}$ près)}}
\rput(4.5,-0.1){\footnotesize \textbf{Nombre de succès}}
\end{pspicture}
\end{center}
\end{minipage}

\item L'entreprise met en place un contrôle de qualité portant sur le
diamètre des axes de rotation fabriqués. On appelle
$\mathrm{L_{C_1}}$ et $\mathrm{L_{C_2}}$ les \emph{limites de
contrôle}.

On admet que $\mathrm{L_{C_1}} = 14,4$~mm et $\mathrm{L_{C_2}}
= 15,9$~mm.

On note $D$ la variable aléatoire qui, à tout axe de rotation
prélevé au hasard dans la production totale des dix machines pendant
un cycle de fabrication, associe son diamètre exprimé en
millimètres.

On admet que $D$ suit une loi normale de moyenne $\mu = \np{15.20}$
et d'écart-type $\sigma = \np{0.3}$.

Un logiciel donne les résultats représentés ci-dessous :

\begin{center}
 \begin{tikzpicture}[xscale=5,yscale=3]
\draw plot [smooth,domain = 14.2:16.2] (\x, {
 1/sqrt(2*3.14*0.09)*exp(-(\x - 15.2)^2/0.09/2) } ) ;
\draw [->] (14,0) -- (16.35,0) ;
\filldraw [fill,black] (14.2,0) -- plot [domain = 14.2:14.4] (\x, {
1/sqrt(2*3.14*0.09)*exp(-(\x -15.2)^2/0.09/2) } ) -- (14.4,0) --
cycle ;
\filldraw [fill,black] (15.9,0) -- plot [domain = 15.9:16.2] (\x, {
1/sqrt(2*3.14*0.09)*exp(-(\x -15.2)^2/0.09/2) } ) -- (16.2,0) --
cycle ;
\node at (16.05,0.4) [above] {\small Zone 2} ;
\node at (16.05,0.2) [above] {\small \textit{proba}$(D
\geqslant 15,9) \approx \np{0,0098}$ } ;
\node at (14.35,0.4) [above] {\small Zone 1} ;
\node at (14.35,0.2) [above] {\small \textit{proba}$(D
\leqslant 14,4) \approx \np{0,0038}} $ } ;
\foreach \i in {14,14.2,14.4,14.6,14.8,15,15.2,15.4,15.6,15.8,16,16.2}
{ \draw (\i,0) -- (\i,-0.05) node [below] {\small \np{\i}} ;
};
\end{tikzpicture}
\end{center}
	\begin{enumerate}
		\item Déterminer à $10^{-4}$ près la probabilité que le diamètre
        d'un axe pris au hasard dans la production soit compris entre
        les limites de contrôle.
		\item Déterminer un intervalle $[\mathrm{L_{S_1}} ;
        \mathrm{L_{S_2}}]$ centré en $\mu = 15,20$ et tel que la
        probabilité que la variable $D$ prenne une valeur entre
        $\mathrm{L_{S_1}}$ et $\mathrm{L_{S_2}}$ soit environ égale à
        \np{0,95}. Les nombres $\mathrm{L_{S_1}}$ et $\mathrm{L_{S_2}}$
        s'appellent les \emph{limites de surveillance}.
	\end{enumerate}
\item Au cours d'un cycle de fabrication la procédure de contrôle
    suivante est mise en place.

    Pour chaque machine on prélève au hasard un échantillon de 20 axes
    dans la production en cours de la machine, on mesure les diamètres
    des axes prélevés et on calcule le diamètre moyen des axes de
    l'échantillon, noté $d$.
\begin{itemize}
\item Si $d \notin [\mathrm{L_{C_1}} ; \mathrm{L_{C_2}}]$ alors le
service de maintenance procède immédiatement au réglage de la machine.
\item Si $d \in [\mathrm{L_{C_1}} ; \mathrm{L_{S_1}}]$ ou $d \in
[\mathrm{L_{S_2}} ; \mathrm{L_{C_2}}]$ alors le service de
maintenance prélève immédiatement un autre échantillon
(procédure d'alerte) et refait les mesures et calculs.
\end{itemize}

\medskip

Une machine est en cours de cycle. Un prélèvement de 20 axes conduit aux mesures suivantes :

\medskip

\small
\begin{tabular}{|*{6}{c|}} \hline
      Diamètre en mm & $[15,09~;~15,11]$ & $[15,11~;~15,13]$ &
      $[15,13~;~15,15]$ & $[15,15~;~15,17]$ & $[15,17~;~15,19]$ \\ \hline
      Effectif & 1 & 2 & 3 & 4 & 3 \\ \hline
      Diamètre en mm & $[15,19~;~15,21]$ & $[15,21~;~15,23]$ &
      $[15,23~;~15,25]$ & $[15,25~;~15,27]$ & $[15,27~;~15,29]$ \\ \hline
      Effectif & 2 & 2 & 1 & 1 & 1 \\ \hline
\end{tabular}
    \normalsize

	\begin{enumerate}
		\item En utilisant le milieu de chacun des intervalles, calculer
        le diamètre moyen des 20 axes prélevés.
		\item Quelle décision prend le service de maintenance ? Expliquer.
    \end{enumerate}
\end{enumerate}

\pagebreak

\begin{center}
  \bfseries
  DOCUMENT RÉPONSE à rendre avec la copie
\end{center}

\textbf{EXERCICE 1 - PARTIE D - Question 2 :}

\begin{center}
\begin{tikzpicture}[xscale=0.7,yscale=0.14,>=latex]
\draw [thick,->] (0,0) -- (15.1,0) ;
\draw [thick,->] (0,0) -- (0,47) ;
\draw [gray] (0,0) grid [xstep=1, ystep = 5] (15.1,47) ;

\foreach \i in {5,10,...,45} { \node at (0,\i) [ left] { \small \i} ; } ;
\foreach \i in {1,...,15} { \node at (\i,0) [ below ] {\small
  \np{\i}} ; } ;

\node at (0,47) [anchor = west ] {tension en Volt} ;
\node at (15,0) [anchor = south east ] {temps en secondes } ;

\end{tikzpicture}
\end{center}

\textbf{EXERCICE 1 - PARTIE D - Question 3. a. :}

\begin{center}
\begin{tabular}{|p{3cm}|*{4}{p{1.5cm}|}} \hline
\centering $t$ & $-\infty$ & 0 & 3 & 7 \hfill $+\infty$ \\ \hline
$10 \mathcal{U}(t)$ & & & & \\ \hline
$30 \mathcal{U}(t-3)$ & & & & \\ \hline
$-20 \mathcal{U}(t-7)$ & & & & \\ \hline
\centering $e(t)$ & & & & \\ \hline
\end{tabular}
\end{center}
\end{document}