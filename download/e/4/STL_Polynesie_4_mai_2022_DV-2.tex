\documentclass[10pt,a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}%ATTENTION codage en utf8 ! 
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{multicol,diagbox}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo}
%Tapuscrit : François Hache & Denis Vergès
\usepackage{pst-all,pst-func}
\usepackage[left=3cm, right=3cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\newcommand{\vectt}[1]{\overrightarrow{\,\mathstrut\text{#1}\,}}
\newcommand{\barre}[1]{\overline{\,#1\vphantom{b}\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS},
pdftitle = {Polynésie  4 mai 2022},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
\newcommand{\e}{\,\text{e\,}}	%%%le e de l'exponentielle
\renewcommand{\d}{\,\text d}	%%%le d de l'intégration
\renewcommand{\i}{\,\text{i}\,}	%%%le i des complexes
\newcommand{\ds}{\displaystyle}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat STL}
\lfoot{\small{Polynésie}}
\rfoot{\small{ 4 mai 2022}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~Baccalauréat STL Biotechnologies~\decofourright\\[10pt]Polynésie  -- 4 mai 2022}}

\end{center}

\vspace{0,25cm}

\textbf{\large Exercice 1 commun à tous les candidats\hfill 4 points\\[7pt](physique-chimie et mathématiques)}

\bigskip

Découverte en 1892 par S.E. Linder et H. Picton puis développée dans les années 1930 par le chimiste suédois Arne Tiselius (Prix Nobel de chimie en 1948), l'électrophorèse est, avec la chromatographie, la principale technique utilisée pour séparer ou caractériser les espèces ioniques d'intérêt biologique, comme les acides aminés.

\smallskip

L'objectif de cet exercice est de déterminer la durée de migration nécessaire pour séparer deux acides aminés, l'acide aspartique et l'acide glutamique, par électrophorèse.

\bigskip

\textbf{PARTIE A - Principe de l'électrophorèse}

\medskip

Une goutte d'un mélange des deux acides aminés à séparer est déposée (point M de la figure ci-dessous) sur une plaque horizontale recouverte de gel d'agarose et soumise à un champ électrostatique, dont la norme est notée E.

\begin{center}
\psset{unit=1cm,arrowsize=2pt 3}
\begin{pspicture}(13,5.5)
\psframe(2,0)(10,3.6)
\psframe[fillstyle=solid,fillcolor=lightgray](2,0)(2.5,3.6)
\psframe[fillstyle=solid,fillcolor=lightgray](10,0)(10.5,3.6)
\psline{->}(3,1.8)(12,1.8)
\psline[linewidth=1.25pt]{->}(5.8,1.8)(7.4,1.8)
\uput[u](3,1.8){M}\uput[u](5.8,1.8){M$'$}\uput[u](7.4,1.8){$\vect{F_e}$}
\uput[d](12,1.8){Axe (M$x$)}
\psdots[dotstyle=+,dotscale=1.8](3,1.8)(5.8,1.8)(11.4,2.3)
\rput(11.4,2.7){Électrode}
\psline(2.25,3.6)(2.25,4.7)(5.75,4.7)
\psline(6.75,4.7)(10.25,4.7)(10.25,3.6)
\pscircle(6.25,4.7){0.5}\rput(6.25,4.7){G}
\rput(5.3,5.3){\textbf{\large -}}\rput(7,5.3){\textbf{\Large +}}
\rput(0.8,2.7){Électrode}\rput(0.8,2.3){\Large $-$}
\rput(6.25,0.3){Plaque de gel d'agarose}
\end{pspicture}
\end{center}


Les acides aminés, sous forme anionique au pH imposé (ion aspartate et ion glutamate), migrent vers l'électrode positive sous l'effet de la force électrostatique,
notée $\vect{F_e}$ et représentée ci-dessus au point M$'$.

L'action du gel sur les molécules est modélisée par une force de frottement $\vect{f}$.

\textbf{Données :}

\medskip

\begin{itemize}
\item masse d'un ion aspartate : $m_{\text{aspart}} = 2,12 \times 10^{-25}$ kg ;
\item masse d'un ion glutamate: $m_{\text{glutam}} = 2,43 \times  10^{-25}$ kg ;
\item norme de la force électrostatique subie par les ions aspartate et glutamate :
$F_e = e \times  E$ avec

\begin{itemize}
\item[$\bullet~~$] $E = 520$ V.m$^{-1}$ : intensité du champ électrostatique ;
\item[$\bullet~~$] $e = 1,6 \times 10^{-19}$ C : valeur absolue de la charge portée par chaque anion d'acide aspartique ou d'acide glutamique.
\end{itemize}
\item expression vectorielle de la force $\vect{f}$ exercée par le gel:

$\vect{f}  = - k.\vect{v}$, avec
\begin{itemize}
\item[$\bullet~~$]$k$ le coefficient caractéristique du constituant et du milieu dans lequel s'effectue la migration :
\begin{description}
\item[ ] pour l'ion aspartate $k_{\text{aspart}} = 2,7 \times  10^{-12}$ N.s.m$^{-1}$ ;
\item[ ] pour l'ion glutamate $k_{\text{glutam}} = 3,0 \times  10^{-12}$ N.s.m$^{-1}$ ; 
\end{description}
\item[$\bullet~~$]$\vect{v}$ le vecteur vitesse de l'ion concerné.
\end{itemize}
\end{itemize}

\medskip

\begin{enumerate}
\item En justifiant la réponse dans la copie, représenter sur le \textbf{DOCUMENT RÉPONSE DR1}, sans souci d'échelle, le vecteur force $\vect{f}$ modélisant l'action du gel sur les anions au point M$'$.
\item Écrire la seconde loi de Newton pour un anion de masse m et l'appliquer dans le cas de l'électrophorèse considérée.
\item Projeter la relation vectorielle sur l'axe (M$x$) et montrer que la valeur de la vitesse $v$ de migration de l'anion considéré est solution de l'équation différentielle :

\[\dfrac{\text{d}v}{\text{d}t} + \dfrac km \times v = \dfrac{e \times E}{m}\]

\end{enumerate}

\bigskip

\textbf{PARTIE B - Étude du mouvement de l'ion d'acide aspartique}

\medskip

Par application numérique, l'équation différentielle ci-dessus peut s'écrire sous la forme:

\[v'= - 1,3 \times 10^{13}v + 3,9\times 10^8,\]

où la vitesse $v$ est exprimée en mètre par seconde (m.s$^{-1}$) et le temps $t$ est exprimé en seconde (s).

\medskip

\begin{enumerate}
\item Déterminer la solution générale $v$ de cette équation différentielle définie sur $[0~;~ +\infty[$.
\item Sachant que $v (0) = 0$, montrer que, pour tout $t \in [0~;~ +\infty[$,

\[v(t) = 3 \times 10^{-5}\left(1 -  \text{e}^{-1,3 \times 10^{13}t}\right)\]

\item Justifier que $\displaystyle\lim_{t \to + \infty}  v(t) = 3 \times 10^{-5}$.
\item On note $t_{90}$ l'instant exprimé en seconde pour lequel la vitesse atteint 90\,\% de
sa vitesse limite. Montrer que $t_{90} = 1,8 \times 10^{-13}$ arrondi à $10^{-14}$.
\end{enumerate}

\bigskip

\textbf{PARTIE C - Détermination de la durée de migration}

\medskip

Les résultats précédents montrent que le régime stationnaire est atteint quasi instantanément, si bien que l'on peut considérer que les constituants du mélange se déplacent suivant un mouvement rectiligne uniforme avec une vitesse constante égale à:

\[v_{\text{lim}} = \dfrac{e \times E}{k}\]

\begin{enumerate}
\item Comparer la vitesse limite de migration des ions glutamate et des ions aspartate.
En fin d'électrophorèse, les taches sont révélées sous lumière ultraviolette. On admet qu'une différence de distance de migration d'au moins 5 mm est nécessaire pour distinguer la tache associée au mouvement des ions glutamate et celle associée au mouvement des ions aspartate.
\item Déterminer la durée minimale de l'électrophorèse et les distances alors parcourues par les ions pour pouvoir distinguer les deux taches correctement. Commenter les valeurs obtenues.
\item Sur un schéma succinct de la plaque, positionner et identifier les taches obtenues après électrophorèse.
\end{enumerate}

\bigskip

\textbf{\large Exercice 3 commun à tous les candidats\hfill 4 points}

\bigskip

\textbf{Vous traiterez 4 questions au choix parmi les 6 questions proposées}

\medskip

Dans cet exercice, on s'intéresse à l'énergie stockée dans la batterie d'un téléphone portable. Cette grandeur s'exprime en kW·h. Lorsque la batterie est totalement chargée, l'énergie stockée vaut 0,715 kW·h.

Lors du branchement de la batterie vide sur une borne de recharge, l'énergie stockée dans la batterie (en kW·h) en fonction du temps $t$ (en heure) est modélisée par une fonction $f$  définie sur $[0~;~+\infty[$ par:

\[f(t) = a\text{e}^{-t} + b \quad \text{où $a$ et $b$ sont deux réels à déterminer}\]

\medskip

\textbf{Question 1 :}

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Sachant que $\displaystyle\lim_{t \to + \infty} f(t) = 0,715$, déterminer la valeur de $b$.
		\item Sachant que $f(0) = 0$, déterminer la valeur de $a$.
	\end{enumerate}	
\end{enumerate}
\medskip
		
\textbf{Dans les questions suivantes, on admet que pour tout nombre réel} \boldmath$t \geqslant 0$\unboldmath

\boldmath\[ f(t) = -0,715\text{e}^{-t} + 0,715\]\unboldmath

\smallskip

\textbf{Question 2 :}

\begin{enumerate}[resume]
\item Montrer que pour tout nombre réel $t \geqslant 0,\: f(t) < 0,715$.
\end{enumerate}

\textbf{Question 3 :}

\begin{enumerate}[resume]
\item 
	\begin{enumerate}
		\item Déterminer la fonction dérivée $f'$ de la fonction $f$.
		\item En déduire le sens de variation de la fonction $f$ sur $[0~;~+\infty[$.
	\end{enumerate}
\end{enumerate}

\medskip

\textbf{Question 4 :}

La durée de demi-charge est le temps nécessaire pour charger à 50\,\% une batterie qui était vide au départ. 

\begin{enumerate}[resume]
\item Déterminer la durée de demi-charge de la batterie de ce téléphone en minute et seconde, arrondie à la seconde.
\end{enumerate}

\medskip

\textbf{Question 5 :}

\begin{enumerate}[resume]
\item On considère la fonction en langage Python suivante:

\begin{center}
\fbox{\begin{tabular}{l}
from math import exp\\
def temps(pourcentage) :\\
\quad t = 0\\
\quad y = 0\\
while y < pourcentage$*0.715$:\\
\qquad t = t+1/60\\
\qquad y = - 0.715*exp(-t)+0.715\\
return(t)
\end{tabular}
}
\end{center}

\smallskip

Que renvoie l'exécution de l'instruction temps(0.15) ? 

Interpréter ce résultat dans le contexte de l'exercice.
\end{enumerate}

\medskip

\textbf{Question 6 :}

On considère la fonction $F$ définie sur $[0~;~+\infty[$ par 

\[F(t) = 0,715 t + 0,715\text{e}^{-t}.\]

\begin{enumerate}[resume]
\item
	\begin{enumerate}
		\item Vérifier que $F$ est une primitive de $f$ sur $[0~;~+\infty[$.
		\item On admet que l'énergie stockée moyenne de la batterie sur [0~;~3,5] est égale à :

\[m = \dfrac{1}{3,5} [F(3,5) - F(0)].\]

Cette énergie stockée moyenne est-elle égale à la moitié de l'énergie stockée maximale ? Justifier la réponse.
		\end{enumerate}
\end{enumerate}

\newpage

\begin{center}
\textbf{DOCUMENT RÉPONSE\\[7pt]À RENDRE AVEC LA COPIE}
\end{center}

\medskip

\textbf{DR1 : principe de l'électrophorèse}

\begin{center}
\psset{unit=1cm,arrowsize=2pt 3}
\begin{pspicture}(13,5.5)
\psframe(2,0)(10,3.6)
\psframe[fillstyle=solid,fillcolor=lightgray](2,0)(2.5,3.6)
\psframe[fillstyle=solid,fillcolor=lightgray](10,0)(10.5,3.6)
\psline{->}(3,1.8)(12,1.8)
\psline[linewidth=1.25pt]{->}(5.8,1.8)(7.4,1.8)
\uput[u](3,1.8){M}\uput[u](5.8,1.8){M$'$}\uput[u](7.4,1.8){$\vect{F_e}$}
\uput[d](12,1.8){Axe (M$x$)}
\psdots[dotstyle=+,dotscale=1.8](3,1.8)(5.8,1.8)(11.4,2.3)
\rput(11.4,2.7){Électrode}
\psline(2.25,3.6)(2.25,4.7)(5.75,4.7)
\psline(6.75,4.7)(10.25,4.7)(10.25,3.6)
\pscircle(6.25,4.7){0.5}\rput(6.25,4.7){G}
\rput(5.3,5.3){\textbf{\large -}}\rput(7,5.3){\textbf{\Large +}}
\rput(0.8,2.7){Électrode}\rput(0.8,2.3){\Large $-$}
\rput(6.25,0.3){Plaque de gel d'agarose}
\end{pspicture}
\end{center}

\medskip

\textbf{DR2 : détermination expérimentale de la valeur de la résistance interne}

\begin{center}
\psset{xunit=0.08cm,yunit=1.75cm,comma=true}
\begin{pspicture}(-10,-0.4)(150,5.5)
\multido{\n=0+10}{16}{\psline[linewidth=0.15pt](\n,0)(\n,5.5)}
\multido{\n=0.0+0.5}{11}{\psline[linewidth=0.15pt](0,\n)(150,\n)}
\uput[u](140,0){$I$ (mA)}\uput[r](0,5.25){U$_{\text{PN}}(\text{V})$}
\psaxes[linewidth=1.25pt,Dx=10,Dy=0.5]{->}(0,0)(0,0)(150,5.5)
\end{pspicture}
\end{center}
\end{document}