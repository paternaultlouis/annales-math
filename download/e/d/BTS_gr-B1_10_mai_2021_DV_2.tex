\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage[normalem]{ulem}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
%Merci François Hache pour la relecture
\usepackage{pst-plot,pst-text,pst-eucl,pst-node,pst-circ,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS groupement B1},
pdftitle = {10 mai 2021},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Groupement B1}}
\rfoot{\small{10 mai 2021}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center}
{\Large\textbf{Brevet de technicien supérieur groupement B1 \\[6pt] 10 mai 2021 - Métropole--Antilles--Guyane--Polynésie}}
  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 10 points}

\medskip



Une étude est menée concernant le train d'atterrissage d'un certain type d'hélicoptère. 

Ce train d'atterrissage est composé d'une roue et d'un amortisseur oléopneumatique permettant d'absorber l'énergie de l'impact au moment de l'atterrissage.

\medskip

On note $f(t)$ la hauteur, en mètre, du centre de gravité de l'hélicoptère par rapport au sol à l'instant $t$ exprimé en seconde.

On suppose que $f$ est une fonction de la variable réelle $t$ définie et deux fois dérivable sur $[0~;~+\infty[$.
\begin{center}
\textbf{Les trois parties de cet exercice peuvent être traitées de façon indépendante}
\end{center}

\textbf{A. Résolution d'une équation différentielle}

\medskip

Une étude mécanique montre que la fonction $f$ est solution de l'équation différentielle 

\[(E) : \quad  y'' + 3y' + 2y = 4,\]

où $y$ est une fonction inconnue de la variable réelle $t$, définie et deux fois dérivable sur $[0~;~+\infty[$,  $y'$ la fonction dérivée de $y$ et $y''$ sa fonction dérivée seconde.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Résoudre dans $\R$ l'équation $r^2 + 3r + 2 = 0$,
		\item En déduire les solutions de l'équation différentielle 
		
\[\left(E_0\right) :\quad  y'' + 3y' + 2y = 0.\]

On fournit les formules suivantes.

\begin{center}
\begin{tabularx}{\linewidth}{|p{5cm}|X|}\hline
Équations	&Solutions sur un intervalle $I$\\ \hline
Équation différentielle :

$ay''+ by'+ cy = 0$,

Équation caractéristique:

$ar^2 + br + c = 0$ de discriminant $\Delta$.&
Si $\Delta > 0,\, y(t) = \lambda \text{e}^{r_1 t} + \mu \text{e}^{r_2 t}$ où $r_1$ et $r_2$ sont les racines de l'équation caractéristique.
 
Si $\Delta = 0,\, y(t) = (\lambda t+ \mu)\text{e}^{r t}$ où $r$ est la racine double de l'équation caractéristique,

Si $\Delta < 0,\, y(t) = [\lambda \cos(\beta t)+ \mu \sin(\beta t)]\text{e}^{\alpha t}$ où $r_1 = \alpha + \text{i}\beta$ et $r_2 = \alpha - \text{i}\beta$ sont les racines
complexes conjuguées de l'équation caractéristique.\\ \hline
\end{tabularx}
\end{center}

	\end{enumerate}
\item Soit $k$ un nombre réel. On définit la fonction constante $g$ sur $[0~;~+\infty[$ par $g(t) = k$.

Déterminer $k$ pour que la fonction $g$ soit solution de l'équation différentielle $(E)$.
\item En déduire l'ensemble des solutions de l'équation différentielle $(E)$.
\end{enumerate}

\bigskip

\textbf{B. Étude de la fonction }\boldmath$f$\unboldmath

\medskip

On admet que la fonction $f$ correspondant à la hauteur du centre de gravité de l'hélicoptère est définie sur $[0~;~+\infty[$ par  

\[f(t)= - \text{e}^{-t} + 1,5\text{e}^{-2t} + 2.\]

Sa courbe représentative $\mathcal{C}$ dans un repère orthogonal est donnée ci-dessous.

\begin{center}
\psset{unit=2cm,comma=true}
\begin{pspicture*}(-0.4,-0.3)(4.75,3.25)
\psgrid[gridwidth=0.5pt,subgridwidth=0.1pt,gridlabels=0pt,subgriddiv=2](0,0)(4.75,3.25)
\psaxes[linewidth=1.25pt,Dx=0.5,Dy=0.5]{->}(0,0)(0,0)(4.75,3.25)
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=red]{0}{4.75}{2 1.5 2.71828 2 x mul exp div add  2.71828 x neg exp sub}
\uput[u](0.5,2.1){\red $\mathcal{C}$}
\end{pspicture*}
\end{center}

\medskip

\begin{enumerate}
\item Déterminer la hauteur du centre de gravité de l'hélicoptère au moment de l'atterrissage à
l'instant $t=0$.
\item On admet que $\displaystyle\lim_{t \to + \infty} \text{e}^{-t} = \displaystyle\lim_{t \to + \infty} \text{e}^{-2t}= 0$.
	\begin{enumerate}
		\item Calculer $\displaystyle\lim_{t \to + \infty} f(t)$.
		\item En déduire que la courbe $\mathcal{C}$ admet une droite asymptote dont on donnera une équation.


	\end{enumerate}
\item 
	\begin{enumerate}
		\item À l'aide du graphique, conjecturer le sens de variation de la fonction $f$ sur $[0~;~ + \infty]$.
		\item Un logiciel de calcul formel donne ci-dessous une expression de la dérivée $f'$ de la fonction $f$. Ce résultat est admis.
		
\begin{center}
\renewcommand{\arraystretch}{1.5}
$\begin{array}{l}
1\quad f(t) : - \text{e}^{-t} + 1,5\text{e}^{-2t} + 2\\
 \quad \to f(t) := - \text{e}^{-t} + \dfrac{3}{2\rule[-5pt]{0pt}{0pt}}\text{e}^{-2t} + 2\\ \hline
2\quad \text{Dérivée}(f(t),t)\\
\quad \to \text{e}^{-t} - 3\text{e}^{-2t}\\
\end{array}$
\end{center}

Montrer que cette dérivée peut aussi s'écrire : $f'(t) = \text{e}^{-2t}\left(\text{e}^{t} - 3\right)$.
		\item Résoudre sur $[0~;~ + \infty]$ l'inéquation $\text{e}^{t} - 3 \geqslant 0$. 
		\item En déduire le signe de $f'(t)$ sur $[0~;~ + \infty]$.
		\item Dresser le tableau de variation de la fonction $f$ sur $[0~;~ + \infty]$.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Étude locale}

\medskip

On rappelle que la fonction $f$ est définie sur $[0~;~ + \infty]$ par  

\[f(t) = - \text{e}^{-t} + 1,5\text{e}^{-2t} + 2\]

et que sa courbe représentative $\mathcal{C}$ dans un repère orthogonal est donnée dans la partie B.

Un logiciel de calcul formel affiche la partie régulière du développement limité à l'ordre 2 de la fonction $f$ au voisinage de zéro.

\begin{center}
\renewcommand{\arraystretch}{2}
$\begin{array}{l}
3 \quad \text{PolynômeTaylor}(f(t), t, 0, 2)\\
\quad \dfrac{5}{2} -2t + \dfrac{5}{2} t^2\\
\end{array}$
\end{center}

\smallskip

\begin{enumerate}
\item \emph{Les deux questions suivantes sont des questions à choix multiples. Une seule réponse est exacte. Recopier sur la copie la réponse qui vous paraît exacte. On ne demande aucune justification.\\
La réponse juste rapporte $1$ point. Une réponse fausse ou une absence de réponse ne rapporte ni n'enlève de point.}

	\begin{enumerate}
		\item Le développement limité de la fonction $f$ à l'ordre 2 au voisinage de 0 est :
	\end{enumerate}
\begin{center}
\renewcommand{\arraystretch}{2.5}
\begin{tabularx}{\linewidth}{|*{3}{>{\centering \arraybackslash \small}X|}}\hline
$\dfrac{5}{2} - 2 t + \dfrac{5}{2}t^2$&
$\dfrac{5}{2} -2 t + \dfrac{5}{2} t^2+ t^2\epsilon(t)\, $

$\text{avec}\, \displaystyle\lim_{t \to + \infty}\epsilon(t) = 0$&$\dfrac{5}{2}  -2 t + \dfrac{5}{2} t^2+ t^2\epsilon(t)\,$

$ \text{avec}\, \displaystyle\lim_{t \to 0}\epsilon(t) = 0$\\ \hline
\end{tabularx}
\end{center}
	\begin{enumerate}[resume]
		\item Une équation de la tangente $T$ à la courbe $\mathcal{C}$ au point d'abscisse 0 est :
	\end{enumerate}	
\begin{center}
\renewcommand{\arraystretch}{2.5}
\begin{tabularx}{\linewidth}{|*{3}{>{\centering \arraybackslash}X|}}\hline		
$y= \dfrac{5}{2}$&$y= \dfrac{5}{2} - 2t$&$y= \dfrac{5}{2} - 2t + \dfrac{5}{2}t^2$\rule[-3mm]{0mm}{9mm}\\ \hline
\end{tabularx}
\end{center}

%	\end{enumerate}
\item Étudier la position relative, au voisinage du point d'abscisse 0, de la courbe $\mathcal{C}$ et de la tangente $T$.
\end{enumerate}

\vspace{0,5cm}

\textbf{EXERCICE 2 \hfill 10 points}

\medskip
\begin{center}
\textbf{Les quatre parties de cet exercice peuvent être traitées de façon indépendante}

\end{center}

\textbf{A. Loi binomiale}

\medskip

Une enquête réalisée auprès d'une grande enseigne de garages automobile permet d'admettre que la probabilité qu'une voiture prélevée au hasard parmi les garages de cette enseigne soit réparée et rendue à son propriétaire le jour même de sa réception est $0,7$.

On interroge $100$ clients au hasard de cette enseigne. Le nombre de clients est suffisamment important pour assimiler ce sondage à un tirage avec remise.

On note $X$ la variable aléatoire qui, à chaque échantillon de $100$ clients ainsi interrogés, associe le nombre de clients dont le véhicule est restitué le jour même.

\medskip

\begin{enumerate}
\item Justifier que la variable aléatoire $X$ suit une loi binomiale dont on précisera les paramètres.
\item Calculer l'espérance de $X$ puis en donner une interprétation.
\item Calculer la valeur, arrondie au dixième, de l'écart type de la variable aléatoire $X$.
\item Calculer la probabilité que, sur un échantillon aléatoire de $100$ clients, exactement 60 clients aient récupéré leur voiture le jour même. $\left(\text{Arrondir à}\, 10^{-3}.\right)$

\end{enumerate}

\bigskip

\textbf{B. Approximation d'une loi binomiale par une loi normale}

\medskip

On décide d'approcher la loi de la variable aléatoire $X$ définie dans la partie A par la loi normale de moyenne $70$ et d'écart type $4,6$. 

On note $Y$ une variable aléatoire de loi normale de moyenne $70$ et d'écart type $4,6$.

En utilisant cette approximation, calculer:

\medskip

\begin{enumerate}
\item la probabilité, arrondie à $10^{- 3}$, qu'au moins $80$ voitures, sur un échantillon de taille $100$, soient restituées à leur propriétaire le jour même, c'est-à-dire $P(Y \geqslant 79,5)$ ;
\item la probabilité, arrondie à $10^{- 3}$, que le nombre de voitures, sur un échantillon de taille 100, restituées à leur propriétaire le jour même soit compris entre $60$ et $80$, c'est-à-dire
$P(59,5 \leqslant Y \leqslant 80,5)$.
\end{enumerate}

\bigskip

\textbf{C. Probabilités conditionnelles}

\medskip

Un garage de cette enseigne possède deux ateliers. Les véhicules qu'il répare sont traités, soit par l'atelier 1, soit par l'atelier 2.

L'atelier 1 répare 60\,\% des véhicules et le reste est réparé par l'atelier 2. 

Suite aux réparations, 1\,\% des véhicules provenant de l'atelier 1 présentent un défaut de réparation et 2,5\,\% de ceux provenant de l'atelier 2 présentent un défaut de réparation.

On prélève au hasard un véhicule parmi ceux ayant été réparés dans ce garage. 

On définit les évènements suivants:

$A$ : \og le véhicule provient de l'atelier 1 \fg{} ;

$B$ : \og le véhicule provient de l'atelier 2 \fg{};

$D$ : \og  le véhicule présente un défaut de réparation \fg.

\medskip

\begin{enumerate}
\item Déduire des informations précédentes $P(A)$, $P(B)$, $P_A(D)$ et $P_B(D)$.

(On rappelle que $P_A(D)$ est la probabilité de l'évènement $D$ sachant que l'évènement $A$ est réalisé.)
\item Calculer $P(A \cap D)$ et $p(B \cap D)$.

Calculer la probabilité qu'un véhicule présente un défaut de réparation.
\end{enumerate}

\bigskip

\textbf{D. Intervalle de confiance}

\medskip

Cette grande enseigne de garages automobile organise une enquête de satisfaction auprès de ses clients. Elle voudrait estimer la proportion inconnue $p$ de clients satisfaits.

Pour cela, elle interroge au hasard un échantillon de $100$ clients parmi l'ensemble de sa clientèle. Cette clientèle est suffisamment importante pour considérer que cet échantillon résulte d'un tirage avec remise.

Soit $F$ la variable aléatoire qui à tout échantillon ainsi prélevé, associe la fréquence, dans cet échantillon, des clients satisfaits. On suppose que $F$ suit la loi normale de moyenne $p$ inconnue et d'écart type $\sqrt{\dfrac{p(1 - p)}{100}}$. 

On fournit la formule suivante :
\begin{center}
\begin{tabularx}{\linewidth}{|>{\centering \arraybackslash}X|}\hline
Intervalle de confiance d'une proportion à $95$\,\%\\ \hline
$\left[f - 1,96\sqrt{\dfrac{f(1 - f)}{n}}~;~f + 1,96\sqrt{\dfrac{f(1 - f)}{n}}\right]$\\ \hline
\end{tabularx}
\end{center}

Pour l'échantillon prélevé, on constate que 87 clients sont satisfaits.

\medskip

\begin{enumerate}
\item Donner une estimation ponctuelle $f$ de la proportion inconnue $p$.
\item Déterminer un intervalle de confiance centré sur $f$ de la proportion $p$ avec le coefficient de confiance 95\,\%. Arrondir les bornes de l'intervalle à $10^{-3}$.
\end{enumerate}
\end{document}
