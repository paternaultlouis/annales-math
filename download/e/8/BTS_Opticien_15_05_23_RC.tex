\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{graphicx,colortbl,xcolor}
\usepackage{textcomp} 
\usepackage{multirow}
\usepackage{enumitem}
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Ronan Charpentier
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS Opticien-lunetier},
pdftitle = {mai 2023},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
\usepackage{tikz}

\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\footnotesize Brevet de technicien supérieur }
\lfoot{\footnotesize{Opticien--lunetier}}
\rfoot{\footnotesize{15 mai 2023}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}
\begin{center} \Large \textbf{\decofourleft~Brevet de technicien supérieur Opticien--lunetier~\decofourright\\[5pt]15 mai 2023}  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 10 points}

\medskip

On s'intéresse à une entreprise qui commercialise des montures de lunettes.

\medskip
\begin{center}

\textit{Les quatre parties de cet exercice peuvent être traitées de façon indépendante.}

\end{center}

\bigskip

\textbf{Partie A - Etude d'une série statistique.}

\medskip


Le graphique ci-dessous représente l'évolution des ventes d'un modèle de monture de lunettes 

depuis l'année 2017.

\begin{center}
\begin{tikzpicture}
\foreach \y in {250,270,290,310,330,350,370,390,410,430} \draw(10,{(\y-250)/40})--(0,{(\y-250)/40})node[left]{$\y$};
\foreach \x in {0,1,2,3,4,5} \draw({2*\x},4.5)--({2*\x},-0.1)node[below]{$\x$};
\draw(0,{(275-250)/40})node{\Large $\bullet$};
\draw(2,{(370-250)/40})node{\Large $\bullet$};
\draw(4,{(383.5-250)/40})node{\Large $\bullet$};
\draw(6,{(406.5-250)/40})node{\Large $\bullet$};
\draw(8,{(408.6-250)/40})node{\Large $\bullet$};
\draw(10,{(413-250)/40})node{\Large $\bullet$};
\draw(5,-1)node{$t$ : rang de l'année à partir de 2017};
%\draw(-2,2.3)node{$N$ : nombre de};
%\draw(-2,2)node{montures};
\draw(1.3,5.05)node{$N$ : nombre de montures};
\end{tikzpicture}
\end{center}


\begin{enumerate}
\item Expliquer pourquoi un ajustement affine de $N$ en $t$ n'est pas pertinent.

\medskip

\item On effectue le changement de variable $z=\ln(415-N)$.

On obtient alors le tableau suivant :

\smallskip

\begin{center}
\begin{tabularx}{\linewidth}{|*{7}{>{\centering \arraybackslash}X|}}\hline 
Année & 2017 & 2018 & 2019 & 2020 & 2021 & 2022 \\ \hline 
$t$ & 0 & 1 & 2 & 3 & 4 & 5 \\ \hline 
$z$ & 4,94 & 3,81 & 3,45 & 2,14 & 1,86 & 0,77 \\ \hline 
\end{tabularx}
\end{center}

\begin{enumerate}
\item A l'aide de la calculatrice, donner le coefficient de corrélation linéaire de la série $(t~;~z)$. Arrondir à $10^{-3}$.
\item Un ajustement affine de $z$ en $t$ est-il pertinent ? Justifier.
\end{enumerate}

\medskip
\item A l'aide de la calculatrice, donner l'équation de la droite de régression linéaire de $z$ en $t$, selon la méthode des moindres carrés, sous la forme $z = at + b$.

Les coefficients $a$ et $b$ seront arrondis à $10^{-2}$.

\medskip
\item En déduire une expression de $N$ à l'aide de $t$ sous la forme :

$$N=415-C \text{e}^{-0,8t} ,$$

où $C$ est une constante que l'on déterminera, à l'unité près.

\medskip

\item On suppose que l'évolution constatée se poursuit.

Quel sera le nombre de montures vendues en 2023 ?

\end{enumerate}


\bigskip

\textbf{Partie B - Résolution d'une équation différentielle.}

\medskip
On considère l'équation différentielle :

$$(E) \: : \: 5y'+4y=1660,$$

où $y$ est une fonction inconnue de la variable $t$, définie et dérivable sur l'intervalle $[0;+\infty[$ , et où $y'$ est sa fonction dérivée.

\medskip
\begin{enumerate}
\item Déterminer les solutions de l'équation différentielle

$$(E_0) \: : \: 5y'+4y=0.$$

\medskip
On fournit la formule suivante :

\begin{center}
\begin{tabular}{|c|c|}
\hline 
Equation différentielle & Solution sur un intervalle $I$ \\ 
\hline 
$ay'+by=0$ & $\rule[-4pt]{0pt}{28pt}f(t)=k \text{e}^{- \dfrac{b}{a} t}$ \\ 
\hline 
\end{tabular} 
\end{center}

\medskip
\item Soit $c$ in nombre réel. On considère la fonction constante $g$, définie par $g(t)=c$.

Déterminer $c$ pour que la fonction $g$ soit solution de l'équation différentielle $(E)$.

\medskip
\item En déduire les solutions de l'équation différentielle $(E)$.

\medskip

\item Déterminer la fonction $f$, solution de l'équation différentielle $(E)$, qui vérifie la condition initiale $f(0)=290$.


\end{enumerate}

\newpage

\textbf{Partie C - Étude d'une fonction.}

\medskip
On considère la fonction $f$, définie sur l'intervalle $[0;+\infty[$ par :

$$f(t)=415-125 \text{e}^{-0,8t}.$$

On admet que cette fonction modélise l'évolution du nombre de montures vendues en fonction du temps :

\begin{itemize}
\item[\:\:]$t$ désigne le temps écoulé, en années, à partir de l'année 2017.
\item[\:\:]$f(t)$ désigne le nombre de montures vendues.
\end{itemize}

On note $\mathcal{C}$ la courbe représentative de la fonction $f$.

Un logiciel de calcul formel fournit les résultats suivants. Ces résultats sont admis et peuvent être utilisés dans les questions suivantes.

\begin{center}
\begin{tikzpicture}[scale=0.9]
\draw[fill=gray!30,draw=gray!30](0,0)rectangle(1,7.2);
\draw(0.5,6.3)node{\Large 1};
\draw(0.5,4.5)node{\Large 2};
\draw(0.5,3)node{\Large 3};
\draw[fill=white,draw=gray!60](0.5,2.4)circle(0.2);
\draw(0.5,0.9)node{\Large 4};
\draw(0,0)--(10,0)--(10,7.2)--(0,7.2)--cycle;
\draw(0,1.8)--(10,1.8);
\draw(0,3.6)--(10,3.6);
\draw(0,5.4)--(10,5.4);
\draw(1.25,6.8)node[right]{dérivée$(415-125\text{e}^{-0,8t})$};
\draw[->,>=latex,line width=1pt](1.25,5.7)--(1.75,5.7);
\draw(1.8,6)node[right]{\textbf{$100 \text{e}^{\dfrac{-4}5 t}$}};

\draw(1.25,5)node[right]{intégrale$(415-125\text{e}^{-0,8t},t)$};
\draw[->,>=latex,line width=1pt](1.25,4.05)--(1.75,4.05);
\draw(1.8,4.2)node[right]{\textbf{$\dfrac{625}{4} \text{e}^{\dfrac{-4}5 t}+415t+c_1$}};

\draw(1.25,3.2)node[right]{Limite$(415-125\text{e}^{-0,8t},\infty)$};
\draw[->,>=latex,line width=1pt](1.25,2.1)--(1.75,2.1);
\draw(1.8,2.1)node[right]{\textbf{$415$}};

\draw(1.25,1.4)node[right]{PolynômeTaylor$(415-125\text{e}^{-0,8t},t,0,2)$};
\draw[->,>=latex,line width=1pt](1.25,0.3)--(1.75,0.3);
\draw(1.8,0.35)node[right]{\textbf{$290+100 t-40 t^2$}};
\end{tikzpicture}
\end{center}

\medskip
\begin{enumerate}
\item On sait que la courbe $\mathcal{C}$ admet une asymptote en $+\infty$.

Donner une équation de cette asymptote.

Interpréter ce résultat dans le contexte de l'exercice.

\medskip
\item Déterminer le sens de variation de la fonction $f$ sur l'intervalle $[0;+\infty[$.

\medskip
\item On note $T$ la tangente à la courbe de $\mathcal{C}$ au point d'abscisse $0$.

Indiquer, sans justifier, laquelle des trois situations ci-dessous représente correctement la position de la courbe $\mathcal{C}$ par rapport à la tangente $T$ au voisinage de zéro.

\begin{center}
\begin{tikzpicture}[scale=0.9]
\draw(0,0)--(3,0)--(3,1)--(0,1)--cycle;
\draw(1.5,0.5)node{Situation 1};
\draw(0.5,1.5)--(2,3);
\draw(2,3)node[above right]{$T$};
\draw[samples=100,domain=0:1.5]plot({0.5+\x},{1.5+\x+\x*\x/4});
\draw(2,3.75)node[left]{$\mathcal{C}$};

\draw(5,0)--(8,0)--(8,1)--(5,1)--cycle;
\draw(6.5,0.5)node{Situation 2};
\draw(5.5,1.5)--(7,3);
\draw(7,3)node[above right]{$T$};
\draw[samples=100,domain=0:1.75]plot({5.5+\x},{1.5+\x-\x*\x/4});
\draw(7.25,2.5)node[right]{$\mathcal{C}$};

\draw(10,0)--(13,0)--(13,1)--(10,1)--cycle;
\draw(11.5,0.5)node{Situation 3};
\draw(10.5,1.5)--(12,3);
\draw(12,3)node[above right]{$T$};
\draw[samples=100,domain=0:2]plot({10.5+\x},{1.5+\x*\x/4});
\draw(12.5,2.5)node[right]{$\mathcal{C}$};
\end{tikzpicture}
\end{center}

\end{enumerate}


\newpage

\textbf{Partie D - Étude d'une suite.}

\medskip

On considère une suite $(u_n)$ définie par $u_0=3000$, et, pour tout entier naturel $n$ :

$$u_{n+1}=0,9 u_n+500.$$

La suite $(u_n)$ représente l'évolution du nombre de clients de l'entreprise.

Ainsi $u_n$ correspond au nombre de clients durant l'année 2017+$n$.

\medskip

\begin{enumerate}
\item Vérifier que le nombre de clients lors de l'année 2018 est égal à 3200.

\medskip
\item Pour tout entier naturel $n$, on pose $v_n=u_n-5000$.

Démonter que $(v_n)$ est une suite géométrique de raison 0,9.

\medskip
\item Exprimer, pour tout entier naturel $n$, le terme $v_n$ en fonction de $n$.

\medskip
\item En déduire que, pour tout entier naturel $n$ :

$$u_n=5000-2000\times 0,9^n.$$

\medskip
\item Déterminer le nombre de clients lors de l'année 2023.

\medskip
\item On considère l'algorithme suivant :


\begin{center}
\begin{tabular}{|l|}
\hline 
\texttt{n}$\leftarrow$\texttt{0}  \\

\texttt{u}$\leftarrow$\texttt{3000} \\

\texttt{Tant que u}$\leqslant$\texttt{4000} \\

\hspace{1cm}\texttt{n}$\leftarrow$\texttt{n+1} \\

\hspace{1cm}\texttt{u}$\leftarrow$\texttt{0,9*u+500} \\

\texttt{Fin Tant que}\\


\hline 
\end{tabular} 
\end{center} 

Quelle est la valeur de la variable $n$ après l'exécution de l'algorithme ?

Interpréter ce résultat dans le contexte de l'énoncé.
\end{enumerate}

\vspace{2cm}
\textit{Remarque : l'énoncé original initialisait \texttt{n} à \texttt{1}, à la première ligne de l'algorithme.}

\newpage

\textbf{Exercice 2 \hfill 10 points}

\medskip

Une usine fabrique des verres ophtalmiques à partir de verres semi-finis.

\medskip

\begin{center}
\textit{Les quatre parties de cet exercice peuvent être traitées de façon indépendante.}

\medskip

\textit{Les résultats approchés sont à arrondir à $10^{-3}$.}

\end{center}


\bigskip

\textbf{Partie A - Probabilités conditionnelles.}

\medskip
L'usine se procure des verres semi-finis auprès de trois fournisseurs différents.

\begin{itemize}
\item[$\bullet$] 30 \,\% des verres semi-finis proviennent d'un premier fournisseur.

Parmi eux, 3 \,\% sont défectueux.
\item[$\bullet$] 60 \,\% des verres semi-finis proviennent d'un deuxième fournisseur.

Parmi eux, 4 \,\% sont défectueux.
\item[$\bullet$] 10 \,\% des verres semi-finis proviennent d'un troisième fournisseur.

Parmi eux, 2 \,\% sont défectueux.

\end{itemize}

On prélève un verre semi-fini au hasard. On considère les événements suivants.

\begin{itemize}
\item [\:\:] $F_1$ : \og le verre semi-fini provient du premier fournisseur \fg{},
\item [\:\:] $F_2$ : \og le verre semi-fini provient du deuxième fournisseur \fg{},
\item [\:\:] $F_3$ : \og le verre semi-fini provient du troisième fournisseur \fg{},
\item [\:\:] $D$ : \og le verre semi-fini est défectueux \fg{}.
 
\end{itemize}

\medskip

\begin{enumerate}
\item Recopier et compléter l'arbre de probabilités suivant :

\medskip

\begin{center}
\begin{tikzpicture}
\draw(0,0)--(3,2)node[right]{$F_1$};
\draw(0,0)--(3,0)node[right]{$F_2$};
\draw(0,0)--(3,-2)node[right]{$F_3$};
\draw(4,2)--(7,2.5)node[right]{$D$};
\draw(4,2)--(7,1.5)node[right]{$\overline{D}$};
\draw(4,0)--(7,0.5)node[right]{$D$};
\draw(4,0)--(7,-0.5)node[right]{$\overline{D}$};
\draw(4,-2)--(7,-1.5)node[right]{$D$};
\draw(4,-2)--(7,-2.5)node[right]{$\overline{D}$};
\end{tikzpicture}
\end{center}


\medskip
\item Calculer la probabilité $P\left(F_1 \cap D\right)$.

\medskip
\item Montrer que la probabilité que le verre semi-fini soit défectueux est égale à 0,035.

\medskip
\item On sait que le verre semi-fini est défectueux.

Quelle est la probabilité qu'il provienne du premier fournisseur ?

\end{enumerate}
\bigskip

\textbf{Partie B - Loi binomiale et loi normale.}

\medskip

On estime que 3,5 \,\% des verres semi-finis du stock de l'usine sont défectueux.

On prélève un échantillon aléatoire de 200 verres semi-finis dans le stock de l'usine.

On considère la variable aléatoire $X$ qui donne le nombre de verres semi-finis défectueux au sein de l'échantillon.

\medskip

\begin{enumerate}
\item \begin{enumerate}
\item On admet que la variable aléatoire $X$ suit une loi binomiale.

Donner ses paramètres.
\item Calculer la probabilité $P(6 \leqslant X \leqslant 10)$.
\end{enumerate}

\medskip
\item On admet que l'on peut approcher la loi de probabilité de la variable aléatoire $X$ par une loi normale d'espérance 7 et d'écart type 2,599.

\begin{enumerate}
\item Justifier la valeur de ces paramètres.
\item On considère une variable aléatoire $Y$ suivant une loi normale d'espérance 7 

et d'écart type 2,6.

Calculer $P(5,5 \leqslant Y \leqslant 10,5)$.

Interpréter dans le contexte.
\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie C - Loi exponentielle.}

\medskip
On s'intéresse au standard téléphonique de l'usine.

On considère $T$ la variable aléatoire qui, à chaque appel au standard, associe le temps d'attente, en minutes.

On admet que $T$ suit la loi exponentielle de paramètre $\lambda=0,2$.

\medskip
On rappelle les formules suivantes :

\begin{center}
\begin{tikzpicture}
\draw(0,0)--(14,0)--(14,1.8)--(0,1.8)--cycle;
\draw(0,1.2)--(14,1.2);\draw(7,0)--(7,1.2);
\draw(7,1.5)node{Loi exponentielle};
\draw(3.5,0.6)node{$P(T \leqslant t)=1-\text{e}^{-\lambda t}$};
\draw(10.5,0.6)node{$E(T)=\dfrac1\lambda$};
\end{tikzpicture}
\end{center}

\begin{enumerate}
\item Calculer l'espérance de la variable aléatoire $T$.

Interpréter dans le contexte.

\medskip
\item On considère un appel au standard, choisi au hasard.

Déterminer la probabilité que le temps d'attente correspondant à cet appel soit compris

entre 2 et 4 minutes.
\end{enumerate}

\newpage

\textbf{Partie D - Estimation}

\medskip
L'usine souhaite estimer la proportion $p$ de clients satisfaits d'un nouveau verre.

Sur un échantillon de 100 clients choisis au hasard, 80 d'entre eux ont déclaré être satisfaits.

\medskip
\begin{enumerate}
\item Donner une estimation ponctuelle $f$ de la proportion $p$.

\medskip
\item Donner une estimation de $p$ par un intervalle de confiance avec le coefficient 

de confiance 90 \,\%.

\smallskip

On fournit la formule suivante :

\begin{center}
\begin{tabular}{|c|}
\hline 
Intervalle de confiance d'une proportion avec un coefficient de confiance de 90 \,\% \\ 
\hline 
\rule[-14pt]{0pt}{34pt} $\left[ f -1,65 \sqrt{\dfrac{f(1-f)}{n}};f +1,65 \sqrt{\dfrac{f(1-f)}{n}} \right]$  \\ 
\hline 
\end{tabular} 
\end{center} 

\item Déterminer les entiers naturels $n$ vérifiant l'inégalité : $1,65 \sqrt{\dfrac{0,16}{n}}\leqslant 0,03$.

Interpréter le résultat dans le contexte.

\end{enumerate}

\end{document}