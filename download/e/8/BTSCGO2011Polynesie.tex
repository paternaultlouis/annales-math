%!TEX encoding = UTF-8 Unicode
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt} 
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength{\textheight}{23,5cm}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\setlength{\voffset}{-1,5cm}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P. M. E. P.}}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Comptabilité et gestion des organisations}}
\rfoot{\small{Polynésie mai 2011}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}
\begin{center}{\Large\textbf{\decofourleft~Brevet de technicien supérieur session 2011~\decofourright\\Polynésie  Comptabilité et gestion des organisations}}
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 9 points}

\begin{center}\textbf{Les trois parties de cet exercice peuvent être traitées de façon indépendante}\end{center}
 
Une usine fabrique en grande quantité deux types de pièces métalliques pour l'industrie: des pièces triangulaires et des pièces carrées.

\bigskip
 
\emph{A. Probabilités conditionnelles}

\medskip
 
On admet que 40\,\% des pièces de la production sont triangulaires, le reste est constitué par les pièces carrées. 

Parmi les pièces triangulaires, 70\,\% ont une masse égale à 30~grammes, les autres ont une masse égale à 10 grammes.
 
Parmi les pièces carrées, 80\,\% ont une masse égale à 30~grammes, les autres ont une masse égale à 10 grammes.
 
On prélève au hasard une pièce dans la production d'une journée de ces deux types de pièces.
 
On considère les évènements suivants :
 
T : \og la pièce prélevée est triangulaire \fg{} ;
 
M : \og la pièce prélevée a une masse égale à 30 grammes \fg.
 
\begin{enumerate}
\item Déduire des informations figurant dans l'énoncé : $P(T),\, P_{T}(M)$ et $P_{\overline{T}}(M)$. 
 
(On rappelle que $P_{T}(M) = P(M / T)$ est la probabilité de l'évènement $M$ sachant que l'évènement $T$ est réalisé.) 
\item Calculer $P(M \cap T)$ et $P\left(M \cap  \overline{T}\right)$. 
\item Déduire de ce qui précède que $P(M) = 0,76$. 
\item Calculer la probabilité qu'une pièce soit carrée sachant que sa masse est égale à 30 grammes.
 
Arrondir à $10^{-2}$.
\end{enumerate}
 
\emph{B. Évènements indépendants}

\medskip
 
Les pièces sont susceptibles de présenter deux défauts appelés \og défaut 1 \fg{} et \og défaut 2 \fg.
 
On prélève une pièce au hasard dans un lot important.
 
On note $D_{1}$ l'évènement : \og la pièce présente le défaut 1 \fg{} ;
 
On note $D_{2}$ l'évènement : \og la pièce présente le défaut 2 \fg.
 
On admet que les probabilités des évènements $D_{1}$ et $D_{2}$ sont : $P\left(D_{1}\right) = 0,01$ et $P\left(D_{2}\right) = 0,02$. 

On suppose de plus que les deux évènements $D_{1}$ et $D_{2}$ sont indépendants.
 
\begin{enumerate}
\item Calculer la probabilité qu'une pièce prélevée au hasard dans le lot présente les deux défauts. 
\item Une pièce est jugée défectueuse si elle présente au moins l'un des deux défauts. Calculer la probabilité qu'une pièce prélevée au hasard dans le lot soit défectueuse.
\end{enumerate}

\bigskip
 
\emph{C. Loi binomiale et loi normale}

\medskip
 
On note $E$ l'évènement : \og une pièce prélevée au hasard dans un stock important de pièces est triangulaire \fg.
 
On suppose que la probabilité de $E$ est $0,40$.
 
On prélève au hasard $60$ pièces dans le stock. Le stock est suffisamment important pour que l'on puisse assimiler ce prélèvement à un tirage avec remise de 60 pièces.
 
On considère la variable aléatoire $X$ qui, à tout prélèvement de 60~pièces, associe le nombre de pièces triangulaires de ce prélèvement.
 
\begin{enumerate}
\item Expliquer pourquoi la variable aléatoire $X$ suit une loi binomiale dont on précisera les paramètres. 
\item On décide d'approcher la loi de la variable aléatoire $X$ par la loi normale de moyenne 24 et d'écart type 3,8. 
	\begin{enumerate}
		\item Justifier les paramètres choisis pour la loi normale. 
		\item On note $Y$ une variable aléatoire suivant la loi normale de moyenne 24 et d'écart type 3,8.
		 
Calculer la probabilité que le nombre de pièces triangulaires d'un prélèvement soit compris entre 20 et 28, c'est-à-dire : $P(19,5 \leqslant  Y \leqslant 28,5)$. Arrondir à $10^{-2}$.
	\end{enumerate} 
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 2 \hfill 11 points}

\medskip

\emph{A. Étude d'une fonction}

\medskip
 
Soit $f$ la fonction définie sur l'intervalle [0~;~6] par 

\[f(x) = \left(2x^2 + 3x\right)\text{e}^{- x}.\]
 
\begin{enumerate}
\item 
	\begin{enumerate}
		\item Démontrer que, pour tout $x$ de l'intervalle [0~;~6], $f'(x) = (-2x + 3)(x + 1)\text{e}^{- x}$. 
		\item Etudier le signe de $f'(x)$ sur [0~;~6]. 
		\item Établir le tableau de variation de $f$ sur [0~;~6]. On y fera figurer la valeur approchée arrondie à $10^{-2}$ du maximum de la fonction $f$.
	\end{enumerate} 
\item
	\begin{enumerate}
		\item Compléter, après l'avoir reproduit, le tableau de valeurs suivant dans lequel les valeurs approchées sont à arrondir à $10^{-2}$.
		
\medskip

\begin{tabularx}{\linewidth}{|*{8}{>{\centering \arraybackslash}X|}}\hline
$x$		& 0	&1 	&2 	&3 	&4 	&5 	&6\\ \hline  
$f(x)$	&	&	&	&	&	&	&\\ \hline
\end{tabularx}

\medskip
 
		\item Construire la courbe représentative $\mathcal{C}$ de $f$ dans un repère orthogonal \Oij. On prendra pour unités graphiques : 2~cm pour 1 sur l'axe des abscisses et 4~cm pour 1 sur l'axe des ordonnées. 
		\item Résoudre graphiquement dans [0~;~6] l'équation $f(x) = 1$. 
Faire apparaître sur la figure les constructions utiles.
	\end{enumerate}
\end{enumerate}
 
\bigskip

\emph{B Calcul intégral}

\medskip
 
\begin{enumerate}
\item Soit $F$ la fonction définie sur l'intervalle [0~;~6] par : 

\[F(x) = \left(- 2x^2 -7x -7\right)\text{e}^{- x}.\]
 
 Démontrer que $F$ est une primitive de $f$ sur [0~;~6]. 
\item On note $I = \displaystyle\int_{0}^6 f(x)\:\text{d}x$. Démontrer que $I  = 7 - 121\text{e}^{- 6}$.
\end{enumerate}

\bigskip
 
\emph{C. Application des résultats des parties A et B.}

\medskip
 
Une société extrait du gravier pour la construction d'autoroutes. Elle envisage l'ouverture d'un nouveau site d'extraction. On admet, qu'au bout de $x$ centaines de jours d'exploitation, la production journalière sur ce site, exprimée en milliers de tonnes, est $f(x)$, où $f$ est la fonction qui a été définie au début de la partie A.
 
\begin{enumerate}
\item Déterminer au bout de combien de jours après l'ouverture du site, la production journalière sera maximale. Quelle est cette production maximale en milliers de tonnes ? 
\item Déterminer au bout de combien de jours après l'ouverture du site la production journalière après avoir atteint son maximum sera revenue à \np{1000}~tonnes. 
\item Déduire de la partie B. la valeur moyenne, $V_{m}$, de $f$ sur [0~;~ 6]. Arrondir $V_{m}$ à $10^{-3}$. 
\end{enumerate}
\end{document}