\documentclass[11pt,a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}%ATTENTION codage en utf8 ! 
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{picins}
\usepackage{caption}
\usepackage[normalem]{ulem}
\usepackage{multicol,diagbox,colortbl}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo}
%Tapuscrit : François Hache
% Relecture :  Denis Vergès
\usepackage{pst-all,pst-func}
\usepackage[left=3cm, right=3cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\newcommand{\vectt}[1]{\overrightarrow{\,\mathstrut\text{#1}\,}}
\newcommand{\barre}[1]{\overline{\,#1\vphantom{b}\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {STI2D},
pdftitle = {Métropole 11 septembre 2024},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
\newcommand{\e}{\,\text{e\,}}	%%%le e de l'exponentielle
\renewcommand{\d}{\,\text d}	%%%le d de l'intégration
\renewcommand{\i}{\,\text{i}\,}	%%%le i des complexes
\newcommand{\ds}{\displaystyle}
\setlength\parindent{0mm}
\setlength\parskip{5pt}

\begin{document}

\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat STI2D}
\lfoot{\small{Métropole Antilles--Guyane}}
\rfoot{\small{11 septembre 2024}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~Baccalauréat STI2D Épreuve d'enseignement de spécialité~\decofourright\\[10pt]Métropole Antilles--Guyane 11 septembre 2024}}

\vspace{0,25cm}

\textbf{\large Physique-Chimie et Mathématiques}

\end{center}

\vspace{0,25cm}



\textbf{\large EXERCICE 1 \hfill physique-chimie et mathématiques \hfill 4 points}

\medskip
\begin{center}
\textbf{Isolation phonique et réverbération}

\end{center}

La réverbération est un phénomène acoustique qui désigne la persistance d'un son dans un espace clos lorsque sa source a cessé d'émettre.

Pour atténuer ce phénomène, une solution consiste à installer des panneaux de matériaux absorbants acoustiques sur les murs. Cet exercice étudie les propriétés d'absorption acoustique de deux matériaux, la mousse de mélamine et le feutre acoustique.

\textbf{Donnée :} On rappelle la relation entre niveau sonore et intensité acoustique:
\[L = 10\log \dfrac{I}{I_0}\]

où:

\begin{itemize}
\item $L$ est le niveau sonore en dB ;
\item $I$ est l'intensité acoustique du son considéré en W \:$\cdot$~m$^{-2}$ ;
\item $I_0$ est l'intensité acoustique correspondant au seuil conventionnel d'audibilité, soit $10^{-12}$ W $\cdot$~m$^{-2}$;
\item log désigne le logarithme décimal.
\end{itemize}

\medskip

\begin{enumerate}
\item Calculer la valeur de l'intensité acoustique $I_1$ correspondant à un son de niveau sonore $L_1 = 85$~dB.

\medskip

Pour caractériser l'absorption des ondes sonores dans la mousse de mélamine, on place une source d'ondes sonores en contact avec une plaque de ce matériau, comme représenté sur la figure 1. On note $I(x)$ l'intensité acoustique de l'onde sonore après traversée d'une épaisseur $x$ de matériau absorbant.

\begin{center}
\psset{unit=1cm,arrowsize=2pt 3}
\begin{pspicture}(0,-0.6)(14,2.5)
\psframe(3.5,0.3)(7.6,2)
\rput(5.55,1.5){Source d'ondes}\rput(5.55,0.9){sonores}
\psframe[fillstyle=solid,fillcolor=cyan](7.6,0)(10.7,2.5)
\psline[linewidth=1.25pt,linecolor=blue](7.6,0)(7.6,2.5)
\psline[linewidth=1.25pt,linecolor=blue]{->}(7.6,1.15)(10.5,1.15)
\uput[d](9.05,1.15){Épaisseur $x$}
\piccaptionside
\rput(7,-0.2){Figure 1 - Émission d'une onde sonore dans une plaque de mousse de mélamine.}
\end{pspicture}
\end{center}

\medskip

Dans un modèle simple, on montre que l'intensité sonore $I(x)$ dans la mousse de mélamine vérifie la relation:

\[\dfrac{\text{d}I}{\text{d}x} = - \mu  I(x)\]

où $\mu$ est un coefficient caractéristique du matériau. Pour la mousse de mélamine, on a : 

$\mu = 0,262$ cm$^{-1}$.

Pour des valeurs de x, en cm, l'intensité acoustique I(x) peut donc être obtenue en résolvant l'équation différentielle:

\[(E) \::\quad y'=  0,262y\]

\item Déterminer les solutions sur $[0~;~+\infty[$ de l'équation différentielle $(E)$.
\item Montrer que la fonction $f$ définie sur $[0~;~+\infty[$ par 
\[f(x) = 3,2\cdot 10^{-4}\e^{-0,262x}\]

 est la solution particulière de $(E)$ vérifiant la condition initiale $f(O) = 3,2\cdot 10^{-4}$.
\item Résoudre sur $[0~;~+\infty[$ l'équation 
\[\e^{-0,262x} =0,5.\]

 Déterminer la distance de propagation $d$ au bout de laquelle l'intensité acoustique de l'onde est divisée par 2.

\end{enumerate}

\bigskip

\textbf{\large EXERCICE 1 \hfill mathématiques \hfill 4 points}

\medskip

\textbf{Dans cet exercice, les questions 1, 2, 3 et 4 sont indépendantes les unes des autres.}

\medskip

\textbf{Question 1}

\emph{Pour cette question, indiquer la lettre de la réponse exacte. Aucune justification n'est demandée.}

\medskip

Pour tout nombre réel $x > 0$, l'expression $3\ln (2x) - \ln (8)$ est égale à :

\begin{center}
\begin{tabularx}{\linewidth}{|*{4}{>{\centering \arraybackslash}X|}}\hline
A&B&C&D\\ \hline
$\ln \left(\dfrac2 x \right)$&$3\ln (x)$& $3\ln \left(\dfrac x4\right)$&$3\ln (2x - 8)$\\ \hline
\end{tabularx}
\end{center}

\textbf{Question 2}

\emph{Pour cette question, indiquer la lettre de la réponse exacte. Aucune justification n'est demandée.}

\medskip

Soit la fonction $g$ définie sur $\R$ par 
\[g(x) = x^2\e^{-2x}.\]

On admet que $g$ est dérivable sur $\R$ et on note $g'$ la fonction dérivée de $g$. Pour tout nombre réel $x$, on a :

\begin{center}
\begin{tabularx}{\linewidth}{|*{4}{>{\centering \arraybackslash}X|}}\hline
A&B&C&D\\ \hline
$g'(x) = 2x\e^{-2x}(1 - x)$& $g'(x) = -4x\e^{-2x}$&$g'(x) = 2x\e^{-2x}(1 + x)$&$g'(x) = - 2x^2\e^{-2x}$\\ \hline
\end{tabularx}
\end{center}

\textbf{Question 3}


Le plan complexe est rapporté à un repère orthonormé \Ouv. On désigne par i le nombre complexe de module 1 et d'argument $\dfrac{\pi}{2}$.

Soient les points A et B d'affixes respectives $z_{\text{A}} = 2\e^{\text{i}\frac{\pi}{}}$ et $z_{\text{B}} = -\sqrt 3 + \text{i}$.

Donner la forme algébrique de $z_{\text{A}}$ ainsi que la forme exponentielle de $z_{\text{B}}$.

\textbf{Question 4}

En faisant apparaître les étapes de calcul, calculer:

\[\displaystyle\int_0^{\frac{\pi}{2}} \cos (2x) \:\text{d}x.\]

\end{document}