\documentclass[11pt,a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}%ATTENTION codage en utf8 ! 
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{multicol,diagbox}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo}
%Tapuscrit : Denis Vergès
\usepackage{pst-all,pst-func}
\usepackage{esvect}
\usepackage[left=3cm, right=3cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\newcommand{\vectt}[1]{\overrightarrow{\,\mathstrut\text{#1}\,}}
\newcommand{\barre}[1]{\overline{\,#1\vphantom{b}\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {STL},
pdftitle = {Polynésie  19 juin 2024},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
\newcommand{\e}{\text{\,e\,}}	%%%le e de l'exponentielle
\renewcommand{\d}{\,\text d}	%%%le d de l'intégration
\renewcommand{\i}{\,\text{i}\,}	%%%le i des complexes
\newcommand{\ds}{\displaystyle}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat STL}
\lfoot{\small{Polynésie}}
\rfoot{\small{19 juin 2024}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}
\addtolength{\headheight}{\baselineskip}
\begin{center} {\Large \textbf{\decofourleft~Baccalauréat STL Biotechnologies~\decofourright\\[10pt]Polynésie -- 19  juin 2024}}

\end{center}

\vspace{0,25cm}

\textbf{\large EXERCICE 1 \hfill (physique-chimie et mathématiques)\hfill 4 points }

\medskip

\textbf{Stabilité d'un antibiotique}

\medskip

L'amoxicilline (noté ici AMOX) est un antibiotique qui possède un large spectre d'action
sur certaines infections bactériennes, mais son action peut être altérée par des enzymes
produites par certaines bactéries résistantes. C'est pour empêcher cela qu'on lui associe
très souvent l'acide clavulanique.

Cette association amoxicilline et acide clavulanique peut être utilisée sous forme de
poudre. Après ajout d'eau et agitation, on obtient une solution facilement assimilable.
Cependant l'amoxicilline et l'acide clavulanique sont peu stables en milieu aqueux : elles
subissent une réaction de dégradation avec l'eau (hydrolyse).

L'objectif de cet exercice est d'étudier quelques aspects de la cinétique de ces réactions
de dégradation par hydrolyse de ces deux espèces chimiques, lorsqu'elles sont prises
seules en solution aqueuse.

Donnée:
\begin{itemize}
\item $pK_A$ du couple acide clavulanique/ion clavulanate : $pK_A = 2,7$.
\end{itemize}

\textbf{Dégradation de l'amoxicilline seule en solution aqueuse}

\medskip

La dégradation de l'amoxicilline est étudiée au laboratoire, à 30\textcelsius{} et à un pH valant 3,5.

La valeur de la concentration initiale en amoxicilline vaut $C_0 = \np{1600}$ \textmu g $\cdot$ mL$^{-1}$

La concentration de l'amoxicilline à l'instant $t$, notée $C_{\text{Amox}}(t)$, est évaluée toutes les
vingt-quatre heures.

\begin{enumerate}
\item Donner la définition de la vitesse de disparition de l'amoxicilline, notée $v_{d, \text{Amox}}$.

On fait l'hypothèse que la dégradation de l'amoxicilline suit une loi cinétique d'ordre 1.

\item Établir l'équation différentielle du premier ordre vérifiée par la fonction $C_{\text{Amox}}(t)$. On notera $k_{\text{Amox}}$ la constante de vitesse.

Pour une loi cinétique d'ordre 1, les solutions générales $C(t)$ de l'équation différentielle
vérifient l'égalité $\ln \left(\frac{C(t)}{C(0)}\right) = - kt$ pour une certaine valeur de $k$.

Dans les conditions opératoires données, on obtient les résultats expérimentaux suivants :

\begin{tabularx}{\linewidth}{|*{6}{>{\centering \arraybackslash}X|}}
\hline
$t$ en $h$& 0 &24 &48& 72& 96\\\hline
$\ln \left(\frac{C_{\text{Amox}}(t)}{C_0}\right)$ & 0 &$- 0,18$& $-0,35$ &$-0,52$&$ -0,70$\\
\hline
\end{tabularx}
\end{enumerate}

Le graphique suivant représente le nuage de points expérimentaux et la modélisation
associée:

\begin{center}
\psset{xunit=0.125cm,yunit=10cm,labelFontSize=\scriptstyle,arrowscale=1.5,comma=true}
\begin{pspicture}(-1,-0.8)(120,0.2)
\multido{\n=0+4}{31}{\psline[linewidth=0.75pt,linecolor=lightgray](\n,-0.8)(\n,0.1)}
\multido{\n=-0.80+0.02}{46}{\psline[linewidth=0.75pt,linecolor=lightgray](0,\n)(120,\n)}
\psaxes[linewidth=0.95pt,Dx=20,Dy=0.1]{->}(0,0)(0,-0.8)(120,0.1)
\psaxes[linewidth=0.95pt,Dx=20,Dy=0.1](0,0)(0,-0.81)(120,0.1)
\psdots[dotstyle=square,fillcolor=red,dotscale =1.3,dotangle=45](0,0)(24,-0.18)(48,-0.35)(72,-0.52)(96,-0.70)
\uput[u](1114,0){$t$ en h}
\uput[d](82,-0.30){$R^2\approx 0.9998$}\uput[ur](1,0){$\ln\left(\frac{C_{\text{Amox}}(t)}{C_0}\right)$}
\uput[dl](96,-0.70){A}

\def\Func{x 0.0073 mul neg}
% \psline[linewidth=0.95pt,linecolor=violet,linestyle=dashed,ArrowInside=-> ]{->}(!0 /x 24.07 def \Func)(!24.07 /x 24.07 def \Func)(24.07,0)
\psplot[plotpoints=1000,linewidth=1.25pt,linecolor=blue]{0}{96}{\Func}
\end{pspicture}
\end{center}

\begin{enumerate}[resume]
\item Justifier que les résultats obtenus confirment l'hypothèse d'une loi cinétique d'ordre 1.

L'ajustement linéaire des points du relevé précédent permet d'obtenir une droite passant
par les points 0(0~;~0) et A$(96~;~- 0,70)$.
\item Déterminer une valeur arrondie à $10^{-4}$ du coefficient directeur de la droite (OA).
En utilisant cette valeur arrondie, en déduire que la droite (OA) a pour équation :
\[y = -0,0073t\]
\item L'ajustement précédent nous permet d'écrire $\ln \left(\frac{C_{\text{Amox}}(t)}{C_0}\right)  = -0,0073t$, pour tout
$t$ appartenant à $ [0~;~+\infty[$.
	\begin{enumerate}
		\item  En déduire que $C_{\text{\text{Amox}}}(t)= \np{1 600}\times \e^{-\np{0,0073}t}$ pour tout $t$ appartenant à $[0~;~+ \infty[$.
		\item Déterminer la limite de la fonction $C_{\text{Amox}}$ en +$\infty[$.
		\item Dresser le tableau des variations de la fonction $C_{\text{Amox}}$ sur $[0~;~+\infty[$.
	\end{enumerate}
\end{enumerate}

\textbf{Dégradation de l'ion clavulanate seul en solution aqueuse}

\medskip

Pour l'acide clavulanique, le suivi temporel de la concentration $C_{\text{Clav}}(t)$ au cours du
temps est réalisé dans les mêmes conditions opératoires que précédemment.

\begin{enumerate}[resume]
\item Justifier que dans ces conditions opératoires, l'espèce prédominante est l'ion clavulanate.
\end{enumerate}

\medskip

La seconde expérience conduit aux observations suivantes :

\begin{itemize}
\item [$\bullet$] valeur de la concentration initiale en ion clavulanate : $C'_0 = 320~ \text{mol}\cdot \text{L}^{-1}$ ;
\item [$\bullet$] valeur de la constante de vitesse de la réaction : $k_{\text{Clav}} = \np[h^{-1}]{0, 19} $.
\end{itemize}
Les résultats expérimentaux, traités avec la même méthode d'ajustement, permettent
d'établir la relation $\ln \left(\frac{C_{\text{Clav}}(t)}{320}\right) = -0,19t$.
\begin{enumerate}[resume]
\item Comparer le coefficient directeur de la droite (OA) à celui de la droite d'équation :
\[y= -0,19t\]
\item Conclure en comparant la cinétique de dégradation de l'ion clavulanate seul à celle
de l'amoxicilline seule.
\end{enumerate}
\vspace{0.5cm}

\textbf{\large EXERCICE 3 \quad  (mathématiques)\hfill 4 points}

\medskip

Dans cet exercice, on considère la fonction $f$ définie sur $\R$ par : 
\[f(x) = 5\e^{2x+1}.\]

\begin{enumerate}

\item  Parmi les programmes suivants, écrits en langage Python, un seul affiche les images
par $f$ des réels 0~;~0, 1~;~0,2~;~$\dots$~;~0,9.

Indiquer sans justifier sur la copie la lettre correspondant à ce programme.

\vspace{0.3cm}

\textbf{a.~}~~\fbox{
\begin{minipage}{4cm}
from math import exp\\ 
for k in range(10) :\\
\phantom{xxxxxx} x=k/10       \\
\phantom{xxxxxx}y=5*exp(2*x+1)  \\        
\phantom{xxxxxx}print(y)    \\    
\end{minipage}
}
\hspace{3cm}
\textbf{b.~}~~\fbox{
\begin{minipage}{4cm}
from math import exp\\
for k in range(10) :\\
\phantom{xxxxxx}y=5*exp(2*k+1)\\
print(y)\\
\end{minipage}
}

\textbf{c.~}~~\fbox{\begin{minipage}{4cm}
from math import exp\\
for k in range(0,9) :\\
\phantom{xxxxxx}y=5*exp(2*x+1)\\
\phantom{xxxxxx}print(y)\\
\end{minipage}}
\hspace{3cm}
\textbf{d.~}~~\fbox{
\begin{minipage}{4cm}
from math import exp\\
for k in range(0,9) :\\
\phantom{xxxxxx}y=5*exp(2*x+1)\\
print(y)\\
\end{minipage}
}
\medskip

\item Résoudre dans $\R $ l'équation $f(x) = 5$.
\item L'affirmation suivante est-elle vraie ou fausse ? Justifier.

\og Tout nombre réel $x$ négatif ou nul a une image par $f$ inférieure ou égale à 5. \fg
\item On considère la fonction $F$ définie sur $\R$ par : 
\[F(x) = ~\dfrac{5}{2}\e^{2 x+ 1}.\]
	\begin{enumerate}
		\item Montrer que la fonction $F$ est une \textbf{primitive } sur $\R$ de la fonction $f$.
		\item En déduire la valeur exacte, puis la valeur approchée à l'entier près, de :
\[\int_0^1 f(x)\mathrm{d}x\]
	\end{enumerate}
\end{enumerate}
\end{document}
