\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet} 
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx,multido}
\usepackage[normalem]{ulem}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{colortbl}
\usepackage{ulem}
\usepackage{lscape}
\usepackage{dcolumn}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{textcomp}
\newcommand{\euro}{\eurologo{}}
\usepackage{pstricks,pst-plot,pst-tree,pst-func,pstricks-add}
\usepackage{tikz}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
%Tapuscrit : Denis Vergès et François Hache (spé)
%Relecture : François Hache
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {TS Métropole--La Réunion},
pdftitle = {11 septembre 2020},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[np]{numprint}
\usepackage[frenchb]{babel}
\frenchbsetup{StandardLists=true}
\newcommand{\e}{\,\text{e\,}}%%%   le e de l'exponentielle
\renewcommand{\d}{\,\text d}%%%    le d de l'intégration
\renewcommand{\i}{\,\text{i}\,}%%% le i des complexes
\newcommand{\ds}{\displaystyle}
\begin{document}
\setlength\parindent{0mm}
\setlength\parskip{5pt}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat S}
\lfoot{\small{Métropole La Réunion Mayotte}}
\rfoot{\small 11 septembre 2020}
\pagestyle{fancy}
\thispagestyle{empty}
\begin{center}
{\Large \textbf{\decofourleft~Corrigé du baccalauréat S Métropole--La Réunion~\decofourright\\[6pt]11 septembre 2020}}
\end{center}

\vspace{0,5cm}

\textbf{Exercice 1 \hfill  5 points}

\textbf{Commun à  tous les candidats}

\medskip

\textbf{Partie A}

\medskip

On considère la fonction $f$ définie sur $\R$ par :
$f(x) = \dfrac{2\text{e}^x}{\text{e}^x + 1}$.

On donne ci-dessous la courbe représentative $\mathcal{C}$ de la fonction $f$ dans un repère orthonormé.

\begin{center}

\psset{unit=0.65cm}
\begin{pspicture}(-8,-3)(14,3)
\psgrid[gridlabels=0pt,subgriddiv=1,gridwidth=0.1pt]
\psaxes[linewidth=1.25pt,labelFontSize=\scriptstyle]{->}(0,0)(-8,-3)(14,3)
\psaxes[linewidth=1.25pt,labelFontSize=\scriptstyle](0,0)(-8,-3)(14,3)
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=red]{-7}{14}{2.71828 x exp  2 mul 2.71828 x exp 1 add div}
\uput[d](13.5,2){\red $\mathcal{C}$}
\end{pspicture}
\end{center}

%\medskip

\begin{enumerate}
\item %Calculer la limite de la fonction $f$ en moins l'infini et interpréter graphiquement le résultat.
On sait que $\displaystyle\lim_{x \to - \infty} \text{e}^x = 0$, donc $\displaystyle\lim_{x \to - \infty} f(x) = \dfrac{0}{1} = 0$.

On en déduit que la courbe $\mathcal{C}$ admet l'axe des abscisses comme asymptote horizontale en $-\infty$.

\item% Montrer que la droite d'équation $y = 2$ est asymptote horizontale à la courbe $\mathcal{C}$.
$f(x) = \dfrac{2\e^x}{\e^x + 1} 
=   \dfrac{2\e^x \times \e^{-x}}{\left (\e^x + 1\right ) \times \e^{-x}} 
= \dfrac{2}{1+\e^{-x}}$

Or $\displaystyle\lim_{x \to + \infty} \text{e}^{-x} = 0$, donc $\displaystyle\lim_{x \to + \infty} f(x) = \dfrac{2}{1} = 2$.

Ceci montre que la droite d'équation $y = 2$ est asymptote horizontale à la courbe $\mathcal{C}$ en $+\infty$.

\item %Calculer $f'(x)$, $f'$ étant la fonction dérivée de $f$, et vérifier que pour tout nombre réel $x$ on a : \[f'(x) = \dfrac{f(x)}{\text{e}^x + 1}.\]
%Sur $\R$, \:$\text{e}^x > 0$, donc $\text{e}^x  + 1 \geqslant 1 > 0$ ; le dénominateur n'étant jamais nul la fonction $f$ est dérivable sur $\R$ et sur cet intervalle :
La fonction $f$ est dérivable sur $\R$ comme quotient de fonctions dérivables sur $\R$.

$f'(x) = \dfrac{2\text{e}^x\left(\text{e}^x + 1 \right) - \text{e}^x \times 2\text{e}^x}{\left(\text{e}^x + 1\right)^2}  = \dfrac{2\text{e}^x}{\left(\text{e}^x + 1\right)^2} =  \dfrac{2\text{e}^x}{\text{e}^x + 1}\times \dfrac{1}{\text{e}^x + 1} = \dfrac{f(x)}{\text{e}^x + 1}$.
\item% Montrer que la fonction $f$ est croissante sur $\R$.
Pour tout $x$, $\e^{x}>0$ donc, comme $f(x)$ est le quotient de deux réels supérieurs à zéro, \:$f(x) > 0$; $f'(x)>0$ comme quotient de deux nombres positifs : la fonction $f$ est donc strictement croissante sur $\R$ de 0 à 2.

\item% Montrer que la courbe $\mathcal{C}$ passe par le point I(0~;~1) et que sa tangente en ce point a pour coefficient directeur $0,5$.
On a $f(0) = \dfrac{2}{1 + 1} = \dfrac{2}{2} = 1$, donc I\,$(0~;~1) \in \mathcal{C}$.

La tangente à $\mathcal{C}$ au point I a pour coefficient directeur 
$f'(0) = \dfrac{f(0)}{\e^{0}+1} = \dfrac{1}{2}$.
\end{enumerate}

%\bigskip
\newpage

\textbf{Partie B}

\medskip

\parbox{0.7\linewidth}{Une entreprise souhaite fabriquer de façon automatisée des flûtes (verres à pied) de forme allongée de contenance $12,5$~cL. Chaque flûte est composée de 
deux parties  comme sur l'illustration ci-contre : un pied, en verre plein, et un contenant de $12,5$~cL.

À l'aide de la fonction $f$ définie dans la \textbf{partie A}, le fabricant modélise le profil du contenant de la flûte de la manière décrite ci-dessous.} 
\hfill
\parbox{0.25\linewidth}
{\scalebox{0.8}{
\psset{unit=1cm}
\begin{pspicture}(-0.5,0)(3,5)
%\psgrid
\def\piedg{\psset{unit=0.2cm}\psplot[plotpoints=2000,linewidth=1.25pt]{-5}{4}{2.71828 x exp  2 mul 2.71828 x exp 1 add div}}
\def\piedd{\psset{unit=0.2cm}\psplot[plotpoints=2000,linewidth=1.25pt]{-5}{4}{2.71828 x exp  2 mul 2.71828 x exp 1 add div neg}}
\psellipse(0.8,0.2)(0.4,0.1)
\psellipse(0.8,1.4)(0.2,0.08)
\psellipse(0.8,4.8)(0.485,0.2)
\rput{90}(0.7,1.6){\piedg}
\rput{90}(0.9,1.6){\piedd}
\psline[linewidth=1.25pt](1.28,2.4)(1.28,4.8)
\psline[linewidth=1.25pt](0.31,2.4)(0.31,4.8)
\psline[linewidth=1.25pt](0.7,0.2)(0.7,0.6)
\psline[linewidth=1.25pt](0.89,0.2)(0.89,0.6)
\psline[linestyle=dashed](1.2,0.2)(2,0.2)
\psline[linestyle=dashed](1.2,1.4)(2,1.4)
\psline[linestyle=dashed](1.2,4.8)(2,4.8)
\psline{<->}(2,0.2)(2,1.4)\psline{<->}(2,1.4)(2,4.8)
\uput[r](2,3.1){\footnotesize contenant}
\uput[r](2,0.8){\footnotesize pied}
\end{pspicture}
}}

Soit A un point de $\mathcal{C}$ d'abscisse $a$ strictement positive. La rotation autour de l'axe des abscisses appliquée à la partie de $\mathcal{C}$  limitée par les points I et A engendre une surface modélisant le contenant de la flûte en prenant pour unité 1 cm. 

Ainsi $x$ et $f(x)$ représentent des longueurs en centimètres et l'objectif de cette partie est de déterminer la valeur de $a$ pour que le volume du contenant soit égal à $12,5$~cL.

\begin{center}
\psset{unit=0.65cm}
\begin{pspicture}(-8,-3)(14,3)
\psgrid[gridlabels=0pt,subgriddiv=1,gridcolor=lightgray]
\psaxes[linewidth=1.25pt,labelFontSize=\scriptstyle]{->}(0,0)(-8,-3)(14,3)
\psplot[plotpoints=2000,linewidth=1.25pt,linestyle=dashed]{-5}{0}{2.71828 x exp  2 mul 2.71828 x exp 1 add div}
\psplot[plotpoints=2000,linewidth=1.25pt]{0}{9.3}{2.71828 x exp  2 mul 2.71828 x exp 1 add div}
\psplot[plotpoints=2000,linewidth=1.25pt]{0}{9.3}{2.71828 x exp  2 mul 2.71828 x exp 1 add div neg}
\psellipse(9.3,0)(1,2)
\psellipse(0,0)(0.6,1)
\uput[u](9.3,2){A}\uput[ul](0,1){I}
\psline[linewidth=.2pt](9.3,0)(9.3,2)\uput[d](9.3,0){$a$}
\uput[u](8,2){$\mathcal{C}$}
\end{pspicture}
\end{center}

%\begin{center}
%\begin{tabular}{|c|}\hline
%Une unité représente 1 cm. \\
%La valeur de $a$ utilisée sur le graphique ci-dessus ne correspond pas à la valeur cherchée.\\ \hline
%\end{tabular}
%\end{center}
%\medskip

Le réel $a$ étant strictement positif, on admet que le volume $V(a)$ de  ce solide en cm$^3$ est donné par la formule :
$V(a) = \pi\displaystyle\int_0^a (f(x))^2\:\text{d}x$.

\begin{enumerate}
\item% Vérifier, pour tout nombre réel $x \geqslant 0$, l'égalité : \[(f(x))^2 = 4\left(\dfrac{\text{e}^x}{\text{e}^x + 1} + \dfrac{- \text{e}^x}{\left(\text{e}^x + 1\right)^2}\right).\]
$4\left(\dfrac{\text{e}^x}{\text{e}^x + 1} + \dfrac{- \text{e}^x}{\left(\text{e}^x + 1\right)^2}\right) 
= 4\left(\dfrac{\text{e}^x\left(\text{e}^x + 1 \right) - \text{e}^x}{\left(\text{e}^x + 1\right)^2} \right) 
= 4\left(\dfrac{\text{e}^{2x} + \text{e}^x - \text{e}^x}{\left(\text{e}^x + 1\right)^2} \right) 
= 4\left(\dfrac{\text{e}^{2x}}{\left(\text{e}^x + 1\right)^2} \right)  \\
\phantom{4\left(\dfrac{\text{e}^x}{\text{e}^x + 1} + \dfrac{- \text{e}^x}{\left(\text{e}^x + 1\right)^2}\right)}
= 4\left(\dfrac{\text{e}^x}{\text{e}^x + 1} \right)^2 = \left(\dfrac{2\text{e}^x}{\text{e}^x + 1} \right)^2
= (f(x))^2$

\item   On détermine une primitive sur $\R$  de chaque fonction:
$g : x \longmapsto \dfrac{\text{e}^x}{\text{e}^x + 1}$ et $h : x \longmapsto \dfrac{-\text{e}^x}{\left(\text{e}^x + 1\right)^2}$.

\begin{list}{\textbullet}{Si $u(x) = \e^x + 1$, alors $u'(x) = \e^x$:}
\item  on a $g(x)=\dfrac{u'(x)}{u(x)}$ qui a pour primitive $x \longmapsto\ln\left |u(x) \right |$.

Or, pour tout $x$, $\e^x + 1 >0 $, donc la  fonction $g$ a pour primitive la fonction $x \longmapsto \ln \left(\text{e}^x + 1\right)$ ;

\item  on a $h(x)= \dfrac{-u'(x)}{(u(x))^2}$ qui a pour primitive $x \longmapsto\dfrac{1}{u(x)}$.

Donc la fonction $h$ a pour primitive la fonction $x \longmapsto\dfrac{1}{\e^x + 1}$.
\end{list}

\item%  En déduire que pour tout réel $a > 0$ : $V(a) = 4\pi\left[\ln \left(\dfrac{\text{e}^a + 1}{2} \right) + \dfrac{1}{\text{e}^a + 1}  - \frac{1}{2}\right]$.
D'après les résultats précédents :

$V(a) = \pi\displaystyle\int_0^a (f(x))^2\:\text{d}x
= \pi\displaystyle\int_0^a 4\left(\dfrac{\text{e}^x}{\text{e}^x + 1} + \dfrac{- \text{e}^x}{\left(\text{e}^x + 1\right)^2}\right)\:\text{d}x 
= \pi\displaystyle\int_0^a 4\dfrac{\text{e}^x}{\text{e}^x + 1} \:\text{d}x + \pi\displaystyle\int_0^a 4 \dfrac{- \text{e}^x}{\left(\text{e}^x + 1\right)^2}\text{d}x\\[5pt]
\phantom{V(a)}
= \pi\left[4\ln \left(\text{e}^x + 1\right)\vphantom{\dfrac{1}{\text{e}^x + 1}}\right]_0^a + \pi\left[4 \dfrac{1}{\text{e}^x + 1}\right]_0^a 
= 4\pi\ln \left(\text{e}^a + 1\right) - 4\pi\ln 2 + \dfrac{4\pi}{\text{e}^a + 1} - 4\pi\dfrac{1}{2}\\[5pt]
\phantom{V(a)} 
= 4\pi\left[\ln \left(\dfrac{\text{e}^a + 1}{2} \right)  + \dfrac{1}{\text{e}^a + 1} - \dfrac{1}{2}\right]$.

\item On cherche une valeur approchée de $a$ à $0,1$  près, sachant qu'une flûte doit contenir 12,5~cL donc 125 cm$^3$.

La calculatrice donne $V(11,1403) \approx 125$ ; donc $a \approx 11,1$ à $10^{-1}$ près.

\end{enumerate}

\bigskip

\textbf{Partie C}

\medskip

Un client commande un lot de $400$ flûtes de $12,5$~cL et constate que $13$ d'entre elles ne sont pas conformes aux caractéristiques annoncées par le fabricant.
Le responsable des ventes lui avait pourtant affirmé que 98\,\% des flûtes vendues par l'entreprise était conforme.

%Le lot de ce client permet-t-il, aux risques de 5\,\%, de mettre en doute l'affirmation du responsable ?

La fréquence de flûtes conformes est $p = 0,98$.

$n=400\geqslant 30$, $np=392\geqslant 5$ et $n\left (1-p\right )=9\geqslant 5$
donc on peut établir un intervalle de fluctuation asymptotique eu risque de  5\,\%:

 $I_{400} = \left[p - 1,96\dfrac{\sqrt{p(1 - p)}}{\sqrt{n}}~;~p + 1,96\dfrac{\sqrt{p(1 - p)}}{\sqrt{n}}\right]\\[7pt]
 \phantom{I_{400}}
  = \left[0,98 - 1,96\dfrac{\sqrt{0,98 \times 0,02}}{20}~;~0,98 + 1,96\dfrac{\sqrt{0,98 \times 0,02}}{20}\right] 
  \approx \left [0,966~;~0,994\strut\right ]$.

Avec 13 flûtes non conformes, la proportion de flûtes conformes est $\dfrac{387}{400} =\np{0,9675}$.

Comme $\np{0,9675} \in \left [0,966~;~0,994\strut\right ]$, le client ne peut pas mettre en doute l'affirmation du responsable.

\vspace{1cm}

\textbf{Exercice 2 \hfill  6 points}

\textbf{Commun à  tous les candidats}

\medskip

\textbf{Partie A}

\medskip

Une machine fabrique des boules destinées à un jeu de hasard. \\
La masse en grammes, de chacune de ces boules peut-être modélisée par une variable aléatoire $M$ suivant une loi normale d'espérance $52$ et d'écart type $\sigma$.\\
Les boules dont la masse est comprise entre $51$ et $53$ grammes sont dites conformes.

\medskip

\begin{enumerate}
\item Avec les réglages initiaux de la machine on a $\sigma = 0,437$.

La probabilité qu'une boule  fabriquée par cette machine soit conforme est  $0,978$ au millième près, soit $1$ à $10^{-1}$ près.

\item On considère que la machine est correctement réglée  si au moins 99\,\% des boules qu'elle fabrique sont conformes.

Déterminer une valeur approchée de la plus grande valeur de $\sigma$ qui permet d'affirmer que la machine est correctement réglée revient à trouver $\sigma$ tel que $P(51 \leqslant  M \leqslant 53) \geqslant 0,99$.

Considérons la variable aléatoire $Y$ telle que $Y = \dfrac{M - 52}{\sigma}$ qui suit donc la loi normale centrée réduite  et on a donc 

$P(51 < M < 53) = 0,99 \iff P( - 1 \leqslant M - 52 \leqslant 1) \geqslant 0,99 \iff 
P\left(\dfrac{-1}{\sigma} \leqslant \dfrac{M - 52}{\sigma} \leqslant \dfrac{1}{\sigma}\right) \geqslant 0,99 \iff $

$2P\left(Y \leqslant \dfrac{1}{\sigma}\right) \geqslant 0,99 \iff 2P\left(Y \leqslant \dfrac{1}{\sigma}\right) \geqslant 1,99 \iff 
P\left(Y \leqslant \dfrac{1}{\sigma}\right) \geqslant 0,995$.

La calculatrice donne $\dfrac{1}{\sigma} \geqslant \np{2,5758}$ soit $\sigma \leqslant 0,388$.

La plus grande valeur de sigma est donc $0,388$ au millième près.
\end{enumerate}

\medskip

\textbf{Partie B}

\medskip

La pesée des boules se fait sur des balances électroniques de précision. Chaque jour, on vérifie que la balance n'est pas déréglée. La durée, en jour, d'utilisation de ces balances avant dérèglement est modélisée par une variable aléatoire $T$ qui suit une loi exponentielle de paramètres $\lambda$. La courbe représentative de la fonction densité de cette variable aléatoire $T$ est donnée ci-dessous.

\begin{center}
\psset{unit=0.2cm,yunit=30cm,comma=true}
\begin{pspicture}(60,0.12)
\pscustom[fillstyle=solid,fillcolor=lightgray!50]
{\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=blue]{0}{11}{2.71828 0.054 x mul neg exp 0.054 mul}
\psline(11,0)(0,0)}
\psaxes[linewidth=1.25pt,Dx=100,Dy=0.05,ticks=y,subticks=5](0,0)(0,0)(60,0.12)
\uput[d](11,0){11}
\psplot[plotpoints=2000,linewidth=1.25pt,linecolor=blue]{0}{60}{2.71828 0.054 x mul neg exp 0.054 mul}
\end{pspicture}
\end{center}

\begin{enumerate}
\item 
	\begin{enumerate}
		\item% Par lecture graphique, donner un encadrement de $\lambda$ d'amplitude $0,01$.
On sait que la fonction densité est définie par $f(t) = \lambda\text{e}^{- \lambda t}$ ; donc $f(0) = \lambda$.

On lit sur la figure : $0,05 < \lambda < 0,06$.

		\item L'aire du domaine grisé, en unité d'aire, est égale à $0,45$ et correspond à $P(M \leqslant 11)$. %Déterminer la valeur exacte de $\lambda$.
		
On sait que l'aire est égale, en unité d'aire, à l'intégrale de la fonction densité sur l'intervalle [0~;~11], soit :

$0,45 =\displaystyle \int_0^{11} \lambda\text{e}^{- \lambda t}\:\text{d}t = \left[-\text{e}^{- \lambda t} \right]_0^{11} = - \text{e}^{- 11\lambda} - (- 1) \iff 0,45 = 1 - \text{e}^{- 11\lambda} \iff 0,55 = \text{e}^{- 11\lambda}$

soit, par croissance de la fonction logarithme népérien :

$\ln 0,55 = - 11\lambda \iff \lambda = \dfrac{\ln 0,55}{- 11} = - \dfrac{\ln 0,55}{11}\approx \np{0,054349} \approx 0,054$ au millième près.
	\end{enumerate}
\end{enumerate}	

Dans la suite, on prendra $\lambda = 0,054$.

\begin{enumerate}[resume]
\item La durée moyenne d'utilisation d'une balance sans qu'elle ne se dérègle est:

$E(T) = \dfrac{1}{\lambda} \approx \dfrac{1}{0,054} \approx 18,51$ soit environ 19~(jours).

\item Une balance est mise en service le 1\up{er} janvier 2020. Elle fonctionne sans se dérégler du 1\up{er} au 20 janvier inclus.

La probabilité qu'elle fonctionne sans se dérégler jusqu'au $31$ janvier inclus est:

$P_{P(T \geqslant 20)}(T \geqslant 20 + 11) = P (T \geqslant 11) = \text{e}^{- 11\lambda} \approx \text{e}^{- 11 \times 0,054}\approx 0,552$.
\end{enumerate}

%\medskip
\newpage

\textbf{Partie C}

\medskip

On dispose de deux urnes $U$ et $V$ contenant des boules fabriquées comme précédemment. Sur chacune des boules est inscrit l'un des nombres $- 1$, 1, ou 2.

L'urne $U$ contient une boule portant le nombre 1 et trois boules portant le nombre $- 1$. 

L'urne $V$ contient une boule portant le nombre 1 et trois boules portant le nombre $2$. 

On considère un jeu lequel chaque partie se déroule de la manière suivante : dans un premier temps on tire au hasard une boule dans l'urne $U$, on note $x$ le nombre inscrit sur cette boule puis on la met dans l'urne $V$. Dans un deuxième temps, on tire au hasard une boule dans l'urne $V$ et on note $y$ le nombre inscrit sur cette boule.

On considère les évènements suivants :

\setlength\parindent{9mm}
\begin{itemize}[label=\textbullet]
\item $U_1$ : \og on tire une boule portant le nombre 1 dans l'urne $U$, c'est-à-dire $x = 1$ \fg{} ;
\item $U_{-1}$ : \og on tire une boule portant le nombre $- 1$ dans l'urne $U$ c'est-à-dire $x =  -1$ \fg{} ;
\item $V_2$ : \og on tire une boule portant le nombre 2 dans l'urne $V$ c'est-à-dire $y = 2$ \fg{}; 
\item $V_1$ : \og on tire une boule portant le nombre 1 dans l'urne $V$ c'est-à-dire $y = 1$ \fg{};
\item $V_{-1}$ : \og on tire une boule portant le nombre $- 1$ dans l'urne $V$ \fg{}, c'est-à-dire $y = -1$ \fg.
\end{itemize}
\setlength\parindent{0mm}

\medskip

\begin{enumerate}
\item On complète l'arbre pondéré:

\begin{center}
\pstree[treemode=R,nodesepB=3pt,levelsep=2.5cm]{\TR{}}
{\pstree{\TR{$U_1$~~}\ncput*{\red $\frac{1}{4}$}}
	{\TR{$V_2$}\naput{\red $\frac{3}{5}$}
	\TR{$V_1$} \nbput{\red $\frac{2}{5}$}
	}
\pstree{\TR{$U_{-1}$~~}\ncput*{\red $\frac{3}{4}$}}
	{\TR{$V_2$}\naput{$\frac{3}{5}$}
	\TR{$V_1$} \ncput*{\red $\frac{1}{5}$}
	\TR{$V_{-1}$} \nbput{\red $\frac{1}{5}$}
	}
}
\end{center}

\item Dans ce jeu, à chaque partie on associe le nombre complexe $z =  x +  \text{i}y$.

On calcule les probabilités des évènements suivants.

	\begin{enumerate}
		\item $A$ : \og $z = - 1 - \text{i}$\fg{} ;
		
Cela signifie que $x = - 1$ et $y = - 1$, donc que 

$P(A) = P\left(U_{-1} \cap V_{-1}\right) = P\left(U_{-1} \right) \times P_{U_{-1}}\left(V_{—1} \right) = \dfrac{3}{4} \times \dfrac{1}{5} = \dfrac{3}{20} = \dfrac{15}{100} = 0,15$.
		\item $B$ : \og $z$ et solution de l'équation $t^2 + 2t + 5 = 0$ \fg{};
		
Avec $\Delta = 4 - 20 = - 16 = (4\text{i})^2 < 0$, on sait qu'il y a deux solutions complexes :

$z_1 = \dfrac{- 2 + 4\text{i}}{2} = - 1 + 2\text{i}$ et $z_2 = \dfrac{- 2 - 4\text{i}}{2} = - 1 - 2\text{i}$.

Dans le premier cas : $x = - 1$ et $y = 2$ et dans le second $x = - 1$ et $y = -2$ ce qui n'arrive pas.

On a donc $P(B) = P\left(U_{-1} \cap V_2\right) = P\left(U_{-1}\right) \times P_{U_{-1}}\left(V_2\right) = \dfrac{3}{4} \times \dfrac{3}{5} = \dfrac{9}{20} = \dfrac{45}{100} = 0,45$.
		\item $C$ : \og Dans le plan complexe rapporté à un repère orthonormal \Ouv{} le point $M$ d'affixe $z$ appartient au disque de centre O et de rayon 2 \fg.
		
Les coordonnées $(x~;~y)$ d'un point $M$ du disque vérifient $x^2 + y^2 \leqslant 4$.

$\bullet~~$si $x = 1$ et $y=1$, alors $x^2+y^2 = 2 \leqslant 4$;
$P(U_1\cap V_1)= \dfrac{1}{4}\times \dfrac{2}{5} = \dfrac{2}{20}$.

$\bullet~~$si $x = - 1$ et $y=-1$, alors $x^2+y^2 = 2\leqslant 4$;
$P(U_{-1}\cap V_{-1})= \dfrac{3}{4}\times \dfrac{1}{5} = \dfrac{3}{20}$.

$\bullet~~$si $x = - 1$ et $y=1$, alors $x^2+y^2 = 2\leqslant 4$;
$P(U_{-1}\cap V_{1})= \dfrac{3}{4}\times \dfrac{1}{5} = \dfrac{3}{20}$.

Conclusion: $P(C) = \dfrac{2}{20} + \dfrac{3}{20} + \dfrac{3}{20} = \dfrac{8}{20} = \dfrac{2}{5}$.
	\end{enumerate}
\item Lors d'une partie, on obtient le nombre 1 sur chacune des boules tirées. On va montrer que le nombre complexe $z$ associé à cette partie vérifie $z^{\np{2020}} = - 2^{\np{1010}}$.

On a donc $z = 1 + 1\text{i} = 1 + \text{i}$, donc$|z|^2 = 1^2 + 1^2 = 2$. 

On peut écrire $z$ sous la forme :
$z = \sqrt{2}\left(\dfrac{\sqrt{2}}{2} + \text{i}\dfrac{\sqrt{2}}{2}\right) = \sqrt{2}\left(\cos \frac{\pi}{4}  + \text{i}\sin \frac{\pi}{4}\right) = \sqrt{2}\e^{\text{i}\frac{\pi}{4}}$.

Il en résulte que : $z^{\np{2020}} = \left(\sqrt{2}\text{e}^{\text{i}\frac{\pi}{4}}\right)^{\np{2020}} = \left (\sqrt{2}\right )^{\np{2020}}\times \left (\e^{\i\frac{\pi}{4}}\right )^{\np{2020}}$.

$\bullet~~$ $\left(\sqrt{2}\right)^{\np{2020}} = \left(\sqrt{2}\right)^{2 \times \np{1010}} = \left(\sqrt{2}^{2}\right)^{\np{1010}} = 2^{\np{1010}}$

$\bullet~~$ $\left(\e^{\i\frac{\pi}{4}}\right)^{\np{2020}} = \e^{\i\frac{\np{2020}\pi}{4}} = \e^{505\i\pi} = \e^{\i\pi}\times \e^{504\i\pi} 
= -1 \times \left (\e^{2\i\pi}\right )^{252} = -1\times 1^{252} = - 1$. 

Finalement : $z^{\np{2020}} = - 2^{\np{1010}}$.
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 3 \hfill 4 points}

\textbf{Commun à  tous les candidats}

\medskip

%\emph{L'objectif de cet exercice est de déterminer les position relative de différents objets de l'espace}
%
%\medskip

L'espace est rapporté à un repère orthonormé \Oijk.

On considère les points A(1~;~1~;~4) ; B(4~;~2~;~5) ; C$(3~;~0~;~- 2)$ et J(1~;~4~;~2).

\smallskip

\hspace*{-0.22cm}\begin{tabular}{l l}
On note :& $\bullet~~$ $\mathcal{P}$ le plan passant par les points A, B et C ;\\
&$\bullet~~$ $\mathcal{D}$ la droite passant par le point J et de vecteur directeur $\vect{u}\begin{pmatrix}1\\1\\3\end{pmatrix}$.
\end{tabular}

\medskip

\begin{enumerate}
\item Position relative de $\mathcal{P}$ et de $\mathcal{D}$
	\begin{enumerate}
		\item On montrer que le vecteur $\vect{n}\begin{pmatrix}1\\ -4\\1\end{pmatrix}$  est normal à $\mathcal{P}$.
		
On a $\vect{\text{AB}}\begin{pmatrix} 3\\1\\1\end{pmatrix}$ ; $\vect{\text{AC}}\begin{pmatrix} 2\\-1\\-6\end{pmatrix}$.

$\bullet~~$ $\vect{\text{AB}} \cdot \vect{n} = 3 - 4  + 1 = 0$ : les vecteurs sont orthogonaux ;

$\bullet~~$  $\vect{\text{AC}} \cdot \vect{n} = 2 + 4  - 6 = 0$ : les vecteurs sont orthogonaux.
 
Le vecteur $\vect{n}$, orthogonal à deux vecteurs manifestement non colinéaires du plan (ABC), est donc un vecteur normal à ce plan.

		\item On détermine une équation cartésienne du plan $\mathcal{P}$.
		
%On sait que $M(x~;~y~;~z) \in (\text{ABC}) \in 
Le vecteur $\vect{n}$ est normal au plan (ABC) donc le plan (ABC) a une équation de la forme  $x - 4y + z + d  = 0$, avec $d \in \R$.

$\text{A}(1~;~1~;~4) \in (\text{ABC})$ donc $1 - 4 + 4 + d  = 0 \iff d = - 1$.

Donc le plan (ABC) a pour équation: $x - 4y + z - 1  = 0$

		\item On va montrer que $\mathcal{D}$ est parallèle à $\mathcal{P}$.
		
		$\vect{u} \cdot \vect{n} = 1 - 4 + 3 = 0$ : les vecteurs $\vect{u}$ (vecteur directeur de $\mathcal{D}$)  et  $\vect{n}$ (vecteur normal à $\mathcal{P}$) sont orthogonaux ce qui montre que 	$\mathcal{D}$ est parallèle à $\mathcal{P}$.
	\end{enumerate}

On rappelle que, un point  I et un nombre réel strictement positif $r$ étant donnés, la sphère de centre I  et de rayon $r$ est l'ensemble des points $M$ de l'espace vérifiant I$M = r$.

On considère le point I(1~;~9~;~0) et on appelle $\mathcal{S}$ la sphère de centre I et de rayon 6.
\item Position relative de $\mathcal{P}$ et de $\mathcal{S}$
	\begin{enumerate}
		\item On va montrer que la droite $\Delta$ passant par I est orthogonale au plan $\mathcal{P}$ coupe ce plan $\mathcal{P}$ au point H\,(3~;~1~;~2).
		
				
La droite $\Delta$ passant par I et orthogonale au plan $\mathcal{P}$ a donc pour vecteur directeur $\vect{n}$.

Donc $M(x~;~y~;~z) \in \Delta \iff \vect{\text{I}M} = \alpha\vect{n}, \: \alpha \in \R$, soit :

$M(x~;~y~;~z) \in \Delta \iff \left\{\begin{array}{l c r}
x - 1&=&\alpha\\
y - 9&=&- 4\alpha\\
z - 0&=&\alpha
\end{array}\right. \iff 
\left\{\begin{array}{l c r}
x &=&1 + \alpha\\
y &=&9 - 4\alpha\\
z &=&\alpha
\end{array}\right., \: \alpha \in \R$.

Le point d'intersection H de $\Delta$ et de $\mathcal{P}$ a ses coordonnées qui vérifient les équations de la droite et celle du plan, soit le système:
$\left\{\begin{array}{r c r}
x &=&1 + \alpha\\
y &=&9 - 4\alpha\\
z &=&\alpha\\
x-4y+z-1 & = & 0
\end{array}\right.$

On a donc $\left (1+\alpha\right ) -4\left ( 9-4\alpha\right ) +\alpha -1=0$ ce qui équivaut $\alpha=2$.

$x=1+\alpha=3$, $y=9-4\alpha = 1$ et $z=\alpha=2$ donc le point H a pour coordonnées (3~;~1~;~2).

		\item On calcule la distance IH.

$\text{IH}^2 = \left (3-1\right )^2 + \left ( 1-9\right )^2 + \left (2-0\right )^2 
= 4+64+4=72$;
donc $\text{IH} = \sqrt{72} = 6\sqrt{2}$. 
		
On admet que pour tout point $M$ du plan $\mathcal{P}$ on a I$M \geqslant \text{IH}$.

		\item% Le plan $\mathcal{P}$ coupe-t-il la sphère $\mathcal{S}$ ? Justifier la réponse
La sphère $\mathcal{S}$ a pour centre I et la distance IH du centre au plan $\mathcal{P}$ est $6\sqrt{2}$ qui est supérieure au rayon $r=6$ de la sphère.

Donc le plan $\mathcal{P}$ ne coupe pas la sphère $\mathcal{S}$ 
		
		
	\end{enumerate}

\item Position relative de $\mathcal{D}$ et de $\mathcal{S}$
	\begin{enumerate}
		\item On détermine une représentation paramétrique de la droite $\mathcal{D}$, passant par J et de vecteur directeur $\vec{u}$..
		
$M(x~;~y~;~z) \in \mathcal{D} \iff \vect{\text{J}M}  = t\vect{u} \iff \left\{\begin{array}{l c l}
x - 1&=&t\\
y - 4&=&t\\
z - 2&=&3t
\end{array}\right., t \in \R$, soit : 

$M(x~;~y~;~z) \in \mathcal{D} \iff \left\{\begin{array}{l c r}
x &=&1 + t\\
y &=&4 + t\\
z &=&2+3t\\
\end{array}\right., t \in \R$.

		\item% Montrer qu'un point $M$ de  coordonnées $(x~;~y~;~z)$ appartient à la sphère $\mathcal{S}$ si et seulement si : $(x - 1)^2 + (y - 9)^2 + z^2 = 36$.
$M(x~;~y~;~z) \in \mathcal{S} \iff \text{I}M = 6 \iff \text{I}M^2 = 36 \iff (x - 1)^2 + (y - (- 4))^2 + (z - 0)^2 = 36\\
\phantom{M(x~;~y~;~z) \in \mathcal{S} }
 \iff (x - 1)^2 + (y - 9)^2 + z^2 = 36.$

		\item %Montrer que la droite $\mathcal{D}$ coupe la sphère en deux points distincts.
%		
%On ne cherchera pas à déterminer les coordonnées de ses points.
On a $\text{IJ}^2 = (1 - 1)^2 + (9 - 4)^2 + (2 - 0)^2 = 25 + 4 = 29$.

Donc IJ $ = \sqrt{29} \approx 5,4 < 6$ longueur du rayon de la sphère : ceci montre que J est intérieur à la sphère et donc toute droite contenant J coupe la sphère en deux points.	
	\end{enumerate}
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 4 \hfill 4 points}

\textbf{Pour les candidats n'ayant pas suivi l'enseignement de spécialité}

\medskip


On considère la suite $\left(u_n\right)$ définie, pour tout entier naturel non nul $n$, par :
$u_n = \dfrac{n(n + 2)}{(n + 1)^2}$.

La suite $\left(v_n\right)$ est définie par :

$v_1 = u_1,\:v_2 = u_1 \times u_2$  et pour tout entier naturel $n \geqslant 3$,\: $v_n = u_1 \times u_2 \times \ldots \times u_n = v_{n-1} \times u_n$.

\medskip

\begin{enumerate}
\item %Vérifier que l'on a $v_2 = \dfrac{2}{3}$ puis calculer $v_3$.
$u_1 = \dfrac{3}{4}$ et $u_2 = \dfrac{8}{9}$, donc $v_2 = u_1 \times u_2 = \dfrac{3}{4} \times \dfrac{8}{9} = \dfrac{3 \times 4 \times 2}{4 \times 3 \times 3} = \dfrac{2}{3}$.

$u_3 = \dfrac{15}{16}$, donc $v_3 = u_1 \times u_2 \times u_3 = v_2 \times u_3 = \dfrac{2}{3} \times \dfrac{15}{16} = \dfrac{5}{8}$.
\item ~

\parbox{0.62\linewidth}{On complète l'algorithme ci-contre afin que, après son exécution, la variable $V$ contiennent la valeur $v_n$ où $n$ est un nombre entier naturel non nul défini par l'utilisateur.}
\hfill
\parbox{0.36\linewidth}{\renewcommand{\arraystretch}{1.7} 
\begin{tabular}{|l l|}\hline
  &Algorithme \\ \hline 
1.&$V \gets 1$\\
2.&Pour $i$ variant de 1 à $n$\\
3.&\quad $U \gets \dfrac{i (i + 2)}{(i + 1)^2}$\\
4.&\quad $V \gets V \times U$\\
5.&Fin Pour \\\hline
\end{tabular}
}
\item 
	\begin{enumerate}
		\item% Montrer que, pour tout entier naturel non nul $n$, $u_n =  1 - \dfrac{1}{(n + 1)^2}$.
On a, quel que soit $n \in \N$,

$u_n = \dfrac{n(n + 2)}{(n + 1)^2} = \dfrac{n^2 + 2n}{(n + 1)^2} = \dfrac{n^2 + 2n + 1 - 1}{(n + 1)^2} = \dfrac{(n +1)^2}{(n + 1)^2} - \dfrac{1}{(n + 1)^2} = 1 - \dfrac{1}{(n + 1)^2}$.

		\item% Montrer que, pour tout entier naturel non nul $n$, $0 < u_n < 1$.
De par sa définition $u_n$ quotient de deux termes supérieurs à $0$ est supérieur à $0$.

D'après la question précédente comme $\dfrac{1}{(n + 1)^2} > 0$, \: $1 - \dfrac{1}{(n + 1)^2} < 1$, soit $u_n < 1$, quel que soit $n \in \N^{*}$.

Conclusion : pour $n \in \N$, \: $0 < u_n < 1$.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item% Montrer que la suite $\left(v_n\right)$ est décroissante.
Quel que soit $n$, 
$\dfrac{v_{n+1}}{v_n} = \dfrac{u_1 \times  \ldots \times u_n \times u_{n+1}}{u_1 \times  \ldots \times u_n} = u_{n+1}$.
		
		Or d'après la question précédente $u_{n+1} < 1$, \: donc $\dfrac{v_{n+1}}{v_n} < 1$, donc la suite $\left(v_n\right)$ est décroissante.
		
		\item% Justifier que la suite $\left(v_n\right)$ est convergente (on ne demande pas de calculer sa limite).
Les termes $u_n$ étant supérieurs à zéro, les termes $v_n$ sont supérieurs à zéro.

On a donc quel que soit $n \in \N^{*}$, \: $0 < v_n$.

La suite $\left(v_n\right)$ est donc décroissante et minorée par $0$ ; d'après le théorème de la convergence monotone, elle  est convergente vers un réel supérieur ou égal à zéro.
	\end{enumerate}
\item 
	\begin{enumerate}
		\item% Vérifier que, pour tout entier naturel non nul $n$, \:$v_{n+1} = v_n \times \dfrac{(n+1)(n+3)}{(n+2)^2}$.
		$v_{n+1} = v_n \times u_{n+1} = v_n \times \dfrac{(n + 1)(n + 3)}{(n + 2)^2}$.

\newpage

		\item Soit la propriété: $v_n = \dfrac{n + 2}{2(n + 1)}$.
		
$\bullet~~$\emph{Initialisation}
		
$v_1 = u_1 = \dfrac{3}{4}$ et $\dfrac{1 + 2}{2 \times 2} = \dfrac{3}{4}$ : la relation est vraie au rang 1.

$\bullet~~$\emph{Hérédité}

Soit $n \in \N$ tel que $n \geqslant 1$ et supposons que $v_n = \dfrac{n + 2}{2(n + 1)}$.

D'après la question précédente : 

$v_{n+1} = v_n \times \dfrac{(n+1)(n+3)}{(n+2)^2} = \dfrac{n + 2}{2(n + 1)} \times \dfrac{(n+1)(n+3)}{(n+2)^2} = \dfrac{n + 3}{2(n + 2)} = \dfrac{(n+1)+2}{2((n+1)+1)}$;

la relation est vraie au rang $n + 1$.

$\bullet~~$\emph{Conclusion}

La relation est vraie au rang 1 et si elle est vraie à un rang au moins égal à 1, elle est vraie au rang suivant; d'après le principe de récurrence : pour tout entier naturel non nul $n$,\:

$v_n = \dfrac{n + 2}{2(n + 1)}$.

		\item %Déterminer la limite de la suite $\left(v_n\right)$.	
On peut puisque $n \ne 0$ écrire $v_n = \dfrac{1 + \frac{2}{n}}{2\left(1 + \frac{1}{n}\right)}$.

Or $\displaystyle\lim_{n \to + \infty} \frac{2}{n} = \displaystyle\lim_{n \to + \infty} \frac{1}{n}$, donc par somme et quotient de limites : $\displaystyle\lim_{n \to + \infty} v_n = \dfrac{1}{2 \times 1} = \dfrac{1}{2}$.
	\end{enumerate}

\item On considère la suite $w_n$ définie par 

$w_1 = \ln \left(u_1\right)$,\: $w_2 = \ln \left(u_1\right) + \ln \left(u_2\right)$  et, pour tout entier naturel $n \geqslant 3$, par

$w_n = \displaystyle\sum_{k=1}^n \ln \left(u_k\right) = \ln \left(u_1\right) + \ln \left(u_2\right) + \ldots + \ln \left(u_n\right)$.

%Montrer que $w_7 = 2w_1$.

On a $w_1 = \ln \left(u_1\right) = \ln \left( \frac{3}{4}\right)$ ;

$w_7 = \ln \left(u_1\right) + \ln \left(u_2\right)  + \ldots + \ln \left(u_7\right) = \ln \left(u_1 \times u_2 \times \ldots \times u_7 \right)  = \ln \left( v_7\right)$ soit d'après le résultat de la question 5. c. :
$w_7 = \ln \dfrac{7 + 2}{2(7 + 1)} = \ln \dfrac{9}{16}$. 

Or $\dfrac{9}{16} = \left(\dfrac{3}{4}\right)^2$, donc :
$w_7 = \ln \left(\dfrac{3}{4}\right)^2 = 2\ln \dfrac{3}{4} = 2w_1$.
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 4 \hfill 5 points}

\textbf{Pour les candidats ayant  suivi l'enseignement de spécialité}

\medskip

\textbf{Partie A}

\medskip

Pour tout entier naturel $n$, on définit les entiers $a_n = 6 \times 5^n - 2$ et $b_n = 3 \times 5^n + 1$.

\medskip
\newcommand{\hmod}[1]{\;[#1]}

\begin{enumerate}
\item 
	\begin{enumerate}
		\item %Montrer que, pour tout entier naturel $n$, chacun des entiers $a_n$ et $b_n$. est congru à $0$ modulo $4$.
	
\begin{list}{\textbullet}{$5\equiv 	1 \hmod{4}$ donc $5^n \equiv 1^n \hmod{4}$ donc $5^n \equiv 1 \hmod{4}$}
\item $6\times 5^n \equiv 6\times 1 \equiv 6 \hmod{4}$ donc $6\times 5^n -2\equiv 6 -2 \equiv 0\hmod{4}$ donc $a_{n} \equiv 0\hmod{4}$
\item $3\times 5^n \equiv 3\times 1 \equiv 3 \hmod{4}$ donc $3\times 5^n +1\equiv 3+1 \equiv 0\hmod{4}$ donc $b_{n} \equiv 0\hmod{4}$
\end{list}		
		
		\item Pour tout entier naturel $n$, 
$2b_n - a_n = 2\left ( 3\times 5^n+1\right ) - \left ( 6\times 5^n - 2 \right ) 
= 6\times 5^n +2 -6\times 5^n +2 = 4$.

		\item% Déterminer le PGCD de $a_n$ et $b_n$.
\begin{list}{\textbullet}{}
\item $2b_n-a_n=4$ donc, d'après le théorème de Bézout, le PGCD de $a_n$ et $b_n$ est un diviseur de 4.
\item 4 divise $a_n$ et 4 divise $b_n$ donc 4 divise leur PGCD.
\end{list}		
		
On peut donc en déduire que le PGCD de $a_n$ et $b_n$ est 4.
		
	\end{enumerate}
\item  
	\begin{enumerate}
		\item $b_{\np{2020}} = 3\times 5^{\np{2020}}+1$
		
$5\equiv -2 \hmod{7}$ donc $5^{\np{2020}}\equiv (-2)^{\np{2020}} \hmod{7}$
donc $3\times 5^{\np{2020}}\equiv 3\times (-2)^{\np{2020}} \hmod{7}$
et donc \\
$3\times 5^{\np{2020}}+1\equiv 3\times (-2)^{\np{2020}} +1\hmod{7}$		

Cela veut dire que $b_{\np{2020}} \equiv  3 \times (-2)^{\np{2020}} + 1\:\: [7]$.

Comme $\np{2020}$ est pair, $(-2)^{\np{2020}} = 2^{\np{2020}}$; on en déduit que
$b_{\np{2020}} \equiv  3 \times 2^{\np{2020}} + 1\:\: [7]$.
	
%		Montrer que $b_{\np{2020}} \equiv  3 \times 2^{\np{2020}} + 1\:\: [7]$.

		\item $\np{2020} = 3 \times 673 + 1$, donc:
$2^{\np{2020}} = 2^{3 \times 673 + 1} = \left (2^3\right )^{673}\times 2$.

Or $2^3 = 8  \equiv 1 \hmod{7}$.
Donc $\left (2^3\right )^{673} \equiv 1^{673} \equiv 1 \hmod{7}$.

On déduit: $\left (2^3\right )^{673}\times 2 \equiv 1\times 2 \equiv 2 \hmod{7}$
donc $3\times \left (2^3\right )^{673}\times 2 \equiv 3 \times 2 \equiv 6 \hmod{7}$.

On peut alors dire que $3\times \left (2^3\right )^{673}\times 2 +1  \equiv 6 +1 \equiv 0 \hmod{7}$, donc $b_{\np{2020}}$ est divisible par 7.

		\item L'entier $b_{n}$ est divisible par 4 pour tout $n$, donc $b_{\np{2020}}$ est divisible par 4; de plus  $b_{\np{2020}}$ est divisible par 7 donc, 4 et 7 étant premiers entre eux,  $b_{\np{2020}}$ est divisible  par 28.
		
L'entier $a_{n}$ est divisible par 4 pour tout $n$, donc $a_{\np{2020}}$ est divisible par 4.	

On sait que le PGCD de $a_n$ et $b_n$ est 4 donc le PGCD de $a_{\np{2020}}$ et $b_{\np{2020}}$ est 4.

Si $a_{\np{2020}}$ est divisible par 7, alors $a_{\np{2020}}$ est divisible par 28, et le PGCD de $a_{\np{2020}}$ et $b_{\np{2020}}$ est 28, ce qui est faux.

Donc $a_{\np{2020}}$ n'est pas divisible par 7.	
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

\parbox{0.7\linewidth}{On considère les suites $\left(u_n\right)$ et $\left(v_n\right)$ définies par:

\[u_0 = v_0 = 1 \:\text{et, pour tout entier naturel }\: n,\:
\left\{
\begin{array}{l !{=} l}
u_{n+1}&3u_n+4v_n\\
v_{n+1}&\phantom{3}u_n + 3v_n
\end{array}\right.\]

Pour un entier naturel $N$ donné, on souhaite calculer les termes de rang $N$ des suites 
$\left(u_n\right)$ et $\left(v_n\right)$ et on se demande si l'algorithme ci-contre permet ce calcul.}\hfill
\parbox{0.25\linewidth}{\begin{tabular}{|l|l|}\hline
\multicolumn{2}{|c|}{\textbf{Algorithme}}\\ \hline
1.&$U \gets 1$\\ \hline
2.&$V\gets 1$\\ \hline
3.&$K \gets  0$\\ \hline
4.& Tant que $K < N$\\ \hline
5.&$U \gets 3U + 4V$\\ \hline
6.&$V \gets U + 3V$\\ \hline
7.&$K \gets K+1$\\ \hline
8.&Fin Tant que\\ \hline
\end{tabular}}

\medskip

\parbox{0.51\linewidth}
{
\begin{enumerate}
\item On fait fonctionner l'algorithme avec $N = 2$.

On complète le tableau ci-contre en donnant les valeurs successivement affectées aux variables $U$,\: $V$ et $K$.
\end{enumerate}} \hfill
\parbox{0.45\linewidth}
{
\begin{tabularx}{\linewidth}{|*{3}{>{\centering \arraybackslash}X|}}\hline
$U$	&$V$&$K$\\ \hline
1	&1	&0\\ \hline
7	& 10&1\\ \hline
\textcolor{red}{61}	& \textcolor{red}{91} & \textcolor{red}{2}\\ \hline
\end{tabularx}
}

\begin{enumerate}[label=\textbf{2.}]
\item L'algorithme ne permet pas de calculer $u_N$ et $v_N$ pour une valeur de $N$ donnée car 

$u_1=3u_0+4v_0=7$ et $v_1=u_0+3v_0=4$ alors que la valeur de $v_1$ calculée par l'algorithme est 10.

On écrit une version corrigée de cet algorithme afin que les variables $U$ et $V$ contiennent bien les valeurs de $u_N$ et $v_N$ à la fin de son exécution:

\begin{center}
\begin{tabular}{|l|l|}\hline
\multicolumn{2}{|c|}{\textbf{Algorithme}}\\ \hline
1.&$U \gets 1$\\ \hline
2.&$V\gets 1$\\ \hline
3.&$K \gets  0$\\ \hline
4.& Tant que $K < N$\\ \hline
5.& \hspace*{1cm}$\red X \gets U$\\ \hline
6.&\hspace*{1cm}$U \gets 3U + 4V$\\ \hline
7.&\hspace*{1cm}$V \gets {\red X} + 3V$\\ \hline
8.&\hspace*{1cm}$K \gets K+1$\\ \hline
9.&Fin Tant que\\ \hline
\end{tabular}
\end{center}


\end{enumerate}

\bigskip

\textbf{Partie C}

\medskip

Pour tout entier naturel $n$, on définit la matrice colonne 
$X_n = \begin{pmatrix}u_n\\v_n\end{pmatrix}$.

\begin{enumerate}
\item% Donner, sans justification, une matrice carrée $A$ d'ordre 2 telle que, pour tout entier naturel $n$ : $X_{n+1} = AX_n.$
$\left\{
\begin{array}{l !{=} l}
u_{n+1}&3u_n+4v_n\\
v_{n+1}&\phantom{3}u_n + 3v_n
\end{array}\right.$
donc
$\begin{pmatrix}u_{n+1}\\v_{n+1}\end{pmatrix}
=
\begin{pmatrix} 3 & 4 \\ 1 & 3\end{pmatrix}
\times
\begin{pmatrix}u_n\\v_n\end{pmatrix}$
c'est-à-dire
$X_{n+1} = AX_n$ avec $A=\begin{pmatrix} 3 & 4 \\ 1 & 3\end{pmatrix}$.


\item  Soit $\mathcal{P}_n$ la propriété: $X_n = A^nX_0$.
\begin{list}{\textbullet}{}
\item \textbf{Initialisation}

$X_0 = \begin{pmatrix}1\\1 \end{pmatrix}$
et
$A^0 = \begin{pmatrix}1 & 0 \\ 0 & 1 \end{pmatrix}$
donc $A^0X_0=X_0$

La propriété est vraie au rang 0.

\item \textbf{Hérédité}

On suppose la propriété vraie pour un rang $n\geqslant 0$, c'est-à-dire $X_n = A^nX_0$.

$X_{n+1} = AX_n = A\left (A^nX_0\right ) = \left (AA^n\right )X_0=A^{n+1}X_0$

Donc la propriété est vraie au rang $n+1$.

\item \textbf{Conclusion}

La propriété est vraie au rang 0 et elle est héréditaire pour tout $n\geqslant 0$, donc, d'après le principe de récurrence, elle est vraie pour tout $n \geqslant 0$.
\end{list}

On a donc démontré que, pour tout entier naturel $n$, $X_n = A^nX_0$.

\item On admet que, pour tout entier naturel $n$, \: $A^n = \dfrac{1}{4}\begin{pmatrix}
2 \times 5^n + 2&4 \times  5^n - 4\\
5^n - 1&2 \times  5^n + 2
\end{pmatrix}$.

$\begin{pmatrix}u_n\\v_n\end{pmatrix}
= X_n = A^nX_0
=   \dfrac{1}{4}
\begin{pmatrix}
2 \times 5^n + 2&4 \times  5^n - 4\\
5^n - 1&2 \times  5^n + 2
\end{pmatrix}
\times
\begin{pmatrix} 1 \\ 1\end{pmatrix}
=
\dfrac{1}{4}
\begin{pmatrix}
2 \times 5^n + 2 + 4 \times  5^n - 4\\
5^n - 1 + 2 \times  5^n + 2
\end{pmatrix}
=
\dfrac{1}{4}
\begin{pmatrix}
6 \times 5^n  -2\\
3\times 5^n +1
\end{pmatrix}\\
\phantom{\begin{pmatrix}u_n\\v_n\end{pmatrix}}
=
\dfrac{1}{4}
\begin{pmatrix}
a_n\\ b_n
\end{pmatrix}
$

Donc $u_n=\dfrac{a_n}{4}$ et $v_n=\dfrac{b_n}{4}$.

%Montrer que, pour tout entier naturel $n$,\: $u_n = \dfrac{a_n}{4}$ et $v_n = \dfrac{b_n}{4}$ ,où $a_n$ et $b_n$ sont les nombres entiers définis dans la \textbf{partie A}.

\item %Justifier que, pour tout entier naturel $n$,\: $u_n$ et $v_n$ sont premiers entre eux.
On sait que le PGCD de $a_n$ est $b_n$ est égal à 4, donc le PGCD de $\dfrac{a_n}{4}$ et $\dfrac{b_n}{4}$ est égal à 1; donc les nombres $u_n$ et $v_n$ sont premiers entre eux.

\end{enumerate}

\end{document}