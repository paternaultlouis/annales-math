\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage[normalem]{ulem}
\usepackage{fancybox}
\usepackage{ulem}
\usepackage{dcolumn}
\usepackage{tabularx}
\usepackage{textcomp}
\usepackage{diagbox}
\usepackage{lscape}
\newcommand{\euro}{\eurologo{}}
\usepackage{pstricks,pst-plot,pst-text,pst-tree,pstricks-add}
\setlength\paperheight{297mm}
\setlength\paperwidth{210mm}
\setlength{\textheight}{23,5cm}
\setlength{\voffset}{-1,5cm}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~ \vect{\jmath},~ \vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Baccalauréat STT},
pdftitle = {CG -- IG Nouvelle-Calédonie novembre 2005},
allbordercolors = white
} 
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{mathrsfs}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\lhead{\small Baccalauréat STT CG--IG}
\lfoot{\small{Nouvelle-Calédonie}}
\rfoot{\small{novembre 2006}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}

\begin{center}\textbf{Durée : 3 heures}

\vspace{0,5cm}

{\Large \textbf{\decofourleft~Baccalauréat STT novembre
 2006~\decofourright}}

{\Large \textbf{Comptabilité et Gestion - Informatique et Gestion}}

{\Large \textbf{Nouvelle-Calédonie}}
 \end{center}

\vspace{0,25cm}

\textbf{\textsc{Exercice 1}\hfill 4 points}

\medskip

M. Logexpo, professeur de mathématiques, fait passer l'oral de rattrapage du baccalauréat, série STT comptabilité gestion. Il a préparé huit exercices classés en deux catégories qui abordent les notions mathématiques suivantes :

\textbf{1\up{re} catégorie}

exercice 1 : fonction $f$ de type logarithme ;

exercice 2 : fonction $g$ de type logarithme ;

exercice 3 : fonction exponentielle ;

exercice 4 : fonction rationnelle.
 
\textbf{2\up{e}  catégorie}

exercice A : probabilités ;

exercice B : programmation linéaire ;

exercice C : statistiques à une variable ;

exercice D : statistiques à deux variables.

\medskip

Un élève qui passe l'oral de rattrapage de mathématiques avec M. Logexpo doit tirer au sort un exercice de chaque catégorie. Tous les résultats seront donnés sous la forme d'une fraction irréductible.

\begin{enumerate}
\item Compléter l'arbre qui se trouve en annexe 1.
\item Un sujet est composé de deux exercices un exercice de chaque catégorie. Combien y-a-t-il de sujets différents possibles ?
\item Chaque sujet ayant la même probabilité d'être tiré au sort, calculer la probabilité des évènements suivants :

$E$ :  \og le sujet comporte une étude de fonction logarithme \fg{} ;

$F$ : \og le sujet comporte un exercice de probabilités \fg.

\item Définir par une phrase les évènements $E~\cap~F$ et $E~\cup~F$, puis calculer la probabilité de chacun de ces évènements.
\end{enumerate}

\vspace{0,5cm}

\textbf{\textsc{Exercice 2}\hfill 4 points}

\medskip

Les données ci-dessous montrent l'évolution du SMIC mensuel (169 h) en euros (les montants sont arrondis à l'unité).

Pour tout entier $i,~x_{i}$ représente le rang de l'année $2000 + i$ ;

$y_{i}$ représente le montant du SMIC au 1\up{er} juillet de l'année $2000 + i$.

\medskip
\begin{tabularx}{\linewidth}{|*6{>{\centering\arraybackslash}X|}}\hline
Date &1\up{er} juillet 2001& 1\up{er} juillet 2002&1\up{er} juillet 2003& 1\up{er}   juillet 2004&	  1\up{er} juillet 2005\\ \hline
$x_{i}$&1& 2&	3 &	4&	5\\ \hline
$y_{i}$&890&	913& 957& \nombre{1013}& \nombre{1067}\\ \hline
\multicolumn{6}{r}{\small (source INSEE)}
\end{tabularx}

\medskip

\begin{enumerate}
\item Représenter sur un graphique le nuage de points $M_{i}\left(x_{i}~;~y_{i}\right)$ de cette série statistique.

On prendra 2~cm pour 1 en abscisse, 1~cm pour 20 en ordonnées en commençant la graduation à $850$.
\item 
	\begin{enumerate}
		\item On considère le nuage formé par les trois premiers points. Calculer les coordonnées du point moyen G$_{1}$ de ce nuage,
		\item On considère le nuage formé des deux derniers points. Calculer les coordonnées du point moyen G$_{2}$ de ce nuage.
		\item Placer G$_{1}$ et G$_{2}$ sur le graphique et tracer la droite (G$_{1}$G$_{2}$).
		\item Déterminer une équation de la droite (G$_{1}$G$_{2}$).
	\end{enumerate}
\item Le SMIC est revalorisé le 1\up{er} juillet de chaque année.

Recopier et compléter la phrase suivante :

\og On s'attend à ce que le SMIC devienne supérieur à \nombre{1120} \euro{} à partir du 1\up{er} juillet \ldots \fg.

Expliquer votre réponse.
\end{enumerate}

\vspace{0,5cm}

\textbf{\textsc{Problème}\hfill 12 points}

\medskip

\textbf{Partie A : lecture graphique}

\medskip

Le plan est muni d'un repère orthogonal \Oij.

On donne en annexe 2, la courbe $\mathcal{C}_{f}$, qui représente une fonction $f$ définie sur $\R$.

A et B sont les points de coordonnées respectives A$(-2~;~-4)$ et B$(0~;~-8)$.

La droite (AB) est la tangente à la courbe $\mathcal{C}_{f}$ au point B.

Vous répondrez, dans cette partie, aux questions suivantes en vous aidant du graphique.

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item  Déterminer $f(0)$.
		\item  Donner un encadrement de $f(1)$ par deux entiers consécutifs.
		\item  Combien l'équation $f(x) = 0$ admet-elle de solutions dans l'intervalle $[-3~;~4]$ ? Justifier.
	\end{enumerate}
\item   Que vaut $f'(0)$ ? Justifier.
\end{enumerate}

\medskip

\textbf{Partie B : étude de la fonction}\boldmath  $f$ \unboldmath

\medskip

On sait maintenant que la fonction $f$ représentée ci-dessous est définie par :

\[f(x) = \text{e}^{2x} - 4\text{e}^x - 5.\]

\begin{enumerate}
\item  Déterminer $\displaystyle\lim_{x \to - \infty} f(x)$ en justifiant avec soin. En déduire l'existence d'une asymptote dont on précisera l'équation.
\item  Vérifier que, pour tout réel $x,~ f(x)= \text{e}^x\left(\text{e}^x - 4\right) - 5$. Déterminer alors $\displaystyle\lim_{x \to + \infty} f(x)$.
\item  
	\begin{enumerate}
		\item  Résoudre l'équation $\text{e}^{2x} - 4\text{e}^x - 5 =  0  \left(\text{on pourra poser} ~X = \text{e}^x \right)$.
		\item  En déduire les coordonnées des points d'intersection éventuels de la courbe $\mathcal{C}_{f}$ avec
l'axe des abscisses.
 	\end{enumerate}
\item 
	\begin{enumerate}
		\item  Déterminer la fonction dérivée de $f$, notée $f'$ et montrer que, pour tout réel $x,~ f'(x) = 2\text{e}^x\left(\text{e}^x -2\right)$ (on rappelle que la dérivée de la fonction $h$ définie par $h(x) = \text{e}^{2x}$ est la fonction $h'$ définie par $h'(x)= 2\text{e}^{2x}$).
		\item  Résoudre l'inéquation : $\text{e}^{x} - 2 > 0$.
		\item  En déduire le signe de $f(x)$ sur $\R$.
	 \end{enumerate}
\item  Dresser le tableau de variations complet de la fonction $f$ sur $\R$.
\end{enumerate}
 
\medskip

\textbf{Partie C : calcul d'aire}

\medskip

\begin{enumerate}
\item  Déterminer une primitive $F$ de la fonction $f$ sur $\R$. 
\item 
	\begin{enumerate}
		\item Hachurer sur le graphique de l'annexe 2, la partie du plan délimitée par la courbe $\mathcal{C}_{f}$, l'axe des abscisses, et les droites d'équation $x  = 0$ et $x = 1$.
		\item Calculer l'aire de la partie hachurée (on en donnera la valeur exacte puis un encadrement d'amplitude 0,1).
	\end{enumerate}
\end{enumerate}

\newpage

\begin{center}
\textbf{Annexe 1}

\textbf{À COMPLÉTER ET À RENDRE AVEC LA COPIE}

\vspace{0,5cm}
Exercice 1
\vspace{0,5cm}

%levelsep=4cm,

\begin{pspicture}(0,-3.5)(5,5)
%\psgrid
\rput(2,4.8){1\up{re} catégorie}
\rput(4,4.8){2\up{e} catégorie}
\psset{treesep=0.5cm,nodesep=2.5pt}
\pstree[linecolor=blue,treemode=R]{\TR{}}
{\pstree{\TR{exercice 1}}
		{\TR{exercice A}
		\TR{exercice B}
		\TR{exercice C}
		\TR{exercice D}
		}
\pstree[linestyle=none]{\TR{exercice 2}}
		{\TR{}
		\TR{}
		\TR{}
		}
\pstree[linestyle=none]{\TR{exercice 3}}
		{\TR{}
		\TR{}
		\TR{}
		}
\pstree[linestyle=none]{\TR{exercice 4}}
		{\TR{}
		\TR{}
		\TR{}
		}		
}
\end{pspicture}
\end{center}

\newpage
\begin{center}
\textbf{À COMPLÉTER ET À RENDRE AVEC LA COPIE}

\vspace{,5cm}

\textbf{Annexe 2}
\end{center}

\vspace{0.5cm}

\textbf{Problème}

\begin{center}
\psset{xunit=1.5cm,yunit=0.5cm}
\begin{pspicture}(-3.5,-14)(5.5,13.5)
\multido{\n=-3.50+0.25}{37}{\psline[linecolor=orange,linewidth=0.2pt](\n,-14)(\n,13)}
\multido{\n=-14+1}{28}{\psline[linecolor=orange,linewidth=0.2pt](-3.5,\n)(5.5,\n)}
\psaxes[linewidth=1.5pt]{->}(0,0)(-3.5,-14)(5.5,13)
\psaxes[linewidth=1.5pt](0,0)(5.5,13)
\uput[d](5.5,-0.2){$x$} \uput[r](0,13.25){$y$} \uput[r](1.8,8){\blue $\mathcal{C}_{f}$}
\uput[ur](0,-8){B} \uput[ur](-2,-4){A}
\psdots(-2,-4)(0,-8)
\psline(-3.5,-1)(3,-14)
\psplot[linecolor=blue,linewidth=1.25pt,plotpoints=5000]{-3.5}{1.9005}{2.71828 x 2 mul exp 2.71828 x exp 4 mul sub 5 sub}
\end{pspicture}
\end{center}
\end{document}