\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage[dvips]{graphicx}
\usepackage{tabularx}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{xspace}
\usepackage{textcomp}
\usepackage{colortbl}
\newcommand{\euro}{\eurologo}
\usepackage{pst-plot,pst-text,pst-tree,pst-func,pstricks-add}%
\usepackage{color,colortbl}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
% Tapuscrit : Denis Vergès
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\setlength\headheight{14pt}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{fancyhdr}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS Comptabilité et gestion},
pdftitle = {Polynésie  - 14 mai  2024},
allbordercolors = white,
pdfstartview=FitH} 
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}

\begin{document}

\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Brevet de technicien supérieur Métropole}
\lfoot{\small{Comptabilité et gestion}}
\rfoot{\small{14 mai 2024}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~Brevet de technicien supérieur Polynésie~\decofourright\\[7pt]14 mai 2024 - Comptabilité et gestion\footnote{Candidats libres ou d'établissement privé hors contrat}}}
\end{center}

\vspace{0.25cm}

\textbf{\large{}Exercice 1 \hfill 10 points}

\emph{Les trois parties de cet exercice sont indépendantes}

\medskip

\textbf{Partie A}

\medskip

Une entreprise, spécialisée dans la fabrication de desserts fruités, souhaite créer deux nouvelles compotes inédites:
\begin{itemize}
\item l'une à la saveur banane/cannelle;
\item l'autre à la saveur poire/fève de tonka.
\end{itemize}

Pour ces deux préparations, elle achète les fruits (bananes et poires) auprès de deux grossistes : le premier s'appelle \og au bon fruit \fg. 

À l'issue des achats, on observe les répartitions suivantes:

\begin{itemize}[label=$\bullet$]
\item 36\,\% des fruits proviennent de chez \og au bon fruit \fg.
\item Parmi les fruits achetés chez le grossiste \og au bon fruit\fg, 30\,\% sont des
bananes.
\item Un cinquième des fruits achetés chez le second grossiste sont des poires.
\end{itemize}

On choisit un fruit au hasard et on suppose que tous les fruits ont la même probabilité d'être choisis.

On considère les évènements suivants:
\begin{description}
\item[ ] $A$ : \og Le fruit provient de chez au bon fruit\fg ;
\item[ ] $B$ : \og Le fruit est une banane \fg.
\end{description}

On note $\overline{A}$ l'évènement contraire de l'évènement $A$.

\medskip

\begin{enumerate}
\item Donner la valeur de $P_A(B)$.
\item Compléter l'arbre de probabilité donné \textbf{en annexe, à rendre avec la
copie}.
\item Calculer la probabilité que le fruit provienne de chez \og au bon fruit\fg et soit une banane.
\item Justifier, en détaillant les calculs, que $P(B) = 0,62$.
\item Sachant que le fruit est une banane, quelle est la probabilité qu'elle provienne
de chez \og au bon fruit\fg ? Arrondir le résultat à \np{0,0001} près.
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

Dans le cadre d'un contrôle qualité, \np{1000} pots sont prélevés au hasard dans la production de compote banane/cannelle et poire/fève de tonka.

Ce tirage est assimilé à un tirage avec remise car le nombre de pots de compote est très grand.

On suppose que la probabilité que la saveur du pot soit poire/fève de tonka est de $0,38$.

Soit $X$ la variable aléatoire qui, dans le lot de \np{1000} pots, associe le nombre de pots saveur poire/fève de tonka.

\medskip

\begin{enumerate}
\item Justifier que la variable aléatoire $X$ suit une loi binomiale dont on donnera les paramètres.
\item Calculer la probabilité d'obtenir dans le lot exactement $350$ pots saveur poire/fève de tonka. Arrondir la probabilité à \np{0,0001} près.
\item 
	\begin{enumerate}
		\item Calculer $P(X \leqslant 400)$. Arrondir la probabilité à \np{0,0001} près.
		\item Interpréter le résultat dans le contexte de l'exercice.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie C}

\medskip

L'entreprise s'intéresse au remplissage de ses pots de compote.

On note $Y$ la variable aléatoire qui, à chaque pot prélevé dans la production, associe la quantité de compote qu'il contient, exprimée en gramme.

On admet que $Y$ suit la loi normale d'espérance $\mu = 90$ et d'écart-type $\sigma = 2$.

\medskip

\begin{enumerate}
\item Interpréter dans le contexte de l'exercice la valeur de l'espérance.
\item Calculer la probabilité que le pot contienne 87~g de compote ou moins. Arrondir
la probabilité à \np{0,0001} près.
\item On estime qu'un pot de compote est conforme lorsque la quantité qu'il contient
est comprise entre 85~g et 95~g.
	\begin{enumerate}
		\item Calculer la probabilité que le pot soit non conforme. Arrondir la probabilité à
\np{0,0001} près.
		\item L'entreprise a produit \np{550000} pots. On estime que 1,2\,\% des pots ne sont
pas conformes. 

À combien de pots non conformes correspond cette estimation?
		\item L'entreprise décide de faire don des pots qui ne sont pas conformes à une
organisation caritative. Sachant qu'un pot est normalement vendu 0,30~\euro, estimer le montant du don.
	\end{enumerate}
\end{enumerate}

\vspace{0.25cm}

\textbf{\large{}Exercice 2 \hfill 10 points}

\bigskip

\textbf{Partie A}

\medskip

Une entreprise d'évènementiel vient de créer une application destinée aux particuliers.

Le 1\up{er} septembre 2022, elle envoie une invitation à télécharger l'application à tout son réseau.

Chaque mois, elle note le nombre de personnes ayant téléchargé l'application. Les cinq premiers mois sont répertoriés dans le tableau ci-dessous. Le rang 0 correspond au mois de septembre 2022.

\begin{center}
\begin{tabularx}{\linewidth}{|m{3cm}|*{5}{>{\centering \arraybackslash}X|}}\hline
Mois &Sept. 2022 &Oct. 2022&Nov. 2022&Déc. 2022&Janv. 2023\\ \hline
Rang du mois $(x_i)$& 0&1&2&3&4\\ \hline
Nombre de téléchargements $(y_i)$&122& 155&180&207 &250\\ \hline
\end{tabularx}
\end{center}

La calculatrice est nécessaire pour la plupart des calculs demandés.

\medskip

\begin{enumerate}
\item Déterminer un ajustement affine de $y$ en fonction de $x$ selon la méthode des moindres carrés. On donnera les valeurs exactes des coefficients de l'équation de la droite.
\item Dans cette question, on suppose que la droite d'ajustement est donnée par l'équation $y = 31x + 121$.

Selon ce modèle :
	\begin{enumerate}
		\item Calculer le nombre de téléchargements en mars 2023.
		\item Déterminer pour quel mois le nombre de téléchargements mensuels sera
de $400$.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

En réalité, le nombre de téléchargements effectués jusqu'à la fin du mois de rang 8 est
donné dans la feuille de calcul ci-dessous.

La ligne 3 est au format pourcentage (arrondi au dixième).

\begin{center}
\begin{tabularx}{\linewidth}{|>{\cellcolor{lightgray}} c|>{\small}m{2.5cm}|*{9}{>{\centering \arraybackslash \small}X|}}\hline
\rowcolor{lightgray}	&A 						&B	&C	&D	&E	&F	&G	&H	&I	&J\\ \hline
1	&Rang du mois $(x_i)$	& 0	&1	&2	&3	&4	&5	&6	&7	&8\\ \hline
2	& Nombre de téléchargements $(y_i)$& 122& 155& 180& 207& 250 &313 &398 &521 &663 \\ \hline
3 	&Taux d'évolution mensuel&\cellcolor[gray]{0.8} &		&16,1\,\%& 15,0\,\%& 20,8\,\%& 25,2\,\%& 27,2\,\%& 30,9\,\%& 27,3\,\%\\ \hline
\end{tabularx}
\end{center}

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Donner une formule qui, saisie dans la cellule C3, permet d'obtenir par recopie vers la droite les différents taux d'évolution mensuels.
		\item Quelle est la valeur de la cellule C3 ? Arrondir à 0,1\,\% près.
	\end{enumerate}
\item Justifier que le taux d'évolution global du nombre de téléchargements entre le mois de rang 4 et le mois de rang 8 est d'environ 165\,\%.
\item En déduire le taux d'évolution mensuel moyen du nombre de téléchargements entre le mois de rang 4 et le mois de rang 8. Arrondir à 0,1\,\% près.
\end{enumerate}

\bigskip

\textbf{Partie C}

\medskip

Pour estimer le nombre de téléchargements à partir du mois de mai 2023, on fait l'hypothèse d'un taux d'évolution mensuel constant de 30\,\%.

On note $u_n$ le nombre de téléchargements mensuels estimé au cours du $n$-ième mois après le mois de mai 2023.

Le nombre de téléchargements au mois de mai 2023 est $u_0 = 663$.

\medskip

\begin{enumerate}
\item Calculer $u_1$ en arrondissant à l'unité.
\item Quelle est la nature de la suite $\left(u_n\right)$ ?

Justifier la réponse et préciser la raison.
\item Exprimer $u_n$ en fonction de l'entier naturel $n$.
\item Selon ce modèle, combien de téléchargements l'entreprise peut-elle espérer en
mai 2024 ? Arrondir le résultat à l'unité.
\item 
	\begin{enumerate}
		\item L'entreprise prévoit de participer à un challenge de l'innovation numérique
qui récompense les applications dès que le nombre de téléchargements dépasse les \np{100000} téléchargements mensuels.

Sur l'annexe, à rendre avec la copie, compléter les deux lignes non renseignées dans l'algorithme pour qu'après exécution, la variable $N$ contienne le nombre de mois après mai 2023 à partir duquel l'entreprise pourra candidater au challenge, selon ce modèle.
		\item Déterminer le nombre de mois après mai 2023 à partir duquel l'entreprise pourra candidater au challenge, selon ce modèle.
	\end{enumerate}
\end{enumerate}

\newpage
\begin{center}\textbf{\Large ANNEXE (à remettre avec la copie)}\end{center}

\bigskip

\textbf{Exercice 1- Question A. 2.}

\begin{center}
\bigskip
  \pstree[treemode=R,nodesepA=0pt,nodesepB=4pt,levelsep=2.8cm,treesep=1.2cm]{\TR{}}
 {
 	\pstree[nodesepA=4pt]{\TR{$A$}\naput{$\ldots$}}
 	  { 
 		  \TR{$B$}\naput{$\ldots$}
 		  \TR{$\overline{B}$}\nbput{$\ldots$}	   
 	  }
 	\pstree[nodesepA=4pt]{\TR{$\overline{A}$}\nbput{$\ldots$}}
 	  {
 		  \TR{$B$}\naput{$\ldots$}
          \TR{$\overline{B}$}\nbput{$\ldots$} 
     }
}
\bigskip
\end{center}


\textbf{Exercice 2 - Partie C - Question 5}

\medskip

\begin{center}
\begin{tabular}{|l|}\hline
$N \gets 0$\\
$U \gets 663$\\
Tant que \ldots \ldots \ldots\\
\qquad $N \gets \ldots \ldots$\\
\qquad $U \gets  1,3*U$\\
Fin Tant que\\
Afficher $N$\\ \hline
\end{tabular}
\end{center}

\end{document}