\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage[mathscr]{eucal}
\usepackage{fancybox}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{tabularx}
\usepackage{colortbl}
\usepackage{lscape}
\usepackage{dcolumn}
\usepackage{multicol}
\usepackage{diagbox}
\usepackage{multirow}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pstricks,multido,pst-plot,pst-tree}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength\paperheight{297mm}
\setlength\paperwidth{210mm}
\setlength{\textheight}{23,5cm}
\setlength{\voffset}{-0,5cm}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm} 
\lhead{\small Baccalauréat 1L mathématiques--informatique}
\rhead{\textbf{A. P. M. E. P.}}
\lfoot{\small{Amérique du Nord}}
\rfoot{\small{3 juin 2011}}
\renewcommand \footrulewidth{.2pt}%
\pagestyle{fancy}
\thispagestyle{empty}
    
\begin{center}{\Large \textbf{Baccalauréat Mathématiques--informatique\\Amérique du Nord 3 juin 2011}} 
\end{center}

\vspace*{0,5cm}

\textbf{\textsc{Exercice 1} \hfill 10 points}

\medskip
 
Thomas Malthus (1766-1834) est un économiste britannique connu pour ses travaux concernant les rapports entre population et production de denrées alimentaires. L'objectif de cet exercice est d'étudier le modèle établi par cet économiste dans son ouvrage \emph{Essai sur le principe des populations} publié en 1798.

\bigskip
 
\textbf{PARTIE 1 : Étude de l'évolution d'une population}

\medskip
 
Un pays possède, en 1800, une population de 20 millions d'habitants (soit \np{20000}~milliers).
 
Pour tout entier positif $n$, on note $u_{n}$ la population, en milliers, de ce pays en l'année $1800 + n$. On a donc $u_{0} = \np{20000}$.
 
Au regard des années précédentes, Malthus émet l'hypothèse qu'à partir de l'année 1800 la population de ce pays va augmenter de 1\,\% par an. 
\begin{enumerate}
\item Justifier que $u_{1} = \np{20200}$. Que représente cette valeur ? 
\item Quelle est la nature de la suite $\left(u_{n}\right)$ ? Exprimer $u_{n}$ en fonction de $n$. 
\item Calculer la population obtenue en 1900 selon ce modèle.
 
Arrondir ce résultat au million d'habitants.
\end{enumerate}

\bigskip
 
\textbf{PARTIE 2 : Étude de l'évolution de la production de denrées alimentaires}

\medskip
 
Malthus constate qu'en 1800 ce pays peut nourrir une population de 25 millions d'habitants.
 
Pour tout entier positif $n$, on note $v_{n}$ le nombre de personnes en milliers que peut nourrir ce pays en l'année $1800 + n$.
 
On a donc $v_{0} = \np{25000}$.
 
Il fait l'hypothèse que grâce au progrès technique, chaque année le pays peut nourrir \np{10000} personnes supplémentaires. 
\begin{enumerate}
\item Justifier que $v_{1} = \np{25010}$. Que représente ce résultat ? 
\item Quelle est la nature de la suite $\left(v_{n}\right)$ ? Exprimer $v_{n}$ en fonction de $n$.
\item Combien de personnes peuvent-être nourries en 1900 selon ce modèle ?
 
Que remarque-t-on ?
\end{enumerate}

\bigskip
 
\textbf{PARTIE 3 : Étude conjointe des deux suites}

\medskip
 
 Dans la feuille de calcul donnée en \textbf{annexe 1}, les termes de la suite $\left(u_{n}\right)$ sont arrondis au dixième. 
\begin{enumerate}
\item Quelle formule peut-on inscrire dans la cellule C3 pour obtenir, par recopie automatique vers le bas, les autres termes de la suite $\left(u_{n}\right)$ ? 
\item Quelle formule peut-on inscrire dans la cellule D3 pour obtenir, par recopie automatique vers le bas, les autres termes de la suite $\left(v_{n}\right)$ ? 
\item Compléter les cellules de C24 à C28 et les cellules D24 à D28 par leurs valeurs. 
\item Selon les modèles de Malthus, à partir de quelle année ce pays ne serait plus en capacité de nourrir l'ensemble de sa population ? 
\item Appliquée à l'Angleterre, la modélisation de Malthus ci-dessus s'est révélée inexacte.
 
Pour quelles raisons, selon vous, la famine attendue ne s'est-elle heureusement pas produite ? 
\end{enumerate}

\vspace*{0,5cm}

\textbf{\textsc{Exercice 2} \hfill 10 points}

\medskip

Dans un lycée, on a demandé à chacun des 700~élèves de premières et terminales le nombre de livres lus dans l'année.
 
Les résultats sont donnés sous forme d'un histogramme en \textbf{annexe 2}.
 
Dans un second temps, on a demandé aux élèves le nombre de films vus au cinéma dans l'année. Les résultats sont consignés en annexe 3.

\bigskip
 
\textbf{PARTIE 1 : Étude du nombre de livres lus dans l'année}

\medskip
 
\begin{enumerate}
\item Parmi les \og plus gros lecteurs \fg{} (ceux qui ont lu 10 livres ou plus), quel est le pourcentage d'élèves de série littéraire ? Arrondir à 1 \,\%. 
\item 
	\begin{enumerate}
		\item Quel est le nombre d'élèves de série L interrogés? 
		\item Parmi les élèves de série L, quel est le pourcentage des élèves qui ont lu au plus 4 livres ?
	\end{enumerate} 
\item Dans l'ensemble des élèves interrogés, quel est le pourcentage d'élèves de série L qui sont \og petits lecteurs\fg (ceux lisant entre 0 et 4 livres) ? Arrondir à 1\,\%.
\end{enumerate}

\bigskip
 
\textbf{PARTIE 2 : Étude du nombre de films vus au cinéma dans l'année}

\medskip
 
La moyenne de cette série est $\mu \approx  8,6$, l'écart-type est $\sigma \approx 3,5$ (arrondis au dixième). 
\begin{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer l'intervalle $[\mu -2\sigma~;~\mu + 2\sigma]$. 
		\item Vérifier que environ 95\,\% des valeurs appartiennent à l'intervalle 
		
\mbox{$[\mu -2\sigma~;~\mu + 2\sigma]$}.
	\end{enumerate} 
\item 	Donner la médiane, le premier et le troisième quartile de cette série. 
\end{enumerate}

\bigskip

\textbf{PARTIE 3 : Comparaison du nombre de films vus dans l'année par les élèves de deux classes}

\medskip
 
On considère deux classes de première L de cet établissement dont l'une est composée d'élèves ayant choisi l'option \og cinéma \fg.
 
Ces classes, appelées A et B, ont le même effectif : 32 élèves.
 
On a représenté en annexe 4 les diagrammes en boîte du nombre de films vus au cinéma dans l'année pour chacune de ces classes, en plaçant aux extrémités le maximum et le minimum.
 
Chaque assertion suivante est-elles vraie ou fausse ? Expliquer votre réponse. 
\begin{enumerate}
\item Dans la classe A environ la moitié des élèves a vu moins de 9 films au cinéma. 
\item Dans la classe B environ huit élèves ont vu 10 films ou plus au cinéma. 
\item Au moins la moitié des élèves de la classe A a vu plus de films au cinéma que les trois-quarts des élèves de la classe B. 
\item Environ le quart des élèves de la classe B a vu moins de films au cinéma que chaque élève de la classe A. 
\end{enumerate}

\newpage

\begin{center}
\textbf{ANNEXE 1 à rendre avec la copie}

\vspace{2,5cm} 

\begin{tabularx}{\linewidth}{|*{5}{>{\centering \arraybackslash}X|}}\hline
&A&B&C&D\\ \hline
1&Ann\'ee&Indice $n$&Suite $u$&Suite $v$\\ \hline
2&1800&0	&\np{20000,0}&\np{25000}\\ \hline
3&1801&1	&\np{20200,0}&\np{25010}\\ \hline
4&1802&2	&\np{20402,0}&\np{25020}\\ \hline
5&1803&3	&\np{20606,0}&\np{25030}\\ \hline
6&1804&4	&\np{20812,1}&\np{25040}\\ \hline
7&1805&5	&\np{21020,2}&\np{25050}\\ \hline
8&1806&6	&\np{21230,4}&\np{25060}\\ \hline
9&1807&7	&\np{21442,7}&\np{25070}\\ \hline
10&1808&8	&\np{21657,1}&\np{25080}\\ \hline
11&1809&9	&\np{21873,7}&\np{25090}\\ \hline
12&1810&10	&\np{22092,4}&\np{25100}\\ \hline
13&1811&11	&\np{22313,4}&\np{25110}\\ \hline
14&1812&12	&\np{22536,5}&\np{25120}\\ \hline
15&1813&13	&\np{22761,9}&\np{25130}\\ \hline
16&1814&14	&\np{22989,5}&\np{25140}\\ \hline
17&1815&15	&\np{23219,4}&\np{25150}\\ \hline
18&1816&16	&\np{23451,6}&\np{25160}\\ \hline
19&1817&17	&\np{23686,1}&\np{25170}\\ \hline
20&1818&18	&\np{23922,9}&\np{25180}\\ \hline
21&1819&19	&\np{24162,2}&\np{25190}\\ \hline
22&1820&20	&\np{24403,8}&\np{25200}\\ \hline
23&1821&21	&\np{24647,8}&\np{25210}\\ \hline
24&1822&22	&			&\\ \hline
25&1823&23	&			&\\ \hline
26&1824&24	&			&\\ \hline
27&1825&25	&			&\\ \hline
28&1826&26	&			&\\ \hline
\end{tabularx}

\newpage

\textbf{Ces ANNEXES ne sont PAS à rendre avec la copie}

\vspace{1cm}

\begin{flushleft} 
\textbf{ANNEXE 2} 
\end{flushleft} 

\psset{xunit=0.9cm,yunit=0.08cm}
\begin{pspicture}(12,130)
\psaxes[linewidth=1.5pt,Dx=20,Dy=10](0,0)(12,130)
\multido{\n=0+10}{14}{\psline[linewidth=0.25pt,linecolor=orange](0,\n)(12,\n)}
\psframe[fillstyle=solid,fillcolor=gray](0.5,0)(1.5,50)
\psframe[fillstyle=solid,fillcolor=gray](4.5,0)(5.5,100)
\psframe[fillstyle=solid,fillcolor=gray](8.5,0)(9.5,100)
\psframe[fillstyle=solid,fillcolor=blue](1.5,0)(2.5,50)
\psframe[fillstyle=solid,fillcolor=blue](5.5,0)(6.5,100)
\psframe[fillstyle=solid,fillcolor=blue](9.5,0)(10.5,50)
\psframe[fillstyle=solid,fillcolor=lightgray](2.5,0)(3.5,120)
\psframe[fillstyle=solid,fillcolor=lightgray](6.5,0)(7.5,50)
\psframe[fillstyle=solid,fillcolor=lightgray](10.5,0)(11.5,80)
\psframe[fillstyle=solid,fillcolor=gray](12.5,0)(13,5)
\psframe[fillstyle=solid,fillcolor=blue](12.5,7)(13,12)
\psframe[fillstyle=solid,fillcolor=lightgray](12.5,14)(13,19)
\uput[r](13,2.5){s\'erie L}
\uput[r](13,9.5){s\'erie ES}
\uput[r](13,16.5){s\'erie S}
\uput[d](2,0){0 à 4 livres}
\uput[d](6,0){5 à 9 livres}
\uput[d](10,0){10 à 14 livres}
\end{pspicture} 

\vspace{1cm}

\begin{flushleft} 
\textbf{ANNEXE 3} 
\end{flushleft}

\medskip

\begin{tabularx}{\linewidth}{|m{2cm}|*{19}{>{\footnotesize \centering \arraybackslash}X|}} \hline
Nombre de films &0 &1 &2 &3 &4 &5 &6& 7 &8 &9 &10 &11 &12 &13 &14 &15 &16 &17 &18\\ \hline 
Nombres d'élèves &5 &10 &15 &30 &30 &30 &60 &67 &90 &85 &70 &60 &60 &40 &20 &10 &5 &8 &5\\ \hline 
\end{tabularx} 

\vspace{1cm}

\begin{flushleft} 
\textbf{ANNEXE 4} 
\end{flushleft}

\psset{xunit=0.7cm,yunit=1cm}
\begin{pspicture}(1,-2)(17,2)
\psline[arrowsize=2pt 3]{->}(1,0)(17,0)
\psaxes[linewidth=1.5pt](0,0)(17,0)
\psline(3,1)(9,1)\psline(14,1)(17,1)\psframe(9,0.75)(14,1.25)\psline(11,0.75)(11,1.25)\rput(1.5,1.75){Classe A}
\psline(1,-1)(5,-1)\psline(10,-1)(17,-1)\psframe(5,-1.25)(10,-0.75)\psline(8,-1.25)(8,-0.75)\rput(1.5,-1.75){Classe B}
\end{pspicture}
\end{center}
\end{document}