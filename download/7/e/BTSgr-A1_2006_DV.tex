\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{graphicx}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\lhead{\small Brevet de technicien supérieur}
\lfoot{\small{Groupement A}}
\rfoot{\small{juin 2006}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}
\begin{center} \Large \textbf{\decofourleft~Brevet de technicien supérieur Métropole ~\decofourright\\[5pt] Groupement A1 mai 2006\footnote{Contrôle industriel et régulation automatique, Informatique et réseaux pour l'industrie et les services techniques, systèmes électroniques}}  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 11 points}

\medskip
 
Le but de cet exercice est d'étudier quelques propriétés d'un filtre numérique $N$ et de comparer des effets de ce filtre avec ceux d'un filtre analogique $A$.

\medskip
 
\textbf{Partie I}

\medskip

On rappelle que tout signal discret causal est nul pour tout entier strictement négatif.

Soient $x(n)$ et $y(n)$ les termes généraux respectifs de deux signaux discrets causaux représentant, respectivement, l'entrée et la sortie d'un filtre numérique $N$. Ce filtre est conçu de telle sorte que, pour tout nombre entier $n$ positif ou nul, on a :
 
\[y(n) - y(n - 2) = 0,04~x(n - 1).\]

\begin{enumerate}
\item On note $\mathcal{Z}x$ et $\mathcal{Z}y$ les transformées respectives des signaux causaux $x$ et $y$. Montrer que, pour tout nombre complexe $z$ différent de $-1$ et $1$, on a :
    
    \[\left(\mathcal{Z}y\right)(z)=\dfrac{0,04z}{(z-1)(z+1)}\left(\mathcal{Z}x\right)(z)\]
    
\item On suppose que le signal d'entrée est l'échelon unité discret :

\[x(n)=e(n) \mbox{ avec } e(n)=\left\{\begin{array}{rr}
    0 & \mbox{ si } n < 0\\
    1 & \mbox {si } n \geqslant 0
    \end{array}
\right.\]
    
	\begin{enumerate}
		\item  Montrer que, pour tout nombre complexe $z$ différent de $-1$ et $1$, on a :
		
    \[\left(\mathcal{Z}y\right)(z)=\dfrac{0,04z^2}{(z-1)^2(z+1)}\]
		\item Calculer les constantes réelles $A$, $B$ et $C$ telles que :
    \[\dfrac{0,04z}{(z-1)^2(z+1)}=\dfrac{A}{(z-1)^2}+\dfrac{B}{z-1}+\dfrac{C}{z+1}\]
		\item En remarquant que :
    \[\dfrac{\left(\mathcal{Z}y\right)(z)}{z}=\dfrac{0,04z}{(z-1)^2(z+1)}\] montrer que, pour tout entier $n$ positif ou nul, on a :
    \[y(n)=0,02n+0,01\left(1-(-1)^n\right)\]
		\item Déterminer $y(2k)$ puis $y(2k+1)$ pour tout nombre entier naturel $k$.
		\item En déduire que pour tout nombre entier naturel $k$, on a : 
		
		$y(2k+1) = y(2k+2)$.
		\item Représenter graphiquement les termes du signal causal $y$ lorsque le nombre entier $n$ est compris entre $-2$ et $5$.
	\end{enumerate}
\end{enumerate}

\vspace{0,5cm}

\textbf{Partie II}

\medskip

On rappelle que la fonction échelon unité, notée $U$, est définie par :

\[\left\{\begin{array}{rr}
U(t)=0 & \mbox{ si } t< 0\\
U(t)=1 & \mbox {si } t \geqslant 0
\end{array}\right.\]

Soit la fonction $f$ définie pour tout nombre réel
$t$ par :

\[f(t) = \sin (20t)U(t)\]

On note $F$ la transformée de Laplace de la fonction $f$. Le signal de sortie du filtre analogique $A$ est représenté par la fonction $s$ dont la transformée de Laplace $S$ est telle que :

\[S(p) = \dfrac{F(p)}{p}\]

\begin{enumerate}
\item Justifier que, pour tout nombre réel $t$ positif ou nul, on a :

\[s(t)=\displaystyle \int_0^t f(u) \text{d}u\]

\item En déduire que, pour tout nombre réel $t$ positif ou nul, on a :

    \[s(t)=\dfrac{1-\cos(20t)}{20}\]
    
\item Donner sans justification la valeur maximale et la valeur minimale de la fonction $s$.
\item Tracer, sur le graphique du document réponse, l'allure de la courbe représentative de la fonction $s$.\\ Il n'est pas demandé d'étudier la fonction $s$.

\vspace{0,5cm}
    
\textsl{La figure du document réponse montre une simulation du résultat obtenu en sortie du filtre numérique soumis à une version échantillonnée de la fonction $f$, lorsque la période d'échantillonnage est $0,02$.}
\end{enumerate}
\newpage
\begin{center}
\textbf{Document à rendre avec la copie}

\vspace{3cm}

\psset{xunit=11cm,yunit=100cm}
\begin{pspicture}(1,0.1)
\psaxes[linewidth=1.5pt,Dx=0.1,Dy=0.01](0,0)(1,0.1)
\psaxes[linewidth=1.5pt,Dx=0.05,Dy=0.005,labels=none](0,0)(1,0.1)
\psframe(0,0)(1,0.1)
\psline(0.03,0)(0.03,0.016)(0.06,0.016)(0.06,0.029)(0.08,0.029)(0.08,0.052)(0.1,0.052)(0.1,0.069)(0.12,0.069)(0.12,0.09)(0.13,0.09)(0.14,0.096)(0.16,0.096)(0.16,0.102)(0.18,0.102)(0.18,0.094)(0.2,0.094)(0.205,0.085)(0.22,0.085)(0.22,0.063)(0.24,0.063)(0.245,0.058)(0.25,0.047)(0.255,0.047)(0.26,0.022)(0.275,0.022)(0.28,0.011)(0.295,0.011)(0.2975,0.00025)(0.3,-0.002)(0.32,-0.002)(0.325,0.001)(0.335,0.001)(0.34,0.0025)(0.355,0.0025)(0.355,0.019)(0.375,0.019)(0.375,0.034)(0.4,0.034)(0.402,0.059)(0.42,0.059)(0.42,0.074)(0.44,0.074)(0.445,0.093)(0.46,0.093)(0.46,0.0975)(0.48,0.0975)(0.48,0.1025)(0.5,0.1025)(0.501,0.092)(0.52,0.092)(0.52,0.0801)(0.54,0.0801)
\psline(0.54,0.0801)(0.545,0.058)(0.56,0.058)(0.57,0.041)(0.58,0.041)(0.58,0.018)(0.6,0.018)(0.6,0.008)(0.62,0.008)(0.62,-0.003)(0.64,-0.003)(0.64,0.002)(0.66,0.002)(0.66,0.006)(0.675,0.006)(0.68,0.024)(0.7,0.024)(0.71,0.04)(0.725,0.04)(0.73,0.066)(0.745,0.066)(0.75,0.079)(0.77,0.079)
\psline(0.77,0.079)(0.77,0.096)(0.78,0.096)(0.78,0.098)(0.805,0.098)(0.81,0.101)(0.83,0.101)(0.83,0.087)(0.845,0.087)(0.848,0.076)(0.865,0.076)(0.87,0.052)(0.88,0.052)(0.88,0.034)(0.9,0.034)(0.91,0.013)(0.925,0.013)(0.925,0.004)(0.94,0.004)(0.94,-0.004)(0.96,-0.004)(0.96,0.003)(0.975,0.003)(0.975,0.0095)(1,0.0095)
\end{pspicture}
\end{center}

\newpage

{\large{\textbf{Exercice 2 \hfill 9 points}}}

\medskip
 
\textbf{Les parties A et B sont indépendantes.}

\medskip
 
\textbf{Partie A}

\medskip

Soient $\alpha$ et $\beta$ deux nombres réels.

Soit $f$ une fonction périodique de période $1$, définie sur l'intervalle $[0~;~1[$ par $f(t)=\alpha t + \beta$.

On appelle $a_0$, $a_n$ et $b_n$ les coefficients de Fourier associés à la fonction $f$.

\medskip

\begin{enumerate}
\item Montrer que $a_0=\dfrac{\alpha}{2} + \beta$.
\item Montrer que $b_n=-\dfrac{\alpha}{n\pi}$ pour tout nombre entier naturel $n$ non nul.

\medskip

On admet que $a_n=0$ pour tout entier naturel $n$ non nul.
    \item On se propose de déterminer les nombres réels $\alpha$ et $\beta$ pour que le développement $S$ en série de Fourier de la fonction $f$ soit défini pour tout nombre réel $t$ par $S(t)=\displaystyle \sum_{n=1}^{+\infty} \dfrac{1}{n} \sin (2n\pi t).$
	\begin{enumerate}
		\item Déterminer les nombres réels $\alpha$ et $\beta$ tels que $a_0=0$ et $b_n=\dfrac{1}{n}.$
    
    En déduire l'expression de la fonction $f$.
		\item Représenter la fonction $f$ sur l'intervalle $[-2~;~2]$ dans un repère orthogonal.
	\end{enumerate}
\end{enumerate}

\bigskip

\textbf{Partie B}

\medskip

On veut résoudre l'équation différentielle :

\[s''(t)+ s(t)=f(t)\]

On admet que l'on obtient une bonne approximation de la fonction $s$ en remplaçant $f(t)$ par les premiers termes du développement en série de Fourier de la fonction $f$ obtenus dans la partie A, c'est-à-dire par :

\[\sin (2\pi t)+\dfrac{1}{2} \sin (4 \pi t)\]

Soit (E) l'équation différentielle :

\[s''(t)+s(t) = \sin (2\pi t)+\dfrac{1}{2} \sin (4 \pi t)\]

\begin{enumerate}
    \item Vérifier que la fonction $s_1$ définie pour tout nombre réel $t$ par :
    \[s_1(t) = \dfrac{1}{1-4\pi ^2} \sin(2\pi t) + \dfrac{1}{2(1-16\pi ^2)} \sin(4 \pi t)\] est solution de l'équation différentielle (E).
    \item Résoudre l'équation différentielle (E).
\end{enumerate}
\end{document}

