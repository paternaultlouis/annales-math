\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx,stmaryrd}
\usepackage{fancybox}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{tabularx}
\usepackage{enumitem}
\usepackage{textcomp}
\usepackage{arydshln}
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pstricks,pst-plot,pst-text,pst-tree,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
%\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
%\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=1.9cm, bottom=2.4cm]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\newcommand{\A}{\fbox{A}~}\newcommand{\B}{\fbox{B}~}\newcommand{\C}{\fbox{C}~}
\newcommand{\D}{\fbox{D}~}\newcommand{\E}{\fbox{E}~}
\renewcommand{\theenumi}{\textbf{\Alph{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Baccalauréat spécialité},
pdftitle = {Évaluation ESciA session 2024\\},
allbordercolors = white,
pdfstartview=FitH} 
\newcommand{\e}{\text{e}}
\usepackage[french]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Mathématiques générales avancées}
\lfoot{\small{TeSciA}}
\rfoot{\small{session 16 mars 2024}}
\pagestyle{fancy}
\thispagestyle{empty}
\begin{center}
{\Large \textbf{\decofourleft~Évaluation ESciA  session 16 mars 2024~\decofourright\\[7pt]Mathématiques générales avancées  Épreuve 1}\\[7pt]Durée : 1h 30 min}

\medskip

\textbf{FONCTIONNEMENT DES QUESTIONS}
\end{center}

$\bullet~~$Les questions à \emph{choix multiples} sont numérotées M1, M2, etc. Le candidat y répond en \textbf{noircissant} la case correspondant à sa réponse dans la feuille-réponse $\square$.

Pour chacune de ces questions, il y a une et une seule bonne réponse.

Toute réponse fausse retire des points aux candidats.

Noircir plusieurs réponses à une même question a un effet de neutralisation (le candidat récoltera 0 point).

\medskip

$\bullet~~$Les questions \emph{à réponse brute} sont numérotées L1, L2, etc.

Elles ne demandent aucune justification : les résultats sont reportés par le candidat dans le cadre correspondant sur la feuille-réponse $\triangle$. Tout débordement de cadre est interdit.

\medskip

$\bullet~~$Les questions \emph{à réponse rédigée} sont numérotées R1, R2, etc. Elles sont écrites dans le cadre correspondant sur la feuille-réponse $\bigcirc$ ou la feuille-réponse $\triangle$, selon le symbole précédant le numéro de la question. Tout débordement de cadre est interdit.

\bigskip

\begin{center}
\textbf{CONSEILS DE BON SENS}\end{center}

$\bullet~~$L'énoncé est (très) long: il n'est absolument pas nécessaire d'avoir tout traité pour avoir une note et un classement excellents.

$\bullet~~$Ne vous précipitez pas pour reporter vos réponses, notamment aux questions à choix multiples. Il est préférable d'avoir terminé un exercice avant d'en reporter les réponses.

$\bullet~~$Ne répondez jamais au hasard à une question à choix multiples !

$\bullet~~$Selon l'exercice, les questions peuvent être dépendantes les unes des autres ou non. Soyez attentifs à la variété des situations.

\newpage

\textbf{\Large Exercice 1. Une fonction rationnelle}

\medskip

On considère dans cet exercice la fonction $f$ qui à tout réel $x$ différent de 0 et $-1$ associe le réel
\[f(x) = \dfrac 1x + \dfrac{1}{x + 1}.\]

$\square$~\textbf{M1} Quand $x$ tend vers $+\infty$, la quantité $f(x)$ tend:


\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A vers aucune limite& \B vers $+\infty$& \C vers 2&\D  vers 0&\E vers 1\\
\end{tabularx}
\end{center}

$\square$~\textbf{M2} Quand $x$ tend vers $0^+$, la quantité $f(x)$ tend : 
\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A vers $-\infty$&\B vers 1&\C vers $+\infty$&\D  vers aucune limite&\E vers 0\\
\end{tabularx}
\end{center}

$\square$~\textbf{M3} Quand $x$ tend vers $0^-$, la quantité $f(x)$ tend : 
\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A vers 1& \B vers aucune limite& \C vers $+\infty$&\D  vers $-\infty$&\E vers 0\\
\end{tabularx}
\end{center}

$\square$~\textbf{M4} La dérivée de $f$ est la fonction qui à $x$ associe:

\A$\dfrac 1x + \dfrac{1}{x + 1}$

\B $- \dfrac 1x - \dfrac{1}{x + 1}$

\C $- \dfrac{1}{x^2} - \dfrac{1}{(x + 1)^2}$

\D aucune des autres valeurs indiquées en général

\E $\dfrac{1}{x^2} + \dfrac{1}{(x + 1)^2}$

\medskip

$\square$~\textbf{M5} Sur l'intervalle $]-1~;~0[$, la fonction $f$ :

\medskip

\A est strictement décroissante

\B est constante de valeur 1

\C  est strictement croissante

\D n'est ni croissante ni décroissante

\medskip

\textbf{R1} Dresser sans justification le tableau de variation de $f$ sur son domaine de définition.

On indiquera en particulier les limites aux bornes des trois intervalles formant le domaine de définition de $f$.

\smallskip

$\square$~\textbf{M6} On donne un réel $y$. L'équation $f(x) = y$ admet alors :

\medskip

\A systématiquement une solution

\B une, deux ou trois solutions, selon la valeur de $y$

\C une ou deux solutions, selon la valeur de $y$

\D exactement deux solutions

\E un nombre fini de solutions, mais peut n'en admettre aucune selon la valeur de $y$

\medskip

\textbf{L1} Préciser la ou les valeurs du réel $y$ pour lesquelles $f(x) = y$ admet une unique solution.

\medskip

$\square$~\textbf{M7} Soit $a$ et $b$ deux réels. Lorsque l'équation $x^2 + ax + b = 0$ possède exactement deux solutions, la somme de ces solutions est:

\begin{center}
\begin{tabularx}{\linewidth}{*{4}{X}p{3.5cm}}
\A $\dfrac a2$& \B $- 2a$& \C  $- \dfrac a2$&\D  $2a$&\E aucune des autres réponses en général\\
\end{tabularx}
\end{center}

\textbf{L2} On fixe ici un réel $y$ pour lequel l'équation $f(x) = y$ admet exactement deux solutions, notées $x_1$ et $x_2$.

Donner en fonction de $y$ la valeur de la somme $x_1 + x_2$.

\bigskip

\textbf{\Large Exercice 2. Une famille de droites}

\medskip

On se place dans un repère orthonormé \Oij{} du plan euclidien. Soit $m$ un nombre réel. On note $C_m$ l'ensemble de points du plan défini dans ce repère par l'équation
\[\left(m^2 - 1\right)x + \left(m^2+ m - 2\right)y - 3m + 4 = m,\]

et on note $\Delta_m$ l'ensemble de points du plan défini dans ce repère par l'équation
\[(m + 1)x + (m + 2)y - 4 = 0.\]

$\square$~\textbf{M8} Laquelle des affirmations suivantes est vraie ?

\medskip

\A $\Delta_m$ est une droite pour certaines valeurs de $m$, mais pas pour d'autres

\B $\Delta_m$ est une droite quelle que soit la valeur de $m$

\C $\Delta_m$ n'est une droite pour aucune valeur possible de $m$

\medskip

$\square$~\textbf{M9} L'ensemble $\Delta_m$ est une droite parallèle à l'axe des abscisses si et seulement si 

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $m = 2$& \B $m = 3$& \C  $m = - 1$&\D  $m = - 2$&\E$m = 0$\\
\end{tabularx}
\end{center}

\smallskip

$\square$~\textbf{M10} $\Delta_m$ est une droite perpendiculaire à l'axe des abscisses si et seulement si :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $m = -2$& \B $m = 0$& \C  $m = 2$&\D  $m = 3$&\E $m = - 1$\\
\end{tabularx}
\end{center}

\smallskip

$\square$~\textbf{M11} L'ensemble $C_1$ est :

\medskip

\A le plan tout entier

\B l'ensemble vide 

\C constitué d'un seul point

\D constitué de deux points

\E constitué de trois points

\medskip

$\square$~\textbf{M12} L'ensemble $C_m$ est :

\medskip

\A une droite ou le plan tout entier, selon la valeur de $m$

\B aucune des autres réponses proposées

\C une droite, quelle que soit la valeur de $m$

\D une droite ou l'ensemble vide, selon la valeur de $m$

\E un ensemble fini quelle que soit la valeur de $m$

\medskip

$\square$~\textbf{M13}  On donne un autre réel $p$, et on suppose que $\Delta_m$ et $\Delta_p$ sont des droites. Pour que $\Delta_m $ et $\Delta_p$ soient perpendiculaires, il faut et il suffit que :

\medskip

\A $(m+1)(p+ 2)- (m + 2)(p + 1) = 1$

\B aucune des autres réponses proposées

\C $(m + 1)(p + 1)- (m + 2)(p + 2) = 0$

\D $(m + 1)(p + 2)- (m + 2)(p + 1) = 0$

\E $(m + 1)(p + 1) + (m + 2)(p + 2) = 0$

\medskip

\textbf{L3} Donner les coordonnées d'un point du plan par lequel passent une infinité de droites $\Delta_m$.

\medskip

\textbf{L4} Donner l'équation d'une droite qui n'est parallèle à aucune des droites $\Delta_m$ .

\bigskip

\textbf{\Large Exercice 3. Calculs, Fractions}

\medskip

$\square$~\textbf{M14}  La puissance quatrième de $\sqrt{1 + \sqrt{1 + 1}}$ est : 

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $2 + \sqrt 2$& \B $5$&\C  $2 + 2\sqrt 2$&\D  $3 + 2\sqrt 2$&\E $3$\\
\end{tabularx}
\end{center}

$\square$~\textbf{M15} Lorsque $x$ est un réel non nul, la fraction $\dfrac{\dfrac 1x - x}{x + \dfrac 1x }$ est systématiquement égale à :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac{x - 1}{x + 1}$& \B aucune des autres propositions& \C  $\dfrac{1}{x^2} - x^2$&\D  $\dfrac{1 - x^2}{x^2 + 1}$&\E $\dfrac{1 - x}{x + 1}$\\
\end{tabularx}
\end{center}

$\square$~\textbf{M16} Étant donné un nombre entier $p$, on considère les nombres $x =1 + 2^p$ et $y = 1 + 2^{-p}$.
 Alors $y$ vaut:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A aucune des autres propositions& \B $\dfrac{x - 1}{x }$& \C  $\dfrac{x  + 1}{x}$&\D  $\dfrac{x}{x - 1}$&\E $\dfrac{x}{x + 1}$\\
\end{tabularx}
\end{center}

\smallskip

$\square$~\textbf{M17} Pour tout choix du nombre réel $x$ différent de 1 et $-1$, la quantité $\dfrac{x^2}{x^2 - 1} - \dfrac{1}{x - 1}$ est égale à :

\A aucune des autres réponses proposées

\B $\dfrac{x^2 +x + 1}{(x + 1)(x - 1)}$

\C $\dfrac{x^2 - x -1}{(x + 1)^2}$

\D $\dfrac{x^2 - x + 1}{(x + 1)(x - 1)}$

\E $\dfrac{x^2 - x -1}{(x + 1)^2(x - 1)}$

\medskip

$\square$~\textbf{M18} Soit $x$ un réel strictement positif. La quantité
\[A = \cfrac{1}{1 + \cfrac{1}{1 + \cfrac {1}{1 + \cfrac 1x}}}.\]
est alors égale à :

\A$\dfrac{2x + 1}{x + 1}$

\B aucune des autres réponses proposées

\C $x$

\D $\dfrac{3x+ 2}{2x+ 1}$

\E $\dfrac{2x + 1}{3x+ 2}$

\medskip

Dans les questions \textbf{M19}  et \textbf{M20}, on considère la fonction $f$ associant à tout réel $x$ différent de 1, 2 et 3 le réel 
\[f(x) = \dfrac{1}{(x - 1)(x - 2)(x - 3)}\]

$\square$~\textbf{M19} On suppose disposer d'un  nombre réel $a$, ou valant $+\infty$, tel que $f(x)$ tende vers $0$ quand $x$ tend vers $a$. Alors :

\A $a = 0$

\B $a = 2$

\C $a = +\infty$

\D on ne peut pas donner précisément la valeur de $a$

\E un tel $a$ n'existe pas

\medskip

$\square$~\textbf{M20} On admet qu'il existe des constantes $\alpha, \beta$ et $\gamma$ vérifiant l'égalité 
\[f(x) = \dfrac{\alpha}{x - 1} + \dfrac{\beta}{x - 2} + \dfrac{\gamma}{x - 3}\]
 pour tout réel $x$ en lequel $f$ est définie. 

La somme $\alpha + \beta + \gamma$ vaut alors :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A 0&\B $6$&\C  $1$&\D  $- 6$&\E $- 1$\\
\end{tabularx}
\end{center}

\medskip

$\square$~\textbf{M21} Pour tout choix des nombres réels $x, y$, et $z$, la valeur de
\[M = x^2y + y^2x + y^2z + xz^2 + yz^2 + zx^2 - (x + y + z)(xy + yz + zx)\]

est :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A {\footnotesize aucune des autres réponses proposées} &\B $3xyz$&\C 0&\D $- xyz$&\E $- 3xyz$
\end{tabularx}
\end{center}

\medskip

$\square$~\textbf{M22} Le nombre d'entiers relatifs $x$ vérifiant $9^{\left(x^2\right)} = 3^{x+1}$ est :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A au moins égal à 4 &\B 1&\C 2&\D 0&\E 3\\
\end{tabularx}
\end{center}

\smallskip

$\square$~\textbf{M23} On dispose de deux entiers relatifs $x$ et $y$ tels que l'on ait les égalités $4^x = 8 \times 2^{x + y}$ et $9^{x+y} = 243 \times 3^{5y}$.

Le produit $xy$ est alors égal à :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A 10&\B 12&\C une autre valeur que celles proposées&
\D on ne peut pas donner la valeur précise de $xy$ &\E 4
\end{tabularx}
\end{center}

\bigskip
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\textbf{\Large Exercice 4. Encadrements}

\medskip

Dans tout cet exercice, on se donne deux réels $x$ et $y$ vérifiant les inégalités 
\begin{center}$-2 \leqslant x \leqslant 5$\quad  et \quad $-3 \leqslant y \leqslant 6$.\end{center}

\smallskip

$\square$~\textbf{M24} Le meilleur encadrement possible pour $|x| + 2y$ est entre :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A 0 et 8 &\B $-4$ et 17 &\C $-6$ et 17 &\D aucune des autres réponses &\E $-8$ et 17
\end{tabularx}
\end{center}

$\square$~\textbf{M25} Le meilleur encadrement possible pour $3y - 2x$ est entre :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $-1$ et 1&\B $-13$ et 27 &\C $-19$ et 22 &\D $- 5$ et 8 &\E $-18$ et 23
\end{tabularx}
\end{center}

\medskip

$\square$~\textbf{M26} Le meilleur encadrement possible pour $\dfrac{2x + 6}{y + 8}$ est entre :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac17$ et $\dfrac{16}{5}$&\B $\dfrac16$ et $\dfrac{12}{7}$ &\C $\dfrac25$ et $\dfrac87$ &\D $\dfrac{2}{13}$ et $\dfrac{16}{7}$ &\E $- 2$ et 6
\end{tabularx}
\end{center}

\smallskip

$\triangle$~\textbf{L5} Donner le meilleur encadrement possible pour $x^2 + y^2$. 

Dans la dernière partie de cet exercice, on s'intéresse au meilleur encadrement possible pour la quantité

\[z = x^2 - 2xy + 3y^2.\]

\smallskip

\textbf{R2}  Démontrer que la plus petite valeur possible pour $z$ est 0.

On suggère pour cela de trouver deux constantes $a$ et $b$ telles que $z$ se réécrive 
$(x - ay)^2 + by^2$.

\medskip

$\square$~\textbf{M27} On fixe $y$ dans $[-3~;~6]$. On considère la fonction $g$ qui à $x$ dans $[- 2~;~5]$ associe 
\[g(x) = x^2- 2xy + 3y^2.\]

La fonction $g$ prend sa valeur maximale :

\medskip

\A en un point de $]-2~;~5[$, pour au moins une valeur de $y$

\B en $x =- 2$, quel que soit le choix de $y$

\C en $x = -2$ ou $x = 5$, selon le choix de $y$

\D $\dfrac{2}{13}$ et $\dfrac{2}{13}$ 

\E en $x = 5$, quel que soit le choix de $y$

\medskip

$\square$~\textbf{M28} La valeur maximale prise par $z$ est:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $82$&\B aucune des autres valeurs proposées &\C 19  &\D 136 &\E 73
\end{tabularx}
\end{center}

$\bigcirc$~R3 Justifier la réponse à la question \textbf{M28}, en considérant comme acquis le résultat de la question \textbf{M27} (qu'on ne justifiera donc pas).

\bigskip

\textbf{\Large Exercice 5. Équations et inégalités}

\medskip

$\square$~\textbf{M29} Les solutions réelles de $x^4 - x^2 - 2= 0$ sont au nombre de :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A 2&\B 3 &\C 0 &\D 1 &\E 4
\end{tabularx}
\end{center}

\medskip

$\square$~\textbf{M30} Le nombre de solutions de l'équation $\dfrac{1}{x^2} = 5|x|$ d'inconnue réelle $x$ est :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A 2&\B 1&\C infini&\D fini et strictement supérieur à 2&\E 0
\end{tabularx}
\end{center}

\medskip

$\square$~\textbf{M31} L'ensemble des nombres réels $x \geqslant 0$ vérifiant $x^4 - 3x^2 + 1 > -1$ est :

\medskip

\A $[0~;~1[ \cup  \left[\sqrt 2~;~+\infty\right[$

\B aucune des autres solutions proposées

\C [0~;~2]

\D $]-\infty~;~ 1[ \cup ]2~;~+\infty[$ 

\E $[0~;~1[ \cup  \left]\sqrt 2~;~+\infty\right[$

\medskip

$\square$~\textbf{M32} On s'intéresse aux constantes $C$ telles que, pour tout élément $x$ de $[-1~;~1]$, on ait $x^2\left(1 - x^2\right) \leqslant C$.

La plus petite valeur possible pour une telle constante C est :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac18$&\B $\dfrac12$&\C $\dfrac14$&\D 1&\E aucune des autres réponses proposées 
\end{tabularx}
\end{center}

\smallskip

\textbf{L6} Donner le domaine de définition $D$ de la fonction $x \longmapsto \dfrac{\ln \left(\left|x^2- \frac14\right|\right)}{\sqrt{3 - x^2}}$.

\medskip

$\square$~\textbf{M33} Pour un réel $a$, on considère la fonction $f_a$ définie sur $\R$ par $f_a(x) = x^2 - 2ax + 3a - 2$. 

L'ensemble des réels a pour lesquels $f_a$ s'annule en exactement deux points est :
\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A [1~;~2] &\B ]1~;~2[&\C {\footnotesize aucune des autres réponses proposées} &\D vide&\E $]-2~;~1[$
\end{tabularx}
\end{center}

$\square$~\textbf{M34} Le nombre de solutions de l'équation $\ln (x) = x - 3$ est:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A 3 &\B 1& \C 2 &\D 0 &\E 4
\end{tabularx}
\end{center}

\textbf{R4} Justifier votre réponse à la question \textbf{M34}.

\bigskip

\textbf{\Large Exercice 6. Limites, dérivées}

\medskip

$\square$~\textbf{M35} La limite de $\e^x - \e^{-2x}$ quand $x$ tend vers $- \infty$ :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A est une limite finie non nulle &\B est 0 &\C n'existe pas &\D est $-\infty$ &\E est $+\infty$ 
\end{tabularx}
\end{center}

\medskip

$\square$~\textbf{M36} La limite de $\ln (2x + 1) - \ln (5x + 2)$ quand $x$ tend vers $+\infty$ :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A est $-\infty$&\B est une limite finie non nulle &\C  n'existe pas &\D est $+\infty$&\E est $0$
\end{tabularx}
\end{center}

$\square$~\textbf{M37} Le signe de $x - \ln(1 + x)$ quand le nombre réel $x$ varie dans l'intervalle $[0~;~ +\infty[$ :

\begin{center}
\begin{tabularx}{\linewidth}{*{3}{X}}
\A est négatif &\B varie selon la valeur de $x$&\C est positif
\end{tabularx}
\end{center}

\textbf{L7}  Donner la dérivée de la fonction $f$ définie pour $x\geqslant 0$ par 
\[f(x) = \ln (1 + x) - x + \dfrac{x^2}{2}.\]

$\square$~\textbf{M38} Quand le nombre réel $x$ varie dans l'intervalle $[0~;~ +\infty[$, le signe de $\ln (1 + x) - x + \dfrac{x^2}{2}$ :

\begin{center}
\begin{tabularx}{\linewidth}{*{3}{X}}
\A varie selon la valeur de $x$&\B est positif &\C est négatif
\end{tabularx}
\end{center}

$\square$~\textbf{M39} Soit $a, b, c, d$ des nombres réels, avec $c \ne 0$. On considère la fonction $f$ définie par $f(x) = \dfrac{ax + b}{cx + d}$ pour tout réel $x$ tel que $cx + d \ne 0$.

La fonction dérivée de $f$ associe alors systématiquement à $x$ le réel :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac{ab + cd}{(cx +d)^2}$ &\B $\dfrac{ac - bd}{(cx +d)^2}$&\C $\dfrac{ad - bc}{(cx +d)^2}$&\D $\dfrac{ab - cd}{(cx +d)^2}$& E  $\dfrac{ad + bc}{(cx +d)^2}$
\end{tabularx}
\end{center}

\smallskip

$\square$~\textbf{M40} La dérivée de la fonction qui à $x$ associe $x^3\e^{-2x}$ est la fonction qui à $x$ associe : 

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\left(3x^2 - 2x^3\right) \e^{2x}$&\B  $- 3x^2\e^{-2x}$&\C $-2x^3\e^{-2x}$&\D $\left(- 3x^2 + 2x^3\right)\e^{-2x}$
\end{tabularx}
\end{center}


$\square$~\textbf{M41} La dérivée de la fonction qui à $x$ associe $\dfrac{x^2 +1}{x^2 + 3}$ 
est la fonction qui à $x$ associe:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac{2x^2 - 6x}{\left(x^2 + 3\right)^2}$&\B  $\dfrac{2x^2 + 6x}{\left(x^2 + 3\right)^2}$&\C $\dfrac{- 4x}{\left(x^2 + 3\right)^2}$&\D $\dfrac{2}{\left(x^2 + 3\right)^2}$&\E $\dfrac{4x}{\left(x^2 + 3\right)^2}$
\end{tabularx}
\end{center}

$\square$~\textbf{M42} La dérivée de la fonction qui à $x$ associe $\dfrac{\e^{2x} + 1}{\e^{2x} + 3}$ est la fonction qui à $x$ associe :

\begin{center}
\begin{tabularx}{\linewidth}{*{3}{X}}
\A $\dfrac{2\e^{2x} +6\e^x}{\left(\e^{2x} + 3\right)^2}$&\B $\dfrac{8\e^{2x}}{\left(\e^{2x} + 3\right)^2}$&\C $\dfrac{4\e^{x}}{\left(\e^{2x} + 3\right)^2}$\\
\D $\dfrac{2}{\left(\e^{2x} + 3\right)^2}$&\E aucune des autres réponses proposées&
\end{tabularx}
\end{center}

$\square$~\textbf{M43} La dérivée de la fonction qui à $x$ associe $\left(\e^{-x} + x\right)^3$ est la fonction qui à $x$ associe:

\begin{center}
\begin{tabularx}{\linewidth}{*{3}{X}}
\A $\left(\e^{-x} + x\right)^2$&\B $3\left(1 + \e^{-x}\right)\left(\e^{-x} + x \right)^2$&\C $3\left(\e^{-x} + x\right)^2$\\
\D $3\left(1 - \e^{-x}\right)\left(\e^{-x} + x \right)^2$&\E aucune des autres réponses proposées&
\end{tabularx}
\end{center}

$\square$~\textbf{M44} La limite de $\dfrac{\e^x - 1 - x}{x}$ quand $x$ tend vers 0 :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A n'existe pas &\B est $-\infty$&\C {\small est une limite finie non nulle}&\D est 0&\E  est $+ \infty$
\end{tabularx}
\end{center}

$\square$~\textbf{M45} La limite de $\dfrac{\e^{2x} - 1 - x}{x}$ quand $x$ tend vers $0$ :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A est une limite finie non nulle &\B est $-\infty$&\C est 0&\D n'existe pas&\E est $+ \infty$
\end{tabularx}
\end{center}

\bigskip

\textbf{\Large Exercice 7. Pile ou Face}

\medskip

Soit $n$ un entier naturel supérieur ou égal à 2.

On lance $n$ fois de suite une pièce équilibrée, et l'on note P lorsque la pièce tombe sur Pile, et F lorsqu'elle tombe sur Face. 

Le résultat est alors donné par une liste de $n$ lettres P ou F{}.

Par exemple, si $n = 4$ on note P F P P pour indiquer qu'on a tiré Pile au premier lancer, puis Face au deuxième, puis Pile au troisième et au quatrième lancer.

\medskip

$\square$~\textbf{M46} La probabilité que la suite des lancers débute par P F vaut:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A 1&\B $\dfrac12$&\C$\dfrac14$&\D {\small aucune des autres réponses proposées}&\E $\dfrac34$
\end{tabularx}
\end{center}

$\square$~\textbf{M47} La probabilité que le premier et le dernier lancer donnent le même résultat est:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac14$&\B $1$&\C$\dfrac12$&\D {\small aucune des autres réponses proposées}&\E $\dfrac34$
\end{tabularx}
\end{center}

$\square$~\textbf{M48} Le nombre total de listes donnant les résultats possibles de l'expérience est :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $2^n$&\B $2^{n-1}$&\C $\dfrac{n(n + 1)}{2}$&\D $\dfrac{n(n - 1)}{2}$&\E $
n!$
\end{tabularx}
\end{center}

$\square$~\textbf{M49} La probabilité d'obtenir au moins deux Pile lors des $n$ lancers est toujours égale à :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac{2^n - (n + 1)}{2^n}$&\B $\dfrac{2^n - n}{2^n}$&\C $\dfrac{n}{2^n}$&\D $\dfrac{1}{2^n}$&\E $\dfrac{n + 1}{2^n}$
\end{tabularx}
\end{center}

On envisage les deux évènements :

$A$ : \og la liste de résultats comporte deux P consécutifs\fg{} ;

$B$ : \og la liste de résultats comporte deux F consécutifs \fg{}
ainsi que leurs négations respectives $\overline{A}$ et $\overline{B}$.

Par exemple, quand la liste des lancers est FPPPF{}, l'évènement $A$ est réalisé (il y a deux Pile consécutifs, aux deuxième et troisième lancers) mais B n'est pas réalisé car il n'y a pas deux Face consécutifs.

$\square$~\textbf{M50} Les probabilités des évènements $A$ et $B$ :

\A  sont égales quelle que soit la valeur de $n$

\B peuvent être égales ou différentes, selon la valeur de $n$

\C sont différentes quelle que soit la valeur de $n$.


$\square$~\textbf{M51} Dans cette question, on suppose $n = 4$. La probabilité de $A \cap B$ vaut alors :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A 1&\B $\dfrac18$&\C $\dfrac14$&\D $\dfrac{1}{16}$&\E$\dfrac12$
\end{tabularx}
\end{center}

$\square$~\textbf{M52} On revient au cas général sur $n$. La probabilité de $A \cap B$ vaut:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac14$&\B $\dfrac{n}{2^n}$&\C $\dfrac12$&\D $\dfrac{1}{2^{n-1}}$&\E$\dfrac{1}{2^n}$
\end{tabularx}
\end{center}

$\square$~\textbf{M53} On revient au cas $n = 4$.

La probabilité de $A$ vaut alors :

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac12$ &\B $\dfrac38$&\C $\dfrac14$&\D $\dfrac34$&\E  $\dfrac58$
\end{tabularx}
\end{center}

Dans la suite, on note $p_n$ la probabilité de $A$. On convient que $p_1 = 0$ et $p_0 = 0$.

\medskip

$\square$~\textbf{M54} La probabilité de $A$ sachant que le tirage commence par F vaut:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac12p_{n-2}$ &\B $\dfrac12p_{n-1}$&\C $p_n$&\D $p_{n-1}$&\E  $\dfrac12p_{n}$
\end{tabularx}
\end{center}

$\square$~\textbf{M55} La probabilité de $A$ sachant que le tirage commence par P F vaut:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $p_{n-1}$ &\B $p_{n-2}$&\C $\dfrac14 p_{n-2}$&\D $p_{n}$&\E  $\dfrac14p_{n-1}$
\end{tabularx}
\end{center}

$\square$~\textbf{M56} La suite $\left(p_n\right)_{n\geqslant 0}$ vérifie, à partir du rang $n = 2$, la relation de récurrence:

\A $p_n = \dfrac12 p_{n-1} + \dfrac14 p_{n-2} + \dfrac18$

\B $p_n = \dfrac14 p_{n-1} + \dfrac14 p_{n-2} + \dfrac12$

\C $p_n = \dfrac12 p_{n-1} + \dfrac12 p_{n-2} + \dfrac18$

\D $p_n = \dfrac14 p_{n-1} + \dfrac14 p_{n-2} + \dfrac18$

\E $p_n = \dfrac12 p_{n-1} + \dfrac14 p_{n-2} + \dfrac14$

\medskip

\textbf{M57} La suite de terme général $u_n = 1 - p_n$ vérifie, à partir du rang $n = 0$, la relation de récurrence :

\A  $u_{n+2} = \dfrac12 u_{n+1} + \dfrac12 u_n - \dfrac18$

\B $u_{n+2} = \dfrac14 u_{n+1} + \dfrac14 u_n + \dfrac34$

\C $u_{n+2} = \dfrac12 u_{n+1} + \dfrac14 u_n$

\D $u_{n+2} = \dfrac12 u_{n+1} + \dfrac14 u_n + \dfrac18$

\E $u_{n+2} = \dfrac14 u_{n+1} + \dfrac14 u_n $

\medskip

$\triangle$ \textbf{L8} On pose $x = \dfrac{1 + \sqrt 5}{4}$ et $y = \dfrac{1 - \sqrt 5}{4}$·

Donner sans justification deux réels $c$ et $d$ tels que $x^2 = cx + d$ et $y^2 = cy + d$.

\medskip

$\triangle$ \textbf{L9} Donner sans justification deux réels $a$ et $b$ tels que $u_0 = a + b$ et $u_1 = ax + by$.

\medskip

$\triangle$ \textbf{R5} Démontrer que $u_n = ax^n +by^n$ pour tout entier naturel $n$.

On pourra tenir pour acquis tous les résultats antérieurs corrects sans en apporter de justification, et on suggère d'introduire la propriété
\begin{center}$\mathcal{P}(n) : \quad \og u_n = ax^n + by^n$ \quad et \quad $u_{n+1} = ax^{n+1} + by_{n+1}$.\fg
\end{center}

$\square$~\textbf{M58} La suite $\left(p_n\right)_{n\in \N}$ :

\A  n'a pas de limite

\B converge vers 0 

\C converge vers $\dfrac12$

\D converge vers 1

\E on ne peut pas conclure au vu de ce qui précède

\bigskip

\textbf{\Large Exercice 8. Fonctions trigonométriques }

\medskip

Questions variées

$\triangle$ \textbf{L10} Donner un nombre réel $a > 0$ tel que les solutions de l'équation 
\[\cos (x) \cdot \sin(x) = 0\]
 soient les réels de la forme $k\alpha$ où $k \in \Z$.

\medskip

$\square$~\textbf{M59} La dérivée de la fonction qui à $x$ dans $\left[0~;~\dfrac{\pi}{6}\right]$ associe $\ln [\cos(3x)]$ est la fonction qui à $x$ associe:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $-\dfrac{\sin(3x)}{3\cos(3x)}$ &\B $-3\dfrac{\sin(3x)}{\cos(3x)}$&\C $\dfrac{3\sin(3x)}{\cos(3x)}$&\D $-3\dfrac{\sin(3x)}{\cos ^2(3x)}$& \E  $-\dfrac{\sin(3x)}{3\cos^2 (3x)}$
\end{tabularx}
\end{center}

$\square$~\textbf{M60} Sur l'intervalle $]0~;~\pi[$, la fonction $f : x\longmapsto \dfrac{\cos (x)}{\sin (x)}$:

\A est croissante puis décroissante 

\B est décroissante puis croissante 

\C est croissante

\D n'est pas définie en tout point 

\E est décroissante

\bigskip

\textbf{\large Étude d'une famille de fonctions}

\medskip

Soit $a$ et $b$ deux nombres réels. On définit une fonction $f_{a,~b}$ sur $\R$ par 
\[f_{a,~b}(x)  = \cos(ax + b) \e^{ax}.\]

$\square$~\textbf{M61} La dérivée de $f_{2,~0}$ est la fonction qui à tout réel $x$ associe : 

\A~~ $f_{2,~0}(x)$

\B~$\left[\cos(2x) + \sin(2x)\right] \e^{2x}$

\C~$2\left[\cos(2x) + \sin(2x)\right]\e^{2x}$

\D~$\left[\cos (2x) - \sin (2x)\right] \e^{2x}$

\E~$\left[\cos(2x) - \sin(2x)\right] \e^{2x}$

\medskip

$\square$~\textbf{M62} Soit, pour $n$ entier naturel, $u_n = \ln \left|f_{2,~0}\left(\frac{\pi}{3} + n\pi\right)\right|$.  La suite de terme général $\dfrac{u_n}{n}$ tend alors vers:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac{\sqrt 3}{2}$&\B $2\pi$ &\C $+ \infty$&\D 0&\E 1
\end{tabularx}
\end{center}

On admet la formule suivante: pour tous $a$ et $x$ dans $\R$,

\[\sin(x - a) = \cos(a) \sin(x) - \sin(a) \cos(x).\]

$\square$~\textbf{M63} En choisissant convenablement le nombre $a$ dans la formule précédente, on peut voir que les solutions de l'équation $\cos (x) = \sin (x) $sont:

\smallskip

\A les nombres de la forme $\dfrac{\pi}{4} + k\pi$ où $k \in \Z$

\B les nombres de la forme $- \dfrac{k\pi}{3}$  où $k \in \Z$ 

\C aucune des autres réponses proposées

\D les nombres de la forme $\dfrac{k\pi}{4}$ où $k \in \Z$

\E les nombres de la forme $\dfrac{k\pi}{6}$ où $k \in \Z$

\medskip

\textbf{Vrai ou faux ?}

On conserve les notations de la partie précédente. Dans les questions \textbf{M64} à \textbf{M69}, on demande d'évaluer la valeur logique (Vrai ou Faux) des propositions indiquées.

\medskip

$\square$~\textbf{M64} En tout réel où la dérivée de $f_{1,~0}$ s'annule, cette dérivée change de signe.

\begin{center}\A Vrai \qquad \B Faux\end{center}

$\square$~\textbf{M65} En tout réel $x$ où la dérivée de $f_{1,~0}$ s'annule, la fonction $f_{1,~0}$ a un maximum local, autrement dit on peut
trouver des réels $y, z$ tels que $y < x < z$ et $f_{1,~0}(x)$ soit la plus grande valeur prise par $f_{1,~0}$ sur le segment $[y~;~z]$

\begin{center}\A Vrai \qquad \B Faux\end{center}

$\square$~\textbf{M66} En l'abscisse de tout point où les courbes représentatives de la fonction $f_{1,~0}$ et de la fonction $x \longmapsto \e^x$ se touchent, la dérivée de $f_{1,~0}$ s'annule.

\begin{center}\A Vrai \qquad \B Faux\end{center}

$\square$~67 Si $a \ne 0$, alors en $+\infty$ la fonction $f_{a,~b}$ tend vers 0 ou $+\infty$.

\begin{center}\A Vrai \qquad \B Faux\end{center}

$\square$~68 Si $a \ne 0$, alors ou bien la fonction $f_{a,~b}$ tend vers 0 en $+\infty$, ou bien elle tend vers 0 en $-\infty$.

\begin{center}\A Vrai \qquad \B Faux\end{center}

$\square$~69 La fonction $f_{a,~b}$ est périodique si et seulement si $a = 0$.

\begin{center}\A Vrai \qquad \B Faux\end{center}

\bigskip

\textbf{\Large Exercice 9. Distance d'un point à une courbe}

\medskip

On considère, dans un repère orthonormé du plan, la courbe $\mathcal{C}$ d'équation $y = - 2x^2$. On considère le point A de
coordonnées $(0~;~-1)$, et le point B de coordonnées (0~;~1).

\medskip

$\square$~\textbf{M70} Soit $b$ et $c$ deux nombres réels.

La plus petite valeur prise par $x^2 + bx + c$ lorsque $x$ varie dans $\R$ est systématiquement:

\A $\dfrac{-4c + b^2}{4}$

\B $\dfrac{4c - b^2}{2}$

\C aucune des autres expressions proposées

\D $\dfrac{4c - b^2}{4}$

\E $2c - b^2$

\medskip

On note $d$ la plus courte distance de A à un point de $\mathcal{C}$.

\medskip

$\square$~\textbf{M71} Que vaut $d$ ?

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $\dfrac{\sqrt 7}{4}$& \B $\dfrac{\sqrt{11}}{5}$& \C  $\dfrac23$&\D  $1$&\E $\dfrac{\sqrt 5}{2}$\\
\end{tabularx}
\end{center}

On considère maintenant un point $M_0$ de $\mathcal{C}$ à distance $d$ de A, et on considère la tangente $\Delta$ à $\mathcal{C}$ au point $M_0$.

\medskip

$\square$~\textbf{M72} Le point d'intersection de $\Delta$ avec l'axe des ordonnées a pour ordonnée:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A cela dépend du choix de $M_0$&\B $0$&\C $\dfrac{\sqrt 5}{2}$&\D $\dfrac{3}{2}$&\E $\dfrac34$\\
\end{tabularx}
\end{center}

$\square$~\textbf{M73} Le point d'intersection de $\Delta$ avec l'axe des abscisses a pour abscisse:

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A $0$&\B $\dfrac34$&\C  cela dépend du choix de $M_0$&\D  $\dfrac{\sqrt 3}{2\sqrt 2}$&\E $\dfrac{\sqrt 3}{4\sqrt 2}$\\
\end{tabularx}
\end{center}

$\square$~\textbf{M74} La plus courte distance de B à un point de C vaut: 

\begin{center}
\begin{tabularx}{\linewidth}{*{5}{X}}
\A aucune des autres valeurs proposées& \B $\dfrac34$& \C  $1$&\D  $\dfrac32$&\E $\dfrac12$\\
\end{tabularx}
\end{center}
\end{document}