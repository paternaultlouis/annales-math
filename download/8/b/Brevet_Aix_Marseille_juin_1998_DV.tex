\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{lscape}
\usepackage{multicol}
\usepackage{diagbox}
\usepackage{multirow} 
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pstricks,pst-plot,pst-text,pst-tree}
\usepackage{pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm,headheight=14pt]{geometry}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
%\usepackage[colorlinks=true,pdfstartview=FitV,linkcolor=blue,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage[dvips]{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {Brevet},
pdftitle = {Aix__Marseille juin 1998},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[french]{babel}
\usepackage[np]{numprint}
\begin{document}
\setlength\parindent{0mm}
\rhead{}
\lhead{\small L'année 1998}
\rfoot{\small Aix--Marseille}
\lfoot{\small juin 1998}
\pagestyle{fancy}
\thispagestyle{empty}
\begin{center} {\huge \textbf{\decofourleft~Brevet Aix--Marseille \footnote{Corse, Montpellier, Nice, Toulouse} juin 1998 \decofourright}}
\end{center}

\bigskip

\textbf{PARTIE NUMÉRIQUE}

\medskip

\textbf{Exercice 1}

\medskip

Calculer:

\[A = \dfrac73 - \dfrac53 \times \dfrac23\qquad \text{et}\qquad B =\sqrt{200} - 4\sqrt 3 \times \sqrt 6\]

(B doit être écrit sous forme $a\sqrt{b}$, où $a$ et $b$ sont des entiers, $b$ étant le plus petit possible).

\medskip

\textbf{Exercice 2}

\medskip

Résoudre le système d'équations :$\left\{\begin{array}{l c l}
\phantom{3}2x + \phantom{40}y&=&90\\
30x + 40y &=& \np{2000}
\end{array}\right.$

\medskip

\textbf{Exercice 3}

\medskip

On donne: $f(x)= x + 2, \quad g(x) = 2, \quad  h(x) = 2x$.

\medskip

\begin{enumerate}
\item Parmi les quatre droites tracées ci-dessous, trois d'entre elles représentent les fonctions $f$,\, $g$ et $h$.

Laquelle représente $f$ ? Laquelle représente $g$ ? Laquelle représente $h$ ?

\begin{center}
\psset{unit=5mm,arrowsize=3pt 2}
\begin{pspicture}(-6,-6)(6,8)
\psgrid[gridlabels=0pt,subgriddiv=2,gridwidth=0.4pt]
\psaxes[linewidth=1.25pt,Dx=10,Dy=10]{->}(0,0)(-6,-6)(6,8)
%\psaxes[linewidth=1.25pt](0,0)(0,0)(1,1)
\uput[d](5.8,0){$x$}\uput[r](0,7.8){$y$}
\uput[dr](1,0){\scriptsize 1}\uput[ul](0,1){\scriptsize 1}
\uput[dr](0,0){O}
\psplot[plotpoints=400,linewidth=1.25pt,linecolor=red]{-6}{3}{x 2 add}\uput[dr](-5,-3){\red $d_2$}
\psplot[plotpoints=400,linewidth=1.25pt,linecolor=blue]{-3}{3}{x 2 mul}\uput[ul](-3,-6){\blue $d_4$}
\psline[linewidth=1.25pt,linecolor=green](-3,2)(3,2)\uput[u](-5.8,2){\green $d_1$}
\psline[linewidth=1.25pt](2,-6)(2,8)\uput[r](2,-5.8){$d_3$}
\end{pspicture}
\end{center}
%fig
\item Parmi ces fonctions, l'une est linéaire, laquelle ? Lesquelles sont affines ?
\end{enumerate}

\medskip

\textbf{Exercice 4}

\medskip

\begin{enumerate}
\item Laquelle de ces surfaces hachurées a pour aire : $25 - (x + 3)^2$ ?
\end{enumerate}

\begin{center}
\psset{unit=1cm,arrowsize=3pt 2}
\begin{pspicture}(9.7,3.4)
%\psgrid
\psframe(0.4,0.4)(2.8,2.8)\psframe(0.4,2.8)(0.6,2.6)
\psframe[fillstyle=hlines](0.8,0.4)(2.8,2.4)
\psframe(0.8,0.4)(1,0.6)\psframe(2.8,0.4)(2.6,0.6)\psframe(2.8,2.4)(2.6,2.2)
\psframe(0.4,0.4)(0.6,0.6)
\rput(1.8,1.4){$S_1$}
\psframe[fillstyle=hlines](4,0.4)(6,2.4)
\psframe[fillstyle=solid,fillcolor=white](4.8,0.4)(6,1.6)
\psframe(4,0.4)(4.2,0.6)\psframe(6,0.4)(5.8,0.6)\psframe(6,2.4)(5.8,2.2)
\psframe[fillstyle=solid,fillcolor=white](5.4,1.6)(6,2.2)
\rput(4.4,1.6){$S_2$}
\psframe[fillstyle=hlines](7.2,0.4)(9.2,2.4)
\psframe[fillstyle=solid,fillcolor=white](7.2,0.4)(8.8,2.)
\psframe[fillstyle=solid,fillcolor=white](7.2,0.4)(8.8,2.)
\psframe(7.2,0.4)(7.4,0.6)\psframe(9.2,0.4)(9,0.6)\psframe(4.8,0.4)(5,0.6)
\psframe(4.8,1.6)(5,1.4)\psframe(8.8,0.4)(8.6,0.6)\psframe(7.2,2)(7.4,1.8)
\psframe(7.2,2.2)(7.4,2.4)\psframe(9.2,2.4)(9,2.2)
\rput(8.4,2.2){$S_3$}
\psline[linewidth=0.4pt]{<->}(0.2,0.4)(0.2,2.8)\uput[l](0.2,1.4){$x + 3$}
\psline[linewidth=0.4pt]{<->}(0.4,3)(2.8,3)\uput[u](1.6,3){$x + 3$}
\psline[linewidth=0.4pt]{<->}(0.8,0.2)(2.8,0.2)\uput[d](1.8,0.2){$5$}
\psline[linewidth=0.4pt]{<->}(2.9,0.4)(2.9,2.4)\uput[r](2.9,1.4){$5$}
\psline[linewidth=0.4pt]{<->}(4.8,0.2)(6,0.2)\uput[d](5.4,0.2){$3$}
\psline[linewidth=0.4pt]{<->}(6.2,0.4)(6.2,1.6)\uput[r](6.2,1){$3$}
\psline[linewidth=0.4pt]{<->}(6.2,1.6)(6.2,2.2)\uput[r](6.2,1.9){$x$}
\psline[linewidth=0.4pt]{<->}(4,2.8)(6,2.8)\uput[u](5,2.8){$5$}
\psline[linewidth=0.4pt]{<->}(3.8,0.4)(3.8,2.4)\uput[l](3.8,1.4){$5$}
\psline[linewidth=0.4pt]{<->}(7.2,2.8)(9.2,2.8)\uput[u](8.2,2.8){$5$}
\psline[linewidth=0.4pt]{<->}(9.4,0.4)(9.4,2.4)\uput[r](9.4,1.4){$5$}
\psline[linewidth=0.4pt]{<->}(7.2,0.2)(8.8,0.2)\uput[d](8,0.2){$x + 3$}
\psline[linewidth=0.4pt]{<->}(7.1,0.4)(7.1,2)\rput{90}(6.8,1.2){$x + 3$}
\end{pspicture}
\end{center}

On pose $E = 25 - (x + 3)^2$.

\begin{enumerate}[resume]
\item Développer et réduire $E$.
\item Factoriser $E$.
\item Calculer $E$ pour $x = 2$, puis en donner la troncature à $0,01$ près.
\item Résoudre l’équation : $(2 - x)(x + 8) = 0$.

Expliquer, en utilisant la question 1., pourquoi l'une des solutions de l'équation était prévisible.
\end{enumerate}

\bigskip

\textbf{PARTIE GÉOMÉTRIQUE}

\medskip

\textbf{Exercice 1}

\medskip

\begin{minipage}{0.68\linewidth}
Une pyramide régulière est représentée ici en perspective :

\medskip
\begin{enumerate}
\item Sur le solide SABCD, nommer les arêtes de même longueur que [SA].

Quelle est la nature de la face ABCD ? Expliquer.
\item Calculer le volume de la pyramide SABCD.
\end{enumerate}
\end{minipage}\hfill
\begin{minipage}{0.28\linewidth}
\psset{unit=1cm}
\begin{pspicture}(3.8,4.2)
%\psgrid
\psline(0.2,0.3)(2,0.3)(3,1.1)(1.6,3.7)(2,0.3)%ADCSD
\psline(1.6,3.7)(0.2,0.3)%SA
\psline[linestyle=dashed](0.2,0.3)(1.2,1.1)(3,1.1)%ABC
\psline[linestyle=dashed](1.2,1.1)(1.6,3.7)(1.6,0.7)(3,1.1)%BSH
\psline(1.6,0.85)(1.75,0.92)(1.75,0.75)
\uput[dl](0.2,0.3){A} \uput[ul](1.2,1.1){B} \uput[r](3,1.1){C} \uput[dr](2,0.3){D} \uput[u](1.6,3.7){S} \uput[dl](1.6,0.7){H}
\end{pspicture}
\end{minipage}

\medskip

\textbf{Exercice 2}

\medskip

\begin{minipage}{0.68\linewidth}
Un câble de $20$~m de long est tendu entre le sommet d'un poteau vertical et le sol horizontal. Il forme un angle de $40\degres$ avec le sol (voir schéma).

\medskip

\begin{enumerate}
\item Calculer la hauteur du poteau.
\item Représenter la situation par une figure à l'échelle 1/200.
(les données de la situation doivent être placées sur la figure).
\end{enumerate}
\end{minipage}\hfill
\begin{minipage}{0.28\linewidth}
\psset{unit=1cm}
\begin{pspicture}(4.3,3)
%\psgrid
\psline(0,0.4)(4,0.4)
\psline(0.5,0.4)(3.4,2.9)(3.4,0.4)
\rput{40}(1.8,1.8){Câble}\rput{90}(3.65,1.6){Poteau}\uput[d](2.2,0.4){Sol}
\psarc(0.5,0.4){0.5}{0}{40}\rput(1.35,0.65){$40\degres$}
\end{pspicture}
\end{minipage}

\medskip

\textbf{Exercice 3}

\medskip

(O, I, J) est un repère orthonormal du plan tel que : OI $= 1$ cm et OJ $= 1$ cm.

\medskip

\begin{enumerate}
\item Tracer le repère et ses axes ainsi que les points :

A(3~;~12),\quad B$(11~;~-6)$,\quad P(7~;~3).

Démontrer que A et B sont symétriques par rapport à P{}.
\item Tracer la droite $d$ d'équation: $y = \dfrac49 x$.

Démontrer que le point P n'est pas sur la droite $d$.
\item Calculer le coefficient directeur de la droite (AB).

Les droites $d$ et (AB) sont-elles perpendiculaires ? Justifier.

Les points A et B sont-ils symétriques par rapport à la droite $d$ ?

Justifier.
\end{enumerate}

\bigskip

\textbf{PROBLÈME}

\medskip

\textbf{PRÉLIMINAIRE}

\medskip

\begin{minipage}{0.63\linewidth}
\begin{enumerate}
\item D'après la figure ci-contre, tracer ABCP en respectant les données suivantes: AB $= 6$ cm,\, BC $= 8$ cm,\, BM $= 3$ cm,\, (CP) \psscalebox{2 1}{$\parallel$} (AB).
\item Mesurer les angles $\widehat{\text{BAM}}$ et $\widehat{\text{AMC}}$.

Pourquoi ces mesures ne permettent-elles pas d'affirmer que (AM) est la
bissectrice de $\widehat{\text{BAC}}$ ?
\end{enumerate}
\end{minipage} \hfill
\begin{minipage}{0.33\linewidth}
\psset{unit=1cm}
\begin{pspicture}(4.7,3)
\psline(1.9,0.4)(4.3,0.4)(0.4,2.4)(1.9,0.4)(1.9,2.4)(0.4,2.4)%CPACBA
\psframe(1.9,2.4)(1.7,2.2)
\uput[l](0.4,2.4){A} \uput[ur](1.9,2.4){B} \uput[dl](1.9,0.4){C} \uput[ur](1.9,1.6){M} 
\uput[dr](4.3,0.4){P}
\end{pspicture}
\end{minipage}

\bigskip

\textbf{Les deux parties peuvent être traitées indépendamment l'une de l’autre}

\bigskip

\textbf{PREMIÈRE PARTIE}

\medskip

\begin{enumerate}
\item En considérant le triangle ABC : 
	\begin{enumerate}
		\item Calculer AC.
		\item Calculer BAC et BAM le plus précisément possible.
	\end{enumerate}
Expliquer pourquoi les valeurs obtenues ne permettent pas d'affirmer que ˆ
(AM) est la bissectrice de BAC .
\item En considérant les triangles ABM et MCP, calculer CP.
\item Quelle est la nature du triangle ACP ? Que peut-on en déduire pour
$\widehat{\text{MAC}}$ et $\widehat{\text{CPM}}$ ?
\item Démontrer alors que $\widehat{\text{MAC}} = \widehat{\text{BAM}}$ et donc que (AM) est bien la bissectrice de $\widehat{\text{BAC}}$.
\end{enumerate}

\bigskip

\textbf{DEUXIEME PARTIE}

\medskip

\begin{enumerate}
\item (AM) est, d'après la première partie, la bissectrice de $\widehat{\text{BAC}}$. Sur
la figure tracée à la première question du prélliminaire :
\begin{itemize}
\item tracer la bissectrice, $d$, de $\widehat{\text{ABM}}$ ;
\item nommer O le point d'intersection de la droite $d$ et de la droite (AM) ;
\item tracer la hauteur issue de O du triangle AOB et la hauteur issue de O du triangle BOM (ces hauteurs sont des rayons du cercle inscrit dans le triangle BAC) ;
\item tracer ce cercle.
\end{itemize}
\item 
	\begin{enumerate}
		\item Calculer l'aire du triangle ABM.
		\item Exprimer l'aire du triangle AOB et l'aire du triangle BOM en
fonction du rayon $r$ du cercle inscrit dans le triangle BAC.
		\item Trouver une relation entre ces trois aires.
En déduire le rayon $r$.
	\end{enumerate}
\end{enumerate}
\end{document}