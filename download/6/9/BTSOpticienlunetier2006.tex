%!TEX encoding = UTF-8 Unicode
\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{graphicx}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Denis Vergès
\usepackage{pst-plot,pst-text,pstricks-add}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\setlength{\textheight}{23,5cm}
\newcommand{\vect}[1]{\mathchoice%
{\overrightarrow{\displaystyle\mathstrut#1\,\,}}%
{\overrightarrow{\textstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptstyle\mathstrut#1\,\,}}%
{\overrightarrow{\scriptscriptstyle\mathstrut#1\,\,}}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O},~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O},~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O},~\vect{u},~\vect{v}\right)$}
\setlength{\voffset}{-1,5cm}
\usepackage{fancyhdr}
\usepackage[frenchb]{babel}
\begin{document}
\setlength\parindent{0mm}
\lhead{\small Brevet de technicien supérieur }
\lfoot{\small{Opticien lunetier}}
\rfoot{\small{juin 2006}}
\renewcommand \footrulewidth{.2pt}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P. M. E. P.}}}
\begin{center} \Large \textbf{\decofourleft~Brevet de technicien supérieur~\decofourright\\ Opticien lunetier session 2006}  
\end{center}

\vspace{0,25cm}

\textbf{Exercice 1 \hfill 10 points}

\begin{center}
\textbf{Les trois parties de cet exercice sont indépendantes.}
\end{center}

Une entreprise est approvisionnée en \og palets \fg pour la fabrication de lentilles.

\medskip

\textbf{Dans cet exercice, les résultats approchés seront arrondis à \boldmath $10^{-2}$ \unboldmath sauf indication contraire de la partie B}

\medskip

\textbf{A. Loi binomiale et loi de Poisson}

\medskip

Dans un lot de ce type de palets, 2\:\% des palets ne sont pas conformes pour le rayon de courbure.

On prélève au hasard 50~palets de ce lot pour vérification du rayon de courbure. Le lot est suffisamment important pour que l'on puisse assimiler ce prélèvement à un tirage avec remise de 50~palets.

On considère le variable aléatoire $X$ qui, à tout prélèvement de 50~palets, associe le nombre de palets non conformes pour le rayon de courbure.

\medskip

\begin{enumerate}
\item  Justifier que la variable aléatoire $X$ suit une loi binomiale dont on déterminera les paramètres.
\item  Calculer la probabilité que, dans un tel prélèvement, un palet et un seul ne soit pas conforme pour le rayon de courbure.
\item  Calculer la probabilité que, dans un tel prélèvement, au plus un palet ne soit pas conforme pour le rayon de courbure.
\item  On considère que la loi suivie par la variable aléatoire $X$ peut être approchée par une loi de Poisson.

Déterminer le paramètre $\lambda$ de cette loi de Poisson.
\item  On désigne par $Y$ une variable aléatoire suivant la loi de Poisson de paramètre $\lambda$ où $\lambda$ est la valeur obtenue au \textbf{4.}.

Calculer $P(Y =  1)$ et $P(Y\leqslant  1)$.
\end{enumerate}

\bigskip

\textbf{B. Évènements indépendants}

\medskip

Dans cette partie, on donnera les valeurs exactes des probabilités.

À l'issue de la fabrication, les lentilles peuvent présenter deux types de défauts :

\setlength\parindent{5mm}
\begin{itemize}
\item une puissance défectueuse,
\item  une épaisseur défectueuse.
\end{itemize}
\setlength\parindent{0mm}

On prélève une lentille au hasard dans la production d'une journée.

On note $A$ l'évènement : \og la lentille présente une puissance défectueuse \fg,

On note $B$ l'évènement : \og la lentille présente une épaisseur défectueuse \fg.

On admet que les probabilités des évènements $A$ et $B$ sont $P(A) = 0,03$ et $P(B) = 0,02$ et on suppose que ces deux évènements sont indépendants.

\medskip

\begin{enumerate}
\item  Calculer la probabilité de l'évènement $E_{1}$ : \og la lentille prélevée présente les deux défauts \fg.
\item Calculer la probabilité de l'évènement $E_{2}$ : \og la lentille prélevée présente au moins un  des deux défauts \fg.
\item  Calculer la probabilité de l'évènement $E_{3}$ : \og la lentille prélevée ne présente aucun défaut \fg.
\item  Calculer la probabilité de l'évènement $E_{4}$ : \og la lentille prélevée présente un seul de deux défauts \fg.

Puisque les évènements $A$ et $B$ sont indépendants, on peut admettre que les événements $\overline{A}$ et $B$ sont indépendants et les évènements $A$ et $\overline{B}$ sont indépendants.
\end{enumerate}

\bigskip

\textbf{C. Test d'hypothèse}

\medskip

Les palets utilisés pour la fabrication des lentilles doivent avoir un diamètre de 9,80 millimètres.

Dans cette partie, on se propose de contrôler la moyenne $\mu$ de l'ensemble des diamètres de palets d'une importante livraison reçue par l'entreprise.\\
On note $Z$ la variable aléatoire qui, à chaque palet prélevé au hasard dans la livraison, associe son diamètre.

La variable aléatoire $Z$ suit la loi normale de moyenne inconnue $\mu$ et d'écart type $\sigma= 0,13$.

On désigne par $\overline{Z}$ la variable aléatoire qui, à chaque échantillon aléatoire de 100~palets prélevé dans la livraison, associe la moyenne des diamètres de ces palets (la livraison est assez importante pour que l'on puisse assimiler ces prélèvements à des tirages avec remise).

L'hypothèse nulle est H$_{0}$ : \og $\mu = 9,80$ \fg. Dans ce cas les palets de la livraison sont conformes pour le diamètre.

L'hypothèse alternative est H$_{1} \og \mu \neq 9,80$ \fg.

Le seuil de signification du test est fixé à $0,05$.

\medskip

\begin{enumerate}
\item  Justifier le fait que, sous l'hypothèse nulle H$_{0},~\overline{Z}$ suit la loi normale de moyenne $9,80$ et d'écart type $0,013$.
\item  Sous l'hypothèse nulle, déterminer le nombre réel $h$ positif tel que :

\[P(9,80 - h \leqslant  \overline{Z} \leqslant  9,80 + h) = 0,95.\]

\item  Énoncer la règle de décision permettant d'utiliser ce test.
\item  On prélève un échantillon de 100~palets et on observe que, pour cet échantillon, la moyenne des diamètres est $\overline{z} = 9,79$.

Peut-on, au seuil de risque de 5\,\%, conclure que les palets de la livraison sont conformes pour le diamètre ?
\end{enumerate}

\vspace{0,5cm}

\textbf{Exercice 2 \hfill 10 points}

\begin{center}

\textbf{Les trois parties de cet exercice peuvent être traitées de façon indépendante.}
\end{center}

\medskip

\textbf{A. Résolution d'une équation différentielle}

\medskip

On considère l'équation différentielle
 
\[(\text{E})~: \quad  y' - y  = \text{e}^x\]

où $y$ est une fonction de la variable réelle $x$, définie et dérivable sur $\R$, et $y'$ sa fonction dérivée.

\medskip

\begin{enumerate}
\item  Déterminer les solutions de l'équation différentielle $\left(\text{E}_{0}\right)~:  \quad  y' -y = 0$.
\item  Soit $h$ la fonction définie sur $\R$ par $h(x) = x\text{e}^x$.\\
Démontrer que la fonction $h$ est une solution particulière de l'équation différentielle (E).
\item  En déduire les solutions de l'équation différentielle (E).
\end{enumerate}

\bigskip

\textbf{B. Étude d'une fonction et tracé de sa courbe représentative}

\medskip

Soit $f$ la fonction définie sur $\R$ par 

\[f(x) = (x + 1)\text{e}^x.\]

On note $\mathcal{C}$ sa courbe représentative dans un repère orthonormal \Oij{} où l'unité graphique est $2$~cm,

\medskip

\begin{enumerate}
\item 
	\begin{enumerate}
		\item Déterminer $\displaystyle\lim_{x \to + \infty} f(x)$.
		\item On admet que $\displaystyle\lim_{x \to - \infty} x\text{e}^x = 0$. En déduire $\displaystyle\lim_{x \to + \infty} f(x)$.
		\item Interpréter graphiquement le résultat obtenu au \text{b.}.
	\end{enumerate}
\item
	\begin{enumerate}
		\item Démontrer que, pour tout $x$ de $\R,~f'(x) = (x +2) \text{e}^x$.
		\item Établir le tableau de variations de $f$.
	 \end{enumerate}
\item
	\begin{enumerate}
		\item Écrire une équation de la tangente T à la courbe $\mathcal{C}$ au point d'abscisse $0$.
		\item Construire sur la feuille de copie quadrillée T et $\mathcal{C}$.
	\end{enumerate}
\end{enumerate}
		
\bigskip

\textbf{C. Calcul intégral}

\medskip

\begin{enumerate}
\item Justifier que la fonction $f$ définie dans la partie B est une solution de l'équation différentielle (E) de la partie A. Donc, pour tout $x$ de $\R,~f(x) = f'(x) - \text{e}^x$.
\item  En déduire une primitive $F$ de $f$ sur $\R$.
\item  Soit $a$ un nombre réel strictement inférieur à $-2$.
On note I $ = \displaystyle\int_{a}^{-2} f(x)\:\text{d}x$.
	\begin{enumerate}
		\item  Démontrer que I $ = - a \text{e}^a -2 \text{e}^{-2}$.
		\item  En déduire l'aire $\mathcal{A}$, en cm$^2$, de la partie du plan limitée par la courbe $\mathcal{C}$, l'axe des abscisses
et les droites d'équation $x = - 4$ et $x = -2$.
		\item  Donner la valeur approchée arrondie à $10^{-2}$ de cette aire.
		\end{enumerate}
\end{enumerate}
\end{document}