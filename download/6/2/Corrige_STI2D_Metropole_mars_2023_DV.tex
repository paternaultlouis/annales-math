\documentclass[11pt,a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}%ATTENTION codage en utf8 ! 
\usepackage{fourier} 
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,makeidx}
\usepackage{fancybox}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{multicol,diagbox}
\usepackage{enumitem}
\usepackage{pifont}
\usepackage{textcomp} 
\newcommand{\euro}{\eurologo}
%Tapuscrit : Denis Vergès
\usepackage{pst-all,pst-func}
\usepackage[left=3cm, right=3cm, top=3cm, bottom=3cm]{geometry}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\newcommand{\vectt}[1]{\overrightarrow{\,\mathstrut\text{#1}\,}}
\newcommand{\barre}[1]{\overline{\,#1\vphantom{b}\,}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {STI2D},
pdftitle = {Polynésie 13 mars 2023},
allbordercolors = white,
pdfstartview=FitH}
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
\newcommand{\e}{\,\text{e\,}}	%%%le e de l'exponentielle
\renewcommand{\d}{\,\text d}	%%%le d de l'intégration
\renewcommand{\i}{\,\text{i}\,}	%%%le i des complexes
\newcommand{\ds}{\displaystyle}
\setlength\parindent{0mm}
\setlength\parskip{5pt}
\begin{document}
\setlength\parindent{0mm}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\small Baccalauréat STI2D}
\lfoot{\small{Métropole Antilles--Guyane}}
\rfoot{\small{20 mars 2023}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}

\begin{center} {\Large \textbf{\decofourleft~Baccalauréat STI2D Épreuve d'enseignement de spécialité~\decofourright\\[10pt]Métropole Antilles--Guyane 20 mars 2023}}

\vspace{0,25cm}

\textbf{\large Physique-Chimie et Mathématiques}

\end{center}

%\medskip

\begin{center}
\textbf{\large EXERCICE 1 physique-chimie et mathématiques (4 points)}

\medskip

\textbf{Le viscosimètre à chute de bille}
\end{center}

La viscosité d'une huile, notée $\nu$, est un paramètre exprimé en kg·m$^{-1}$·s$^{-1}$, dont la connaissance est essentielle pour toute utilisation de cette huile.

Cet exercice propose un exemple de méthode de mesure de la valeur de la viscosité d'une huile de moteur Diesel du commerce.

Pour réaliser cette mesure, on utilise un \og viscosimètre à chute de bille \fg, constitué d'une éprouvette remplie d'huile de moteur dans laquelle est lâchée une bille métallique sphérique.

On se place dans le référentiel terrestre supposé galiléen et la bille est lâchée sans vitesse initiale depuis la position $z = 0$.

\begin{center}
\psset{unit=1cm,arrowsize=2pt 3}
\begin{pspicture}(6,6)
\psline(0.8,5.8)(0.8,0)(1.8,0)(1.8,5.8)
\psline(0.5,0)(2.1,0)
\psframe[fillstyle=solid,fillcolor=lightgray](0.8,0)(1.8,5.8)
\multido{\n=0.0+0.9}{6}{\psline(0.8,\n)(1.2,\n)}
\pscircle*(1.3,4){0.2}
\psline[linewidth=0.6pt]{->}(0.1,5.8)(0.1,0.6)
\uput[u](0.1,5.8){$z = 0$}\uput[d](0.1,0.6){$z$}
\rput(4.4,4){Bille en acier}
\rput(4.4,2.15){Éprouvette}
\rput(4.4,1.8){graduée remplie}
\rput(4.4,1.45){d'huile moteur}
\psline{->}(3.3,4)(1.6,4)
\psline{->}(3.,1.8)(1.8,1.8)
\end{pspicture}
\end{center}

\textbf{Données :}

\begin{itemize}
\item Rayon de la bille utilisée : $R = 1,1$ cm.
\item Volume de la bille : $V = 5,6$~cm$^3 = 5,6 \times 10^{-6}$~m$^3$.
\item Masse de la bille métallique : $m = 20,1$~g.
\item Masse volumique de l'huile étudiée : $\rho_{\text{huile}} = 8,40 \times 10^2$~kg·m$^{-3}$.
\item Intensité de la gravitation : $g = 9,8$~m.s$^{-2}$.
\end{itemize}

\medskip

Les forces exercées sur la bille métallique sont : 

\begin{itemize}
\item La poussée d'Archimède, notée $\vect{P_A}$ de même direction que le poids $\vect{P}$ et de sens opposé. Sa valeur est $P_A = \rho_{\text{huile}} V g$, où $\rho_{\text{huile}}$ est la masse volumique de l'huile.
\item La force de frottement fluide exercée par l'huile sur la bille est notée $\vect{f}$. Elle est ici de même direction que le poids $\vect{P}$ et de sens opposé. Sa valeur est donnée par la relation
$f = 6 \pi \eta Rv$, où $v$ est la valeur de la vitesse de la bille, $\eta$ est la viscosité de l'huile et $R$ le rayon de la bille.
\end{itemize}

\begin{enumerate}[start=3]
\item On note $v$ la fonction définie sur $[0~;~+\infty[$ comme la projection du vecteur vitesse $\vect{v}$ sur l'axe (O$z$).

%Montrer que $v$ vérifie l'équation différentielle
%\[\dfrac{\text{d}v}{\text{d}t} = - \dfrac{6\pi \nu Rv}{m} + g - \dfrac{\rho_{\text{huile}} Vg}{m}.\]
La seconde loi de Newton donne sur l'axe O$z$ :

$m\dfrac{\text{d}v}{\text{d}t} = - 6\rho h R v + mg - \rho_{\text{huile}}Vg$, d'où puisque $m \ne 0$ :

\[\dfrac{\text{d}v}{\text{d}t} = - \dfrac{6\pi \nu Rv}{m} + g - \dfrac{\rho_{\text{huile}} Vg}{m}.\]

En explicitant les valeurs numériques, on admet que $v$ est solution de l'équation différentielle $(E)$ suivante où $v(t)$ est exprimée en m.s$^{-1}$ et $t$ en s :
\[(E) : \qquad \dfrac{\text{d}v}{\text{d}t} = - 6,8v + 7,5.\]

\item  %Au début de l'expérience, la bille est introduite dans l'éprouvette avec une vitesse nulle. 

%Démontrer que la solution $v$ de cette équation sur $[0~;~+\infty[$ vérifiant cette condition initiale est définie par :

$\bullet~~$Les solutions de l'équation différentielle : $\dfrac{\text{d}v}{\text{d}t} + 6,8v = 0$, sont définies par :

$v(t) = K\text{e}^{-6,8t}$, avec $K \in \R$.

$\bullet~~$Une solution particulière constante de cette équation est donnée par $\dfrac{\text{d}v}{\text{d}t} = 0 \iff  - 6,8v + 7,5 = 0 \iff v = \dfrac{7,5}{6,8} = \dfrac{75}{68}$.

La solution générale de cette équation différentielle est donc donnée par : 

$v(t) = K\text{e}^{-6,8t} + \dfrac{75}{68}$ et comme $v(0) = 0 \iff K + \dfrac{75}{68} = 0 \iff K = - \dfrac{75}{68}$, soit finalement :
\[v(t) = - \dfrac{75}{68}\text{e}^{-6,8t} + \dfrac{75}{68}.\]

\item %Déterminer la valeur exacte de $\displaystyle\lim_{t \to + \infty} v(t)$ notée $v_{\text{lim}}$ exprimée en m.s$^{-1}$.
On sait que $\displaystyle\lim_{t \to + \infty} \text{e}^{-6,8t} = 0$ et également que $\displaystyle\lim_{t \to + \infty} - \dfrac{75}{68}\text{e}^{-6,8t} = 0$, donc par somme de limites $\displaystyle\lim_{t \to + \infty} = v(t) = v_{\text{lim}} = \dfrac{75}{68}$.

\item On mesure expérimentalement une vitesse limite $v_{\text{lim}} = 1,1~$m.s$^{-1}$.

On peut en déduire la valeur de la viscosité $\eta$ par la relation suivante :

\[\eta = \dfrac{\left(m - \rho_{\text{huile}} V\right)g}{6\pi R v_{\text{lim}}}.\]

%Calculer cette valeur et comparer le résultat à la valeur $\eta =  0,66$~kg·m$^{-1}$·\,s$^{-1}$ fournie par le fabricant.
On calcule $\eta = \dfrac{\left(20,1\cdot 10^{-3} - 840 \times 5,6 \cdot 10^{-6}\right)\times 9,81}{\left(6\pi \times 0,011 \times 1,1\right)} \approx 0,66$~kg·m$^{-1}$·\,s$^{-1}$.

C'est à peu près la valeur fournie par le fabricant.
\end{enumerate}

\begin{center}
\textbf{\large EXERCICE 3  \quad mathématiques \hfill 4 points}
\end{center}

\medskip

\textbf{Les questions 1, 2, 3 et 4 sont indépendantes les unes des autres. Chacune
d'elles est notée sur un point.}

\bigskip

\textbf{Question 1}

\emph{Pour cette question, indiquer la lettre de la réponse exacte.\\
Aucune justification n'est demandée.}

\medskip

%L'expression $\dfrac{\left(\text{e}^{-3x}\right)^2 \times \left(\text{e}^{2x}\right)^{-3}}{\text{e}^{5x} \times \text{e}^{6x}}$ vaut:

%\begin{center}
%\renewcommand\arraystretch{2.1}
%\begin{tabularx}{\linewidth}{|*{4}{>{\centering \arraybackslash}X|}}\hline
%A				&B						&C					&D\\ \hline
%$\text{e}^{-1}$	&$\dfrac{2}{5}x^{-3}$	&$\text{e}^{- x}$	&$\text{e}^{-23x}$\\ \hline
%\end{tabularx}
%\end{center}
$\dfrac{\left(\text{e}^{-3x}\right)^2 \times \left(\text{e}^{2x}\right)^{-3}}{\text{e}^{5x} \times \text{e}^{6x}} = \dfrac{\text{e}^{-6x}\times \text{e}^{-6x}}{\text{e}^{11x}} = \dfrac{\text{e}^{-12x}}{\text{e}^{11x}} = \text{e}^{-12x - 11x} = \text{e}^{- 23x}$. Réponse D.
\medskip

\textbf{Question 2}

\medskip

%Soit $f$ la fonction définie sur $\R$ par 
%\[f(x) = \text{e}^{2x}(-3x + 1).\]
En posant $u(x) = \text{e}^{2x}$, d'où $u'(x) = 2\text{e}^{2x}$ et $v(x) = - 3x + 1$, d'où $v'(x) = - 3$, on a $f'(x) = (uv)'(x) = u'(x) \times v(x) + u(x) v'(x) = 2\text{e}^{2x}(-3x + 1) - 3\text{e}^{2x} = \text{e}^{2x}[2(- 3x + 1)- 3] = \text{e}^{2x}(-6x + 2 - 3) = \text{e}^{2x}(- 6x - 1)$.

%On admet que la fonction $f$ est dérivable sur $\R$ et on note $f'$ la fonction dérivée de $f$ sur $\R$.

%Montrer que 
%\[f'(x) = \text{e}^{2x}(-6x - 1).\]

\medskip

\textbf{Question 3}

\medskip

%On désigne par i le nombre complexe de module 1 et d'argument $\dfrac{\pi}{2}$.
%
%Mettre le nombre complexe $\sqrt 3 + \text{i}$ sous forme exponentielle en détaillant les calculs.
$\bullet~~$Avec $z = \sqrt 3 + \text{i}$, on a $|z|^2 = \left(\sqrt 3  \right)^2 + 1^2 = 3 + 1 = 4 = 2$, d'où $|z| = 2$.

$\bullet~~$On peut alors en factorisant 2 , écrire :

$z = 2\left(\dfrac{\sqrt 3}{2} + \text{i}\dfrac12\right)$.

Or on sait que $\cos \frac{\pi}{6} = \dfrac{\sqrt 3}{2}$ et $\sin \frac{\pi}{6} = \dfrac{1}{2}$, donc :

$z = 2\left(\cos \frac{\pi}{6} + \text{i}\sin \frac{\pi}{6}  \right) = 2 \text{e}^{\frac{\pi}{6}}$, écriture exponentielle de $z$.
\medskip

\textbf{Question 4}

\medskip

%Résoudre sur l'intervalle $]0~;~+\infty[$ l'équation:

\[\dfrac{2}{3\ln (10)} \ln (x) - 2,88 = 4.\]
$\dfrac{2}{3\ln (10)} \ln (x) - 2,88 = 4 \iff \dfrac{2}{3\ln (10)} \ln (x)  = 6,88 \iff \dfrac{1}{3\ln (10)}\ln (x) = 3,44 \iff  \ln (x) = 3 \times \dfrac{3,44 \ln (10)}{2} \iff \ln (x) = 10,32 \ln (10) \iff \ln (x) = \ln \left(10^{10,32} \right) \iff x = 10^{10,32}$, soit environ $2,089 \times 10^{10}$ par croissance de la fonction logarithme népérien.

$S = \left\{10^{10,32}\right\}$.
\end{document}