\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[scaled=0.875]{helvet}
\renewcommand{\ttdefault}{lmtt}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{fancybox}
\usepackage{tabularx}
\usepackage[normalem]{ulem}
\usepackage{pifont}
\usepackage{graphicx,colortbl,xcolor}
\usepackage{textcomp} 
\usepackage{multirow}
\usepackage{enumitem}
\newcommand{\euro}{\eurologo{}}
%Tapuscrit : Ronan Charpentier & François Hache
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\D}{\mathbb{D}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\C}{\mathbb{C}}
\renewcommand{\theenumi}{\textbf{\arabic{enumi}}}
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\theenumii}{\textbf{\alph{enumii}}}
\renewcommand{\labelenumii}{\textbf{\theenumii.}}
\usepackage[left=3.5cm, right=3.5cm, top=3cm, bottom=3cm]{geometry}
\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{%
pdfauthor = {APMEP},
pdfsubject = {BTS Opticien-lunetier},
pdftitle = {mai 2024},
allbordercolors = white,
pdfstartview=FitH}  
\usepackage[french]{babel}
\DecimalMathComma
\usepackage[np]{numprint}
%\usepackage{tikz}
\usepackage{pst-all,pst-func}
\usepackage{multicol}
\begin{document}
\setlength\parindent{0mm}
\setlength\parskip{4pt}
\rhead{\textbf{A. P{}. M. E. P{}.}}
\lhead{\footnotesize Brevet de technicien supérieur }
\lfoot{\footnotesize{Opticien--lunetier}}
\rfoot{\footnotesize{14 mai 2024}}
\pagestyle{fancy}
\thispagestyle{empty}
\marginpar{\rotatebox{90}{\textbf{A. P{}. M. E. P{}.}}}
\begin{center} \Large \textbf{\decofourleft~Brevet de technicien supérieur Opticien--lunetier~\decofourright\\[5pt]14 mai 2024}  
\end{center}

\textbf{L'usage de la calculatrice avec mode examen actif est autorisé.\\
L'usage de la calculatrice sans mémoire, \og   type collège \fg{} est autorisé.}

\vspace{0.5cm}

\textbf{\Large{}Exercice 1 \hfill 10 points}

\medskip

Pour fabriquer des montures, on chauffe un matériau à 150~\degres C puis on le sort du four et on le laisse refroidir à température ambiante (28~\degres C).

\begin{center}
\textit{Les trois parties peuvent être traitées de manière indépendante.}
\end{center}

\medskip

\textbf{Partie A - Étude d'une série statistique}

\medskip

Pour étudier le refroidissement du matériau, on a réalisé des relevés de température et réalisé un croquis.

\medskip

\begin{minipage}{0.5\linewidth}
\setlength{\tabcolsep}{1pt}
\small
\begin{tabularx}{\linewidth}{|m{2.1cm}|*{7}{>{\centering \arraybackslash}X|}}
\hline
Temps $t$\newline (minutes)&0&1&2&3&4&5&6\\
\hline
Température\newline  $y$ (°C) &150&113&82&65&52&44&40\\
\hline
\end{tabularx}
\end{minipage}
\hfill
\begin{minipage}{0.47\linewidth}
\scalebox{0.8}{
\psset{xunit=1cm, yunit=0.025}
\def\xmin {-1}   \def\xmax {7}
\def\ymin {-20}   \def\ymax {160}
\begin{pspicture*}(\xmin,\ymin)(\xmax,\ymax)
\psgrid[xunit=1cm,yunit=0.5cm,subgriddiv=1,  gridlabels=0, gridcolor=lightgray] 
\psaxes[ticksize=0pt 0pt, labels=none](0,0)(\xmin,\ymin)(\xmax,\ymax) 
\uput[dl](0,0){O}
\listplot[plotstyle=dots,linecolor=blue]{0 150 1 113 2 82 3 65 4 52 5 44 6 40}
\uput[l](0,150){\footnotesize $150\degres$C} 
\uput[d](1,0){\footnotesize 1 min} \uput[d](2,0){\footnotesize 2 min} 
\end{pspicture*}
}
\end{minipage}

\medskip

\begin{enumerate}
\item Expliquer pourquoi un ajustement affine de $y$ en $t$ n'est pas pertinent.
\item On pose $z=\ln(y-28)$.

Recopier et compléter le tableau. Les valeurs de $z$ seront arrondies au centième.

\medskip

\begin{center}
\begin{tabularx}{0.9\linewidth}{|m{3.5cm}|*{7}{>{\centering \arraybackslash}X|}}
\hline
Temps $t$ (minutes)&0&1&2&3&4&5&6\\
\hline
Température $y$ (~\degres C)&150&113&82&65&52&44&40\\
\hline
$z=\ln(y-28)$ & $4,80$ & $4,44$&&&&&\\
\hline
\end{tabularx}
\end{center}

\medskip

\item On note $r$ le coefficient de corrélation de la série $(t\;;\;z)$.

On sait que $r\approx -0,999$.

Sur la base de cette information, répondre aux deux questions suivantes en justifiant.

\begin{enumerate}
\item La corrélation de la série $(t\;;\;z)$ est-elle bonne ?
\item Le nuage de points $(t\;;\;z)$ a-t-il une allure croissante ?
\end{enumerate}

\item À l'aide de la calculatrice, donner l'équation de la droite de régression linéaire de $z$ en $t$, selon la méthode des moindres carrés, sous la forme $z=at+b$.

Les coefficients $a$ et $b$ seront arrondis à $10^{-1}$.

\item En déduire une expression de $y$ en fonction de $t$ sous la forme

\[y = C \text{e}^{-0,4t}+28,\]

où $C$ est une constante que l'on arrondira à l'unité.
\end{enumerate}

\bigskip

\textbf{Partie B - Équation différentielle.}

\medskip
On considère l'équation différentielle :

$$(E) \: : \: 5y'+2y=56,$$

où $y$ est une fonction inconnue de la variable $t$, définie et dérivable sur l'intervalle $[0~;~+\infty[$,

et où $y'$ est sa fonction dérivée.

\medskip

\begin{enumerate}
\item Déterminer les solutions de l'équation différentielle

$$(E_0) \: : \: 5y'+2y=0.$$

On fournit la formule suivante :

\begin{center}
\begin{tabular}{|c|c|}
\hline 
Équation différentielle & Solutions sur un intervalle $I$ \\ 
\hline 
$ay'+by=0$ & $\rule[-8pt]{0pt}{24pt}f(t)=k \text{e}^{- \frac{b}{a} t}$ \\ 
\hline
\end{tabular}
\end{center}

\item Soit $A$ un nombre réel. On considère la fonction constante $g$, définie par $g(t)=A$.

Déterminer $A$ pour que la fonction $g$ soit solution de l'équation différentielle $(E)$.

\item En déduire les solutions de l'équation différentielle $(E)$.

\item Déterminer la fonction $f$, solution de l'équation différentielle $(E)$, qui vérifie la condition initiale $f(0)=150$.
\end{enumerate}

\bigskip

\textbf{Partie C - Étude de fonction}

\medskip
On considère la fonction $f$, définie sur l'intervalle $[0~;~+\infty[$ par :

$$f(t)=122 \text{e}^{-0,4t}+28.$$

On admet que la fonction $f$ modélise l'évolution de la température du matériau au fil du temps : ainsi, $f(t)$ représente la température, en degrés Celsius, $t$ minutes après la sortie du four.

\medskip

\begin{enumerate}
\item Donner la limite de $f$ en $+\infty$. Ce résultat est-il cohérent avec le contexte de l'exercice ?

\item On cherche à partir de quel instant la température du matériau devient inférieure à 50~\degres C.

\begin{enumerate}
\item Montrer que cela revient à résoudre l'inéquation: $\text{e}^{-0,4t} \leqslant \dfrac{11}{61}$.
\item Résoudre cette inéquation, puis déterminer à partir de quel instant, exprimé en minutes et secondes, la température devient inférieure à 50~\degres C.
\end{enumerate}

\item On considère la fonction $F$ définie sur l'intervalle $\left[ 0~;~+\infty\right[$ par 
\[F(t)=-305 \text{e}^{-0,4t}+28t.\]

Vérifier que la fonction $F$ est une primitive de la fonction $f$ sur l'intervalle $\left[ 0~;~+\infty\right[$.

\item Déterminer la température moyenne du matériau durant les $6$ premières minutes qui suivent la sortie du four. Arrondir au dixième.

\smallskip

On fournit pour cela la formule suivante :

\begin{center}
\fbox{\begin{minipage}{12cm}
\begin{center}
Valeur moyenne d'une fonction $f$ sur l'intervalle $[a~;~b]$.

$\displaystyle M=\dfrac{1}{b-a} \int_a^b f(t) \: \mathrm{d}t$
\end{center}
\end{minipage}}
\end{center}

\end{enumerate}

\bigskip

\textbf{\Large{}Exercice 2 \hfill 10 points}

\begin{center}
\textit{Les quatre parties sont indépendantes.}
\end{center}

\textbf{Partie A - Probabilités conditionnelles}

\medskip

Le tableau ci-dessous décrit le stock de paires de lunettes d'un opticien.

\medskip

\begin{center}
\setlength{\tabcolsep}{2pt}
\setlength{\fboxrule}{4pt}
\begin{tabularx}{\linewidth}{|>{\centering \arraybackslash}m{2cm}|*{4}{>{\centering \arraybackslash}X|}}
\hline
 & Verres\newline Polarisés\newline \textbf{L} & Verres\newline Photochromiques\newline H & Autre type\newline  de verre\newline \textbf{A} & ~\newline Total \\ 
\hline
Modèle \newline \textsc{classique}\newline \textbf{C} & $17,5\;\%$ &$10,5\;\%$ &$42\;\%$ & \Ovalbox{$70\;\%$}\\ 
\hline
Modèle \newline \textsc{sport}\newline \textbf{S} & $16,5\;\%$ &$10,5\;\%$ & \Ovalbox{$3\;\%$} &$30\;\%$\\
\hline
~\newline Total\newline~ & $34\;\%$ & \Ovalbox{$21\;\%$} &$45\;\%$ &$100\;\%$\\ 
\hline
\end{tabularx}
\end{center}

\medskip

Le tableau ci-dessus permet ainsi de voir notamment que :

\begin{itemize}
\item[] 70 \,\% des paires de lunettes sont des modèles \textsc{classique}.
\item[] 3 \,\% des paires de lunettes sont des modèles \textsc{sport} équipées d'un autre type de verre.
\item[] 21\,\% des paires de lunettes sont équipées de verres photochromiques.
\end{itemize}

\medskip

On prélève au hasard une paire de lunettes. On considère les évènements suivants :

\begin{itemize}
\item [\:\:] $C$ : \og la paire de lunettes est un modèle \textsc{classique} \fg{},
\item [\:\:] $S$ : \og la paire de lunettes est un modèle \textsc{sport}  \fg{},
\item [\:\:] $L$ : \og la paire de lunettes est équipée de verres polarisés  \fg{},
\item [\:\:] $H$ : \og la paire de lunettes est équipée de verres photochromiques  \fg{},
\item [\:\:] $A$ : \og la paire de lunettes est équipée d'un autre type de verre\fg{}.
\end{itemize}

\begin{center}
\textit{Les résultats seront arrondis, le cas échéant, au millième.}
\end{center}

\begin{enumerate}
\item Donner la valeur de la probabilité $P\left(L \cap S\right)$.

\item Déterminer la probabilité $P\left(L \cup S\right)$.

\item Déterminer valeur de la probabilité de $L$ sachant $S$, notée  $P_S\left(L\right)$.

\item Les évènements $L$ et $S$ sont-ils indépendants ? Justifier.

\item Recopier et compléter l'arbre suivant qui représente la situation décrite par le tableau.

\begin{center}
%\bigskip
{%\small
\psset{treesep=1cm,levelsep=3cm,labelsep=2pt,nodesepB=4pt}
\pstree[treemode=R,nodesepA=0pt]{\TR{}}
{\pstree[nodesepA=4pt]{\TR{C}\naput{$0,7$}}
      {
          \TR{L}\naput{\ldots}
          \TR{H}\taput[tpos=0.7]{$0,15$}
	      \TR{A} \nbput{\ldots}
	   }
      \pstree[nodesepA=4pt]{\TR{S}\nbput{\ldots}}
              {
	           \TR{L}\naput{$0,55$}
	            \TR{H}\taput[tpos=0.7]{$\cdots$}
	            \TR{A} \nbput{\ldots}
	           }
      }
}% fin du \small
%\bigskip
\end{center}

\end{enumerate}

\bigskip

\textbf{Partie B - Loi binomiale}

\medskip

Parmi les clients de l'opticien, la proportion de retraités est égale à $62$\,\%.

Un jour donné, l'opticien accueille $90$ clients. On note $X$ la variable aléatoire qui donne le nombre de retraités parmi les $90$~clients accueillis ce jour.

\medskip

\begin{enumerate}
\item On admet que la variable aléatoire $X$ suit une loi binomiale.

Donner ses paramètres ainsi que son espérance.

\item Calculer la probabilité $P(X=55)$. Arrondir au millième.

\item Quelle est la probabilité qu'au moins $50$\,\% des clients accueillis ce jour soient des retraités ? Arrondir au millième.
\end{enumerate}

\bigskip

\textbf{Partie C - Loi normale}

\medskip

Le chiffre d'affaires d'un opticien en 2023 a été égal à \np{80000} euros. Il espère que son chiffre d'affaires en 2024 sera supérieur.

Son chiffre d'affaires, en euros, estimé pour 2024, est donné par une variable aléatoire $Z$ qui suit une loi normale dont la courbe de densité est représentée ci-dessous.

\medskip

\begin{center}
\psset{xunit=1cm, yunit=20cm}
\def\xmin{-1}   \def\xmax{9} \def\ymin{-0.04} \def\ymax{0.31}
\begin{pspicture*}(\xmin,\ymin)(\xmax,\ymax)
%\psaxes[ticksize=-2pt 2pt, Dx=1, Dy=0.1, labels=none](0,0)(\xmin,0)(\xmax,0)
\def\m{4}% moyenne  
\def\s{1.5}% écart type
\def\inf{1} \def\sup{7}
\pscustom[fillstyle=solid,linestyle=solid,fillcolor= blue!30]
{\psGauss[mue=\m,sigma=\s]{\inf}{\sup}
\psplot{\sup}{\inf}{0}
\closepath}
\psgrid[yunit=1cm,subgriddiv=5, gridlabels=0, gridcolor=gray](0,0)(\xmin,0)(\xmax,6)
\psGauss[mue=\m,sigma=\s]{\xmin}{\xmax}
\psline[linewidth=1.2pt](\xmin,0)(\xmax,0)
\psdots(1,0)(7,0)
\footnotesize
\multido{\n=60000+20000,\i=0+2}{5}{\uput[d](\i,0){\np{\n}}}
\multido{\n=70000+20000,\i=1+2}{4}{\uput{12pt}[d](\i,0){\np{\n}}}
\end{pspicture*}
\end{center}

\begin{enumerate}
\item On note $\mu$ l'espérance de la variable aléatoire $Z$.

Déterminer graphiquement la valeur de $\mu$.

\item On note $\sigma$ l'écart-type de la variable aléatoire $Z$.

On sait que la zone grisée correspond à une probabilité égale à 0,95.

Expliquer pourquoi on a: $\sigma \approx$ \np{15000}.

\item Quelle est la probabilité que le chiffre d'affaires en 2024 soit supérieur à celui de 2023 ? Arrondir au millième.

\item Si entre 2023 et 2024, son chiffre d'affaires augmente de 30\,\%, l'opticien embauchera un nouvel employé.

Quelle est la probabilité que l'opticien embauche un nouvel employé ?

Arrondir au millième.
\end{enumerate}

\bigskip

\textbf{Partie D - Test d'hypothèse}

\medskip

Afin de développer le commerce, une commune rurale décide de construire des parkings pour les commerçants dont la proportion de clients venant en voiture est comprise entre 50\,\% et 60\,\%. Lorsque la proportion est inférieure, le parking n'est pas nécessaire. Lorsque la proportion est supérieure, le commerçant devra obligatoirement s'installer en périphérie de la commune.

\medskip

Un opticien affirme à la mairie de cette commune que 55\,\% de ses clients viennent en voiture.

\medskip

Afin de contrôler cette affirmation, la mairie met en place un test bilatéral au seuil de 5\,\% sur un échantillon aléatoire de 130 clients.

\medskip

On note $F$ la variable aléatoire qui, à chaque échantillon aléatoire de 130 clients, associe la proportion de ceux qui viennent en voiture. On suppose que $F$ suit une loi normale d'espérance $p$ inconnue et d'écart-type $\displaystyle \sqrt{\dfrac{p(1-p)}{130}}$.

\medskip

L'hypothèse nulle est $H_0$ \:: \og $p=0,55$ \fg{}.

L'hypothèse alternative est $H_1$\:: \og $p \neq 0,55$ \fg{}.

\medskip

\begin{enumerate}
\item Justifier que, sous l'hypothèse nulle, la variable aléatoire $F$ suit une loi normale d'espérance $0,55$ et d'écart-type $0,044$.

\item Déterminer, sous l'hypothèse nulle, le réel positif $h$ tel que 
\[P(0,55 - h < F < 0,55 + h)=0,95.\]

\item Sur un échantillon de 130 clients, la mairie a noté que 88 étaient venus en voiture.

Que peut-on conclure ?
\end{enumerate}
\end{document}