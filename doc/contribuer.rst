Contribuer
==========

Il y a plusieurs manière de contribuer : rapporter des bugs, poursuivre l'indexation des fichiers manquants, ou corriger des problèmes.

Rapporter des bugs
------------------

Si vous voyez une erreur, merci de me prévenir :

- (méthode préférée) soit en créant un ticket (issues) sur le logiciel de suivi de problèmes (bug tracker) dédié : https://forge.apps.education.fr/paternaultlouis/annales-math/issues ;
- soit en m'envoyant un courrier électronique à l'adresse `spalax(at)gresille(point)org <mailto://spalax(at)gresille(point)org>`__.

Remarquez que les erreurs sur les fichiers eux-mêmes (fautes d'orthographes, erreurs dans les corrigés, etc.) ne doivent pas m'être rapportées à moi, mais à `Denis Vergès <http://www.apmep.fr/_Denis-Verges_>`__, puisque ce projet n'est qu'une *copie* du site de l'APMEP. Certaines de ces erreurs sont tout de même présentent sur le logiciel de suivi, avec le tag `APMEP <https://forge.apps.education.fr/paternaultlouis/annales-math/issues?label_name%5B%5D=apmep>`__, afin d'en garder tout de même une trace.

Les erreurs qui peuvent apparaître dans ce projet sont :

- documents manquants (présents sur le site de l'APMEP, mais non indexés) ;
- documents mal triés ;
- bugs dans :ref:`aspirateur` ;
- etc.

Améliorer l'indexation
----------------------

  Un tutoriel pour expliquer comment indexer les fichiers est disponible :ref:`dans une partie dédiée <indexation>`. Si cette indexaction ne fonctionne pas (elle « oublie » des pages ou fichiers sur le site de l'APMEP), il faut mettre les mains dans le code pour le corriger. Bon courage !

.. _corriger:

Corriger
--------

L'outil :ref:`aspirateur` possède une :ref:`commande <aspirateurlint>` permettant de rechercher automatiquement des erreurs courantes.
