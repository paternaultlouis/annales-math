.. _aspirateur:

Aspirateur
==========

L'outil ``aspirateur`` est le logiciel permettant de rechercher et télécharger les documents depuis le site de l'APMEP, de les trier, et de faire d'autres opérations sur ces fichiers.

.. contents::
   :local:

Fichiers ``data``
-----------------

Ces fichiers ``data/*.json``, situés à la racine du projet, contiennent les méta-informations concernant les pages, les fichiers, les catégories et les tags. Il est utile pour faire le lien entre les fichiers téléchargés et le site de l'APMEP d'où ils proviennent.

.. _aspirateurcommandes:

Binaire ``./aspirateur``
------------------------

*Dans toute cette partie, on suppose que le répertoire courant est la racine du projet.*

Ce logiciel est placé dans le dossier `lib/aspirateur <https://forge.apps.education.fr/paternaultlouis/annales-math/-/tree/main/lib/aspirateur?ref_type=heads>`__, et un raccourci est placé à la racine du projet. Il est écrit en Python3.

Pour installer les dépendances, il est préférable de créer auparavant un `virtualenv <https://virtualenv.pypa.io/en/stable/>`__, puis de lancer la commande ``pip install -r lib/requirements``.

Il est ensuite possible de lancer le logiciel depuis la racine du projet. Par exemple :

.. code-block:: bash

   ./aspirateur --help

Après la plupart des commandes, il est nécessaire d'utiliser ``git`` pour valider et publier les modifications (téléchargement ou modification de fichiers).
