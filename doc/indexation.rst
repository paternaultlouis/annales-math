.. _indexation:

Tutoriel pour indexer de nouveaux fichiers
==========================================

.. highlight:: shell

Sur cette page est expliqué comment faire pour indexer de nouveaux fichiers, c'est-à-dire :

- les télécharger depuis le site de l'APMEP ;
- s'assurer que le :ref:`logiciel <aspirateur>` les classe correctement.

.. note::

   Toutes les commandes sont à exécuter dans un
   `terminal <https://fr.wikipedia.org/wiki/%C3%89mulateur_de_terminal>`__.

.. contents:: Sommaire
   :local:

1. Mise en place de l'environnement de travail
----------------------------------------------

* Installez les logiciels requis, de préférence avec le logiciel de gestion de paquet de votre système d'exploitation :

  * `git <http://git-scm.com>`__ ;
  * `python3 <https://www.python.org/downloads/>`__.

* Pour télécharger l'ensemble des documents déjà indexés, ainsi que le :ref:`logiciel <aspirateur>` permettant cette indexation, il faut utiliser `git <http://git-scm.com>`__.

  * Si vous avez un compte sur `la forge de l'Éducation nationale <http://https://forge.apps.education.fr>`__, `« forkez » <https://forge.apps.education.fr/paternaultlouis/annales-math/forks/new>`__ le projet, puis clonez le dépôt ::

       git clone git@forge.apps.education.fr:VOTRE-NOM-D-UTILISATEUR-ICE/annales-math.git

    Avoir un tel compte n'est absolument pas nécessaire, mais facilitera :ref:`la dernière partie <publication>`.

  * Si vous n'avez pas un tel compte, clonez simplement le dépôt ::

       git clone https://forge.apps.education.fr/paternaultlouis/annales-math.git

  Dans un cas comme dans l'autre, un dossier ``annales-math`` a été créé ; déplacez vous dedans. Toute la suite de cette page suppose que vous êtes dans ce dossier.

* Il faut ensuite installer les dépendances nécessaires à l'exécution de :ref:`aspirateur`, de préférence dans un `virtualenv <https://virtualenv.pypa.io/en/stable/>`__. ::

      python3 -m pip install -r lib/requirements.txt

2. Indexation des fichiers
--------------------------

Un système est mis en place pour ne pas re-télécharger trop souvent les pages et fichiers[cache]_ (les fichiers sont téléchargés au plus tôt une semaine après le dernier essai, au plus tard un an après le dernier essai, et entre les deux, plus le dernier changement est vieux, plus on attend avant de télécharger à nouveau). Donc, à part la première fois, les commandes suivantes ne vont pas *tout* télécharger à chaque fois.

.. [cache] Je ne sais pas trop à quel point ça dérange l'APMEP de tout re-télécharger, mais de mon côté, ça me prend plusieurs heures, donc je préfère ne traiter que quelques fichiers à chaque fois.

Nettoyage des fichiers obsolètes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Certains fichiers téléchargés depuis le site web de l'APMEP ont disparus du site web (le plus souvent, ils ont été renommés). La commande suivante vérifie *tous* les fichiers, et supprime ceux qui ont disparus du site web de l'APMEP.

.. code-block:: shell

   $ ./aspirateur download clean

Indexation de nouvelles pages et fichiers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Demandons à :ref:`aspirateur` de rechercher de nouvelles pages et fichiers.

.. code-block:: shell

    $ ./aspirateur scan

Le logiciel cherche de nouvelles pages et fichiers, et devine leur classification. Il ouvre ensuite un logiciel d'édition de texte (le nom de ce logiciel est cherché dans la variable d'environnement ``EDITOR``) pour vous permettre de vérifier et corriger les éventuelles erreurs. Une fois les erreurs corrigées, enregistrer et quitter le logiciel. Les corrections sont alors prises en compte et les nouveaux fichiers ajoutés au dépôt.

Il est aussi possible de restreindre la recherche. Par exemple, pour ne rechercher que dans les pages de l'année 2020, on peut lancer :

.. code-block:: shell

    $ ./aspirateur scan +2020

Voir l'aide ``./aspirateur scan --help`` pour plus d'informations.

Téléchargement des nouveaux fichiers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Les nouveaux fichiers indexés dans la commande précédente n'ont pas été téléchargés. Il faut maintenant lancer la commande suivante,  qui va télécharger les fichiers manquants.

.. code-block:: shell

   ./aspirateur download all --fix

.. _publication:

3. Publication des modifications
--------------------------------

Reste maintenant à publier les modifications du dépôt. Analysez les changement dans le dépôt avec ``git status``, puis ajoutez les nouveaux fichiers (``git add``), validez les modifications (``git commit``), puis :

* si vous avez un compte sur http://https://forge.apps.education.fr, créez une *pull request* ;
* sinon, `envoyez-moi <https://ababsurdo.fr/apropos/>`__ les *patches* par courriel.

Attendez enfin que je prenne en compte les modifications, et les nouveaux fichiers indexés seront `disponibles pour tous <https://paternaultlouis.forge.apps.education.fr/annales-math/>`__.
