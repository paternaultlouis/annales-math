.. _telecharger:

Télécharger
===========

Une archive de toutes les annales et leurs sources
--------------------------------------------------

Par ici : `annales-math.zip <https://nuage03.apps.education.fr/index.php/s/mmjaM5azMBHmRXD>`__.

Le dépôt git
------------

En utilisant `git <http://git-scm.com>`__, il est possible de télécharger les annales, et surtout, plus tard, mettre à jour les fichiers (et obtenir les nouveaux documents) sans télécharger à nouveau l'ensemble de l'archive.

Cette méthode est un peu technique dans la mesure où elle nécessite la connaissance élémentaire de l'utilisation d'un `terminal <https://fr.wikipedia.org/wiki/%C3%89mulateur_de_terminal>`__.

#. Premier téléchargement

   Dans un terminal, lancez la commande suivante, puis déplacez vous dans le fichier qui vient d'être créé.

   .. code-block:: bash

      git clone https://forge.apps.education.fr/paternaultlouis/annales-math.git
      cd annales-math

   Les annales se trouvent maintenant dans le dossier ``download``, mais rangées de manière arbitraire.

#. Installation des paquets Python

   Dans un terminal, installer les paquets python nécessaires pour faire fonctionner :ref:`aspirateur` :

   .. code-block:: bash

      python3 -m pip install -r lib/requirements.txt

#. Classement des fichiers

   Toujours dans un terminal, lancer la commande suivante.

   .. code-block:: bash

      ./aspirateur html

    Vous avez maintenant, dans le répertoire ``public``, toutes les annales, classées et renommées de manière (un peu plus) normalisées.

#. Mise à jour

   Quelques mois passent, et de nouveaux sujets sont disponibles. Pour mettre votre dossier à jour, déplacez-vous dans le dossier ``annales-math`` créé précédemment, et lancez les commandes suivantes :

   .. code-block:: bash

      git pull
      ./aspirateur html

   Les nouveaux fichiers ont été téléchargés, et classés.
