`annales-math` ∞ Compilation de sujets d'examens et concours de mathématiques
=============================================================================

Le but du projet `annales-math <https://forge.apps.education.fr/paternaultlouis/annales-math>`__ est de télécharger et trier toutes les `annales des examens, concours, etc. <http://www.apmep.fr/-Annales-examens-concours->`__ répertoriées par `l'APMEP <http://www.apmep.fr>`__ afin de :

- permettre de :ref:`télécharger <telecharger>` *tous* ces documents sur son ordinateur (`annales-math.zip <https://nuage03.apps.education.fr/index.php/s/mmjaM5azMBHmRXD>`__) ;
- offrir une `présentation alternative <https://paternaultlouis.forge.apps.education.fr/annales-math/>`__.

Ce projet n'a absolument pas pour but de remplacer le travail fourni par l'APMEP : il vise à le compléter.

Raison d'être
-------------

La principale raison pour laquelle j'ai décidé de télécharger toutes les annales de l'APMEP est la connexion internet aléatoire de mon lycée.

- Lorsque la connexion est saturée, afficher une simple page du `site web de l'APMEP <http://www.apmep.fr>`__ peut prendre une minute (sans exagérer). Du coup, rechercher *un seul* exercice dans les annales peut prendre cinq minutes (affichage de la page *Annales*, puis de *Terminale ES*, puis *2017*, puis téléchargement de quelques annales, puis retour à *Terminale ES*, puis affichage de *2016* …).
- Il m'arrive de travailler dans un train, sans connexion à internet. Avoir les annales à porter de main peut être utile.

Ce travail permet en outre les choses suivantes :

- Présenter *toutes* les annales `sur une seule page <https://paternaultlouis.forge.apps.education.fr/annales-math/>`__. Lorsque l'on sait ce que l'on cherche, cela représente un gain de temps.
- Renommer les annales de manière logique : par exemple, le corrigé du sujet « Métropole-la Réunion » de juin 2017 en terminale ES s'appelle `bacES-2017-juin-metropolelareunion-corrige.tex` : son nom contient des éléments qui vont du plus général (série `bacES`, puis année `2017`, etc.) jusqu'au plus particulier (`corrigé`). Ce qui signifie que lorsque plusieurs annales sont téléchargées dans le même dossier, un tri par ordre alphabétique constitue par la même occasion un tri logique : les fichiers sont alors triés par série, puis année, etc. A contrario, il ne semble pas y avoir de règle quant au nom des fichiers des documents présentés sur le site de l'APMEP, ce qui fait que des fichiers téléchargés apparaissent dans un ordre alphabétique, qui semble arbitraire d'un point de vue logique.

Table des matières
------------------

.. toctree::
   :maxdepth: 1

   telecharger
   contribuer
   indexation
   aspirateur


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
